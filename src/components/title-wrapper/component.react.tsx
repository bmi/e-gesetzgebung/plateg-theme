// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useLayoutEffect, useState } from 'react';
import { useRouteMatch } from 'react-router-dom';

export interface TitleWrapperComponentProps {
  children: React.ReactElement;
}
export function TitleWrapperComponent(props: TitleWrapperComponentProps): React.ReactElement {
  const routeMatcherVorbereitung = useRouteMatch<{ pageName: string }>('/*');
  const [wrapperWidth, setWrapperWidth] = useState<number | '100%'>('100%');
  const [holderWidth, setHolderWidth] = useState<number | '100%'>('100%');

  useLayoutEffect(() => {
    const updateWidth = () => {
      setWrapperWidth(document.body.scrollWidth - 1);
      setHolderWidth(document.body.clientWidth - 1);
    };
    window.addEventListener('resize', updateWidth);
    window.addEventListener('scroll', updateWidth);
    return () => {
      window.removeEventListener('resize', updateWidth);
      window.removeEventListener('scroll', updateWidth);
    };
  }, []);

  useLayoutEffect(() => {
    const timeout = setTimeout(() => {
      setWrapperWidth('100%');
      setHolderWidth('100%');
    }, 100);
    return () => {
      if (timeout) {
        clearTimeout(timeout);
      }
    };
  }, [routeMatcherVorbereitung?.url]);

  return (
    <div style={{ width: wrapperWidth }}>
      <div style={{ position: 'sticky', left: 0, width: holderWidth, background: 'white' }}>{props.children}</div>
    </div>
  );
}
