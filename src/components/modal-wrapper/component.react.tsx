// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Modal, ModalProps } from 'antd';
import React, { useEffect } from 'react';

import { AriaController } from '../../controllers';
import { CloseOutlined } from '../icons/CloseOutlined';

export function ModalWrapper(props: ModalProps): React.ReactElement {
  useEffect(() => {
    if (props.open) {
      AriaController.setAriaLabelsByClassName('ant-modal-close', 'Dialog schließen');
      AriaController.setElementIdByClassName('ant-modal-close', 'ModalClose-btn');
      AriaController.setAttrByClassName('ant-modal-header', 'role', 'dialog');
    }
  }, [props.open]);

  return <Modal {...props} closeIcon={<CloseOutlined />}></Modal>;
}
