// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './delete-confirm-popover.less';

import { Button, Popover } from 'antd';
import { TooltipPlacement } from 'antd/es/tooltip';
import React, { RefObject, useEffect, useRef } from 'react';

import { ExclamationCircleFilled } from '../icons/ExclamationCircleFilled';

interface DeleteConfirmPopoverProps {
  opened: boolean;
  close: () => void;
  onDelete: () => void;
  content: DeleteConfirmContent;
  placement?: TooltipPlacement;
  children: React.ReactElement;
  mainDeleteBtnId?: string;
  getPopupContainer?: () => HTMLElement;
}
export interface DeleteConfirmContent {
  title: string;
  yesBtnText: string;
  noBtnText: string;
  infoText?: string;
}
export function DeleteConfirmPopover(props: Readonly<DeleteConfirmPopoverProps>): React.ReactElement {
  const cancelBtnRef = useRef<HTMLButtonElement>(null);
  const deleteBtnRef = useRef<HTMLButtonElement>(null);
  useEffect(() => {
    if (props.opened) {
      setTimeout(() => {
        deleteBtnRef.current?.focus();
      }, 100);
    }
  }, [props.opened]);
  const onKeyDownHandler = (e: React.KeyboardEvent<HTMLElement>, btnRef: RefObject<HTMLButtonElement>) => {
    if (e.code === 'Tab' && !e.shiftKey) {
      e.preventDefault();
      btnRef.current?.focus();
    }
  };
  const popoverContent = (
    <div className="comment-popover-div">
      <p>{props.content.infoText}</p>
      <div className="ant-popover-buttons">
        <Button
          onClick={() => {
            props.onDelete();
            props.close();
          }}
          ref={deleteBtnRef}
          onKeyDown={(e) => onKeyDownHandler(e, cancelBtnRef)}
          type="primary"
          disabled={false}
        >
          {props.content.yesBtnText}
        </Button>
        <Button
          onClick={() => {
            props.close();
            props.mainDeleteBtnId && document.getElementById(props.mainDeleteBtnId)?.focus();
          }}
          ref={cancelBtnRef}
          onKeyDown={(e) => onKeyDownHandler(e, deleteBtnRef)}
          type="default"
          disabled={false}
        >
          {props.content.noBtnText}
        </Button>
      </div>
    </div>
  );
  const popoverTitle = (
    <>
      <span role="img" aria-label="Achtung-Icon" className="anticon anticon-exclamation-circle">
        <ExclamationCircleFilled />
      </span>
      <span className="comment-popover-title">{props.content.title}</span>
    </>
  );
  return (
    <Popover
      open={props.opened}
      content={popoverContent}
      placement={props.placement}
      title={popoverTitle}
      overlayClassName="delete-confirm-popover"
      getPopupContainer={props.getPopupContainer}
    >
      {props.children}
    </Popover>
  );
}
