// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Observable, Subject } from 'rxjs';
import { debounceTime, filter, map, switchMap } from 'rxjs/operators';

import {
  Configuration,
  EmaillisteResponseDTO,
  UserControllerApi,
  UserEntityDTO,
  UserEntityShortDTO,
  UserEntityShortDTOStatusEnum,
  UserSearchShortResult,
} from '@plateg/rest-api';

import { GlobalDI } from '../../shares/injector';

export class EmailSelectController {
  private readonly userController = GlobalDI.getOrRegister(
    'userController',
    () => new UserControllerApi(new Configuration()),
  );
  public searchObserver: Subject<string> = new Subject<string>();

  private getFindUsersCall(
    suchStrings: Array<string>,
    ressortId?: string,
    referatId?: string,
  ): Observable<UserSearchShortResult> {
    return this.userController.getUserShortListBySearchStrings({ suchStrings, ressortId, referatId });
  }

  private getFindUsersBRCall(suchStrings: Array<string>): Observable<UserSearchShortResult> {
    return this.userController.getUserShortListBySearchStringsForBundesrat({ suchStrings });
  }

  private getFindUsersBTCall(suchStrings: Array<string>): Observable<UserSearchShortResult> {
    return this.userController.getUserShortListBySearchStringsForBundestag({ suchStrings });
  }

  public getUsersByEmailCall(emails: string[]): Observable<UserEntityShortDTO[]> {
    return this.userController.getUserShortListByEmails({ emails }).pipe(map((data) => data.dtos));
  }

  public searchBRObservable(): Observable<(UserEntityShortDTO | EmaillisteResponseDTO)[]> {
    return this.createSearchObservable((term: string) => this.getFindUsersBRCall(this.splitSuchString(term)));
  }

  public searchBTObservable(): Observable<(UserEntityShortDTO | EmaillisteResponseDTO)[]> {
    return this.createSearchObservable((term: string) => this.getFindUsersBTCall(this.splitSuchString(term)));
  }

  public searchObservable(
    ressortId?: string,
    referatId?: string,
  ): Observable<(UserEntityShortDTO | EmaillisteResponseDTO)[]> {
    return this.createSearchObservable((term: string) =>
      this.getFindUsersCall(this.splitSuchString(term), ressortId, referatId),
    );
  }

  private createSearchObservable(
    switchMapFunction: (term: string) => Observable<UserSearchShortResult>,
  ): Observable<(UserEntityShortDTO | EmaillisteResponseDTO)[]> {
    return this.searchObserver.pipe(
      map((s) => s.trim()),
      filter((s) => this.filterSuchstring(s)),
      debounceTime(800),
      switchMap(switchMapFunction),
      map((data: UserSearchShortResult) => {
        return [...data.emaillistDtos.dtos, ...data.dtos];
      }),
    );
  }

  private filterSuchstring(suchstring: string): boolean {
    const minChars = 3;
    return suchstring.length >= minChars && this.splitSuchString(suchstring).some((v) => v.length >= minChars);
  }

  private splitSuchString(suchstring: string): string[] {
    return suchstring.split('[\\s,]');
  }

  public getRessortId(item: UserEntityShortDTO): string {
    return String(item.ressortId);
  }

  public getRessortString(item: UserEntityDTO | UserEntityShortDTO): string {
    const ressort = 'ressortKurzbezeichnung' in item ? item.ressortKurzbezeichnung : '';
    const abteilungString = item.abteilung ? `, ${item.abteilung}` : '';
    const fachreferatString = item.abteilung && item.fachreferat ? `, ${item.fachreferat}` : '';
    return ressort ? ` (${ressort}${abteilungString}${fachreferatString})` : '';
  }

  public isUserNotActive(user: UserEntityShortDTO | UserEntityDTO) {
    if (user.status === undefined || !user.gid) {
      return false;
    }

    return user.status && user.status !== UserEntityShortDTOStatusEnum.Ok;
  }

  public isEmailForbidden(user: UserEntityShortDTO | UserEntityDTO, allowUnknownEmail = false) {
    return (!user.gid || user.status === UserEntityShortDTOStatusEnum.Unbekannt) && !allowUnknownEmail;
  }
}
