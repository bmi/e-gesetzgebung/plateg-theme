// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import { of } from 'rxjs';
import sinon from 'sinon';

import {
  Configuration,
  RawAjaxResponse,
  UserControllerApi,
  UserEntityListResponseShortDTO,
  UserEntityShortDTO,
  UserEntityShortDTOStatusEnum,
  UserSearchShortResult,
} from '@plateg/rest-api';

import { GlobalDI } from '../../shares';
import { EmailSelectController } from './controller';

describe(`Test: EmailSelectController`, () => {
  const ctrl = new EmailSelectController();
  const userController = GlobalDI.getOrRegister<UserControllerApi>(
    'userController',
    () => new UserControllerApi(new Configuration()),
  );
  describe(`Test: getUsersByEmailCall`, () => {
    let getUsersByEmailStub: sinon.SinonStub;
    before(() => {
      getUsersByEmailStub = sinon.stub(userController, 'getUserShortListByEmails');
    });
    after(() => {
      getUsersByEmailStub.restore();
    });

    it('email doesnt exist, empty array is returned', (done) => {
      getUsersByEmailStub.returns(
        of({
          dtos: [],
        } as unknown as RawAjaxResponse<UserEntityListResponseShortDTO>),
      );
      const result = ctrl.getUsersByEmailCall(['lalalaal@web.de']);
      result.subscribe((data) => {
        expect(data).to.be.empty;
        done();
      });
    });

    it('one passed in emails exist, one user is returned', (done) => {
      getUsersByEmailStub.returns(
        of({
          dtos: [{ email: 'abc@xyz.de', name: 'abc' }],
        } as unknown as RawAjaxResponse<UserEntityListResponseShortDTO>),
      );
      const result = ctrl.getUsersByEmailCall(['abc@xyz.de']);
      result.subscribe((data) => {
        expect(data.length).to.eql(1);
        expect(data[0].name).to.eql('abc');
        done();
      });
    });

    it('both passed in emails exist, two users are returned', (done) => {
      getUsersByEmailStub.returns(
        of({
          dtos: [
            {
              email: 'gerd@xyz.de',
              name: 'Gerd Müller',
            },
            { email: 'abc@xyz.de', name: 'abc' },
          ],
        } as unknown as RawAjaxResponse<UserEntityListResponseShortDTO>),
      );
      const result = ctrl.getUsersByEmailCall(['gerd@xyz.de', 'abc@xyz.de']);
      result.subscribe((data) => {
        expect(data.length).to.eql(2);
        expect(data[0].name).to.eql('Gerd Müller');
        expect(data[1].name).to.eql('abc');
        done();
      });
    });
  });

  describe(`Test: searchObservable`, () => {
    let getFindUsersStub: sinon.SinonStub;
    before(() => {
      getFindUsersStub = sinon.stub(userController, 'getUserShortListBySearchStrings');
    });
    after(() => {
      getFindUsersStub.restore();
    });

    it('search term exist, one match returned', (done) => {
      getFindUsersStub.returns(
        of({
          complete: true,
          dtos: [{ email: 'abc@xyz.de', name: 'abc' }],
          emaillistDtos: {
            dtos: [],
          },
        } as unknown as RawAjaxResponse<UserSearchShortResult>),
      );
      const sub = ctrl.searchObservable().subscribe({
        next: (data) => {
          expect(data.length).to.eql(1);
          expect((data[0] as UserEntityShortDTO).name).to.eql('abc');
          sub.unsubscribe();
          done();
        },
        error: (error) => {
          done(error);
        },
      });
      ctrl.searchObserver.next('abc');
    });

    it('search term doesnt exist, empty array returned', (done) => {
      getFindUsersStub.returns(
        of({
          complete: true,
          dtos: [],
          emaillistDtos: {
            dtos: [],
          },
        } as unknown as RawAjaxResponse<UserSearchShortResult>),
      );
      const sub = ctrl.searchObservable().subscribe({
        next: (data) => {
          expect(data.length).to.eql(0);
          sub.unsubscribe();
          done();
        },
        error: (error) => {
          done(error);
        },
      });
      ctrl.searchObserver.next('abc');
    });
  });

  describe(`Test: searchBTObservable`, () => {
    let getFindBTUsersStub: sinon.SinonStub;
    before(() => {
      getFindBTUsersStub = sinon.stub(userController, 'getUserShortListBySearchStringsForBundestag');
    });
    after(() => {
      getFindBTUsersStub.restore();
    });

    it('search term exist, one match returned', (done) => {
      getFindBTUsersStub.returns(
        of({
          complete: true,
          dtos: [{ email: 'abc@xyz.de', name: 'abc' }],
          emaillistDtos: {
            dtos: [],
          },
        } as unknown as RawAjaxResponse<UserSearchShortResult>),
      );
      const sub = ctrl.searchBTObservable().subscribe({
        next: (data) => {
          expect(data.length).to.eql(1);
          expect((data[0] as UserEntityShortDTO).name).to.eql('abc');
          sub.unsubscribe();
          done();
        },
        error: (error) => {
          sub?.unsubscribe();
          done(error);
        },
      });
      ctrl.searchObserver.next('abc');
    });

    it('search term doesnt exist, empty array returned', (done) => {
      getFindBTUsersStub.returns(
        of({
          complete: true,
          dtos: [],
          emaillistDtos: {
            dtos: [],
          },
        } as unknown as RawAjaxResponse<UserSearchShortResult>),
      );
      const sub = ctrl.searchBTObservable().subscribe({
        next: (data) => {
          expect(data.length).to.eql(0);
          sub.unsubscribe();
          done();
        },
        error: (error) => {
          done(error);
        },
      });
      ctrl.searchObserver.next('abc');
    });
  });

  describe(`Test: searchBRObservable`, () => {
    let getFindBRUsersStub: sinon.SinonStub;
    before(() => {
      getFindBRUsersStub = sinon.stub(userController, 'getUserShortListBySearchStringsForBundesrat');
    });
    after(() => {
      getFindBRUsersStub.restore();
    });

    it('search term exist, one match returned', (done) => {
      getFindBRUsersStub.returns(
        of({
          complete: true,
          dtos: [{ email: 'abc@xyz.de', name: 'abc' }],
          emaillistDtos: {
            dtos: [],
          },
        } as unknown as RawAjaxResponse<UserSearchShortResult>),
      );
      const sub = ctrl.searchBRObservable().subscribe({
        next: (data) => {
          expect(data.length).to.eql(1);
          expect((data[0] as UserEntityShortDTO).name).to.eql('abc');
          sub.unsubscribe();
          done();
        },
        error: (error) => {
          sub?.unsubscribe();
          done(error);
        },
      });
      ctrl.searchObserver.next('abc');
    });

    it('search term doesnt exist, empty array returned', (done) => {
      getFindBRUsersStub.returns(
        of({
          complete: true,
          dtos: [],
          emaillistDtos: {
            dtos: [],
          },
        } as unknown as RawAjaxResponse<UserSearchShortResult>),
      );
      const sub = ctrl.searchBRObservable().subscribe({
        next: (data) => {
          expect(data.length).to.eql(0);
          sub.unsubscribe();
          done();
        },
        error: (error) => {
          done(error);
        },
      });
      ctrl.searchObserver.next('abc');
    });
  });

  describe(`Test: getRessortString`, () => {
    it('user has values for all field, correct string is returned', () => {
      const user: UserEntityShortDTO = {
        ressortKurzbezeichnung: 'r1',
        abteilung: 'abteilung1',
        fachreferat: 'fachreferat1',
        email: 'u1@xyz.de',
      };
      expect(ctrl.getRessortString(user)).to.eql(` (r1, abteilung1, fachreferat1)`);
    });
    it('user has no ressort, empty string is returned', () => {
      const user: UserEntityShortDTO = {
        abteilung: 'abteilung1',
        fachreferat: 'fachreferat1',
        email: 'u1@xyz.de',
      };
      expect(ctrl.getRessortString(user)).to.eql(``);
    });
    it('user has values for all field except abteilung, correct string is returned', () => {
      const user: UserEntityShortDTO = {
        ressortKurzbezeichnung: 'r1',
        fachreferat: 'fachreferat1',
        email: 'u1@xyz.de',
      };
      expect(ctrl.getRessortString(user)).to.eql(` (r1)`);
    });
    it('user has values for all field except fachreferat, correct string is returned', () => {
      const user: UserEntityShortDTO = {
        ressortKurzbezeichnung: 'r1',
        abteilung: 'abteilung1',
        email: 'u1@xyz.de',
      };
      expect(ctrl.getRessortString(user)).to.eql(` (r1, abteilung1)`);
    });
  });
  describe(`Test: isUserNotActive`, () => {
    it('user is not active', () => {
      const user: UserEntityShortDTO = {
        ressortKurzbezeichnung: 'r1',
        abteilung: 'abteilung1',
        fachreferat: 'fachreferat1',
        email: 'u1@xyz.de',
        status: UserEntityShortDTOStatusEnum.Geloescht,
        gid: '1',
      };
      expect(ctrl.isUserNotActive(user)).to.eql(true);
    });
    it('user is active', () => {
      const user: UserEntityShortDTO = {
        ressortKurzbezeichnung: 'r1',
        abteilung: 'abteilung1',
        fachreferat: 'fachreferat1',
        email: 'u1@xyz.de',
        status: UserEntityShortDTOStatusEnum.Ok,
        gid: '1',
      };
      expect(ctrl.isUserNotActive(user)).to.eql(false);
    });
    it('user id undefined', () => {
      const user: UserEntityShortDTO = {
        email: 'u1@xyz.de',
      };
      expect(ctrl.isUserNotActive(user)).to.eql(false);
    });
    it('user status undefined', () => {
      const user: UserEntityShortDTO = {
        email: 'u1@xyz.de',
        gid: '1',
      };
      expect(ctrl.isUserNotActive(user)).to.eql(false);
    });
  });

  describe(`Test: isEmailForbidden`, () => {
    it('user exist', () => {
      const user: UserEntityShortDTO = {
        ressortKurzbezeichnung: 'r1',
        abteilung: 'abteilung1',
        fachreferat: 'fachreferat1',
        email: 'u1@xyz.de',
        status: UserEntityShortDTOStatusEnum.Geloescht,
        gid: '1',
      };
      expect(ctrl.isEmailForbidden(user)).to.eql(false);
    });
    it('user exist', () => {
      const user: UserEntityShortDTO = {
        ressortKurzbezeichnung: 'r1',
        abteilung: 'abteilung1',
        fachreferat: 'fachreferat1',
        email: 'u1@xyz.de',
        status: UserEntityShortDTOStatusEnum.Unbekannt,
        gid: '1',
      };
      expect(ctrl.isEmailForbidden(user, true)).to.eql(false);
    });
    it('user doesnt exist and emails not allowed', () => {
      const user: UserEntityShortDTO = {
        email: 'u1@xyz.de',
      };
      expect(ctrl.isEmailForbidden(user)).to.eql(true);
    });
    it('user doesnt exist and emails allowed', () => {
      const user: UserEntityShortDTO = {
        email: 'u1@xyz.de',
      };
      expect(ctrl.isEmailForbidden(user, true)).to.eql(false);
    });
  });
});
