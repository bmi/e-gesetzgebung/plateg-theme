// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Select, SelectProps } from 'antd';
import { DefaultOptionType, SelectValue } from 'antd/lib/select';
import React, { ReactElement, useEffect, useState } from 'react';

import { SelectDown } from '../icons/SelectDown';

interface SelectOptionItemInterface {
  [s: string]: string;
}

export function SelectWrapper<T extends SelectValue>({
  options,
  ...props
}: Readonly<SelectProps<T>>): React.ReactElement {
  useEffect(() => {
    if (props.id) {
      document.getElementById(`${props.id}`)?.removeAttribute('aria-autocomplete');
    }
  }, [options]);

  useEffect(() => {
    if (props.id) {
      document
        .getElementById(`${props.id}`)
        ?.setAttribute('title', getActiveValueOpt(options as DefaultOptionType[], props.value as string));
    }
  }, [props.value]);

  // Prepare active value for screen reader
  const getActiveValueOpt = (optionsList: DefaultOptionType[], activeValue: string) => {
    if (!optionsList) {
      return '';
    }

    return optionsList.reduce((previousValue: string, item: DefaultOptionType) => {
      const itemProps = item as SelectOptionItemInterface;
      if (itemProps.value === activeValue) {
        return itemProps.title || itemProps.value;
      }
      return previousValue;
    }, '');
  };

  const dropdownRender = (menu: ReactElement): ReactElement => {
    // Remove role attr for fixing issue with wrong counter for screen reader
    if (props.id) {
      document.getElementById(`${props.id}_list`)?.setAttribute('aria-setsize', `${(options || []).length}`);
    }
    return menu;
  };
  const observeAccessibilityElement = (accessbilityElement: HTMLElement, elm: HTMLElement) => {
    const observer = new MutationObserver((mutationsList) => {
      if (mutationsList.find((mutation) => mutation.type === 'attributes')) {
        elm.setAttribute('value', accessbilityElement?.getAttribute('title') ?? '');
        elm.setAttribute('aria-expanded', accessbilityElement?.getAttribute('aria-expanded') ?? '');
        if (!accessbilityElement.hasAttribute('aria-activedescendant')) {
          accessbilityElement?.setAttribute('aria-activedescendant', elm.id);
        }
      }
    });
    observer.observe(accessbilityElement, { attributes: true });
    return observer;
  };
  const createAccessibilityElement = (id: string) => {
    const accessbilityElement = document.getElementById(`${id}`);
    if (!accessbilityElement) return;

    const labelElement = document.querySelector(`[for="${id}"]`);
    const elm = document.createElement('input');
    elm.id = `${id}_spnTitle`;
    elm.style.opacity = '0';
    elm.style.position = 'absolute';
    elm.style.left = '0';
    elm.style.width = '100%';
    elm.style.height = '100%';
    elm.setAttribute('tabindex', '-1');
    if (labelElement) {
      labelElement.setAttribute('for', elm.id);
    }

    accessbilityElement.parentElement?.append(elm);
    return { accessbilityElement, elm };
  };
  const handleFocus = (accessbilityElement: HTMLElement, elm: HTMLElement) => {
    const firstTime = !elm.getAttribute('value');
    accessbilityElement.setAttribute('aria-activedescendant', elm.id);

    if (firstTime) {
      elm.setAttribute('value', accessbilityElement.getAttribute('title') ?? '');
      elm.setAttribute('unselectable', 'on');
      const attributes = [
        'autocomplete',
        'type',
        'role',
        'aria-label',
        'aria-haspopup',
        'aria-owns',
        'aria-controls',
        'readonly',
      ];

      for (const iterator of attributes) {
        elm.setAttribute(iterator, accessbilityElement.getAttribute(iterator) ?? '');
        accessbilityElement.removeAttribute(iterator);
      }
    }
  };
  useEffect(() => {
    const userAgent = window.navigator.userAgent;
    if (!props.id || userAgent.indexOf('Firefox') === -1) return;
    const alternativeElmFocus = (inpElement: HTMLElement, event: KeyboardEvent) => {
      if (event.key !== 'Tab') return;
      const focusableElements = Array.from(
        document.querySelectorAll('a, button, input, select, textarea, [tabindex]:not([tabindex="-1"])'),
      );
      const currentIndex = focusableElements.indexOf(inpElement);
      const nextIndex = event.shiftKey
        ? (currentIndex - 1) % focusableElements.length
        : (currentIndex + 1) % focusableElements.length;
      const nextElement = focusableElements[nextIndex] as HTMLElement | undefined;
      if (!nextElement) {
        return;
      }
      if (nextElement.classList.contains('info-icon')) {
        setTimeout(() => {
          nextElement.focus();
        }, 20);
      } else {
        nextElement.focus();
      }
    };

    const result = createAccessibilityElement(props.id);
    if (!result) return;

    const focusEvent = () => handleFocus(result.accessbilityElement, result.elm);
    const alternativeKeyupEvent = (e: KeyboardEvent) => alternativeElmFocus(result.accessbilityElement, e);
    result.accessbilityElement.addEventListener('focus', focusEvent);
    result.accessbilityElement.addEventListener('keydown', alternativeKeyupEvent);
    const observer = observeAccessibilityElement(result.accessbilityElement, result.elm);

    return () => {
      result.accessbilityElement.removeEventListener('focus', focusEvent);
      result.accessbilityElement.removeEventListener('keydown', alternativeKeyupEvent);
      observer.disconnect();
    };
  }, [props.id]);
  const [open, setOpen] = useState(false);

  return (
    <Select
      size={props.size ?? 'large'}
      onClick={() => setOpen(!open)}
      onKeyDown={(e) => {
        if (e.key === ' ') {
          e.preventDefault();
          setOpen(!open);
        }
        if (e.key === 'ArrowDown' && !open) {
          e.preventDefault();
          setOpen(!open);
        }
        if (e.key === 'ArrowUp' && !open) {
          e.preventDefault();
          setOpen(!open);
        }
        if (e.altKey && e.key === 'ArrowDown' && !open) {
          e.preventDefault();
          setOpen(!open);
        }
        if (e.key === 'Escape' && open) {
          e.preventDefault();
          setOpen(!open);
        }
        if (e.key === 'Enter' && !props.mode && open) {
          e.preventDefault();
          setOpen(!open);
        }
      }}
      onBlur={() => setOpen(false)}
      className="select-container"
      virtual={false}
      popupClassName="dropdown-menu"
      {...props}
      suffixIcon={
        props.suffixIcon ? (
          <span aria-hidden onClick={() => setOpen(!open)}>
            {props.suffixIcon}
          </span>
        ) : (
          <SelectDown style={{ width: 16, height: 16, marginRight: 4 }} />
        )
      }
      open={open}
      dropdownRender={dropdownRender}
      options={options?.map((item, index) => {
        const itemProps = item as SelectOptionItemInterface;
        const modifiedLabelProps = {
          'aria-label': itemProps.title ?? itemProps.value,
          'aria-posinset': index + 1,
          'aria-setsize': options.length,
          ...(item.label as ReactElement<React.HTMLAttributes<HTMLElement>>).props,
        };
        const modifiedLabel = React.cloneElement(item.label as ReactElement, modifiedLabelProps);
        return {
          ...item,
          label: modifiedLabel,
        };
      })}
    />
  );
}
