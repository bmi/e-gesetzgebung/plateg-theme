// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Observable, Subject } from 'rxjs';
export class DrawerController {
  private readonly closeOtherObserver: Subject<string> = new Subject<string>();

  public closeOtherDrawers(selectedDrawerId: string): void {
    this.closeOtherObserver.next(selectedDrawerId);
  }

  public subscribeCloseOtherDrawers(): Observable<string> {
    return this.closeOtherObserver.asObservable();
  }
}
