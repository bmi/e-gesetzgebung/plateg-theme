// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './info-component.less';

import { Button, Drawer } from 'antd';
import React, { MouseEvent, useCallback, useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useLocation } from 'react-router';
import { Subscription } from 'rxjs';

import { EinstellungDrawerBreiteType } from '@plateg/rest-api';

import { AriaController } from '../../controllers/AriaController';
import { DrawerHeightController } from '../../controllers/DrawerHeightController';
import { GlobalDI } from '../../shares/injector';
import { CloseOutlined } from '../icons/CloseOutlined';
import { InfoCircleFilled } from '../icons/InfoCircleFilled';
import { InfoCircleOutlined } from '../icons/InfoCircleOutlined';
import { ThreeDots } from '../icons/ThreeDots';
import { useAppSelector } from '../store';
import { TooltipComponent } from '../tooltipComponent/component.react';
import { DrawerController } from './controller';

interface InfoComponentProps {
  title: string;
  titleWithoutPrefix?: boolean;
  children: React.ReactElement[] | React.ReactElement;
  buttonText?: string | React.ReactElement;
  buttonId?: string;
  isContactPerson?: boolean;
  id?: string;
  prefilledWidth?: string | number;
  getContainer?: string;
  disabled?: boolean;
  // props to position tooltips correctly
  withLabel?: boolean;
  withLabelEmail?: boolean;
  withLabelRequired?: boolean;
  tooltipBtnStyle?: React.CSSProperties;
  buttonAriaLabel?: string;
}
const defaultDrawerWidth = 456;
const minDrawerWidth = 456;
const maxDrawerWidth = 1104;

export function InfoComponent(props: InfoComponentProps): React.ReactElement {
  const location = useLocation();
  const [isVisible, setIsVisible] = useState(false);
  const [drawerWidth, setDrawerWidth] = useState(props.prefilledWidth ?? defaultDrawerWidth);
  const [drawerController] = useState<DrawerHeightController>(new DrawerHeightController());
  const ctrl = GlobalDI.getOrRegister('DrawerController', () => new DrawerController());
  const btnIcon = useRef<HTMLButtonElement>(null);

  const drawerID = props.id ?? props.title;

  const appStoreSetting = useAppSelector((state) => state.setting);
  const storeDrawerWidth = appStoreSetting.setting?.barrierefreiheitInformationsleisteBreite;

  const isFirefoxWindows = navigator.userAgent.includes('Windows') && navigator.userAgent.includes('Firefox');

  useEffect(() => {
    const sub: Subscription = ctrl.subscribeCloseOtherDrawers().subscribe({
      next: (data) => {
        setIsVisible(drawerID === data);
      },
      error: (error) => {
        console.error(error);
      },
    });
    return () => {
      sub.unsubscribe();
    };
  }, []);

  const getDrawerSize = (value: EinstellungDrawerBreiteType): number => {
    switch (value) {
      case EinstellungDrawerBreiteType.Schmal:
        return 456;
      case EinstellungDrawerBreiteType.Normal:
        return 558;
      case EinstellungDrawerBreiteType.Breit:
        return 660;
      default:
        return 456;
    }
  };

  useEffect(() => {
    if (isVisible) {
      drawerController.registerListener();
      AriaController.setAriaLabelsByClassName('ant-drawer-close', 'Schließen');
      AriaController.removeAriaAttrByClassName('ant-drawer-content', 'aria-modal');
    } else {
      if (props.prefilledWidth) {
        setDrawerWidth(props.prefilledWidth);
      } else if (storeDrawerWidth) {
        setDrawerWidth(getDrawerSize(storeDrawerWidth));
      }
      drawerController.removeListener();
    }
  }, [isVisible]);

  useEffect(() => {
    drawerController.updateDrawerHeight();
  });

  useEffect(() => {
    setIsVisible(false);
  }, [location]);

  const { t } = useTranslation();

  const handleMouseDown = (e: MouseEvent<HTMLElement>) => {
    e.preventDefault();
    document.addEventListener('mouseup', handleMouseUp, true);
    document.addEventListener('mousemove', handleMouseMove, true);
  };

  const handleMouseUp = (e: globalThis.MouseEvent) => {
    document.removeEventListener('mouseup', handleMouseUp, true);
    document.removeEventListener('mousemove', handleMouseMove, true);
  };

  const handleMouseMove = useCallback((e: globalThis.MouseEvent) => {
    const newWidth = document.body.offsetWidth - e.clientX;
    if (newWidth > minDrawerWidth && newWidth < maxDrawerWidth) {
      setDrawerWidth(newWidth);
    }
  }, []);

  let buttonADrawerTitle = '';
  if (props.titleWithoutPrefix) {
    buttonADrawerTitle = props.title;
  } else {
    buttonADrawerTitle = props.isContactPerson
      ? t('theme.infoComponent.contact')
      : t('theme.infoComponent.title', { title: props.title });
  }

  const manageDrawerVisibility = () => {
    if (!isVisible) {
      ctrl.closeOtherDrawers(drawerID);
    }
    setIsVisible(!isVisible);
    btnIcon.current?.focus();

    improveTabNavigationDrawer();
  };

  const onClickTooltip = () => {
    btnIcon.current?.blur();
  };

  const getTooltipClassProp = () => {
    const tooltipLabelClassname = 'info-component-in-label';
    let classnameVar = '';
    if (props.withLabelRequired) {
      classnameVar = `${tooltipLabelClassname}-required`;
      if (props.withLabelEmail) {
        classnameVar = `${tooltipLabelClassname}-required ${tooltipLabelClassname}-required-email`;
      }
    } else if (props.withLabel) {
      classnameVar = tooltipLabelClassname;
      if (props.withLabelEmail) {
        classnameVar = `${tooltipLabelClassname} ${tooltipLabelClassname}-email`;
      }
    }
    return classnameVar;
  };

  const activeClassName = isVisible ? 'active' : '';
  const trigger = props.buttonText ? (
    <Button
      type="text"
      id={props.buttonId}
      className={`help-link ${activeClassName}`}
      onClick={manageDrawerVisibility}
      aria-expanded={isVisible}
      ref={btnIcon}
      disabled={props.disabled ?? false}
      aria-label={props.buttonAriaLabel}
    >
      {props.buttonText}
    </Button>
  ) : (
    <span className={getTooltipClassProp()}>
      <TooltipComponent onClickTooltip={onClickTooltip} overlayStyle={{ maxWidth: '260px' }} title={buttonADrawerTitle}>
        <button
          style={props.tooltipBtnStyle}
          type="button"
          className={`info-icon ${activeClassName}`}
          onClick={manageDrawerVisibility}
          aria-expanded={isVisible}
          aria-label={buttonADrawerTitle}
          ref={btnIcon}
          disabled={props.disabled ?? false}
        >
          <InfoCircleFilled />
          <InfoCircleOutlined />
        </button>
      </TooltipComponent>
    </span>
  );

  return (
    <span>
      {trigger}
      <Drawer
        styles={{
          content: { borderLeft: '2px solid #738eea' },
          header: { marginRight: isFirefoxWindows ? '0.5vw' : '0' },
          body: { marginRight: isFirefoxWindows ? '0.5vw' : '0' },
        }}
        title={<h1>{buttonADrawerTitle}</h1>}
        placement="right"
        onClose={manageDrawerVisibility}
        closeIcon={<CloseOutlined />}
        open={isVisible}
        getContainer={props.getContainer || '.main-content-area'}
        mask={false}
        width={drawerWidth}
        destroyOnClose={true}
      >
        <div onMouseDown={(e: MouseEvent<HTMLElement>) => handleMouseDown(e)} className="drawer-dragger">
          <ThreeDots />
        </div>
        {props.children}
      </Drawer>
    </span>
  );
}

// this function helps to skip first and last empty tab navigation step when opened drawer for better bitv navigation

const warningMessage = (variable: string) =>
  `Selector under variable ${variable} failed to be selected. Drawer tab navigation might be broken!`;

const improveTabNavigationDrawer = () => {
  setTimeout(() => {
    setTabIndexes();
    addCloseButtonFocus();
  }, 0);
};

const setTabIndexes = () => {
  const elementToSkipStart = document.querySelector<HTMLElement>('[data-sentinel="start"][data-sentinel]');
  if (elementToSkipStart) {
    elementToSkipStart.tabIndex = -1;
    elementToSkipStart.addEventListener('keydown', (event) => {
      if (event.key === 'Tab') {
        event.preventDefault();
        const nextElement = elementToSkipStart.children[0];
        if (nextElement instanceof HTMLElement) {
          nextElement.focus();
        } else {
          console.warn(warningMessage('nextElement'));
        }
      }
    });
  } else {
    console.warn(warningMessage('elementToSkipStart'));
  }
};

const addCloseButtonFocus = () => {
  const parentElement = document.querySelector<HTMLElement>('.ant-drawer-body');
  if (parentElement) {
    const clickableElements = parentElement.querySelectorAll<HTMLElement>(
      'a, button, input[type="button"], input[type="submit"], input[type="reset"], [role="button"], [role="link"]',
    );
    if (clickableElements.length > 0) {
      const lastElement = clickableElements[clickableElements.length - 1];
      lastElement.addEventListener('keydown', (event) => {
        if (event.key == 'Tab') {
          event.preventDefault();
          const closeBtn = document.querySelector<HTMLElement>('.ant-drawer-close');
          if (closeBtn) {
            closeBtn.focus();
          } else {
            console.warn(warningMessage('closeBtn'));
          }
        }
      });
    }
  } else {
    console.warn(warningMessage('parentElement'));
  }
};
