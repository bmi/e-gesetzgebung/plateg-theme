// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { message } from 'antd';
import React from 'react';
import { v4 as uuidv4 } from 'uuid';

import { CheckOutlinedGreen, CloseOutlined, SystemErrorIcon } from '../index';

declare type NoticeType = 'info' | 'success' | 'error' | 'warning' | 'loading';
export function displayMessage(content: string, type: string): void {
  const messageblock = document.createElement('div');
  messageblock.setAttribute('class', 'alert-messages-accessibility-block');
  messageblock.setAttribute('role', 'alert');
  document.body.appendChild(messageblock);

  setTimeout(() => {
    messageblock.innerText = content;
  }, 200);
  const messageID = uuidv4();
  const messageConfig = {
    type: type as NoticeType,
    content: (
      <>
        <span
          dangerouslySetInnerHTML={{
            __html: content,
          }}
        />
        <span className="close-icon" onClick={() => message.destroy(messageID)}>
          <CloseOutlined />
        </span>
      </>
    ),
    className: `custom-${type}-message`,
    key: messageID,
    icon: (
      <span className="anticon">{type === 'success' ? <CheckOutlinedGreen /> : <SystemErrorIcon style={{}} />}</span>
    ),
    duration: 6,
  };

  setTimeout(() => {
    if (messageblock) {
      messageblock.remove();
    }
  }, 1000);

  void message.open(messageConfig);
}
