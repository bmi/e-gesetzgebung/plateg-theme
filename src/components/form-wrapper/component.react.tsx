// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Form } from 'antd';
import { FormInstance } from 'antd/lib/form';
import React, { useEffect } from 'react';
import { useHistory } from 'react-router';

import { ErrorBox } from '../error-box/component.react';
import { GeneralFormWrapper } from '../general-form-wrapper/component.react';
import { LeftOutlined } from '../icons/LeftOutlined';
import { RightOutlined } from '../icons/RightOutlined';
import { PageLeavePrompt } from '../page-leave-prompt-component/component.react';

interface WrapperProps<T> {
  projectName?: string;
  title: React.ReactElement;
  saveDraft: () => void;
  children: React.ReactNode;
  previousPage: string;
  nextPage: string;
  isDirty: () => boolean;
  form: FormInstance;
  formInitialValue: T;
  setFormInstance: (form: FormInstance | undefined) => void;
  showMandatoryFieldInfo?: boolean;
  handleFormChanges?: () => void;
  isTitleStretched?: boolean;
}
export function FormWrapper<T>(props: WrapperProps<T>): React.ReactElement {
  const history = useHistory();

  useEffect(() => {
    props.form.setFieldsValue(props.formInitialValue);
    props.setFormInstance(props.form);
    return () => props.setFormInstance(undefined);
  }, [props.form]);

  const onFinish = () => {
    // It is automatically saved in PageLeavePrompt
    // props.saveDraft();
    history.push(props.nextPage);
  };

  const onFinishFailed = () => {
    document.getElementById('errorBox')?.focus();
    window.scrollTo(0, 0);
  };

  let nextPageTitle = 'Nächster Schritt';
  if (props.nextPage?.endsWith('fertigstellung')) {
    nextPageTitle = 'Zur Fertigstellung';
  }

  const shouldStay = () => {
    if (props.projectName === 'eGFA') {
      return false;
    }
    return props.form.getFieldsError().some((item) => item.errors.length > 0);
  };

  return (
    <div style={{ marginBottom: '120px' }}>
      <PageLeavePrompt
        projectName={props.projectName}
        saveDraft={props.saveDraft}
        shouldSave={props.isDirty}
        shouldStay={shouldStay}
      />

      <GeneralFormWrapper
        form={props.form}
        layout="vertical"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        onFieldsChange={props.handleFormChanges}
        noValidate
      >
        <section className="content-section">
          <div className={props.isTitleStretched ? 'form-heading-stretched' : ''}>
            {props.title}
            {props.showMandatoryFieldInfo && (
              <p className="ant-typography p-no-style required-fields-info" style={{ marginTop: '-15px' }}>
                Pflichtfelder sind mit einem * gekennzeichnet.
              </p>
            )}
          </div>
          <Form.Item shouldUpdate noStyle>
            {() => <ErrorBox fieldsErrors={props.form.getFieldsError()} />}
          </Form.Item>
          {props.children}
        </section>
        <div className="form-control-buttons">
          {props.previousPage !== '' && (
            <Button
              id={`${props.projectName || 'theme-generic'}-${props.previousPage}-navigation-btn`}
              type="text"
              size={'large'}
              className="btn-prev"
              onClick={() => history.push(props.previousPage)}
            >
              <LeftOutlined /> Vorheriger Schritt
            </Button>
          )}
          <Button
            id={`${props.projectName || 'theme-generic'}-${props.nextPage}-navigation-btn-${
              history.location.key as string
            }`}
            type="text"
            size={'large'}
            className="btn-next"
            htmlType="submit"
          >
            {nextPageTitle} <RightOutlined />
          </Button>
        </div>
      </GeneralFormWrapper>
    </div>
  );
}
