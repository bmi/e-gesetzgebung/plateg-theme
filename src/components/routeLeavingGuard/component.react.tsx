// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './route-leaving.less';

import { Location } from 'history';
import React, { useEffect, useState } from 'react';
import { Prompt } from 'react-router-dom';

import { ModalWrapper } from '../modal-wrapper/component.react';

interface Props {
  when?: boolean;
  navigate: (path: string) => void;
  shouldBlockNavigation: (location: Location) => boolean | string;
  title?: string;
  btnOk?: string;
  btnCancel?: string;
}
export function RouteLeavingGuard({
  when,
  navigate,
  shouldBlockNavigation,
  title,
  btnOk,
  btnCancel,
}: Props): React.ReactElement {
  const [modalVisible, setModalVisible] = useState(false);
  const [lastLocation, setLastLocation] = useState<Location | null>(null);
  const [confirmedNavigation, setConfirmedNavigation] = useState(false);
  const [titleState, setTitleState] = useState(title);

  useEffect(() => {
    if (modalVisible) {
      // Timeout is needed, since cancelButton might not be rendered in this moment
      setTimeout(() => document?.getElementById('cancelButton')?.focus(), 200);
    }
  }, [modalVisible]);

  const closeModal = () => {
    setModalVisible(false);
  };
  const handleBlockedNavigation = (nextLocation: Location): boolean => {
    const titleOrBoolean = shouldBlockNavigation(nextLocation);
    if (!confirmedNavigation && titleOrBoolean) {
      // If title state is undefined, the shouldBlockNavigation function will return the title
      if (titleState === undefined) {
        setTitleState(String(titleOrBoolean));
      }
      setModalVisible(true);
      setLastLocation(nextLocation);
      return false;
    }
    return true;
  };
  const handleConfirmNavigationClick = () => {
    setModalVisible(false);
    setConfirmedNavigation(true);
  };
  useEffect(() => {
    if (confirmedNavigation && lastLocation) {
      // Navigate to the previous blocked location with your navigate function
      navigate(lastLocation.pathname);
    }
  }, [confirmedNavigation, lastLocation]);

  return (
    <>
      <Prompt when={when} message={handleBlockedNavigation} />
      <ModalWrapper
        title={<h3>{'Hinweis'}</h3>}
        open={modalVisible}
        onOk={handleConfirmNavigationClick}
        onCancel={closeModal}
        okText={btnOk ? btnOk : 'Ja'}
        cancelButtonProps={{ id: 'cancelButton' }}
        cancelText={btnCancel ? btnCancel : 'Nein'}
        className="route-leaving-component"
      >
        {/* previous title is shown as content */}
        <span className="title-text">{titleState}</span>
      </ModalWrapper>
    </>
  );
}
