// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './choicesList.less';

import { Button, Checkbox } from 'antd';
import { CheckboxChangeEvent } from 'antd/lib/checkbox';
import { CheckboxValueType } from 'antd/lib/checkbox/Group';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { useAppSelector } from '../../store';

interface ChoicesListComponentInterface {
  fieldName: string;
  choicesList: ChoicesItemInterface[];
  listTitle: string;
  updateSelectedItems: (items: string[]) => void;
  selectedItems: ChoicesItemInterface[];
  onCancel: () => void;
  isVisible: boolean;
  isCheckAllHidden?: boolean;
}
export interface ChoicesItemInterface {
  label: string;
  value: string;
  disabled?: boolean;
}
export function ChoicesListComponent(props: ChoicesListComponentInterface): React.ReactElement {
  const { t } = useTranslation();
  const CheckboxGroup = Checkbox.Group;
  const valuesList = props.choicesList.map((item) => item.value);
  const [checkedList, setCheckedList] = useState<CheckboxValueType[]>([]);
  const [indeterminate, setIndeterminate] = useState(false);
  const [checkAll, setCheckAll] = useState(false);
  const [choicesListLocal, setChoicesListLocal] = useState<ChoicesItemInterface[]>([]);
  const [hasScrollBar, setHasScrollBar] = useState<boolean>(false);
  const appStoreSettings = useAppSelector((state) => state.setting);
  const onCheckAllChange = (e: CheckboxChangeEvent) => {
    setCheckedList(e.target.checked ? valuesList : []);
    setIndeterminate(false);
    setCheckAll(e.target.checked);
  };
  const onChange = (list: CheckboxValueType[]) => {
    setCheckedList(list);
    setIndeterminate(!!list.length && list.length < props.choicesList.length);
    setCheckAll(list.length === props.choicesList.length);
  };

  useEffect(() => {
    setChoicesListLocal(props.choicesList);
  }, [props.choicesList]);

  useEffect(() => {
    if (props.selectedItems) {
      const preparedList = props.selectedItems.map((item) => item.value);
      setIndeterminate(!!preparedList.length && preparedList.length < props.choicesList.length);
      setCheckAll(preparedList.length === props.choicesList.length);
      setCheckedList(preparedList || []);
    }
  }, [props.selectedItems]);

  useEffect(() => {
    if (appStoreSettings.setting?.barrierefreiheitAeussererScrollbalken === false) {
      setHasScrollBar(true);
    }
  }, [appStoreSettings]);

  return (
    <div className={`choices-list-holder ${!props.isVisible ? 'hidden' : ''}`}>
      <strong className="items-counter">
        {t('theme.multiChoice.selectedItemsLabel', {
          selectedItems: checkedList.length,
          allItems: props.choicesList.length,
        })}
      </strong>
      {!props.isCheckAllHidden && (
        <Checkbox indeterminate={indeterminate} onChange={onCheckAllChange} checked={checkAll}>
          {t('theme.multiChoice.checkAll')}
        </Checkbox>
      )}

      <fieldset className="choices-list">
        <legend>{props.listTitle}</legend>
        <CheckboxGroup value={checkedList} onChange={onChange} role="list" className={hasScrollBar ? 'has-scroll' : ''}>
          {choicesListLocal.map((item, index) => {
            return (
              <Checkbox
                key={item.value}
                value={item.value}
                disabled={item.disabled}
                aria-label={`${item.label} ${index + 1} von ${choicesListLocal.length}`}
              >
                {item.label}
              </Checkbox>
            );
          })}
        </CheckboxGroup>
      </fieldset>
      <Button type="primary" onClick={() => props.updateSelectedItems(checkedList as string[])} className="btn-apply">
        {t('theme.multiChoice.btnApply')}
      </Button>
      <Button onClick={props.onCancel}>{t('theme.multiChoice.btnCancel')}</Button>
    </div>
  );
}
