// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './tagsList.less';

import { Button } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { CloseOutlined } from '../../icons/CloseOutlined';
import { ChoicesItemInterface } from '../choices-list/component.react';

interface TagsListInterface {
  selectedItems: ChoicesItemInterface[];
  fieldName: string;
  removeItem: (itemId: string) => void;
  itemType: string;
  disabled?: boolean;
}

export function TagsListComponent(props: TagsListInterface): React.ReactElement {
  const { t } = useTranslation();
  const [items, setItems] = useState<ChoicesItemInterface[]>([]);

  useEffect(() => {
    setItems(props.selectedItems);
  }, [props.selectedItems]);

  return (
    <div className="tag-holder">
      {items.map((item, index) => {
        return (
          <div className={`tag-element `} key={index} id={`selected-address-${props.fieldName}-${index}`}>
            {item.label}
            <Button
              className="btn-close"
              type="text"
              disabled={props.disabled}
              onClick={() => props.removeItem(item.value)}
              aria-label={`${t('theme.multiChoice.removeItemText')} ${item.label}`}
              id={`delete-address-${props.fieldName}-${index}`}
            >
              <CloseOutlined />
            </Button>
          </div>
        );
      })}
      {!items.length
        ? t(`theme.multiChoice.noSelectionText`, {
            itemType: props.itemType,
            e: props.itemType.toString().endsWith('ung') ? 'e' : '',
          })
        : null}
    </div>
  );
}
