// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './multi-choice.less';

import { Button, FormInstance } from 'antd';
import { Rule } from 'antd/lib/form';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { FormItemWithInfo } from '../form-item-with-info/component.react';
import { ChoicesItemInterface, ChoicesListComponent } from './choices-list/component.react';
import { TagsListComponent } from './tags-list/component.react';

export interface MultiChoiceComponentProps {
  choices: ChoicesItemInterface[];
  fieldName: string;
  fieldLabel: React.ReactElement;
  listTitle: string;
  form: FormInstance;
  itemType: string;
  rules?: Rule[];
  disabled?: boolean;
  onChange?: (values: string[]) => void;
  isCheckAllHidden?: boolean;
}
export function MultiChoiceComponent(props: MultiChoiceComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const [showChoicesList, setShowChoicesList] = useState<boolean>(false);
  const [selectedItems, setSelectedItems] = useState<ChoicesItemInterface[]>([]);
  const btnShowRef = useRef<HTMLButtonElement>(null);

  // Update selected items and close choices list
  const updateSelectedItems = (items: string[]) => {
    const filteredItems = props.choices.filter((choice) => items.some((item) => item === choice.value));
    setSelectedItems(filteredItems);
    props.form.setFieldsValue({ [props.fieldName]: items });
    void props.form.validateFields([props.fieldName]);
    setShowChoicesList(false);
    onCancelHandler();
    props.onChange?.(items);
  };

  // remove selected item and close choices list
  const removeSelectedItem = (itemId: string) => {
    const filteredItems = selectedItems.filter((item) => item.value !== itemId);
    setSelectedItems(filteredItems);
    props.form.setFieldsValue({ [props.fieldName]: filteredItems.flatMap((item) => item.value) });
    void props.form.validateFields([props.fieldName]);
    props.onChange?.(filteredItems.flatMap((item) => item.value));
  };

  const onCancelHandler = () => {
    setShowChoicesList(false);
    btnShowRef.current?.focus();
  };

  useEffect(() => {
    const selectedItemsIds = props.form.getFieldValue(props.fieldName) as string[];
    setSelectedItems(props.choices.filter((choice) => selectedItemsIds.some((id) => choice.value === id)));
  }, [props.choices]);

  return (
    <div className="multi-choice-holder">
      <FormItemWithInfo initialValue={[]} name={props.fieldName} label={props.fieldLabel} rules={props.rules}>
        <TagsListComponent
          selectedItems={selectedItems}
          fieldName={props.fieldName}
          removeItem={removeSelectedItem}
          itemType={props.itemType}
          disabled={props.disabled}
        />
        <Button
          type="link"
          className={`show-choices-btn`}
          onClick={() => setShowChoicesList(!showChoicesList)}
          id={`show-choices-${props.fieldName}`}
          ref={btnShowRef}
          disabled={props.disabled}
        >
          {t(`theme.multiChoice.${showChoicesList ? 'btnHideChoices' : 'btnShowChoices'}`)}
        </Button>

        <ChoicesListComponent
          fieldName={props.fieldName}
          choicesList={props.choices}
          listTitle={props.listTitle}
          updateSelectedItems={updateSelectedItems}
          selectedItems={selectedItems}
          onCancel={onCancelHandler}
          isVisible={showChoicesList && props.choices.length > 0}
          isCheckAllHidden={props.isCheckAllHidden}
        />
      </FormItemWithInfo>
    </div>
  );
}
