// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Checkbox, Col, DatePicker, Form, Input, Radio, Row, Select, TimePicker, Typography } from 'antd';
import React, { useState } from 'react';
import { Link } from 'react-router-dom';

import { CheckOutlined } from '../icons/CheckOutlined';
import { DeleteOutlined } from '../icons/DeleteOutlined';
import { ExternalLinkOutlined } from '../icons/ExternalLinkOutlined';
import { LeftOutlined } from '../icons/LeftOutlined';
import { PlusOutlined } from '../icons/PlusOutlined';
import { RightOutlined } from '../icons/RightOutlined';
import { InfoComponent } from '../info-component/component.react';

export function Styleguide(): React.ReactElement {
  const [visibleDrawer, setVisibleDrawer] = useState('');
  const { TextArea } = Input;
  const [form] = Form.useForm();
  const { Option } = Select;
  const { Title, Paragraph } = Typography;
  return (
    <div className="main-content-area styleguide-layout">
      <div className="example">
        <Title level={1}>H1 Almost before we knew it, we had left the ground. 30px/38px</Title>

        <Title level={2}>H2 Almost before we knew it, we had left the ground. 24px/32px</Title>
        <Title level={3}>H3 Almost before we knew it, we had left the ground. 20px/28px</Title>
        <Title level={4}>H4 Almost before we knew it, we had left the ground. 18px/26px</Title>
        <Title level={5}>H5 Almost before we knew it, we had left the ground. 16px/24px</Title>
        <Row>
          <Col></Col>
          <Col span={12}>Primary</Col>
          <Col span={12}>
            <Button id="theme-generic-styleGuide-primary-btn" type="primary">
              Primary Button
            </Button>
          </Col>
        </Row>
        <br />
        <Row>
          <Col></Col>
          <Col span={12}>Secondary</Col>
          <Col span={12}>
            <Button id="theme-generic-styleGuide-secondary-btn">Secondary Button</Button>
          </Col>
        </Row>
        <br />
        <Row>
          <Col></Col>
          <Col span={12}>Big Primary</Col>
          <Col span={12}>
            <Button id="theme-generic-styleGuide-bigPrimary-btn" type="primary" size={'large'}>
              Primary Button
            </Button>
          </Col>
        </Row>
        <br />
        <Row>
          <Col></Col>
          <Col span={12}>Secondary</Col>
          <Col span={12}>
            <Button id="theme-generic-styleGuide-bigSecondary-btn" size={'large'}>
              Secondary Button
            </Button>
          </Col>
        </Row>
        <br />
        <Row>
          <Col span={12}>Info icon in Title</Col>
          <Col span={12}>
            <Title level={2}>
              Info icon
              <InfoComponent
                title="Info icon in Title"
                visibleDrawer={visibleDrawer}
                drawerName="info-icon-title-drawer"
                setDrawerName={setVisibleDrawer}
              >
                <p className="ant-typography p-no-style">
                  Fließtext Regular 18/26 Für Teilbereiche bieten sich insbesondere die getrennte Betrachtung des
                  Regelungsinhalts, des Verfahrens, der Organisation und der Art der Regelung an. Sie können aber auch
                  eigene Teilbereiche für bedeutsame Aspekte des Regelungsvorhabens anlegen.
                </p>
              </InfoComponent>
            </Title>
          </Col>
        </Row>
        <br />
        <Row>
          <Col span={12}>Info icon in Paragraph</Col>
          <Col span={12}>
            <Paragraph>
              <CheckOutlined />
              Info icon
              <InfoComponent
                title="Info icon in Paragraph"
                visibleDrawer={visibleDrawer}
                drawerName="info-icon-paragraph-drawer"
                setDrawerName={setVisibleDrawer}
              >
                <p className="ant-typography p-no-style">
                  Fließtext Regular 18/26 Für Teilbereiche bieten sich insbesondere die getrennte Betrachtung des
                  Regelungsinhalts, des Verfahrens, der Organisation und der Art der Regelung an. Sie können aber auch
                  eigene Teilbereiche für bedeutsame Aspekte des Regelungsvorhabens anlegen.
                </p>
              </InfoComponent>
            </Paragraph>
          </Col>
        </Row>
        <br />
        <Row>
          <Col span={12}>Text Button Primary</Col>
          <Col span={12}>
            <Button id="theme-generic-styleGuide-textButtonPrimary-btn" type="text">
              Text Button
            </Button>
          </Col>
        </Row>
        <br />
        <Row>
          <Col span={12}>Text Button Primary Icon</Col>
          <Col span={12}>
            <Button id="theme-generic-styleGuide-textButtonPrimaryIcon-btn" type="text">
              <LeftOutlined />
              Text Button
            </Button>
          </Col>
        </Row>
        <br />
        <Row>
          <Col span={12}>Text Button Secondary Icon</Col>
          <Col span={12}>
            <Button id="theme-generic-styleGuide-textButtonSecondaryIcon-btn" type="link">
              <PlusOutlined />
              Text Button
            </Button>
          </Col>
        </Row>
        <br />
        <Row>
          <Col span={12}>Text Link</Col>
          <Col span={12}>
            <Link id="theme-generic-styleGuide-textLink-link" to={'#'}>
              Text Link
            </Link>
          </Col>
        </Row>
        <br />
        <Row>
          <Col span={12}>Text Link Icon</Col>
          <Col span={12}>
            <Link id="theme-generic-styleGuide-textLinkIcon-link" to={'#'}>
              Text Link
            </Link>
            <ExternalLinkOutlined />
          </Col>
        </Row>
        <br />
        <Row>
          <Col span={12}>Checkbox</Col>
          <Col span={12}>
            <Form.Item>
              <Checkbox id="theme-generic-styleguide-justiz-chkBox">
                Justiz
                <InfoComponent
                  title="Info icon in Chekbox"
                  visibleDrawer={visibleDrawer}
                  drawerName="info-icon-chekbox-drawer"
                  setDrawerName={setVisibleDrawer}
                >
                  <p className="ant-typography p-no-style">
                    Fließtext Regular 18/26 Für Teilbereiche bieten sich insbesondere die getrennte Betrachtung des
                    Regelungsinhalts, des Verfahrens, der{' '}
                    <a id="theme-generic-styleguide-teilbereicheInfo-link" href="http://link.de">
                      Organisation und der Art
                    </a>{' '}
                    der Regelung an. Sie können aber auch eigene Teilbereiche für bedeutsame Aspekte des
                    Regelungsvorhabens anlegen.
                  </p>
                </InfoComponent>
              </Checkbox>
            </Form.Item>
          </Col>
        </Row>
        <br />
        <Row>
          <Col span={12}>Radiobutton</Col>
          <Col span={12}>
            <Form.Item>
              <Radio.Group name="radiogroup">
                <Radio id="theme-generic-styleguide-negativ-radio" value="negativ">
                  negativ
                </Radio>
                <Radio id="theme-generic-styleguide-neutral-radio" value="neutral">
                  neutral
                </Radio>
                <Radio id="theme-generic-styleguide-positiv-radio" value="positiv">
                  positiv
                </Radio>
              </Radio.Group>
            </Form.Item>
          </Col>
        </Row>

        <br />
        <Row>
          <Col span={12}>Radiobutton horizontal</Col>
          <Col span={12}>
            <Form.Item>
              <Radio.Group className="horizontal-radios" name="radiogroup-horizontal">
                <Radio id="theme-generic-styleguide-horizontalRadiosNegativ-radio" value="negativ">
                  negativ
                </Radio>
                <Radio id="theme-generic-styleguide-horizontalRadiosNeutral-radio" value="neutral">
                  neutral
                </Radio>
                <Radio id="theme-generic-styleguide-horizontalRadiosPositiv-radio" value="positiv">
                  positiv
                </Radio>
              </Radio.Group>
            </Form.Item>
          </Col>
        </Row>

        <Title level={2}>Headline Semibold 24px</Title>
        <div className="intro">
          <Title level={3}>Headline Semibold 20px</Title>
          <p className="ant-typography p-no-style">
            Fließtext Regular 18/26 Hier können Sie in einem ersten Schritt Ideen für das Regelungsvorhaben sammeln.
            Zerlegen Sie das Regelungsvorhaben <a href="http://link.de">dafür zunächst</a> in Teilbereiche. Überlegen
            Sie sich nun alternative Regelungsmöglichkeiten für die Teilbereiche und legen Sie diese als Optionen an -
            ratsam ist es, hier immer auch die Beibehaltung des Status quo als Option zu berücksichtigen. Die
            Zusammenstellung der einzelnen Optionen aus den Teilbereichen erfolgt im zweiten Schritt.
          </p>
        </div>

        <Title level={4}>Headline Semibold 18px</Title>
        <p className="ant-typography p-no-style">
          Fließtext Regular 18/26 Für Teilbereiche bieten sich insbesondere die getrennte Betrachtung des
          Regelungsinhalts, des Verfahrens, der Organisation und der Art der Regelung an. Sie können aber auch eigene
          Teilbereiche für bedeutsame Aspekte des Regelungsvorhabens anlegen.
        </p>

        <Form form={form} layout="vertical">
          <Title level={4}>1. Ziel Headline Semibold 18px</Title>
          <Form.Item
            name="name1"
            label={
              <span>
                Lable Medium 18px
                <InfoComponent
                  title="Test Drawer"
                  visibleDrawer={visibleDrawer}
                  drawerName="test-drawer-name"
                  setDrawerName={setVisibleDrawer}
                >
                  <p className="ant-typography p-no-style">
                    Fließtext Regular 18/26 Für Teilbereiche bieten sich insbesondere die getrennte Betrachtung des
                    Regelungsinhalts, des Verfahrens, der Organisation und der Art der Regelung an. Sie können aber auch
                    eigene Teilbereiche für bedeutsame Aspekte des Regelungsvorhabens anlegen.
                  </p>
                  <p className="ant-typography p-no-style">
                    Fließtext Regular 18/26 Für Teilbereiche bieten sich insbesondere die getrennte Betrachtung des
                    Regelungsinhalts, des Verfahrens, der Organisation und der Art der Regelung an. Sie können aber auch
                    eigene Teilbereiche für bedeutsame Aspekte des Regelungsvorhabens anlegen.
                  </p>
                </InfoComponent>
              </span>
            }
          >
            <Input id="theme-generic-styleGuide-name1-txtBox" />
          </Form.Item>
          <Form.Item
            name="name2"
            label={
              <span>
                Lable Medium 18px
                <InfoComponent
                  title="Test Drawer2"
                  visibleDrawer={visibleDrawer}
                  drawerName="test-drawer-name2"
                  setDrawerName={setVisibleDrawer}
                >
                  <p className="ant-typography p-no-style">
                    Fließtext Regular 18/26 Für Teilbereiche bieten sich insbesondere die getrennte Betrachtung des
                    Regelungsinhalts, des Verfahrens, der Organisation und der Art der Regelung an. Sie können aber auch
                    eigene Teilbereiche für bedeutsame Aspekte des Regelungsvorhabens anlegen.
                  </p>
                  <p className="ant-typography p-no-style">
                    Fließtext Regular 18/26 Für Teilbereiche bieten sich insbesondere die getrennte Betrachtung des
                    Regelungsinhalts, des Verfahrens, der Organisation und der Art der Regelung an. Sie können aber auch
                    eigene Teilbereiche für bedeutsame Aspekte des Regelungsvorhabens anlegen.
                  </p>
                </InfoComponent>
              </span>
            }
          >
            <TextArea id="theme-generic-styleGuide-name2-txtArea" rows={5} />
            <div style={{ textAlign: 'right', marginTop: '8px' }}>
              <Button
                id="theme-generic-styleGuide-deleteButtonSmall1-btn"
                size={'small'}
                icon={<DeleteOutlined />}
                type="link"
              >
                Ziel entfernenn
              </Button>
            </div>
          </Form.Item>

          <Title level={4}>2. Ziel Headline Semibold 18px</Title>
          <Form.Item
            name="name3"
            label={
              <span>
                Lable Medium 18px
                <InfoComponent
                  title="Test Drawer3"
                  visibleDrawer={visibleDrawer}
                  drawerName="test-drawer-name3"
                  setDrawerName={setVisibleDrawer}
                >
                  <p className="ant-typography p-no-style">
                    Fließtext Regular 18/26 Für Teilbereiche bieten sich insbesondere die getrennte Betrachtung des
                    Regelungsinhalts, des Verfahrens, der Organisation und der Art der Regelung an. Sie können aber auch
                    eigene Teilbereiche für bedeutsame Aspekte des Regelungsvorhabens anlegen.
                  </p>
                  <p className="ant-typography p-no-style">
                    Fließtext Regular 18/26 Für Teilbereiche bieten sich insbesondere die getrennte Betrachtung des
                    Regelungsinhalts, des Verfahrens, der Organisation und der Art der Regelung an. Sie können aber auch
                    eigene Teilbereiche für bedeutsame Aspekte des Regelungsvorhabens anlegen.
                  </p>
                </InfoComponent>
              </span>
            }
          >
            <Input id="theme-generic-styleGuide-name3-txtBox" />
          </Form.Item>
          <Form.Item
            name="name4"
            label={
              <span>
                Lable Medium 18px
                <InfoComponent
                  title="Test Drawer4"
                  visibleDrawer={visibleDrawer}
                  drawerName="test-drawer-name4"
                  setDrawerName={setVisibleDrawer}
                >
                  <p className="ant-typography p-no-style">
                    Fließtext Regular 18/26 Für Teilbereiche bieten sich insbesondere die getrennte Betrachtung des
                    Regelungsinhalts, des Verfahrens, der Organisation und der Art der Regelung an. Sie können aber auch
                    eigene Teilbereiche für bedeutsame Aspekte des Regelungsvorhabens anlegen.
                  </p>
                  <p className="ant-typography p-no-style">
                    Fließtext Regular 18/26 Für Teilbereiche bieten sich insbesondere die getrennte Betrachtung des
                    Regelungsinhalts, des Verfahrens, der Organisation und der Art der Regelung an. Sie können aber auch
                    eigene Teilbereiche für bedeutsame Aspekte des Regelungsvorhabens anlegen.
                  </p>
                </InfoComponent>
              </span>
            }
          >
            <TextArea id="theme-generic-styleGuide-name4-txtArea" rows={5} />
            <div style={{ textAlign: 'right', marginTop: '8px' }}>
              <Button
                id="theme-generic-styleGuide-deleteButtonSmall2-btn"
                size={'small'}
                icon={<DeleteOutlined />}
                type="link"
              >
                Ziel entfernenn
              </Button>
            </div>
          </Form.Item>
          <div style={{ marginTop: '-8px' }}>
            <Button
              id="theme-generic-styleGuide-deleteButtonSmall3-btn"
              size={'small'}
              icon={<PlusOutlined />}
              type="link"
            >
              Ziel entfernenn
            </Button>
          </div>

          <Form.Item name="name-select" label="Label select">
            <Select id="theme-generic-nameSelectDropDown-select" placeholder="Choose person" style={{ width: '100%' }}>
              <Option value="jack">Jack</Option>
              <Option value="lucy">Lucy</Option>
              <Option value="disabled" disabled>
                Disabled
              </Option>
              <Option value="Yiminghe">yiminghe</Option>
            </Select>
          </Form.Item>

          <Row gutter={8}>
            <Col span={12}>
              <Form.Item name="name-date" label="Date">
                <DatePicker id="theme-generic-styleguide-nameDate-picker" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item name="mainDeadlineTime" label="Time">
                <TimePicker format={'HH:mm'} />
              </Form.Item>
            </Col>
          </Row>

          <div className="form-control-buttons">
            <Button
              id="theme-generic-styleGuide-previousStepSubmit-btn"
              type="text"
              size={'large'}
              className="btn-prev"
            >
              <LeftOutlined /> Vorheriger Schritt
            </Button>
            <Button
              id="theme-generic-styleGuide-nextStepSubmit-btn"
              type="text"
              size={'large'}
              className="btn-next"
              htmlType="submit"
            >
              Nächster Schritt <RightOutlined />
            </Button>
          </div>
        </Form>
      </div>
    </div>
  );
}
