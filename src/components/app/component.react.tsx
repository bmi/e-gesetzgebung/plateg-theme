// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../../shares/i18n';

import React from 'react';
import { Route, Switch } from 'react-router-dom';

import { Styleguide } from '../styleguide/component.react';

export function ExampleThemeApp(): React.ReactElement {
  return (
    <Switch>
      <Route exact path="/">
        <Styleguide />
      </Route>
    </Switch>
  );
}
