// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Col, ColProps } from 'antd';
import React from 'react';

export function StandardContentCol(props: ColProps & React.RefAttributes<HTMLDivElement>): React.ReactElement {
  return (
    <Col
      {...props}
      xs={{ span: 22, offset: 1 }}
      md={{ span: 14, offset: 2 }}
      lg={{ span: 16, offset: 3 }}
      xl={{ span: 12, offset: 3 }}
      xxl={{ span: 9, offset: 8 }}
    ></Col>
  );
}
