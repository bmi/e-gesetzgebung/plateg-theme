// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, FormProps } from 'antd';
import { ValidateErrorEntity } from 'rc-field-form/lib/interface';
import React, { useLayoutEffect } from 'react';

export function GeneralFormWrapper(props: FormProps): React.ReactElement {
  useLayoutEffect(() => {
    setRequiredAttributes();
  });

  const setRequiredAttributes = () => {
    document.querySelectorAll('.ant-form-item-required').forEach((item) => {
      const inputId = item.getAttribute('for');
      if (inputId) {
        const elem = document.getElementById(inputId);
        if (elem?.classList.contains('ant-radio-group')) {
          Array.from(elem.getElementsByClassName('ant-radio-input')).forEach((radioOption) => {
            radioOption?.setAttribute('required', 'true');
          });
        } else {
          elem?.setAttribute('required', 'true');
        }
      }
    });
  };
  const handleFinishFailed = (errorInfo: ValidateErrorEntity<any>) => {
    const errorBoxTitle = document?.getElementById('errorBox-title');
    if (errorBoxTitle) {
      errorBoxTitle.focus();
      window.scrollTo(0, 0);
    }
    props.onFinishFailed?.(errorInfo);
  };

  const preventEnterKeySubmission = (e: React.KeyboardEvent<HTMLFormElement>) => {
    const target = e.target;
    if (
      (e.code === 'Enter' || e.code === 'NumpadEnter') &&
      target instanceof HTMLInputElement &&
      target.tagName.toLowerCase() === 'input'
    ) {
      e.preventDefault();
    }
  };

  return (
    <Form noValidate={true} {...props} onFinishFailed={handleFinishFailed} onKeyDown={preventEnterKeySubmission}></Form>
  );
}
