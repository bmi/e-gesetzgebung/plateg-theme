// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './hinweis-component.less';

import React from 'react';

import { ExclamationCircleFilled } from '../icons/ExclamationCircleFilled';

export interface HinweisComponentProps {
  title?: string;
  content: React.ReactElement;
  mode?: string;
  hinweisId?: string;
  styleDiv?: React.CSSProperties;
}

export function HinweisComponent(props: HinweisComponentProps): React.ReactElement {
  const modeClassName = props.mode ? `mode-${props.mode}` : '';
  return (
    <div className={`hinweis-component ${modeClassName}`} id={props.hinweisId || ''} style={{ ...props.styleDiv }}>
      <span className="hinweis-title">
        {props.mode === 'warning' ? <ExclamationCircleFilled /> : undefined}
        {`Hinweis${props.title ? ': ' + props.title : ' '}`}
      </span>
      <div className={`hinweis-content ${props.mode ? 'has-mode' : ''}`}>{props.content}</div>
    </div>
  );
}
