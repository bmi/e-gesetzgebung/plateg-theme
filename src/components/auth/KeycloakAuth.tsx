// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import Keycloak, { KeycloakConfig, KeycloakFlow, KeycloakInitOptions } from 'keycloak-js';
import React, { useEffect } from 'react';

import { BASE_PATH, EinstellungenControllerApi } from '@plateg/rest-api';
import { AuthClientTokens } from '@react-keycloak/core';
import { ReactKeycloakProvider, useKeycloak as useCloak } from '@react-keycloak/web';

import { GlobalDI } from '../../shares/injector';
import { RootState, store, useAppDispatch, useAppSelector } from '../store';
import { setAppSetting } from '../store/slices/appSettingSlice';
import { clearKcToken, setKcToken } from '../store/slices/kcTokenSlice';

interface KeycloakAuthProps {
  children: React.ReactElement;
  forceLogin?: boolean;
}
export const useKeycloak: typeof useCloak = useCloak;
export const logoutAction = (keycloak: Keycloak) => {
  const plattformLogoutCallback = () => {
    window.location.href = `${BASE_PATH}/logout`;
  };
  keycloak
    .logout()
    .then(() => {
      store.dispatch(clearKcToken());
      localStorage.removeItem('logoutTimer');
      localStorage.removeItem('stellvertretungActiveId');
      plattformLogoutCallback();
    })
    .catch((error) => {
      console.error(error);
      localStorage.removeItem('logoutTimer');
      localStorage.removeItem('stellvertretungActiveId');
      plattformLogoutCallback();
    });
};

export function KeycloakAuth(props: Readonly<KeycloakAuthProps>): React.ReactElement {
  const einstellungenControllerApi = GlobalDI.getOrRegister(
    'einstellungenControllerApi',
    () => new EinstellungenControllerApi(),
  );
  const dispatch = useAppDispatch();
  const appStore = {
    setting: useAppSelector((state) => state.appSetting.appSetting),
  };
  useEffect(() => {
    const sub = einstellungenControllerApi.getEinstellungen().subscribe({
      next: (data) => {
        dispatch(setAppSetting(data));
      },
    });
    return () => {
      sub?.unsubscribe();
    };
  }, []);
  const loadTokens = (): AuthClientTokens | undefined => {
    return (store.getState() as RootState).kcToken.kcToken;
  };

  let keycloakOptions: KeycloakConfig | undefined;
  let initOptions: KeycloakInitOptions | undefined;
  let keycloak: Keycloak | undefined;
  if (appStore.setting) {
    keycloakOptions = {
      realm: appStore.setting.iamEinstellungen.realm,
      url: appStore.setting.iamEinstellungen.authServerUrl,
      clientId: appStore.setting.iamEinstellungen.resource,
    };
    initOptions = {
      checkLoginIframe: false,
      flow: appStore.setting.iamEinstellungen.flowType as KeycloakFlow,
      onLoad: props.forceLogin ? 'login-required' : 'check-sso',
      ...loadTokens(),
    };
    keycloak = new Keycloak(keycloakOptions);
  }
  const saveTokens = (tokens: AuthClientTokens) => {
    dispatch(setKcToken(tokens));
  };

  if (keycloak) {
    return (
      <ReactKeycloakProvider authClient={keycloak} initOptions={initOptions} onTokens={saveTokens}>
        {props.children}
      </ReactKeycloakProvider>
    );
  } else {
    return <></>;
  }
}
