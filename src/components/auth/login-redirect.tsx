// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';

import { useKeycloak } from '@react-keycloak/web';

interface LoginRedirectProps {
  children: React.ReactElement;
}
export function LoginRedirect(props: Readonly<LoginRedirectProps>): React.ReactElement {
  const { keycloak } = useKeycloak();
  const [loggedIn, setLoggedIn] = useState<boolean | undefined>(false);
  useEffect(() => {
    if (keycloak.authenticated) {
      setLoggedIn(true);
    } else {
      setLoggedIn(false);
    }
  }, [keycloak.authenticated]);
  if (loggedIn) {
    return props.children;
  } else {
    return <>loading.....</>;
  }
}
