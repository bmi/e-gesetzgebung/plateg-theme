// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import React from 'react';

import { RightOutlined } from '../../icons/RightOutlined';

export interface NextStepComponentProps {
  buttonText: string;
  formName?: string;
  componentReferenceContext?: string;
}

export function NextStepComponent(props: NextStepComponentProps): React.ReactElement {
  return (
    <Button
      id={`theme-generic-${props.componentReferenceContext || ''}-${props.formName || ''}-${
        props.buttonText
      }-nextStep-btn`}
      htmlType="submit"
      form={props.formName}
      type="text"
      className="btn-prev"
    >
      {props.buttonText}
      <RightOutlined />
    </Button>
  );
}
