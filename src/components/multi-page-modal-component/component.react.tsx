// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useRef, useState } from 'react';

import { ModalComponent } from '../modal-component/component.react';
import { CancelButtonComponent } from './cancel-button/component.react';
import { NextStepComponent } from './next-step/component.react';
import { PrevStepComponent } from './prev-step/component.react';
import { PrimaryButtonComponent } from './primary-button/component.react';

export interface PrimaryInsteadNextButtonProps {
  buttonText: string;
  shouldNavToNextPage?: boolean;
  disabled?: boolean;
  hidden?: boolean;
}

export interface ModalFormPage {
  content: React.ReactElement;
  nextOnClick?: Function;
  primaryInsteadNextBtn?: PrimaryInsteadNextButtonProps;
  formName?: string;
}

export interface MultiPageModalComponentProps {
  className?: string;
  title: string | React.ReactNode;
  pages: ModalFormPage[];
  isVisible: boolean;
  setIsVisible: (isVisible: boolean) => void;
  cancelBtnText: string;
  nextBtnText: string;
  prevBtnText: string;
  activePageIndex: number;
  setActivePageIndex?: (activePageIndex: number) => void;
  componentReferenceContext?: string;
  rootClassName?: string;
}

export function MultiPageModalComponent(props: MultiPageModalComponentProps): React.ReactElement {
  const [footerRight, setFooterRight] = useState<React.ReactElement[]>([]);
  const focusResetRef = useRef<HTMLAnchorElement>(null);

  useEffect(() => {
    //reset focus on page change to utility anchor (just for this purpose)
    focusResetRef?.current?.setAttribute('style', 'display: initial;');
    focusResetRef?.current?.focus();
    focusResetRef?.current?.setAttribute('style', 'display: none;');
  }, [props.activePageIndex]);

  const footerLeft = [
    <CancelButtonComponent
      key={`cancel-${props.activePageIndex}`}
      setIsVisible={props.setIsVisible}
      buttonText={props.cancelBtnText}
      activePageIndex={props.activePageIndex}
      componentReferenceContext={props.componentReferenceContext}
    />,
  ];

  const setFooterElements = (activePageIndex: number) => {
    const page = props.pages[activePageIndex];
    const rightFooterElements: React.ReactElement[] = [];
    if (activePageIndex > 0 && props.setActivePageIndex) {
      rightFooterElements.push(
        <PrevStepComponent
          key={`prev-${activePageIndex}`}
          activePageIndex={activePageIndex}
          setActivePageIndex={props.setActivePageIndex}
          buttonText={props.prevBtnText}
          componentReferenceContext={props.componentReferenceContext}
        />,
      );
    }
    if (page.primaryInsteadNextBtn) {
      rightFooterElements.push(
        <PrimaryButtonComponent
          key={`primary-${activePageIndex}`}
          activePageIndex={activePageIndex}
          formName={page.formName}
          onCLick={page.nextOnClick}
          setActivePageIndex={props.setActivePageIndex}
          shouldNavToNextPage={page.primaryInsteadNextBtn.shouldNavToNextPage}
          buttonText={page.primaryInsteadNextBtn.buttonText || ''}
          disabled={page.primaryInsteadNextBtn.disabled}
          hidden={page.primaryInsteadNextBtn.hidden}
        />,
      );
    } else if (activePageIndex < props.pages.length - 1) {
      rightFooterElements.push(
        <NextStepComponent
          componentReferenceContext={props.componentReferenceContext}
          key={`next-${activePageIndex}`}
          buttonText={props.nextBtnText}
          formName={page.formName}
        />,
      );
    }

    setFooterRight(rightFooterElements);
  };

  useEffect(() => {
    setFooterElements(props.activePageIndex);
  }, [props.activePageIndex, props.pages]);

  return (
    <ModalComponent
      rootClassName={props.rootClassName}
      className={props.className}
      title={props.title}
      isVisible={props.isVisible}
      setIsVisible={props.setIsVisible}
      content={
        <>
          <a href="#" ref={focusResetRef} />
          {props.pages[props.activePageIndex].content}
        </>
      }
      footerLeft={footerLeft}
      footerRight={footerRight}
    />
  );
}
