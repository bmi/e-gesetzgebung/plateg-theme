// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import React from 'react';

import { LeftOutlined } from '../../icons/LeftOutlined';

export interface PrevStepComponentProps {
  activePageIndex: number;
  setActivePageIndex: (activePageIndex: number) => void;
  buttonText: string;
  componentReferenceContext?: string;
}

export function PrevStepComponent(props: PrevStepComponentProps): React.ReactElement {
  const onClick = () => {
    props.setActivePageIndex(props.activePageIndex - 1);
  };
  return (
    <Button
      id={`theme-generic-${props.componentReferenceContext || ''}-${props.activePageIndex - 1}-${
        props.buttonText
      }-previousStep-btn`}
      onClick={onClick}
      type="text"
      className="btn-prev"
    >
      <LeftOutlined />
      {props.buttonText}
    </Button>
  );
}
