// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import React from 'react';

export interface PrimaryButtonComponentProps {
  buttonText: string;
  formName?: string;
  onCLick?: Function;
  activePageIndex: number;
  setActivePageIndex?: (activePageIndex: number) => void;
  shouldNavToNextPage?: boolean;
  disabled?: boolean;
  hidden?: boolean;
}

export function PrimaryButtonComponent(props: PrimaryButtonComponentProps): React.ReactElement {
  if (props.formName) {
    return (
      <Button
        id={`theme-generic-${props.buttonText.toLowerCase().split(' ').join('-')}-primarySumit-btn`}
        htmlType="submit"
        form={props.formName}
        type="primary"
        disabled={props.disabled}
        hidden={props.hidden}
      >
        {props.buttonText}
      </Button>
    );
  } else {
    const onClick = () => {
      if (props.onCLick) {
        props.onCLick();
      }
      if (props.shouldNavToNextPage && props.setActivePageIndex) {
        props.setActivePageIndex(props.activePageIndex + 1);
      }
    };
    return (
      <Button
        id={`theme-generic-${props.buttonText.toLowerCase().split(' ').join('-')}-primarySumit-btn`}
        onClick={onClick}
        type="primary"
        disabled={props.disabled}
        hidden={props.hidden}
      >
        {props.buttonText}
      </Button>
    );
  }
}
