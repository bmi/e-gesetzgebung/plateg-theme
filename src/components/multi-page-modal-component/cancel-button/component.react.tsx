// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import React from 'react';

export interface CancelButtonComponentProps {
  setIsVisible: (isVisible: boolean) => void;
  buttonText: string;
  formName?: string;
  activePageIndex?: number;
  componentReferenceContext?: string;
}

export function CancelButtonComponent(props: CancelButtonComponentProps): React.ReactElement {
  const onClick = () => {
    props.setIsVisible(false);
  };
  return (
    <Button
      id={`theme-generic-${props.componentReferenceContext || ''}-${props.activePageIndex || 0}-${
        props.buttonText
      }-cancel-btn`}
      onClick={onClick}
      type="default"
    >
      {props.buttonText}
    </Button>
  );
}
