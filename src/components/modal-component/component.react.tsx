// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './modal-component.less';

import React, { useEffect } from 'react';

import { CloseOutlined } from '../icons/CloseOutlined';
import { ModalWrapper } from '../modal-wrapper/component.react';

interface ModalComponentProps {
  isVisible: boolean;
  setIsVisible: (isVisible: boolean) => void;
  title: string | React.ReactNode;
  footerLeft: React.ReactElement[];
  footerRight: React.ReactElement[];
  content: React.ReactElement;
  className?: string;
  rootClassName?: string;
}

export function ModalComponent(props: ModalComponentProps): React.ReactElement {
  useEffect(() => {
    if (!props.isVisible) {
      return;
    }
    const modalContent = document.querySelector(`.ant-modal`);
    if (!modalContent) {
      return;
    }
    const introDivs = modalContent.querySelectorAll('div[tabindex="0"]');
    if (introDivs[0]) {
      introDivs[0].removeAttribute('aria-hidden');
      introDivs[0].textContent = 'Modal Start';
    }
    if (introDivs[introDivs.length - 1]) {
      introDivs[introDivs.length - 1].removeAttribute('aria-hidden');
      introDivs[introDivs.length - 1].textContent = 'Modal Ende';
    }
  }, [props.isVisible]);

  const displayFooter = (items: React.ReactElement[]) =>
    React.Children.map(items, (item) => React.cloneElement(item, { key: crypto.randomUUID() }));

  return (
    <ModalWrapper
      rootClassName={props.rootClassName}
      closeIcon={<CloseOutlined />}
      onCancel={() => props.setIsVisible(false)}
      className={`plateg-modal ${props.className?.length ? props.className : ' '}`}
      open={props.isVisible}
      title={props.title}
      maskClosable={false}
      footer={[
        <div key="footer-left" className="plateg-modal-footer-left">
          {displayFooter(props.footerLeft)}
        </div>,
        <div key="footer-spacer" className="plateg-modal-footer-spacer"></div>,
        <div key="footer-right" className="plateg-modal-footer-right">
          {displayFooter(props.footerRight)}
        </div>,
      ]}
    >
      {props.content}
    </ModalWrapper>
  );
}
