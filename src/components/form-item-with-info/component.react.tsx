// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/restrict-template-expressions */
import { Form, FormItemProps } from 'antd';
import React, { useEffect, useLayoutEffect, useState } from 'react';

interface FormItemWithInfoProps extends FormItemProps {
  customName?: string;
  //use this prop instead of disabling info component directly
  disabled?: boolean;
  name?: string;
  insideModal?: boolean;
}
export function FormItemWithInfo(props: Readonly<FormItemWithInfoProps>): React.ReactElement {
  const [info, setInfo] = useState<HTMLButtonElement>();
  const { customName, insideModal, ...restProps } = props;

  useLayoutEffect(() => {
    if (insideModal) {
      setTimeout(() => {
        moveInfoButton();
      }, 20);
    } else {
      moveInfoButton();
    }
  });

  useEffect(() => {
    setDisabledAttr(props.disabled, info);
  }, [props.disabled]);

  const getLabel = () => {
    const actualName = customName ?? props.name;
    if (actualName) {
      return document.querySelector(`label[for='${actualName.toString().replace(/,/g, '_')}']`);
    }
    return undefined;
  };

  const setDisabledAttr = (disabled?: boolean, infoElem?: HTMLButtonElement) => {
    if (disabled) {
      infoElem?.setAttribute('disabled', 'true');
    } else {
      infoElem?.removeAttribute('disabled');
    }
  };

  const moveInfoButton = () => {
    const label = getLabel();
    const infoElement = label?.getElementsByTagName('button')?.[0];
    const parent = label?.parentElement;
    if (infoElement) {
      parent?.appendChild(infoElement);
      setDisabledAttr(props.disabled, infoElement);
      setInfo(infoElement);
    }
  };

  return <Form.Item {...restProps}></Form.Item>;
}
