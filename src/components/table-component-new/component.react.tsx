// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './table-component.less';

import { Button, GetProp, Table, TableProps } from 'antd';
import { ColumnsType, TablePaginationConfig } from 'antd/lib/table';
import { Key, SorterResult } from 'antd/lib/table/interface';
import React, { ReactElement, useEffect, useLayoutEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Subscription } from 'rxjs';

import { PaginierungDTOSortByEnum } from '@plateg/rest-api';

import { AriaController } from '../../controllers';
import { CommonRow } from '../content-view';
import { DirectionDownOutlined } from '../icons/DirectionDownOutlined';
import { DirectionRightOutlined } from '../icons/DirectionRightOutlined';
import { useAppSelector } from '../store';
import { getAllRows, getExpandableConfig, paginationButtonsRender, SortedInfo } from '../table-component';

type TableScrollType = GetProp<TableProps<CommonRow>, 'scroll'>;
type TableStickyType = GetProp<TableProps<CommonRow>, 'sticky'>;

export interface PagData {
  totalItems: number;
}

export interface PagDataReq {
  currentPage: number;
  columnKey: PaginierungDTOSortByEnum;
  sortOrder: string | null;
}

export interface TableColumn {
  content: string;
  dataIndex?: string;
  key: string;
  sorter?: {};
}

export interface ExpandOptionsProps<T extends CommonRow> {
  isExpandable: boolean;
  expandableCondition?: (record: T) => boolean;
  expandedRowRender?: (record: T) => ReactElement;
}

export interface TableComponentProps<T extends CommonRow> {
  isLight?: boolean;
  columns: ColumnsType<T>;
  content: T[];
  id: string;
  tabKey?: string;
  itemsPerPage?: number;
  bePagination?: boolean;
  sortedInfo?: SortedInfo;
  expandOptions?: ExpandOptionsProps<T>;
  autoHeight?: boolean;
  rowClassName?: (record: T, index: number) => string;
  className?: string;
  customButtonAriaLables?: { closed: string; opened: string };
  onChange?: (
    pagination: TablePaginationConfig,
    filters: Record<string, (boolean | Key)[] | null>,
    SorterResult: SorterResult<T> | SorterResult<T>[],
  ) => any;
  scroll?: TableScrollType;
  sticky?: TableStickyType;
}

export function TableComponent<T extends CommonRow>(props: TableComponentProps<T>): React.ReactElement {
  const { t } = useTranslation();

  const { expandOptions, content, itemsPerPage = 20 } = props;
  const { isExpandable = false, expandableCondition, expandedRowRender } = expandOptions || {};
  const [columns, setColumns] = useState<ColumnsType<T>>();

  const rowClassName = props.rowClassName;
  const appStoreSettings = useAppSelector((state) => state.setting);

  const sortedInfo = props.sortedInfo;
  const [customExpandedRows, setCustomExpandedRows] = useState<string[] | undefined>();
  const [currentExpandedRowsIds, setCurrentExpandedRowsIds] = useState<string[]>([]);

  const updateHeight = () => {
    const table = document.querySelector(`#${props.id} .ant-table-content`) as HTMLElement;
    if (!table) {
      return;
    }
    const height =
      window.innerHeight - (table.getBoundingClientRect().top + document.documentElement.scrollTop) + window.scrollY;
    table.style.maxHeight = `${height}px`;
  };
  const [isBarrierefreiheitGeoeffneteAnmerkungenInTabellen, setIsBarrierefreiheitGeoeffneteAnmerkungenInTabellen] =
    useState<boolean>(false);

  const tablePaginationTab = useAppSelector((state) => state.tablePagination.tabs[props.tabKey as string]);
  const pagInfoResult = props.tabKey ? tablePaginationTab.result : undefined;

  useEffect(() => {
    if (!props.autoHeight) {
      updateHeight();
      window.addEventListener('scroll', updateHeight, true);
    }
    setExpandTableHeadLabel();
    let subscribe: Subscription;

    if (!props.isLight && appStoreSettings.setting?.barrierefreiheitGeoeffneteAnmerkungenInTabellen === true) {
      setIsBarrierefreiheitGeoeffneteAnmerkungenInTabellen(true);
      setCustomExpandedRows(
        getAllRows(content).map((val) => {
          return val.id;
        }),
      );
    }

    return () => {
      subscribe?.unsubscribe();
      if (!props.autoHeight) {
        window.removeEventListener('scroll', updateHeight, true);
      }
    };
  }, [appStoreSettings]);

  useEffect(() => {
    const newColumns = props.columns.slice();
    newColumns.forEach((column) => {
      column.sortOrder = sortedInfo?.getSortOrderOf(column.key?.toString() || '');
      if (column.key === sortedInfo?.columnKey && props.bePagination) {
        column.sorter = true;
      }
    });
    AriaController.removeAriaAttrByClassName('ant-table-column-sorter-up', 'aria-label');
    AriaController.removeAriaAttrByClassName('ant-table-column-sorter-down', 'aria-label');
    setColumns(newColumns);
  }, [props.columns, sortedInfo]);

  useLayoutEffect(() => {
    AriaController.setAriaLabelsByQuery('.ant-table-column-sorter-up.active', t('theme.tableComponent.sorterActiveUp'));
    AriaController.setAriaLabelsByQuery(
      '.ant-table-column-sorter-down.active',
      t('theme.tableComponent.sorterActiveDown'),
    );
    AriaController.setAriaAttrByClassName('ant-table-pagination', 'role', 'list');
    AriaController.setAriaLabelsByClassName('ant-table-pagination', 'Paginierung');
    AriaController.setAriaAttrByClassName('ant-pagination-item', 'role', 'listitem');
  });

  useEffect(() => {
    if (content.length > 0 && (isBarrierefreiheitGeoeffneteAnmerkungenInTabellen || isExpandable)) {
      setCustomExpandedRows(
        getAllRows(content).map((val) => {
          return val.id;
        }),
      );
      setCurrentExpandedRowsIds(
        getAllRows(content)
          .filter((item) => item.children && item.children?.length > 0)
          .map((val) => {
            return val.id;
          }),
      );
    }
  }, [content]);

  const setExpandTableHeadLabel = () => {
    const invisibleLabel = document.createElement('span');
    invisibleLabel.textContent = 'erweiterbare Einträge';
    invisibleLabel.className = 'sr-only';
    document.querySelectorAll('th.ant-table-row-expand-icon-cell')?.forEach((item) => {
      item.appendChild(invisibleLabel);
    });
  };

  const buttonClosed = (onClick: React.MouseEventHandler<HTMLElement>, elementId: string) => (
    <Button
      id={`theme-generic-styleGuide-expnadDetailView-btn-${elementId}`}
      aria-label={props.customButtonAriaLables?.closed ?? 'Detailsicht aufklappen'}
      onClick={onClick}
      type="text"
    >
      <DirectionRightOutlined />
    </Button>
  );

  const buttonOpened = (onClick: React.MouseEventHandler<HTMLElement>, elementId: string) => (
    <Button
      id={`theme-generic-styleGuide-openDetailView-btn-${elementId}`}
      aria-label={
        props.customButtonAriaLables?.opened ??
        'Detailansicht (zuklappbar) wird in der nächsten Tabellenzeile angezeigt'
      }
      onClick={onClick}
      type="text"
    >
      <DirectionDownOutlined />
    </Button>
  );

  const paginationBeProps: TablePaginationConfig = {
    ...(props.bePagination && pagInfoResult?.currentPage !== undefined && { current: pagInfoResult.currentPage + 1 }),
    ...(props.bePagination && pagInfoResult?.totalItems !== undefined && { total: pagInfoResult.totalItems }),
  };

  const expandedConfig = isExpandable
    ? getExpandableConfig(true, buttonOpened, buttonClosed, expandedRowRender, expandableCondition)
    : undefined;

  return (
    <div className={'table-component'}>
      <Table
        id={props.id}
        className={props.className ? props.className : ''}
        expandable={{
          ...expandedConfig,
          expandedRowKeys: customExpandedRows,
          onExpand: (expanded, record) => {
            setCustomExpandedRows(undefined);
            if (expanded) {
              setCurrentExpandedRowsIds([...currentExpandedRowsIds, record.id]);
            } else {
              setCurrentExpandedRowsIds([...currentExpandedRowsIds.filter((id) => id !== record.id)]);
            }
          },
          expandedRowClassName: () => {
            return `expanded-row`;
          },
        }}
        columns={columns}
        dataSource={content}
        pagination={{
          defaultPageSize: itemsPerPage,
          pageSize: itemsPerPage,
          position: ['bottomCenter'],
          hideOnSinglePage: true,
          showSizeChanger: false,
          showTitle: true,
          itemRender: (_, typ, element) => {
            return paginationButtonsRender(typ, element);
          },
          ...paginationBeProps,
        }}
        rowKey={(record) => record.id}
        onChange={props.onChange}
        rowClassName={rowClassName}
        showSorterTooltip={false}
        onRow={(record) => {
          if (currentExpandedRowsIds.includes(record.id)) {
            return { className: 'expanded-parent' };
          }
          return {};
        }}
        scroll={props.scroll}
        sticky={props.sticky}
      />
    </div>
  );
}
