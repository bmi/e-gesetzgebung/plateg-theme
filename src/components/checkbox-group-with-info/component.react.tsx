// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Checkbox } from 'antd';
import { CheckboxGroupProps } from 'antd/lib/checkbox';
import React, { useLayoutEffect } from 'react';

export interface CheckboxItem {
  label: React.ReactElement;
  value: string;
  title: string;
  disabled?: boolean;
}

interface CheckboxGroupWithInfoProps extends CheckboxGroupProps {
  items: CheckboxItem[];
  id: string;
}

export function CheckboxGroupWithInfo(props: CheckboxGroupWithInfoProps): React.ReactElement {
  useLayoutEffect(() => {
    const items = Array.from(document.querySelectorAll(`#${props.id} label`));
    items.forEach((item, id) => {
      item.setAttribute(`aria-label`, props.items[id].title);
    });
  });
  return <Checkbox.Group options={props.items} {...(({ items, ...o }) => o)(props)} />;
}
