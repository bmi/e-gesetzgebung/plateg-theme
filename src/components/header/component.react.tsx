// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './header.less';

import { Layout } from 'antd';
import React, { useEffect, useLayoutEffect, useRef, useState } from 'react';
import { useLocation, useRouteMatch } from 'react-router-dom';

import { HeaderController } from '../../controllers';
import { DropdownMenuItem } from '../table-component/table-sub-components/dropdown-button-component/component.react';

export interface HeaderContent {
  headerLeft?: React.ReactElement[];
  headerCenter?: React.ReactElement[];
  headerRight?: React.ReactElement[];
  headerRightAlternative?: DropdownMenuItem[];
  headerLast?: React.ReactElement[];
  setMenuOffset?: (offset: number) => void;
}
export interface ElementPropsInterface {
  [s: string]: any;
}

export interface HeaderComponentProps {
  ctrl: HeaderController;
  headerId?: string;
}
export function HeaderComponent(props: HeaderComponentProps): React.ReactElement {
  const { Header } = Layout;
  const [headerContentProps, setHeaderContentProps] = useState<HeaderContent>({});
  const [headerRightAlternative, setHeaderRightAlternative] = useState<React.ReactElement[]>([]);
  const [isRightSectionVisible, setIsRightSectionVisible] = useState<boolean>(true);
  const headerRef = useRef<HTMLDivElement>(null);
  const headerRightRef = useRef<HTMLDivElement>(null);
  const headerLeftRef = useRef<HTMLDivElement>(null);
  const headerCenterRef = useRef<HTMLDivElement>(null);
  const headerLastRef = useRef<HTMLDivElement>(null);
  const location = useLocation();
  useEffect(() => {
    setOffsets();
    setTimeout(() => {
      prepareAlternativeMenu();
    }, 10);
    window.addEventListener('resize', setOffsets);
    window.addEventListener('resize', prepareAlternativeMenu);
    window.addEventListener('scroll', setOffsets);
    return () => {
      window.removeEventListener('resize', setOffsets);
      window.removeEventListener('resize', prepareAlternativeMenu);
      window.removeEventListener('scroll', setOffsets);
    };
  }, [headerContentProps]);

  useEffect(() => {
    setTimeout(() => {
      setOffsets();
    }, 100);
  }, [location]);

  useEffect(() => {
    if (headerContentProps.headerRight) {
      setHeaderRightAlternative(
        headerContentProps.headerRight?.filter(
          (elem) => (elem.props as ElementPropsInterface).elementId === 'headerRightAlternative',
        ),
      );
    } else {
      setHeaderRightAlternative([]);
    }
  }, [headerContentProps.headerRight]);

  useEffect(() => {
    props.ctrl.setHeaderContentState(setHeaderContentProps);
  }, []);

  const prepareAlternativeMenu = () => {
    if (headerRef.current && headerLeftRef.current) {
      setIsRightSectionVisible(true);
      const headerWidth = Math.round(headerRef.current.getBoundingClientRect().width);

      const headerLeftWidth = headerLeftRef.current?.getBoundingClientRect().width || 0;
      const headerRightWidth = headerRightRef.current?.getBoundingClientRect().width || 0;
      const headerLastWidth = headerLastRef.current?.getBoundingClientRect().width || 0;
      const rightSectionsWidth = headerRightWidth + headerLastWidth;
      if (Math.round(headerLeftWidth + rightSectionsWidth) > headerWidth) {
        // If width all items is bigger than header width - hide section with buttons and show dropdown menu.
        setIsRightSectionVisible(false);
      }
    }
  };

  const setHeaderLeftVertical = (height: number) => {
    const header = headerLeftRef.current;
    const olList = header?.querySelector('ol');
    if (olList) {
      header?.querySelectorAll('.ant-breadcrumb-separator').forEach((item) => (item.textContent = ''));
      olList.style.flexDirection = 'row';
      if (height > 25) {
        olList.style.flexDirection = 'column';
      }
    }
  };
  useEffect(() => {
    const header = headerLeftRef.current;
    let resizeTimer: NodeJS.Timeout | null = null;
    const handleResize = (entries: ResizeObserverEntry[]) => {
      for (const entry of entries) {
        const { height } = entry.contentRect;
        setHeaderLeftVertical(height);
      }
    };
    const debouncedResizeHandler = (entries: ResizeObserverEntry[]) => {
      if (resizeTimer) {
        clearTimeout(resizeTimer);
      }
      resizeTimer = setTimeout(() => handleResize(entries), 200);
    };
    const resizeObserver = new ResizeObserver(debouncedResizeHandler);
    if (header) {
      resizeObserver.observe(header);
    }
    return () => {
      resizeObserver.disconnect();
    };
  }, []);

  const setOffsets = () => {
    if (headerRef.current && headerContentProps.setMenuOffset) {
      const headerHeight = headerRef.current.offsetHeight;

      const headerOffset =
        headerRef.current.getBoundingClientRect().top > 0 ? headerRef.current.getBoundingClientRect().top : 0;

      setMenuOffset(headerHeight, headerOffset);
    }
  };

  const setMenuOffset = (headerHeight: number, headerOffset: number) => {
    if (headerContentProps.setMenuOffset) {
      headerContentProps.setMenuOffset(headerHeight + headerOffset);
    }
  };

  const routeMatcherVorbereitung = useRouteMatch<{ pageName: string }>('/*');
  const [wrapperWidth, setWrapperWidth] = useState<number | '100%'>('100%');
  const [holderWidth, setHolderWidth] = useState<number | '100%'>('100%');

  useLayoutEffect(() => {
    const updateHeaderWidth = () => {
      setWrapperWidth(document.body.scrollWidth - 1);
      setHolderWidth(document.body.clientWidth - 1);
    };
    window.addEventListener('resize', updateHeaderWidth);
    window.addEventListener('scroll', updateHeaderWidth);
    return () => {
      window.removeEventListener('resize', updateHeaderWidth);
      window.removeEventListener('scroll', updateHeaderWidth);
    };
  }, []);

  useLayoutEffect(() => {
    const timeout = setTimeout(() => {
      setWrapperWidth('100%');
      setHolderWidth('100%');
    }, 100);
    return () => {
      if (timeout) {
        clearTimeout(timeout);
      }
    };
  }, [routeMatcherVorbereitung?.url]);

  return (
    <Header
      className="header-component site-layout-background"
      id={props.headerId || ''}
      style={{ width: wrapperWidth }}
    >
      <div className="header-wrapper" ref={headerRef} style={{ position: 'sticky', left: '0', width: holderWidth }}>
        <div className="header-left" ref={headerLeftRef}>
          {headerContentProps.headerLeft}
        </div>
        <div className="header-center" ref={headerCenterRef}>
          <div className="holder">{headerContentProps.headerCenter}</div>
        </div>
        <div className="header-right" ref={headerRightRef}>
          {isRightSectionVisible &&
            headerContentProps.headerRight?.filter(
              (elem) => (elem.props as ElementPropsInterface).elementId !== 'headerRightAlternative',
            )}
        </div>

        <div className="header-last" ref={headerLastRef}>
          {!isRightSectionVisible && headerRightAlternative}
          {headerContentProps.headerLast}
        </div>
      </div>
    </Header>
  );
}
