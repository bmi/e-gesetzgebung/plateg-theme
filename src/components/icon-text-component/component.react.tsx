// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import './icon-text.less';

export interface IconTextComponentProps {
  text: string;
  icon: React.ReactElement;
}

export function IconTextComponent(props: IconTextComponentProps) {
  return (
    <span className="icon-text">
      {props.icon}
      {props.text}
    </span>
  );
}
