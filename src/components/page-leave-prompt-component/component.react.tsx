// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Location } from 'history';
import React from 'react';
import { useHistory } from 'react-router';

import { RouteLeavingGuard } from '../routeLeavingGuard/component.react';

interface PageLeavePromptProps {
  projectName: string;
  saveDraft: () => void;
  shouldSave: () => boolean;
  shouldStay?: () => boolean;
}

export function PageLeavePrompt(props: PageLeavePromptProps): React.ReactElement {
  const regexPattern = new RegExp(`/${props.projectName.toLowerCase()}/`);
  const history = useHistory();

  const shouldBlock = (location: Location) => {
    if (props.shouldStay && props.shouldStay()) {
      document.getElementById('errorBox')?.focus();
      window.scrollTo(0, 0);
      return 'Achtung: Es liegen Validierungsfehler vor. Ungespeicherte Änderungen gehen verloren.';
    } else if (props.shouldSave()) {
      if (regexPattern.test(location.pathname)) {
        props.saveDraft();
        return false;
      } else {
        return `Ihre Änderungen wurden nicht gespeichert. Möchten Sie die Änderungen verwerfen?`;
      }
    } else {
      return false;
    }
  };

  return (
    <RouteLeavingGuard
      navigate={(path: string) => history.push(path)}
      shouldBlockNavigation={shouldBlock}
      btnOk="Änderungen verwerfen"
      btnCancel="Abbrechen"
    />
  );
}
