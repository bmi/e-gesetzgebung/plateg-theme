// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './card-grid.less';

import { List } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import React, { useEffect, useState } from 'react';
import { CommonRow } from '../content-view';
import { useAppSelector } from '../store';
import { SortedInfo } from '../table-component';

interface CardGridProps<T extends CommonRow> {
  id: string;
  tabKey?: string;
  cardComponent: (item: T) => React.ReactElement;
  content: T[];
  columns: ColumnsType<T>;
  itemsPerPage?: number;
  sortedInfo?: SortedInfo;
  bePagination?: boolean;
  onPageChange?: (page: number) => void;
}

export function CardGridComponent<T extends CommonRow>(props: Readonly<CardGridProps<T>>): React.ReactElement {
  const itemsPerPage = props.itemsPerPage || 9;

  const [content, setContent] = useState<T[]>(props.content);
  const tablePaginationTab = useAppSelector((state) => state.tablePagination.tabs[props.tabKey as string]);
  const pagInfoResult = props.tabKey ? tablePaginationTab.result : undefined;

  useEffect(() => {
    setContent(props.content);
  }, [props.content]);

  useEffect(() => {
    let sortedContent = [...props.content];
    if (props.sortedInfo?.columnKey) {
      const { columnKey, order } = props.sortedInfo;
      sortedContent = sortedContent.sort((a, b) => {
        const key = columnKey as keyof T;
        const comparison = a[key] > b[key] ? 1 : -1;
        return order === 'ascend' ? comparison : -comparison;
      });
    }
    setContent(sortedContent);
  }, [props.sortedInfo]);

  const paginationBeProps = {
    ...(props.bePagination && pagInfoResult?.currentPage !== undefined && { current: pagInfoResult.currentPage + 1 }),
    ...(props.bePagination && pagInfoResult?.totalItems !== undefined && { total: pagInfoResult.totalItems }),
  };

  return (
    <List
      className="ggp-tile-list"
      grid={{ gutter: [40, 22], column: 4, xs: 1, sm: 1, md: 2, lg: 2, xl: 2, xxl: 3 }}
      dataSource={content}
      pagination={{
        position: 'bottom',
        align: 'center',
        defaultPageSize: itemsPerPage,
        pageSize: itemsPerPage,
        hideOnSinglePage: true,
        onChange: (page) => {
          if (props.onPageChange) {
            props.onPageChange(page);
          }
        },
        ...paginationBeProps,
      }}
      renderItem={(item, index) => (
        <List.Item key={index} className="ggp-tile-list-item">
          {props.cardComponent(item)}
        </List.Item>
      )}
    />
  );
}
