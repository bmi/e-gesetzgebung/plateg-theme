// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './hidden-info.less';

import Title from 'antd/lib/typography/Title';
import React, { useEffect, useRef, useState } from 'react';
import { createRoot } from 'react-dom/client';

import { InfoCircleFilled } from '../icons/InfoCircleFilled';
import { InfoCircleOutlined } from '../icons/InfoCircleOutlined';
import { TooltipComponent } from '..';

export interface HiddenInfoComponentProps {
  title: string;
  text: string | React.ReactElement;
  disabled?: boolean;
  customSelector?: string;
}

export function HiddenInfoComponent(props: HiddenInfoComponentProps): React.ReactElement {
  const [isVisible, setIsVisible] = useState(false);
  const activeClassName = isVisible ? 'active' : '';
  const triggerRef = useRef<HTMLButtonElement>(null);
  const div = document.createElement('div');
  const containerRoot = createRoot(div);
  const [container] = useState<HTMLDivElement>(div);

  const infoContent: React.ReactElement<any>[] = [
    <div aria-label="Informationen" className="hidden-info-content" key="Informationen">
      <Title level={3}>
        <InfoCircleOutlined />
        {props.title}
      </Title>
      {props.text}
    </div>,
  ];

  const setContent = () => {
    if (container) {
      container.style.marginTop = isVisible ? '20px' : 'auto';
      container.style.display = isVisible ? 'block' : 'none';
      containerRoot.render(infoContent);
    }
  };

  useEffect(() => {
    const selectedParent = triggerRef.current?.closest(props.customSelector ?? '.ant-form-item');
    selectedParent?.insertBefore(div, selectedParent?.lastChild?.nextSibling || null);
  }, []);

  useEffect(() => {
    setContent();
  }, [isVisible]);

  return (
    <TooltipComponent title={props.title} className="hidden-info-tooltip" placement="right">
      <button
        ref={triggerRef}
        id={`theme-generic-${props.title.toLowerCase().split(' ').join('-')}-toggleShowInformation-btn`}
        aria-expanded={isVisible}
        aria-label={isVisible ? 'Informationen sind eingeblendet' : 'Informationen einblenden'}
        type="button"
        className={`info-icon ${activeClassName}`}
        onClick={() => setIsVisible(!isVisible)}
        disabled={props.disabled}
      >
        <InfoCircleFilled />
        <InfoCircleOutlined />
      </button>
    </TooltipComponent>
  );
}
