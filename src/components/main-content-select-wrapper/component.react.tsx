// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { SelectProps } from 'antd';
import React from 'react';

import { SelectWrapper } from '../select-wrapper/component.react';

export function MainContentSelectWrapper(props: SelectProps): React.ReactElement {
  const popupContainer = document.getElementsByClassName('main-content-area')[0];
  const getPopupContainerProp = popupContainer ? { getPopupContainer: () => popupContainer as HTMLElement } : undefined;
  return <SelectWrapper {...props} {...getPopupContainerProp}></SelectWrapper>;
}
