// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import Title from 'antd/lib/typography/Title';
import React from 'react';

interface CombinedTitleProps {
  title?: string;
  suffix?: string;
  level?: number;
  additionalContent?: React.ReactElement;
}

export function CombinedTitle(props: CombinedTitleProps): React.ReactElement {
  const titleString = (() => {
    if (props.title && props.suffix) {
      return props.title + (props.suffix?.length ? `: ${props.suffix}` : '');
    } else if (props.title || props.suffix) {
      return props.title ?? props.suffix;
    }
  })();
  return (
    <Title level={props.level ?? 1}>
      {titleString}
      {props.additionalContent}
    </Title>
  );
}
