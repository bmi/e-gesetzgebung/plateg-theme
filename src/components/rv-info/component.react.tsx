// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './rv-info.less';

import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import {
  CodelistenResponseDTO,
  RegelungsvorhabenEntityWithDetailsResponseDTO,
  VorhabenStatusType,
} from '@plateg/rest-api';

import { CodeListenController } from '../../controllers/CodeListenController';
import { GlobalDI } from '../../shares';

export interface RVInfoComponentProps {
  regelungsvorhaben?: RegelungsvorhabenEntityWithDetailsResponseDTO;
  id?: string;
  codeList?: CodelistenResponseDTO;
  isCreator?: boolean;
}

export function RVInfoComponent(props: RVInfoComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const ctrlCheckRv = GlobalDI.getOrRegister('CodeListenController', () => new CodeListenController());
  const noEntry = 'Keine Angabe';
  const missingInfo = 'Wird später eingetragen';
  let tab;
  switch (props.regelungsvorhaben?.dto.status) {
    case VorhabenStatusType.Entwurf:
      tab = 'entwurfe';
      break;
    case VorhabenStatusType.Archiviert:
      tab = 'archiv';
      break;
    case VorhabenStatusType.InBearbeitung:
      tab = 'meineRegelungsvorhaben';
      break;
  }

  if (!props.regelungsvorhaben || !tab) {
    return <></>;
  }

  const einleiter = (() => {
    if (!props.regelungsvorhaben?.dto.erstelltVon?.dto.name) {
      return undefined;
    }
    let einleiterString = props.regelungsvorhaben.dto.erstelltVon.dto.name;
    einleiterString = einleiterString.concat(
      ` (${props.regelungsvorhaben.dto.erstelltVon.dto.ressort?.bezeichnung ?? noEntry}`,
    );
    einleiterString = einleiterString.concat(
      `, ${props.regelungsvorhaben.dto.erstelltVon.dto.fachreferat ?? noEntry})`,
    );
    return einleiterString;
  })();

  const ressortRv = props.regelungsvorhaben.dto.technischesFfRessort?.bezeichnung
    ? props.regelungsvorhaben.dto.technischesFfRessort?.bezeichnung
    : ctrlCheckRv.getRessortName(props.codeList, props.regelungsvorhaben?.dto?.technischesFfRessort);
  const abteilungRv = props.regelungsvorhaben.dto.technischeFfAbteilung?.bezeichnung
    ? props.regelungsvorhaben.dto.technischeFfAbteilung?.bezeichnung
    : ctrlCheckRv.getAbteilungName(props.codeList, props.regelungsvorhaben?.dto?.technischeFfAbteilung);
  const referatRv = props.regelungsvorhaben.dto.technischesFfReferat?.bezeichnung
    ? props.regelungsvorhaben.dto.technischesFfReferat?.bezeichnung
    : ctrlCheckRv.getReferatName(props.codeList, props.regelungsvorhaben?.dto?.technischesFfReferat);

  return (
    <div key="rv-info-drawer" className="rv-info-drawer">
      <dl>
        <dt>{t('theme.rvInfoComponent.regelungsvorhaben')}</dt>
        <dd>{props.regelungsvorhaben?.dto.langtitel || noEntry}</dd>
        <dt>{t('theme.rvInfoComponent.kurzbezeichnung')}</dt>
        <dd>{props.regelungsvorhaben?.dto.kurzbezeichnung || noEntry}</dd>
        <dt>{t('theme.rvInfoComponent.abkuerzung')}</dt>
        <dd>{props.regelungsvorhaben?.dto.abkuerzung || noEntry}</dd>
        <dt>{t('theme.rvInfoComponent.kurzbeschreibung')}</dt>
        <dd>{props.regelungsvorhaben?.dto.kurzbeschreibung || noEntry}</dd>
        <dt>{t('theme.rvInfoComponent.vorhabentyp')}</dt>
        <dd>{t(`theme.rvInfoComponent.vorhabenart.${props.regelungsvorhaben?.dto.vorhabenart}`) || noEntry}</dd>
        <dt>{t('theme.rvInfoComponent.ffRessort')}</dt>
        <dd>{ressortRv || noEntry}</dd>
        <dt>{t('theme.rvInfoComponent.ffAbteilung')}</dt>
        <dd>{abteilungRv || missingInfo}</dd>
        <dt>{t('theme.rvInfoComponent.ffReferat')}</dt>
        <dd>{referatRv || missingInfo}</dd>
        <dt>{t('theme.rvInfoComponent.einleiter')}</dt>
        <dd>{einleiter || noEntry}</dd>
        {props.isCreator && (
          <Link
            className="rv-link"
            id={`${props.id ? props.id + '-' : ''}regelungsvorhabenDatenblatt-link`}
            target="_blank"
            to={`/regelungsvorhaben/${tab}/${props.regelungsvorhaben.base.id}/datenblatt`}
          >
            {t('theme.rvInfoComponent.link')}
          </Link>
        )}
      </dl>
    </div>
  );
}
