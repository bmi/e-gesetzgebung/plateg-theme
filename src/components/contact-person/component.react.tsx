// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './contact-person.less';

import React from 'react';
import { useTranslation } from 'react-i18next';

import { UserEntityDTO } from '@plateg/rest-api';

interface ContactPersonInterface extends UserEntityDTO {
  additionalInfo?: string;
  adresse?: string;
}
export function ContactPerson(props: { user: ContactPersonInterface }): React.ReactElement {
  const { t } = useTranslation();
  const user = props.user;
  const noEntry = <i>{t('theme.contactPerson.keineAngabe')}</i>;
  return (
    <div className="contactPerson">
      <dl>
        <dt>{t('theme.contactPerson.name')}</dt>
        <dd>
          <b>{user?.name || noEntry}</b>
        </dd>
        <dt>{t('theme.contactPerson.anrede')}</dt>
        <dd>{user?.anrede || noEntry}</dd>
        <dt className="sectionEnd">{t('theme.contactPerson.titel')}</dt>
        <dd>{user?.titel || noEntry}</dd>
        <dt>{t('theme.contactPerson.eMail')}</dt>
        <dd>
          {user?.email ? (
            <a id="theme-generic-contactPersonSendEmail-link" href={`mailto:${user?.email}`}>
              {user?.email}
            </a>
          ) : (
            noEntry
          )}
        </dd>
        <dt className="sectionEnd">{t('theme.contactPerson.telefon')}</dt>
        <dd>{user?.telefon || noEntry}</dd>
        <dt>{t('theme.contactPerson.ressort')}</dt>
        <dd>{user?.ressort?.kurzbezeichnung || noEntry}</dd>
        <dt>{t('theme.contactPerson.abteilung')}</dt>
        <dd>{user?.abteilung || noEntry}</dd>
        <dt>{t('theme.contactPerson.referat')}</dt>
        <dd>{user?.fachreferat || noEntry}</dd>
        {user?.adresse && (
          <>
            <dt>{t('theme.contactPerson.adresse')}</dt>
            <dd>{user?.adresse}</dd>
          </>
        )}
      </dl>

      {user?.additionalInfo ? (
        <div
          dangerouslySetInnerHTML={{
            __html: user?.additionalInfo,
          }}
        ></div>
      ) : null}
    </div>
  );
}
