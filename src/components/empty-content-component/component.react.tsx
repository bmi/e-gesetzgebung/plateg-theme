// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './empty-content.less';

import { Image } from 'antd';
import Title from 'antd/lib/typography/Title';
import React, { Fragment } from 'react';
import { useTranslation } from 'react-i18next';

interface EmptyContentComponentProps {
  images: ImageItem[];
  children: React.ReactElement[] | React.ReactElement;
  noIndex?: boolean;
}
interface ImageItem {
  label: string;
  imgSrc: ImageModule;
  height?: number;
  imgAlt?: string;
}
export interface ImageModule {
  default: string;
}
export function EmptyContentComponent({ images, children }: EmptyContentComponentProps): React.ReactElement {
  const { t } = useTranslation();
  return (
    <Fragment>
      <Title className="empty-content-title" level={2}>
        {t('theme.emptyContent.title')}
      </Title>
      <ol className="empty-content-holder">
        {images.map((item, index) => {
          const { imgSrc, label, height, imgAlt } = item;
          return (
            <li key={index} className="info-image-item">
              <div className="image-container">
                <Image height={height || 170} preview={false} src={imgSrc.default} alt={imgAlt || ''} loading="lazy" />
              </div>
              <span className="image-text">{label}</span>
            </li>
          );
        })}
      </ol>
      <div className="empty-content-devider-line"></div>
      {children}
    </Fragment>
  );
}
