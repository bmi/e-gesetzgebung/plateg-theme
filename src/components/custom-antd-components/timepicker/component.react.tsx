// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Omit } from 'antd/es/_util/type';
import { PickerTimeProps } from 'antd/es/date-picker/generatePicker';
import * as React from 'react';

import DatePicker from '../datepicker/component.react';

const TimePicker = React.forwardRef<any, Omit<PickerTimeProps<Date>, 'picker'>>((props, ref) => {
  return <DatePicker id="theme-generic-timepicker-picker" {...props} picker="time" mode={undefined} ref={ref} />;
});

TimePicker.displayName = 'TimePicker';

export default TimePicker;
