// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './custom-datepicker.less';

import { Button, Form, FormInstance, Input, InputRef, Popover } from 'antd';
import { Rule } from 'antd/lib/form';
import { addMonths, addYears, format, isValid, parse } from 'date-fns';
import { de } from 'date-fns/locale';
import { FieldData } from 'rc-field-form/lib/interface';
import React, { RefObject, useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { Calendar, CalendarOutlined, FormItemWithInfo, LeftOutlined, RightOutlined } from '../..';
import { AriaController } from '../../../controllers/AriaController';
import { DoubleLeftOutlined } from '../../icons/DoubleLeftOutlined';
import { DoubleRightOutlined } from '../../icons/DoubleRightOutlined';

import type { CellRenderInfo } from 'antd/node_modules/rc-picker/lib/interface';
interface CustomDatepickerProps {
  aliasDate: string;
  aliasTime: string;
  required: boolean;
  form: FormInstance;
  disabledDate: (date: Date) => boolean;
  additionalValidators: { validator: () => void; message: string }[] | Rule[];
  errorRequired: string;
  errorDateTime: string;
  errorInvalidFormat: string;
  errorDisabledDate: string;
  setDate: (date: Date | undefined) => void;
  timePlaceholderName: string;
  customLabel?: string | React.ReactElement;
  onFormChange?: () => void;
  customCellRender?: (date: Date) => React.ReactElement;
  getPopupContainer?: () => HTMLElement;
  textPlaceholder?: string;
  minDate?: Date;
  maxDate?: Date;
}

export function CustomDatepicker(props: CustomDatepickerProps): React.ReactElement {
  const DATE_REGEX = /^\d\d.\d\d.\d\d\d\d$/;
  const [isVisible, setIsVisible] = useState<boolean>(false);
  const inputRef = useRef<InputRef>(null);
  const btnPrevYear = useRef<HTMLButtonElement>(null);
  const btnPrevMonth = useRef<HTMLButtonElement>(null);
  const btnCloseCalendar = useRef<HTMLButtonElement>(null);
  const calendarHolderRef = useRef<HTMLDivElement>(null);

  const { t } = useTranslation();
  useEffect(() => {
    if (props.form.getFieldValue(props.aliasDate)) {
      const dateValue = props.form.getFieldValue(props.aliasDate) as Date;
      setTimeout(() => {
        props.form.setFields([
          { name: `${props.aliasDate}-calendar-popup`, value: dateValue },
          { name: `${props.aliasDate}-date-placeholder`, value: format(dateValue, 'dd.MM.yyyy') },
        ]);
      }, 100);
    }
  }, [props.form.getFieldValue(props.aliasDate)]);

  const handleClickOutside = (event: MouseEvent) => {
    if (calendarHolderRef.current && !calendarHolderRef.current.contains(event.target as Node)) {
      setIsVisible(false);
    }
  };

  useEffect(() => {
    document.addEventListener('click', handleClickOutside, true);
    return () => {
      document.removeEventListener('click', handleClickOutside, true);
    };
  }, []);

  const onKeyDownHandler = (
    e: React.KeyboardEvent<HTMLElement>,
    btnRef?: RefObject<HTMLButtonElement>,
    btnRefPrev?: RefObject<HTMLButtonElement>,
  ) => {
    if (e.code === 'Tab' && !e.shiftKey && btnRef) {
      e.preventDefault();
      btnRef.current?.focus();
    }
    if (e.code === 'Tab' && e.shiftKey && btnRefPrev) {
      e.preventDefault();
      btnRefPrev.current?.focus();
    }

    if (e.code === 'Escape') {
      e.preventDefault();
      setIsVisible(false);
      inputRef.current?.focus();
    }
    if (e.key === 'Escape' && isVisible) {
      e.stopPropagation();
    }
  };

  const calendarPopup = (
    <div style={{ width: '400px', margin: '0 auto' }} ref={calendarHolderRef} onKeyDown={(e) => onKeyDownHandler(e)}>
      <Form.Item name={`${props.aliasDate}-calendar-popup`} dependencies={[props.aliasDate]}>
        <div>
          <Calendar
            disabledDate={props.disabledDate}
            fullscreen={false}
            headerRender={({ value, onChange }) => {
              const currentValue = value || new Date();
              return (
                <div onClick={(event) => event.stopPropagation()} className="custom-calendar-header">
                  <div className="left-buttons">
                    <Button
                      type="text"
                      onClick={() => onChange(addYears(currentValue, -1))}
                      ref={btnPrevYear}
                      aria-label={t(`theme.datepicker.prevYear`)}
                      onKeyDown={(e) => onKeyDownHandler(e, btnPrevMonth, btnCloseCalendar)}
                    >
                      <DoubleLeftOutlined />
                    </Button>
                    <Button
                      type="text"
                      onClick={() => onChange(addMonths(currentValue, -1))}
                      className="btn-month"
                      ref={btnPrevMonth}
                      aria-label={t(`theme.datepicker.prevMonth`)}
                    >
                      <LeftOutlined />
                    </Button>
                  </div>
                  <div className="right-buttons">
                    <Button
                      type="text"
                      onClick={() => onChange(addMonths(currentValue, 1))}
                      className="btn-month"
                      aria-label={t(`theme.datepicker.nextMonth`)}
                    >
                      <RightOutlined />
                    </Button>
                    <Button
                      type="text"
                      onClick={() => onChange(addYears(currentValue, 1))}
                      aria-label={t(`theme.datepicker.nextYear`)}
                    >
                      <DoubleRightOutlined />
                    </Button>
                  </div>
                  <div className="date-span-div">
                    <span className="date-span">{format(currentValue, 'MMM yyyy', { locale: de })}</span>
                    <span className="selected-date" aria-live="polite">
                      {format(currentValue || new Date(), 'dd.MM.yyyy')}
                    </span>
                  </div>
                </div>
              );
            }}
            onSelect={(value) => {
              const formattedDate = format(value, 'dd.MM.yyyy');
              if (!props.disabledDate?.(value)) {
                props.form.setFields([
                  { name: props.aliasDate, value: value },
                  { name: `${props.aliasDate}-date-placeholder`, value: formattedDate },
                ]);
                props.setDate(value);
                props.onFormChange?.();
              }
            }}
            fullCellRender={(date: Date, info: CellRenderInfo<Date>) => {
              if (props.customCellRender) {
                return props.customCellRender(date);
              } else {
                return info.originNode;
              }
            }}
            value={props.form.getFieldValue(props.aliasDate) as Date}
            validRange={[props.minDate || new Date(1000, 0, 1), props.maxDate || new Date(3000, 11, 31)]} // wide range of default values to make sure that all dates are enabled by default
          />
          <div className="calendar-close">
            <Button
              type="link"
              size="large"
              className="blue-text-button"
              onClick={() => {
                setIsVisible(false);
                inputRef.current?.focus();
              }}
              onKeyDown={(e) => {
                onKeyDownHandler(e, btnPrevYear);
              }}
              aria-label="Close calendar"
              ref={btnCloseCalendar}
            >
              {t(`theme.datepicker.close`)}
            </Button>
          </div>
        </div>
      </Form.Item>
    </div>
  );
  function isValidDate(dateString: string): boolean {
    if (!DATE_REGEX.test(dateString)) {
      return false;
    }
    const [day, month, year] = dateString.split('.').map(Number);
    const isLeapYear = (year: number) => (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
    const daysInMonth = [
      31, // January
      isLeapYear(year) ? 29 : 28, // February (leap year or not)
      31, // March
      30, // April
      31, // May
      30, // June
      31, // July
      31, // August
      30, // September
      31, // October
      30, // November
      31, // December
    ];
    // Check if the day is valid for the given month
    if (day < 1 || day > daysInMonth[month - 1]) {
      return false;
    }
    // Check if the month is valid
    if (month < 1 || month > 12) {
      return false;
    }
    return true;
  }
  const setValue = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.value) {
      const vals: FieldData[] = [];
      vals.push({ name: props.aliasDate + '-date-placeholder', value: e.target.value });
      const parts = e.target.value.match(/(\d+)/g);
      if (DATE_REGEX.test(e.target.value) && parts?.length === 3 && isValidDate(e.target.value)) {
        const selectedDate = new Date(Number(parts[2]), Number(parts[1]) - 1, Number(parts[0]), 1);
        vals.push({ name: props.aliasDate, value: selectedDate });
        vals.push({ name: props.aliasDate + '-calendar-popup', value: selectedDate });
        props.setDate(selectedDate);
      }
      props.form.setFields(vals);
    } else {
      props.form.resetFields([
        props.aliasDate,
        props.aliasDate + '-calendar-popup',
        props.aliasDate + '-date-placeholder',
      ]);
      props.form.setFields([{ name: `${props.aliasDate}-calendar-popup`, value: new Date() }]);
      props.setDate(undefined);
    }
  };

  const calendarHolderKeyDownHandler = (event: KeyboardEvent) => {
    if (event.key === 'Enter') {
      event.preventDefault();
      setIsVisible(false);
      inputRef.current?.focus();
    }
  };
  const calendarHolderClickHandler = () => {
    setIsVisible(false);
    inputRef.current?.focus();
  };
  return (
    <div>
      <Popover
        open={isVisible}
        content={calendarPopup}
        id={`Popover-id-${props.aliasDate}`}
        afterOpenChange={(isOpened: boolean) => {
          AriaController.setAriaAttrByClassName('ant-picker-panel', 'label', t(`theme.datepicker.navHint`));
          AriaController.setAriaAttrByClassName('ant-picker-content', 'hidden', 'true');
          const panelElement = calendarHolderRef.current?.querySelector('.ant-picker-panel');
          if (isOpened) {
            panelElement?.setAttribute('role', 'presentation');
            panelElement?.addEventListener('keydown', (e) => calendarHolderKeyDownHandler(e as KeyboardEvent));
            calendarHolderRef.current
              ?.querySelector('.ant-picker-panel')
              ?.addEventListener('click', calendarHolderClickHandler);
          } else {
            panelElement?.removeEventListener('keydown', (e) => calendarHolderKeyDownHandler(e as KeyboardEvent));
            panelElement?.removeEventListener('click', calendarHolderClickHandler);
          }
        }}
        getPopupContainer={props.getPopupContainer}
      >
        <div>
          <FormItemWithInfo
            htmlFor={`date-input-${props.aliasDate}`}
            name={`${props.aliasDate}-date-placeholder`}
            dependencies={[props.aliasTime]}
            label={props.customLabel || 'Datum (tt.mm.jjjj)'}
            rules={[
              {
                required: props.required && !!props.errorRequired.length,
                message: props.errorRequired,
              },
              {
                // Überprüft, dass auch ein Datum eingegeben wird, wenn eine Uhrzeit eingegeben wurde
                validator: () => {
                  if (
                    !props.required &&
                    props.form.getFieldValue(props.timePlaceholderName) !== undefined &&
                    props.form.getFieldValue(props.timePlaceholderName) !== '' &&
                    (props.form.getFieldValue(`${props.aliasDate}-date-placeholder`) === '' ||
                      props.form.getFieldValue(`${props.aliasDate}-date-placeholder`) === undefined)
                  ) {
                    return Promise.reject(props.errorDateTime);
                  } else {
                    return Promise.resolve();
                  }
                },
              },
              {
                validator: (_rule, value: string) => {
                  if (value && (!DATE_REGEX.test(value) || !isValid(parse(value, 'dd.MM.yyyy', new Date())))) {
                    return Promise.reject(props.errorInvalidFormat);
                  } else {
                    return Promise.resolve();
                  }
                },
                validateTrigger: 'submit',
              },
              {
                validator: (_rule, value: string) => {
                  if (
                    value &&
                    DATE_REGEX.test(value) &&
                    props.disabledDate?.(new Date(parse(value, 'dd.MM.yyyy', new Date())))
                  ) {
                    return Promise.reject(props.errorDisabledDate);
                  } else {
                    return Promise.resolve();
                  }
                },
                validateTrigger: 'submit',
              },
              ...props.additionalValidators,
            ]}
          >
            <Input
              ref={inputRef}
              type={'text'}
              autoComplete="off"
              id={`date-input-${props.aliasDate}`}
              placeholder={
                props.textPlaceholder === undefined ? t(`theme.datepicker.placeholder`) : props.textPlaceholder
              }
              onChange={setValue}
              suffix={
                <div
                  onClick={() => {
                    setIsVisible(!isVisible);
                  }}
                >
                  <CalendarOutlined label={typeof props.customLabel === 'string' ? props.customLabel : undefined} />
                </div>
              }
              onClick={() => setIsVisible(!isVisible)}
              onKeyDown={(e) => {
                if (e.code === 'Enter') {
                  e.preventDefault();
                  setIsVisible(!isVisible);
                }
                if (e.code === 'Tab' && isVisible) {
                  e.preventDefault();
                  (calendarHolderRef.current?.querySelector('.ant-picker-panel') as HTMLDivElement)?.focus();
                }
                if (e.key === 'Escape' && isVisible) {
                  e.stopPropagation();
                  setIsVisible(!isVisible);
                }
              }}
            />
          </FormItemWithInfo>
        </div>
      </Popover>
    </div>
  );
}
