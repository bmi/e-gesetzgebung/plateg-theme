// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function RightOutlined(): React.ReactElement {
  return (
    <svg
      aria-hidden="true"
      data-name="Ebene 1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 6.5 11.5"
      fill="currentColor"
      className="anticon"
      height="1em"
      width="1em"
    >
      <path d="M1.28.22A.75.75,0,0,0,.22,1.28L4.69,5.75.22,10.22a.75.75,0,0,0,0,1.06.75.75,0,0,0,1.06,0l5-5a.75.75,0,0,0,0-1.06Z" />
    </svg>
  );
}
