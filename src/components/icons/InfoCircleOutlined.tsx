// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function InfoCircleOutlined(): React.ReactElement {
  return (
    <svg
      aria-hidden="true"
      data-name="Ebene 1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 20 20"
      fill="currentColor"
      className="anticon"
      height="1.9rem"
      width="1.9rem"
    >
      <g data-name="Gruppe 3066">
        <path d="M9.5,19A9.5,9.5,0,1,1,19,9.5,9.51,9.51,0,0,1,9.5,19Zm0-18A8.5,8.5,0,1,0,18,9.5,8.51,8.51,0,0,0,9.5,1Z" />
        <path
          data-name="Pfad 5099"
          d={
            'M10.69,9.7v3.58A1.19,1.19,0,0,1,9.53,14.5h0a1.19,1.19,0,0,1-1.19-1.21V9.7a1.19,1.19,0,1,1,2.38,0ZM9.5,7A1.39,' +
            '1.39,0,0,1,8.11,5.65,1.4,1.4,0,0,1,9.5,4.25a1.4,1.4,0,0,1,0,2.8h0Z'
          }
        />
      </g>
    </svg>
  );
}
