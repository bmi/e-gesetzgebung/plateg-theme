// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function CheckOutlinedGreen(): React.ReactElement {
  return (
    <svg
      aria-hidden="true"
      data-name="Ebene 1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 19 19"
      fill="currentColor"
      height="1em"
      width="1em"
    >
      <rect data-name="Rechteck 1748-5" className="cls-1" x="-2.5" y="-2.5" width="24" height="24" fill="none" />
      <g data-name="Gruppe 3066-4">
        <circle className="cls-2" fill="#1d7c00" cx="9.5" cy="9.5" r="9.5" />
      </g>
      <path
        className="cls-3"
        fill="#fff"
        d="M7.32,13.19a1,1,0,0,1-.67-.26l-2.16-2A1,1,0,1,1,5.84,9.47l1.51,1.39,5.42-4.55a1,1,0,0,1,1.29,1.53L8,13A1.05,1.05,0,0,1,7.32,13.19Z"
      />
    </svg>
  );
}
