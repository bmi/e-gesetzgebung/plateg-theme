// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function FolderRvOutlined(): React.ReactElement {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 28.71 21.54"
      aria-hidden="true"
      data-name="Ebene 1"
      fill="currentColor"
      className="anticon"
      height="1em"
      width="1em"
    >
      <g>
        <path
          style={{ fill: '#0032d9' }}
          d="M1.55,21.54c-.86,0-1.55-.7-1.55-1.55H0V1.55C0,.69,.7,0,1.55,0H7.25c.4,0,.78,.15,1.07,.42l1.83,1.73c.1,.1,.24,.15,.38,.15H27.16c.86,0,1.55,.7,1.56,1.56V19.98c0,.86-.7,1.56-1.56,1.56H1.55ZM1,1.55V19.98c0,.31,.25,.55,.55,.56H27.15c.31,0,.55-.25,.56-.56V3.86c0-.31-.25-.56-.56-.56H10.53c-.4,0-.78-.15-1.07-.43l-1.83-1.72c-.11-.1-.24-.15-.38-.15H1.55c-.3,0-.55,.25-.55,.55ZM5,9.73c-.28,0-.5-.22-.5-.5s.22-.5,.5-.5h7.88c.28,0,.5,.22,.5,.5s-.22,.5-.5,.5H5Z"
        />
      </g>
    </svg>
  );
}
