// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function CloseOutlined(props: React.SVGProps<SVGSVGElement>): React.ReactElement {
  return (
    <svg
      {...props}
      aria-hidden="true"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 9.5 9.5"
      fill="currentColor"
      className="anticon"
      height="1em"
      width="1em"
    >
      <g data-name="Ebene 2">
        <g data-name="Ebene 1">
          <path
            d={
              'M9.28,8.22a.75.75,0,0,1,0,1.06.75.75,0,0,1-1.06,0L4.75,5.81,1.28,9.28a.75.75,0,0,1-1.06,0,.75.75,0,0,1,0-1.06L3.69,' +
              '4.75.22,1.28A.75.75,0,0,1,1.28.22L4.75,3.69,8.22.22A.75.75,0,0,1,9.28,1.28L5.81,4.75Z'
            }
          />
        </g>
      </g>
    </svg>
  );
}
