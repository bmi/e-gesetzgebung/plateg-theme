// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './icons.less';

export { default as ThreeDotsActive } from './ThreeDotsActive';
export { default as ThreeDotsHover } from './ThreeDotsHover';
export { default as ThreeDots } from './ThreeDots';
export { default as ThreeDotsDisabled } from './ThreeDotsDisabled';

export { KabinettverfahrenDurchfuehrenOutlined } from './KabinettverfahrenDurchfuehrenOutlined';
export { DokumenteErstellenOutlined } from './DokumenteErstellenOutlined';
export { RegelungsentwurfAbstimmenOutlined } from './RegelungsentwurfAbstimmenOutlined';
export { FolderRvOutlined } from './FolderRvOutlined';
export { DirectionRightOutlined } from './DirectionRightOutlined';
export { DirectionRightOutlinedNew } from './DirectionRightOutlinedNew';
export { RightDirectionArrow } from './RightDirectionArrow';
