// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function EmailOutlined(): React.ReactElement {
  return (
    <svg
      aria-hidden="true"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 17.6 14.12"
      fill="currentColor"
      className="anticon"
      height="1em"
      width="1em"
    >
      <path
        data-name="Pfad 7098"
        d={
          'M38.388,913.765H22.038a.636.636,0,0,0-.63.64v12.83a.651.651,0,0,0,.63.65h16.35a.636.636,0,0,0,.62-.65v-12.83A.627.627,' +
          '0,0,0,38.388,913.765Zm-1.86,1.44v.01l-6.32,5.01-6.31-5.02Zm1.07,11.23H22.818v-10.45h.01l6.71,5.32a1.069,1.069,0,0,0,1.35,0l6.71-5.32Z'
        }
        transform="translate(-21.408 -913.765)"
      />
    </svg>
  );
}
