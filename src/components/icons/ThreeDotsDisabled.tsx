// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function ThreeDotsDisabled(): React.ReactElement {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
      <rect data-name="Rechteck 2655" width="24" height="24" fill="none" />
      <g data-name="Gruppe 5730" transform="translate(-1715 -439)">
        <circle data-name="Ellipse 450" cx="2" cy="2" r="2" transform="translate(1718 449)" fill="#909090" />
        <circle data-name="Ellipse 451" cx="2" cy="2" r="2" transform="translate(1725 449)" fill="#909090" />
        <path
          data-name="Pfad 6072"
          d="M2,0A2,2,0,1,1,0,2,2,2,0,0,1,2,0Z"
          transform="translate(1732 449)"
          fill="#909090"
        />
      </g>
    </svg>
  );
}

export default ThreeDotsDisabled;
