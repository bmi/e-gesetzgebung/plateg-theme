// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function IconImportant(props: { label?: string }): React.ReactElement {
  const svgProps = props.label ? { role: 'img', 'aria-label': props.label } : { 'aria-hidden': true };
  return (
    <svg
      {...svgProps}
      data-name="Ebene 1"
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="currentColor"
      className="anticon"
    >
      <rect style={{ fill: 'none' }} className="a" width="24" height="24" />
      <g transform="translate(-216 -536)">
        <path
          style={{ fill: '#520b82', stroke: '#510b81' }}
          className="b"
          d="M9,0A9,9,0,1,1,0,9,9,9,0,0,1,9,0Z"
          transform="translate(237 557) rotate(180)"
        />
        <path
          style={{ fill: '#fff' }}
          className="c"
          d="M156.6,262.942l1.629,3.3,3.643.529-2.636,2.57.622,3.628-3.258-1.713-3.258,1.713.622-3.628-2.636-2.57,3.643-.529Z"
          transform="translate(71.535 279.454)"
        />
      </g>
    </svg>
  );
}
