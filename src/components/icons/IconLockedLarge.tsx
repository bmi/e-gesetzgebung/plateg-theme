// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function IconLockedLarge(props: { label?: string }): React.ReactElement {
  const svgProps = props.label ? { role: 'img', 'aria-label': props.label } : { 'aria-hidden': true };
  return (
    <svg
      {...svgProps}
      xmlns="http://www.w3.org/2000/svg"
      width="72"
      height="72"
      viewBox="0 0 106 106"
      fill="currentColor"
      className="anticon"
    >
      <rect width="106" height="106" fill="none" />
      <path
        d="M53 13C31.0467 13 13.25 30.7967 13.25 52.75C13.25 74.7033 31.0467 92.5 53 92.5C74.9533 92.5 92.75 74.7033 92.75 52.75C92.75 30.7967 74.9533 13 53 13Z"
        fill="black"
        stroke="black"
      />
      <path
        d="M68.9496 47.0028H65.5662V42.7111C65.5662 35.7023 59.8642 30 52.8552 30C45.8461 30 40.1441 35.7023 40.1441 42.7111V47.0028H36.7607C35.7883 47.0028 35 47.7909 35 48.7635V70.2391C35 71.2116 35.7883 72 36.7607 72H68.9496C69.9219 72 70.7103 71.2116 70.7103 70.2391V48.7635C70.7103 47.7909 69.9219 47.0028 68.9496 47.0028ZM44.1402 42.7111C44.1402 37.9056 48.0497 33.9961 52.8552 33.9961C57.6606 33.9961 61.5701 37.9056 61.5701 42.7111V47.0028H44.1402V42.7111ZM54.9977 60.7758V64.3139C54.9977 64.7113 54.6727 65.0365 54.2754 65.0365H51.4349C51.0376 65.0365 50.7126 64.7113 50.7126 64.3139V60.7758C49.6616 60.103 48.9271 59.0147 48.9271 57.7159C48.9271 55.6448 50.6856 53.9661 52.8551 53.9661C55.0245 53.9661 56.7832 55.6448 56.7832 57.7159C56.7832 59.0145 56.0487 60.103 54.9977 60.7758Z"
        fill="white"
      />
    </svg>
  );
}
