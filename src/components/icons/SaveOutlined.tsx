// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function SaveOutlined(): React.ReactElement {
  return (
    <svg
      aria-hidden="true"
      data-name="Ebene 1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 14.49 14.5"
      fill="currentColor"
      className="anticon"
      height="1em"
      width="1em"
    >
      <g data-name="Gruppe 5266">
        <path d="M5.85,13h0ZM11.09,0a.84.84,0,0,1,.38.21A.76.76,0,0,0,11.09,0Z" />
        <path
          d={
            'M14.27,3,11.47.22A.84.84,0,0,0,11.09,0H1.15A1.14,1.14,0,0,0,0,1.15V13.34a1.15,1.15,0,0,0,1.15,1.15H13.34a1.16,1.16,' +
            '0,0,0,1.15-1.15V3.55A.75.75,0,0,0,14.27,3ZM4.51,1.5H10.2l0,3.09-5.72,0ZM8.65,13H5.85V10.85A.13.13,0,0,1,6,10.71H8.51a.14.14,' +
            '0,0,1,.14.14ZM13,13H10.15V10.85A1.64,1.64,0,0,0,8.51,9.21H6a1.63,1.63,0,0,0-1.63,1.64V13H1.5V1.5H3V4.63A1.47,1.47,0,0,0,4.47,' +
            '6.09h5.76a1.47,1.47,0,0,0,1.46-1.46V2.56L13,3.86Z'
          }
        />
        <path d="M5,14.49H5Z" />
      </g>
    </svg>
  );
}
