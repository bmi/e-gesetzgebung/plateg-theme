// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function EditOutlined(): React.ReactElement {
  return (
    <svg
      aria-hidden="true"
      data-name="Ebene 1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 14.44 14.71"
      fill="currentColor"
      className="anticon"
      height="1em"
      width="1em"
    >
      <path
        d={
          'M13.73,1.36,13.06.68A2.53,2.53,0,0,0,11.35,0,2.38,2.38,0,0,0,9.67.73L1.28,9.29h0A1.46,1.46,0,0,0,.91,' +
          '10L0,13.49A1,1,0,0,0,0,14a1,1,0,0,0,1,.74l.24,0,3.44-.89a1.47,1.47,0,0,0,.67-.39l8.4-8.57A2.48,2.48,0,0,0,' +
          '13.73,1.36ZM2.16,11.15l1.38,1.38L1.69,13Zm10.5-7.37-7.79,8-2-2,7.84-8a.91.91,0,0,1,1.29,0l.62.63A1,1,0,0,1,12.66,3.78Z'
        }
      />
    </svg>
  );
}
