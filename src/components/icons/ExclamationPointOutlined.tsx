// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function ExclamationPointOutlined(): React.ReactElement {
  const styles =
    '.excl_point_a,.excl_point_c{fill:none;}.excl_point_b{fill:#fff;stroke-width:4px;}.excl_point_b,.excl_point_c{stroke:#a00;}.excl_point_c{stroke-linecap:round;stroke-width:6px;}.excl_point_d{fill:#a00;}';
  return (
    <svg aria-hidden="true" width="72" height="72" viewBox="0 0 72 72">
      <defs></defs>
      <g transform="translate(-924 -431)">
        <style>{styles}</style>
        <rect className="excl_point_a" width="72" height="72" transform="translate(924 431)" />
        <circle className="excl_point_b" cx="32" cy="32" r="32" transform="translate(992.5 499) rotate(180)" />
        <path className="excl_point_c" d="M0,0V19" transform="translate(960.5 452.5)" />
        <circle className="excl_point_d" cx="3.5" cy="3.5" r="3.5" transform="translate(964 487) rotate(180)" />
      </g>
    </svg>
  );
}
