// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function DoubleLeftOutlined(): React.ReactElement {
  return (
    <svg
      aria-hidden="true"
      data-name="Ebene 1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 24 24"
      fill="currentColor"
      className="anticon"
      height="2em"
      width="2em"
    >
      <g data-name="Gruppe 14253" transform="translate(-964 -400)">
        <rect data-name="Rechteck 2281" transform="translate(964 400)" />
        <g data-name="Gruppe 4386" transform="translate(970.999 408)">
          <path
            data-name="Linie 366"
            d="M106.212,152.6a.748.748,0,0,1-.53-.22l-4-4a.75.75,0,0,1,0-1.061l4-4a.75.75,0,0,1,1.061,1.061l-3.471,3.471,3.471,3.471a.75.75,0,0,1-.53,1.28Z"
            transform="translate(-102.211 -143.842)"
          />
          <path
            data-name="Linie 366"
            d="M106.212,152.6a.748.748,0,0,1-.53-.22l-4-4a.75.75,0,0,1,0-1.061l4-4a.75.75,0,0,1,1.061,1.061l-3.471,3.471,3.471,3.471a.75.75,0,0,1-.53,1.28Z"
            transform="translate(-96.211 -143.842)"
          />
        </g>
      </g>
    </svg>
  );
}
