// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function InfoCircleFilled(): React.ReactElement {
  return (
    <svg
      aria-hidden="true"
      data-name="Ebene 1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 20 20"
      fill="currentColor"
      className="active-icon"
      height="1.9rem"
      width="1.9rem"
    >
      <path
        fill="none"
        d="M10,5.71a1.4,1.4,0,0,1-1.39,1.4,1.42,1.42,0,0,1-1.4-1.4,1.4,1.4,0,0,1,1.4-1.39A1.39,1.39,0,0,1,10,5.71Z"
      />
      <path
        fill="none"
        d="M9.75,9.74v3.6a1.19,1.19,0,0,1-1.16,1.22h0a1.2,1.2,0,0,1-1.2-1.2V9.76a1.21,1.21,0,0,1,1.2-1.21A1.19,1.19,0,0,1,9.75,9.74Z"
      />
      <path
        d={
          'M9.5,0A9.5,9.5,0,1,0,19,9.5,9.51,9.51,0,0,0,9.5,0Zm1.19,13.28A1.19,1.19,0,0,1,9.53,14.5h0a1.2,1.2,0,0,1-1.2-1.2V9.7A1.21,1.21,' +
          '0,0,1,9.5,8.49a1.19,1.19,0,0,1,1.19,1.19ZM9.5,7.05a1.42,1.42,0,0,1-1.4-1.4A1.4,1.4,0,0,1,9.5,4.26a1.4,1.4,0,0,1,0,2.79Z'
        }
      />
    </svg>
  );
}
