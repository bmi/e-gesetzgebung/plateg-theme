// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function UserOutlined(): React.ReactElement {
  return (
    <svg
      aria-hidden="true"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 36 36"
      fill="currentColor"
      className="anticon"
    >
      <g transform="translate(-372.79 -400.49)">
        <path
          fill="#fff"
          d="M390.792,423.5a7.532,7.532,0,1,1,7.44-7.532A7.494,7.494,0,0,1,390.792,423.5Zm0-13.563a6.032,6.032,0,1,0,5.94,6.031A5.992,5.992,0,0,0,390.792,409.937Z"
        />
        <path
          fill="none"
          d="M401.01,431.44a16.486,16.486,0,0,1-20.44,0c1.83-2.02,5.78-3.45,10.22-3.45S399.18,429.42,401.01,431.44Z"
        />
        <path
          fill="none"
          d={
            'M407.29,418.49a16.382,16.382,0,0,1-4.83,11.67c-.1.1-.2.2-.31.29-2.11-2.37-6.35-3.96-11.36-3.96s-9.25,' +
            '1.59-11.36,3.96c-.11-.09-.21-.19-.31-.29a16.5,16.5,0,1,1,28.17-11.67Z'
          }
        />
        <path
          fill="#fff"
          d={
            'M390.79,400.49a18,18,0,0,0-12.23,31.2c.38.36.77.69,1.18,1.01a18,18,0,0,0,22.1,0c.41-.32.8-.65,1.18-1.01a18,18,' +
            '0,0,0-12.23-31.2Zm0,34.5a16.354,16.354,0,0,1-10.22-3.55c1.83-2.02,5.78-3.45,10.22-3.45s8.39,1.43,10.22,3.45A16.354,16.354,' +
            '0,0,1,390.79,434.99Zm11.67-4.83c-.1.1-.2.2-.31.29-2.11-2.37-6.35-3.96-11.36-3.96s-9.25,1.59-11.36,3.96c-.11-.09-.21-.19-.31-.29a16.5,16.5,0,1,1,23.34,0Z'
          }
        />
      </g>
    </svg>
  );
}
