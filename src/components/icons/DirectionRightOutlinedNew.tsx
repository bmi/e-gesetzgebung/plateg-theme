// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function DirectionRightOutlinedNew(props: React.SVGProps<SVGSVGElement>): React.ReactElement {
  return (
    <svg fill="currentColor" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" {...props}>
      <g id="icon-directional-right=default, Hover=False, Active=False">
        <path
          id="icon-outlined-directional-right-default"
          d="M10.03 6.46106C9.8878 6.32858 9.69975 6.25645 9.50545 6.25988C9.31115 6.26331 9.12576 6.34202 8.98835 6.47944C8.85094 6.61685 8.77222 6.80223 8.7688 6.99653C8.76537 7.19084 8.83749 7.37888 8.96997 7.52106L13.44 11.9911L8.96997 16.4611C8.89628 16.5297 8.83718 16.6125 8.79619 16.7045C8.7552 16.7965 8.73316 16.8958 8.73138 16.9965C8.7296 17.0972 8.74813 17.1973 8.78585 17.2907C8.82357 17.384 8.87971 17.4689 8.95093 17.5401C9.02215 17.6113 9.10698 17.6675 9.20037 17.7052C9.29376 17.7429 9.39379 17.7614 9.49449 17.7596C9.5952 17.7579 9.69451 17.7358 9.78651 17.6948C9.87851 17.6538 9.96131 17.5947 10.03 17.5211L15.03 12.5211C15.1704 12.3804 15.2493 12.1898 15.2493 11.9911C15.2493 11.7923 15.1704 11.6017 15.03 11.4611L10.03 6.46106Z"
        />
      </g>
    </svg>
  );
}
