// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function GesetzesfolgenabschaetzungMenuIcon(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      aria-hidden
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <g id="icon-menu-Gesetzesfolgenabsch&#195;&#164;tzung">
        <g id="Group 63">
          <g id="Group 62">
            <g id="Group 72">
              <path
                id="Vector (Stroke)"
                fillRule="evenodd"
                clipRule="evenodd"
                d="M11.7408 0.650606C11.9002 0.553965 12.1 0.553965 12.2594 0.650606L20.1623 5.44359C20.3118 5.53422 20.403 5.69631 20.403 5.87109C20.403 6.04587 20.3118 6.20797 20.1624 6.29861L12.2594 11.0922C12.1001 11.1889 11.9002 11.1889 11.7408 11.0922L3.83786 6.29861C3.68842 6.20797 3.59716 6.04587 3.59717 5.87109C3.59717 5.69631 3.68844 5.53422 3.83789 5.44359L11.7408 0.650606ZM5.06132 5.87113L12.0001 10.0799L18.9389 5.87113L12.0001 1.66289L5.06132 5.87113Z"
                fill="white"
              />
              <path
                id="Vector (Stroke)_2"
                fillRule="evenodd"
                clipRule="evenodd"
                d="M1.74664 9.56894C1.89945 9.47913 2.08839 9.47693 2.24324 9.56316L9.94448 13.8514C10.103 13.9396 10.2012 14.1068 10.2012 14.2882V22.8647C10.2012 23.042 10.1074 23.206 9.95459 23.2958C9.80179 23.3856 9.61285 23.3878 9.45799 23.3015L1.75676 19.0133C1.59826 18.9251 1.5 18.7579 1.5 18.5765V10C1.5 9.82275 1.59384 9.65875 1.74664 9.56894ZM2.5 10.8507V18.2826L9.20124 22.014V14.5821L2.5 10.8507Z"
                fill="white"
              />
              <path
                id="Vector (Stroke)_3"
                fillRule="evenodd"
                clipRule="evenodd"
                d="M22.2532 9.56894C22.406 9.65875 22.4998 9.82275 22.4998 10V18.5765C22.4998 18.7579 22.4016 18.9251 22.2431 19.0133L14.5418 23.3015C14.387 23.3878 14.198 23.3856 14.0452 23.2958C13.8924 23.206 13.7986 23.042 13.7986 22.8647V14.2882C13.7986 14.1068 13.8968 13.9396 14.0553 13.8514L21.7566 9.56316C21.9114 9.47693 22.1004 9.47913 22.2532 9.56894ZM14.7986 14.5821V22.014L21.4998 18.2826V10.8507L14.7986 14.5821Z"
                fill="white"
              />
            </g>
          </g>
        </g>
      </g>
    </svg>
  );
}
