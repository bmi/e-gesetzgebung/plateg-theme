// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function DrucksachennummerMenuIcon(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      aria-hidden
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        fill-rule="evenodd"
        clip-rule="evenodd"
        d="M2.93958 0.5C2.66343 0.5 2.43958 0.723858 2.43958 1V23.0039C2.43958 23.28 2.66343 23.5039 2.93958 23.5039H21.0604C21.3366 23.5039 21.5604 23.28 21.5604 23.0039V7C21.5604 6.86652 21.5071 6.73859 21.4122 6.64468L15.3518 0.644679C15.2582 0.551993 15.1317 0.5 15 0.5H2.93958ZM3.43958 22.5039V1.5H14.5V7C14.5 7.27614 14.7239 7.5 15 7.5H20.5604V22.5039H3.43958ZM19.8447 6.5L15.5 2.1986V6.5H19.8447ZM7.5 12.5C7.5 12.2239 7.72386 12 8 12H16C16.2761 12 16.5 12.2239 16.5 12.5C16.5 12.7761 16.2761 13 16 13H8C7.72386 13 7.5 12.7761 7.5 12.5ZM8 15.5C7.72386 15.5 7.5 15.7239 7.5 16C7.5 16.2761 7.72386 16.5 8 16.5H16C16.2761 16.5 16.5 16.2761 16.5 16C16.5 15.7239 16.2761 15.5 16 15.5H8Z"
        fill="white"
      />
    </svg>
  );
}
