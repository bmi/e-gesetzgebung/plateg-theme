// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function StartseiteMenuIcon(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      aria-hidden
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <g id="icon-menu-Startseite">
        <g id="Frame 36">
          <path
            id="Vector (Stroke)"
            fillRule="evenodd"
            clipRule="evenodd"
            d="M22.4295 10.3706C22.6336 10.5558 22.75 10.8187 22.75 11.0943V21.7727C22.75 22.3125 22.3125 22.75 21.7727 22.75H14.7994C14.2597 22.75 13.8221 22.3125 13.8221 21.7727V17.521C13.8221 16.4974 12.998 15.678 12.0016 15.678C11.4997 15.678 11.0477 15.8805 10.7165 16.2144C10.3835 16.5501 10.1779 17.0132 10.1779 17.521V21.7727C10.1779 22.3125 9.74033 22.75 9.2006 22.75H2.22727C1.68754 22.75 1.25 22.3125 1.25 21.7727V11.0943C1.25 10.8186 1.36645 10.5557 1.57065 10.3705L11.345 1.50346C11.7176 1.16547 12.2859 1.16552 12.6584 1.50357L22.4295 10.3706ZM21.7727 11.0943L12.0016 2.22727L2.22727 11.0943V21.7727H9.2006V17.521C9.2006 16.7447 9.51579 16.0372 10.0227 15.5262C10.5296 15.0152 11.2283 14.7007 12.0016 14.7007C13.5451 14.7007 14.7994 15.9651 14.7994 17.521V21.7727H21.7727V11.0943Z"
            fill="white"
          />
        </g>
      </g>
    </svg>
  );
}
