// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function HRAMenuIcon(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      aria-hidden
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <g id="icon-menu-Abstimmung">
        <g id="Group 69">
          <g id="Group 68">
            <g id="Group 70">
              <g id="Group 71">
                <path
                  id="Vector (Stroke)"
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M1.50195 2.5C1.50195 2.22386 1.72581 2 2.00195 2H22.002C22.2781 2 22.502 2.22386 22.502 2.5V18.3334C22.502 18.6095 22.2781 18.8334 22.002 18.8334H11.0645L5.62728 22.8473C5.14226 23.2057 4.38936 22.8953 4.38936 22.2344V18.8334H2.00195C1.72581 18.8334 1.50195 18.6095 1.50195 18.3334V2.5ZM2.50195 3V17.8334H4.88936C5.1655 17.8334 5.38936 18.0572 5.38936 18.3334V21.78L10.603 17.9311C10.689 17.8676 10.7931 17.8334 10.9 17.8334H21.502V3H2.50195ZM5.03338 22.0427C5.03326 22.0428 5.03314 22.0429 5.03301 22.043L5.03319 22.0429L5.03338 22.0427Z"
                  fill="white"
                />
                <path
                  id="Vector (Stroke)_2"
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M16.375 7.16931C16.5577 7.37643 16.5378 7.69239 16.3307 7.87503L10.7307 12.8131C10.4589 13.0662 10.0441 13.0616 9.77701 12.8031L7.65212 10.7448C7.45377 10.5527 7.44873 10.2361 7.64086 10.0378C7.83299 9.83946 8.14954 9.83442 8.34788 10.0265L10.2687 11.8872L15.6693 7.12498C15.8764 6.94235 16.1924 6.96219 16.375 7.16931Z"
                  fill="white"
                />
              </g>
            </g>
          </g>
        </g>
      </g>
    </svg>
  );
}
