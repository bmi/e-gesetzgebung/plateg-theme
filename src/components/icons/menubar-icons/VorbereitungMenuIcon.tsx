// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function VorbereitungMenuIcon(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      aria-hidden
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <g id="icon-menu-Vorbereitung">
        <g id="Frame 41">
          <g id="Group 40">
            <path
              id="icon-menu-Vorbereitung-Vector (Stroke)"
              fillRule="evenodd"
              clipRule="evenodd"
              d="M9.82642 1.30717C8.48997 1.30717 7.41629 2.3881 7.41629 3.71731C7.41629 3.99345 7.19243 4.21731 6.91629 4.21731H1.40015V22.6928H7.82642C8.10256 22.6928 8.32642 22.9167 8.32642 23.1928C8.32642 23.469 8.10256 23.6928 7.82642 23.6928H0.900146C0.624004 23.6928 0.400146 23.469 0.400146 23.1928V3.71731C0.400146 3.44117 0.624004 3.21731 0.900146 3.21731H6.45265C6.69386 1.5738 8.10644 0.307175 9.82642 0.307175C11.5464 0.307175 12.959 1.5738 13.2002 3.21731H18.7527C19.0288 3.21731 19.2527 3.44117 19.2527 3.71731V13.4551C19.2527 13.7312 19.0288 13.9551 18.7527 13.9551C18.4766 13.9551 18.2527 13.7312 18.2527 13.4551V4.21731H12.7366C12.4604 4.21731 12.2366 3.99345 12.2366 3.71731C12.2366 2.3881 11.1629 1.30717 9.82642 1.30717Z"
              fill="white"
            />
            <path
              id="icon-menu-Vorbereitung-Vector (Stroke)_2"
              fillRule="evenodd"
              clipRule="evenodd"
              d="M5.4646 13.4538C5.4646 13.1776 5.68846 12.9538 5.9646 12.9538H13.6876C13.9638 12.9538 14.1876 13.1776 14.1876 13.4538C14.1876 13.7299 13.9638 13.9538 13.6876 13.9538H5.9646C5.68846 13.9538 5.4646 13.7299 5.4646 13.4538Z"
              fill="white"
            />
            <path
              id="icon-menu-Vorbereitung-Vector (Stroke)_3"
              fillRule="evenodd"
              clipRule="evenodd"
              d="M5.4646 9.72281C5.4646 9.44667 5.68846 9.22281 5.9646 9.22281H13.6876C13.9638 9.22281 14.1876 9.44667 14.1876 9.72281C14.1876 9.99895 13.9638 10.2228 13.6876 10.2228H5.9646C5.68846 10.2228 5.4646 9.99895 5.4646 9.72281Z"
              fill="white"
            />
            <path
              id="icon-menu-Vorbereitung-Vector (Stroke)_4"
              fillRule="evenodd"
              clipRule="evenodd"
              d="M23.4574 15.8848C23.6503 16.0824 23.6464 16.399 23.4487 16.5918L16.5651 23.3075L16.5644 23.3083C16.0439 23.8139 15.2128 23.8139 14.6923 23.3083L12.4251 21.097C12.2274 20.9042 12.2234 20.5876 12.4162 20.3899C12.609 20.1922 12.9256 20.1883 13.1233 20.3811L15.3891 22.591C15.389 22.5909 15.3892 22.5911 15.3891 22.591C15.5215 22.7193 15.7349 22.7195 15.8672 22.5913C15.8671 22.5914 15.8674 22.5912 15.8672 22.5913L22.7504 15.876C22.948 15.6832 23.2646 15.6871 23.4574 15.8848Z"
              fill="white"
            />
          </g>
        </g>
      </g>
    </svg>
  );
}
