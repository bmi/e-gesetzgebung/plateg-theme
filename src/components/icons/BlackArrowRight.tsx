// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function BlackArrowRight(props: React.SVGProps<SVGSVGElement>): React.ReactElement {
  return (
    <svg {...props} width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
      <g>
        <path d="M15 12.148L10 16.148L10 8.14801L15 12.148Z" fill="black" />
      </g>
    </svg>
  );
}
