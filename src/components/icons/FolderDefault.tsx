// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function FolderDefault(): React.ReactElement {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      style={{ verticalAlign: 'top', marginTop: '1px' }}
    >
      <rect data-name="Rechteck 3889" width="24" height="24" fill="none" />
      <g data-name="icon/folder/default" transform="translate(-670.641 -371.51)">
        <path
          data-name="Pfad 7191"
          d="M690.14,380.34v8.17a.5.5,0,0,1-.5.5h-14a.508.508,0,0,1-.5-.5v-10a.5.5,0,0,1,.5-.5h4.86a.467.467,0,0,1,.34.13l1.85,1.7h6.95A.5.5,0,0,1,690.14,380.34Z"
          fill="#9dafec"
        />
      </g>
    </svg>
  );
}
