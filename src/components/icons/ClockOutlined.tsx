// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function ClockOutlined(props: { label?: string }): React.ReactElement {
  const svgProps = props.label ? { role: 'img', 'aria-label': props.label } : { 'aria-hidden': true };
  return (
    <svg
      {...svgProps}
      data-name="Ebene 1"
      xmlns="http://www.w3.org/2000/svg"
      fill="currentColor"
      className="anticon"
      width="23"
      height="23"
      viewBox="0 0 20 19"
    >
      <rect data-name="Rechteck 2284" width="24" height="24" fill="none" />
      <g data-name="Gruppe 5202" transform="translate(-883.5 -1611.5)">
        <rect
          data-name="Rechteck 2283"
          width="13.566"
          height="13.566"
          rx="6.783"
          transform="translate(888.5 1616.5)"
          fill="none"
          stroke="currentColor"
          strokeLinejoin="round"
          strokeWidth="1.5"
        />
        <path
          data-name="Pfad 5966"
          d="M-18829,11435.146v4.32l2.729,1.6"
          transform="translate(19724.281 -9815.885)"
          fill="none"
          stroke="currentColor"
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="1.5"
        />
      </g>
    </svg>
  );
}
