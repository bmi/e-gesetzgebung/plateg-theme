// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useTranslation } from 'react-i18next';

export function CheckOutlined(): React.ReactElement {
  const { t } = useTranslation();
  // prettier-ignore
  return (
    <svg
      data-name="Ebene 1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 12.43 8.71"
      fill="currentColor"
      className="anticon"
      height="1em"
      width="1em"
      aria-labelledby="selected"
      role='img'
    >
      <title id="selected">{t('theme.icons.checkOutlinedTitle')}</title>
      <path
        data-name="Pfad 5489"
        d="M12,2.06,4.56,8.41a1.3,1.3,0,0,1-1.69,0L.38,6.12a1.13,1.13,0,0,1-.09-1.6l.06-.06a1.26,1.26,0,0,1,1.75,0l1.65,1.5L10.36.3a1.29,1.29,0,0,1,1.75.09,1.14,1.14,0,0,1,0,1.61Z"
      />
    </svg>
  );
}
