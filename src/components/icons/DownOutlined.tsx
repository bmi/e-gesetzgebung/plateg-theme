// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function DownOutlined(): React.ReactElement {
  return (
    <svg
      aria-hidden="true"
      data-name="Ebene 1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 11.5 6.5"
      fill="currentColor"
      className="anticon"
      height="1em"
      width="1em"
    >
      <path d="M11.28.22a.75.75,0,0,0-1.06,0L5.75,4.69,1.28.22A.75.75,0,0,0,.22,1.28l5,5a.75.75,0,0,0,1.06,0l5-5A.75.75,0,0,0,11.28.22Z" />
    </svg>
  );
}
