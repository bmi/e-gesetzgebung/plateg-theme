// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function DokumenteErstellenOutlined(): React.ReactElement {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 22.61 29.1"
      aria-hidden="true"
      data-name="Ebene 1"
      fill="currentColor"
      className="anticon"
      height="1em"
      width="1em"
    >
      <g>
        <path
          style={{ fill: '#0032d9' }}
          d="M.5,29.1c-.28,0-.5-.22-.5-.5V4.88c0-.13,.05-.26,.15-.35L4.53,.15c.09-.09,.22-.15,.35-.15H22.11c.28,0,.5,.22,.5,.5V28.6c0,.28-.22,.5-.5,.5H.5Zm.5-1.01H21.6V1H5.73V5.13c0,.28-.22,.5-.5,.5H1V28.09ZM1.46,4.63h3.28V1.35L1.46,4.63Zm3.37,15.19c0-.59,.47-1.07,1.06-1.07,.59,0,1.07,.47,1.07,1.06,0,.59-.47,1.07-1.06,1.07h0c-.59,0-1.06-.48-1.06-1.06h0Zm3.73,.5c-.28,0-.5-.22-.5-.5s.22-.5,.5-.5h9.22c.28,0,.5,.22,.5,.5s-.22,.5-.5,.5H8.55Zm-3.73-5.95c0-.59,.48-1.07,1.07-1.07,.59,0,1.07,.48,1.07,1.07,0,.59-.48,1.07-1.07,1.07-.59,0-1.06-.48-1.06-1.07h0Zm3.73,.5c-.28,0-.5-.22-.5-.5s.22-.5,.5-.5h9.22c.28,0,.5,.22,.5,.5s-.22,.5-.5,.5H8.55Z"
        />
      </g>
    </svg>
  );
}
