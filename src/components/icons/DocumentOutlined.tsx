// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function DocumentOutlined(): React.ReactElement {
  return (
    <svg
      aria-hidden="true"
      data-name="Ebene 1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 15.5 18.5"
      fill="currentColor"
      className="anticon"
      height="1em"
      width="1em"
    >
      <rect data-name="Rechteck 2597" fill="none" x="-4.14" y="-2.64" width="24" height="24" />
      <path
        d={
          'M15.29,5.54,10.09.23A.74.74,0,0,0,9.55,0H.75A.76.76,0,0,0,0,.75v17a.76.76,0,0,0,.75.75h14a.76.76,0,0,0,' +
          '.75-.75V6.06A.78.78,0,0,0,15.29,5.54Zm-5.06-3,2.9,2.95h-2.9ZM1.5,17V1.5H8.73V6.22A.75.75,0,0,0,9.48,7H14V17Z'
        }
      />
    </svg>
  );
}
