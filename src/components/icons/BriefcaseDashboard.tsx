// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function BriefcaseDashboard(): React.ReactElement {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 21.513 19.69"
      aria-hidden="true"
      data-name="Ebene 1"
      fill="currentColor"
      className="anticon"
      height="19.69"
      width="21.513"
    >
      <g transform="translate(0.001)">
        <path
          data-name="Pfad 19515"
          d="M.63,10.39l7.71,2.12v.87a.5.5,0,0,0,.5.5h3.83a.5.5,0,0,0,.5-.5v-.87l7.72-2.12v9a.3.3,0,0,1-.3.3H.93a.3.3,0,0,1-.3-.3ZM13.61,0H8.13A1.5,1.5,0,0,0,6.71,1.54V3.18H8.2L8.17,1.5l5.4.04V3.18h1.5V1.57A1.516,1.516,0,0,0,13.61,0ZM.32,4.18A.389.389,0,0,0,0,4.59V8.81a.426.426,0,0,0,.13.35.537.537,0,0,0,.16.1l.34.09,7.71,2.12v-.93a.5.5,0,0,1,.5-.5h3.83a.5.5,0,0,1,.5.5v.92l7.72-2.11.34-.09a.572.572,0,0,0,.16-.11.414.414,0,0,0,.12-.32V4.63a.39.39,0,0,0-.35-.45H.32Zm9.02,6.86v1.84h2.83V11.04Z"
          fill="#fff"
        />
      </g>
    </svg>
  );
}
