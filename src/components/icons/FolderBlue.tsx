// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function FolderBlue(): React.ReactElement {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="15" height="11" viewBox="0 0 15 11">
      <g data-name="icon/folder/default" transform="translate(-675.14 -378.01)">
        <path
          data-name="Pfad 7191"
          d="M690.14,380.34v8.17a.5.5,0,0,1-.5.5h-14a.508.508,0,0,1-.5-.5v-10a.5.5,0,0,1,.5-.5h4.86a.467.467,0,0,1,.34.13l1.85,1.7h6.95A.5.5,0,0,1,690.14,380.34Z"
          fill="#9dafec"
        />
      </g>
    </svg>
  );
}
