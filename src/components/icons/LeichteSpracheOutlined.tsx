// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function LeichteSpracheOutlined(props: { label?: string }): React.ReactElement {
  const svgProps = props.label ? { role: 'img', 'aria-label': props.label } : { 'aria-hidden': true };
  return (
    <svg
      {...svgProps}
      data-name="Ebene 1"
      xmlns="http://www.w3.org/2000/svg"
      fill="currentColor"
      className="anticon"
      width="24"
      height="24"
      viewBox="0 0 24 24"
    >
      <g data-name="Gruppe 13738" transform="translate(-1802 -20)">
        <rect data-name="Rechteck 6201" width="24" height="24" transform="translate(1802 20)" fill="none" />
        <path
          data-name="Pfad 17407"
          d="M-7.05-7.05a10.471,10.471,0,0,1,1.484-1.18,9.628,9.628,0,0,1,1.688-.883,10.506,10.506,0,0,1,1.859-.555,10.016,10.016,0,0,1,1.984-.2H.153v8.8H-.019a10.1,10.1,0,0,0-2,.2,10.106,10.106,0,0,0-1.859.563A10.386,10.386,0,0,0-5.565.575,10.061,10.061,0,0,0-7.05,1.747,10.669,10.669,0,0,0-8.542.56a9.962,9.962,0,0,0-1.7-.9A10.064,10.064,0,0,0-12.1-.9a10.214,10.214,0,0,0-2.008-.2h-.141v-8.8h.141a10.214,10.214,0,0,1,2.008.2,10.265,10.265,0,0,1,1.875.563,9.585,9.585,0,0,1,1.7.9A10.909,10.909,0,0,1-7.05-7.05Zm1.688-3.109a2.561,2.561,0,0,1-.758.523,2.22,2.22,0,0,1-.93.2,2.22,2.22,0,0,1-.93-.2,2.561,2.561,0,0,1-.758-.523,2.364,2.364,0,0,1-.523-.742,2.22,2.22,0,0,1-.2-.93v-.047a2.183,2.183,0,0,1,.2-.914,2.364,2.364,0,0,1,.523-.742,2.34,2.34,0,0,1,.75-.523,2.273,2.273,0,0,1,.938-.2,2.22,2.22,0,0,1,.93.2,2.561,2.561,0,0,1,.758.523,2.364,2.364,0,0,1,.523.742,2.183,2.183,0,0,1,.2.914v.047a2.22,2.22,0,0,1-.2.93A2.364,2.364,0,0,1-5.362-10.159Z"
          transform="translate(1821.051 38.253)"
          fill="#fff"
        />
      </g>
    </svg>
  );
}
