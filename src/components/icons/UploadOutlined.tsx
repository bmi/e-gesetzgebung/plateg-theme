// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function UploadOutlined(): React.ReactElement {
  return (
    <svg
      aria-hidden="true"
      data-name="Ebene 1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 14.83 14.54"
      fill="currentColor"
      className="anticon"
      height="1em"
      width="1em"
    >
      <path d="M14.08,9.05a.75.75,0,0,0-.75.75V13H1.5V9.8A.75.75,0,0,0,0,9.8v4a.75.75,0,0,0,.75.75H14.08a.74.74,0,0,0,.75-.75v-4A.74.74,0,0,0,14.08,9.05Z" />
      <path
        d={
          'M4.78,4.14a.79.79,0,0,0,.53-.22L6.67,2.56V9.77a.75.75,0,0,0,1.5,0V2.56L9.52,3.92a.79.79,0,0,0,.53.22.75.75,' +
          '0,0,0,.53-.22.74.74,0,0,0,0-1.06L8,.22A.72.72,0,0,0,7.7.06a.71.71,0,0,0-.57,0,.78.78,0,0,0-.24.16L4.25,2.86a.75.75,0,0,0,0,1.06A.79.79,0,0,0,4.78,4.14Z'
        }
      />
    </svg>
  );
}
