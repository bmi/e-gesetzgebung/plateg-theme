// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function LeftOutlined(): React.ReactElement {
  return (
    <svg
      aria-hidden="true"
      data-name="Ebene 1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 6.5 11.5"
      fill="currentColor"
      className="anticon"
      height="1em"
      width="1em"
    >
      <path d="M5.75,11.5a.74.74,0,0,0,.53-.22.75.75,0,0,0,0-1.06L1.81,5.75,6.28,1.28A.75.75,0,0,0,5.22.22l-5,5a.75.75,0,0,0,0,1.06l5,5A.74.74,0,0,0,5.75,11.5Z" />
    </svg>
  );
}
