// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function ExternalLinkOutlined(): React.ReactElement {
  return (
    <svg
      aria-hidden="true"
      data-name="Ebene 1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 12.52 12.52"
      fill="currentColor"
      className="anticon"
      height="1em"
      width="1em"
    >
      <path
        d={
          'M11.77,6.66a.75.75,0,0,0-.75.75V11H1.5V1.5H5.3A.76.76,0,0,0,6.05.75.76.76,0,0,0,5.3,0H.75A.76.76,0,0,0,0,' +
          '.75v11a.75.75,0,0,0,.75.75h11a.74.74,0,0,0,.75-.75V7.41A.74.74,0,0,0,11.77,6.66Z'
        }
      />
      <path
        d={
          'M12.47.46a.76.76,0,0,0-.41-.4A.72.72,0,0,0,11.77,0H9A.75.75,0,0,0,9,1.5H10L5.24,6.22a.75.75,0,0,0,0,1.06.74.74,' +
          '0,0,0,.53.22.71.71,0,0,0,.53-.22L11,2.56V3.5a.75.75,0,0,0,1.5,0V.75A1,1,0,0,0,12.47.46Z'
        }
      />
    </svg>
  );
}
