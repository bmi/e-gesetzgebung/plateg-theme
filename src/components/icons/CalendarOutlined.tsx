// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function CalendarOutlined(props: { label?: string }): React.ReactElement {
  const svgProps = props.label ? { role: 'img', 'aria-label': props.label } : { 'aria-hidden': true };
  return (
    <svg
      {...svgProps}
      data-name="Ebene 1"
      xmlns="http://www.w3.org/2000/svg"
      fill="currentColor"
      className="anticon"
      width="23"
      height="23"
      viewBox="1.5 1 20 20"
    >
      <rect data-name="Rechteck 2284" width="24" height="24" fill="none" />
      <g data-name="Gruppe 4393" transform="translate(-115.931 -79.953)">
        <rect
          data-name="Rechteck 2283"
          width="13"
          height="11.708"
          transform="translate(121.43 86.521)"
          fill="none"
          stroke="currentColor"
          strokeLinejoin="round"
          strokeWidth="1.5"
        />
        <g data-name="Gruppe 4392">
          <line
            data-name="Linie 509"
            y2="2.613"
            transform="translate(124.804 84.954)"
            fill="none"
            stroke="currentColor"
            strokeLinecap="round"
            strokeWidth="1.5"
          />
          <line
            data-name="Linie 510"
            y2="2.613"
            transform="translate(131.056 84.954)"
            fill="none"
            stroke="currentColor"
            strokeLinecap="round"
            strokeWidth="1.5"
          />
        </g>
        <line
          data-name="Linie 511"
          x2="13"
          transform="translate(121.43 90.162)"
          fill="none"
          stroke="currentColor"
          strokeLinejoin="round"
          strokeWidth="1.5"
        />
      </g>
    </svg>
  );
}
