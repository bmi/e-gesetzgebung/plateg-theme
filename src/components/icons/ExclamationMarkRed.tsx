// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function ExclamationMarkRed(props: { style: React.CSSProperties }): React.ReactElement {
  return (
    <svg
      aria-hidden="true"
      data-name="Ebene 1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 19 19"
      width="1em"
      height="1em"
      style={props.style}
    >
      <circle fill="#a00" cx="9.5" cy="9.5" r="9.5" />
      <path
        data-name="Pfad 5099-4"
        fill="#fff"
        d={
          'M8.3,9.3V5.72A1.2,1.2,0,0,1,9.47,4.5h0a1.21,1.21,0,0,1,1.19,1.21V9.3a1.2,1.2,0,1,1-2.39,' +
          '0ZM9.5,12a1.39,1.39,0,0,1,1.39,1.39,1.4,1.4,0,1,1-2.79,0A1.39,1.39,0,0,1,9.49,12Z'
        }
      />
    </svg>
  );
}
