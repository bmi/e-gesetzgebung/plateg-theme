// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function IconUnlockedLarge(props: { label?: string }): React.ReactElement {
  const svgProps = props.label ? { role: 'img', 'aria-label': props.label } : { 'aria-hidden': true };
  return (
    <svg
      {...svgProps}
      xmlns="http://www.w3.org/2000/svg"
      width="72"
      height="72"
      viewBox="0 0 106 106"
      fill="currentColor"
      className="anticon"
    >
      <rect width="106" height="106" fill="none" />
      <path
        d="M53 13C31.0467 13 13.25 30.7967 13.25 52.75C13.25 74.7033 31.0467 92.5 53 92.5C74.9533 92.5 92.75 74.7033 92.75 52.75C92.75 30.7967 74.9533 13 53 13Z"
        fill="#1D7C00"
        stroke="#1D7C00"
      />
      <path
        d="M69.0077 46.96H65.548C65.6302 43.1869 65.4549 39.1638 63.4122 35.8743C60.92 31.8604 56.3214 29.6299 51.4098 30.0505C46.4491 30.4762 42.2568 33.487 40.4685 37.9071C40.054 38.9317 40.5487 40.0985 41.5734 40.5129C42.5973 40.9277 43.7647 40.4328 44.1795 39.4082C45.3993 36.3927 48.3011 34.3349 51.7521 34.039C55.2073 33.7395 58.2928 35.218 60.0117 37.986C61.497 40.3779 61.621 43.6441 61.5443 46.96H36.7637C35.7897 46.96 35 47.7495 35 48.7237V70.2361C35 71.2103 35.7897 72 36.7637 72H69.0077C69.9816 72 70.7713 71.2103 70.7713 70.2361V48.7237C70.7713 47.7495 69.9816 46.96 69.0077 46.96ZM55.0319 60.7566V64.3008C55.0319 64.6988 54.7063 65.0245 54.3083 65.0245H51.463C51.065 65.0245 50.7395 64.6988 50.7395 64.3008V60.7566C49.6867 60.0826 48.9509 58.9925 48.9509 57.6914C48.9509 55.6169 50.7124 53.9353 52.8857 53.9353C55.0587 53.9353 56.8204 55.6169 56.8204 57.6914C56.8204 58.9923 56.0846 60.0826 55.0319 60.7566Z"
        fill="white"
      />
    </svg>
  );
}
