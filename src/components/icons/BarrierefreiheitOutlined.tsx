// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function BarrierefreiheitOutlined(props: { label?: string }): React.ReactElement {
  const svgProps = props.label ? { role: 'img', 'aria-label': props.label } : { 'aria-hidden': true };
  return (
    <svg
      {...svgProps}
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      className="anticon"
    >
      <path
        d="M12 8C13.1046 8 14 7.10455 14 6C14 4.89545 13.1046 4 12 4C10.8954 4 10 4.89545 10 6C10 7.10455 10.8954 8 12 8Z"
        fill="white"
      />
      <path
        d="M13.1574 12.851C13.1574 13.4211 13.2624 13.9852 13.466 14.5092L15.1845 18.9321C15.3356 19.321 15.1723 19.7712 14.8198 19.9379C14.4673 20.1045 14.059 19.9244 13.908 19.5356L12.1894 15.1127C12.1761 15.0784 12.1631 15.044 12.1504 15.0094C12.1248 14.9399 12.0641 14.8935 11.9962 14.8935C11.9266 14.8935 11.8649 14.9421 11.8407 15.014C11.8049 15.1203 11.7667 15.2256 11.7261 15.33L10.092 19.5356C9.94098 19.9244 9.53274 20.1045 9.18021 19.9379C8.82772 19.7712 8.66439 19.321 8.81549 18.9321L10.4496 14.7266C10.7089 14.0592 10.8426 13.3407 10.8426 12.6146V11.2474C9.17572 11.0393 7.62978 10.3871 6.30583 9.40072C5.98802 9.16395 5.90436 8.68784 6.11903 8.33729C6.3337 7.98675 6.76541 7.89453 7.08318 8.13131C8.48657 9.17681 10.1776 9.78727 12 9.78727C13.8224 9.78727 15.5134 9.17681 16.9168 8.13131C17.2346 7.89453 17.6663 7.98675 17.881 8.33729C18.0956 8.68784 18.012 9.16395 17.6942 9.40072C16.3702 10.3871 14.8243 11.0393 13.1574 11.2474V12.851Z"
        fill="white"
        stroke="white"
      />
    </svg>
  );
}
