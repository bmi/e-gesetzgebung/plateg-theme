// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function DownloadOutlined(): React.ReactElement {
  return (
    <svg
      aria-hidden="true"
      data-name="Ebene 1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 14.83 14.54"
      fill="currentColor"
      className="anticon"
      height="1em"
      width="1em"
    >
      <path d="M14.08,9.05a.75.75,0,0,0-.75.75V13H1.5V9.8A.75.75,0,0,0,0,9.8v4a.75.75,0,0,0,.75.75H14.08a.74.74,0,0,0,.75-.75v-4A.74.74,0,0,0,14.08,9.05Z" />
      <path
        d={
          'M6.89,10.31a.84.84,0,0,0,.24.16.72.72,0,0,0,.29.06.71.71,0,0,0,.28-.06A1.07,1.07,0,0,0,8,10.31l2.63-2.64a.74.74,0,0,0,' +
          '0-1.06.75.75,0,0,0-1.06,0L8.17,8V.75a.75.75,0,0,0-1.5,0V8L5.31,6.61A.75.75,0,0,0,4.25,7.67Z'
        }
      />
    </svg>
  );
}
