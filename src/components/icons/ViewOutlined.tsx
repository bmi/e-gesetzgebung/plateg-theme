// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function ViewOutlined(): React.ReactElement {
  return (
    <svg
      aria-hidden="true"
      data-name="Ebene 1"
      className="anticon"
      fill="currentColor"
      xmlns="http://www.w3.org/2000/svg"
      width="1em"
      height="1em"
    >
      <g transform="translate(438.5 206)">
        <g transform="translate(-245.141 42.949)">
          <path
            d="M-192-242c0.1-0.1,0.1-0.2,0.2-0.3c0.5-0.9,1.1-1.6,1.8-2.3c0.6-0.5,1.2-0.9,1.9-1.1
			c0.4-0.1,0.8-0.2,1.2-0.3c0.8-0.1,1.6,0,2.4,0.2c0.8,0.3,1.6,0.7,2.2,1.4c0.7,0.7,1.3,1.5,1.7,2.3c0,0,0,0,0,0.1
			c-0.1,0.2-0.2,0.3-0.3,0.5c-0.4,0.8-1,1.5-1.7,2.1c-0.7,0.6-1.5,1.1-2.4,1.3c-0.6,0.1-1.2,0.2-1.8,0.2c-1.2-0.1-2.3-0.5-3.2-1.2
			c-0.7-0.5-1.3-1.2-1.7-1.9C-191.7-241.3-191.8-241.6-192-242C-192-241.9-192-241.9-192-242L-192-242z M-188.8-242
			c0,1.4,1.2,2.6,2.6,2.6l0,0c1.4,0,2.6-1.2,2.6-2.6c0-1.4-1.2-2.6-2.6-2.6l0,0C-187.7-244.6-188.8-243.4-188.8-242L-188.8-242z"
          />
          <path
            d="M-184.8-242c0,0.8-0.6,1.4-1.4,1.4c-0.8,0-1.4-0.6-1.4-1.4c0-0.8,0.6-1.4,1.4-1.4
			C-185.4-243.4-184.8-242.8-184.8-242z"
          />
        </g>
      </g>
    </svg>
  );
}
