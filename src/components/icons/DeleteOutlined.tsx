// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function DeleteOutlined(): React.ReactElement {
  return (
    <svg
      aria-hidden="true"
      data-name="Ebene 1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 14.5 16.06"
      fill="currentColor"
      className="anticon"
      height="1em"
      width="1em"
    >
      <path
        d={
          'M13.75,3H11.36L10.86.6a.75.75,0,0,0-.74-.6H4.38a.75.75,0,0,0-.73.6L3.14,3H.75a.75.75,0,0,0,0,1.5h.3l.79,10.87a.75.75,0,0,0,.75.' +
          '69h9.32a.75.75,0,0,0,.75-.69L13.45,4.5h.3a.75.75,0,0,0,0-1.5ZM5,1.5H9.51L9.83,3H4.68Zm6.22,13.06H3.29L2.56,4.5h9.38Z'
        }
      />
    </svg>
  );
}
