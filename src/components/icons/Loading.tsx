// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function Loading(): React.ReactElement {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="53" height="53" viewBox="0 0 53 53" overflow="visible">
      <g id="a" data-name="Loading Status" transform="translate(2 2)">
        <circle
          id="b"
          cx="24.5"
          cy="24.5"
          r="24.5"
          transform="translate(0 0)"
          fill="none"
          stroke="#f2f2f2"
          style={{ strokeWidth: '4' }}
        />
        <path
          id="c"
          d="M105.342,92.105a24.507,24.507,0,0,1,22.575,14.963"
          transform="translate(-80.842 -92.105)"
          fill="none"
          stroke="#0032d9"
          style={{ strokeWidth: '4', strokeDasharray: '189', strokeLinecap: 'round' }}
        ></path>
        <animateTransform
          attributeName="transform"
          type="rotate"
          dur="1s"
          repeatCount="indefinite"
          from="0 24.5 24.5"
          to="360 24.5 24.5"
          begin="0s"
        ></animateTransform>
      </g>
    </svg>
  );
}
