// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function BerechtigungenOutlined(props: { label?: string }): React.ReactElement {
  const svgProps = props.label ? { role: 'img', 'aria-label': props.label } : { 'aria-hidden': true };
  return (
    <svg
      {...svgProps}
      data-name="Ebene 1"
      xmlns="http://www.w3.org/2000/svg"
      fill="currentColor"
      className="anticon"
      width="24"
      height="24"
      viewBox="0 0 24 24"
    >
      <g id="Gruppe_13875" data-name="Gruppe 13875" transform="translate(1605.992 -30)">
        <g id="Gruppe_13871" data-name="Gruppe 13871" transform="translate(-1974.789 -366.49)">
          <g id="Gruppe_7667" data-name="Gruppe 7667" transform="translate(377.485 404.022)">
            <path
              id="Pfad_6680"
              data-name="Pfad 6680"
              d="M386.659,415.132a3.348,3.348,0,1,1,3.307-3.348A3.331,3.331,0,0,1,386.659,415.132Z"
              transform="translate(-383.353 -408.437)"
              fill="currentColor"
            />
          </g>
          <g id="Rechteck_3351-2" data-name="Rechteck 3351-2" transform="translate(372.79 400.49)">
            <path
              id="Pfad_6681"
              data-name="Pfad 6681"
              d="M389.655,429.523a7.327,7.327,0,0,1-9.085,0,7.495,7.495,0,0,1,9.085,0Z"
              transform="translate(-377.112 -415.767)"
              fill="none"
            />
            <path
              id="Pfad_6682"
              data-name="Pfad 6682"
              d="M388.957,409.324a7.281,7.281,0,0,1-2.147,5.187c-.044.044-.089.089-.138.129a8.122,8.122,0,0,0-10.1,0c-.049-.04-.093-.084-.138-.129a7.335,7.335,0,1,1,12.52-5.187Z"
              transform="translate(-373.623 -401.323)"
              fill="none"
            />
            <path
              id="Pfad_6683"
              data-name="Pfad 6683"
              d="M380.79,400.49a8,8,0,0,0-5.436,13.867c.169.16.342.307.524.449a8,8,0,0,0,9.823,0c.182-.142.356-.289.524-.449a8,8,0,0,0-5.436-13.867Zm5.187,13.187c-.044.044-.089.089-.138.129a8.122,8.122,0,0,0-10.1,0c-.049-.04-.093-.084-.138-.129a7.334,7.334,0,1,1,10.374,0Z"
              transform="translate(-372.79 -400.49)"
              fill="currentColor"
            />
          </g>
        </g>
      </g>
    </svg>
  );
}
