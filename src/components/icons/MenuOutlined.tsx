// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function MenuOutlined(): React.ReactElement {
  return (
    <svg
      aria-hidden="true"
      data-name="Ebene 1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 22 16"
      fill="currentColor"
      className="anticon"
      height="1em"
      width="1em"
    >
      <rect data-name="Rechteck 2683" fill="none" x="-1" y="-4" width="24" height="24" />
      <path fill="#fff" d="M1,2H21a1,1,0,0,0,0-2H1A1,1,0,0,0,1,2Z" />
      <path fill="#fff" d="M21,7H1A1,1,0,0,0,1,9H21a1,1,0,0,0,0-2Z" />
      <path fill="#fff" d="M21,14H1a1,1,0,0,0,0,2H21a1,1,0,0,0,0-2Z" />
    </svg>
  );
}
