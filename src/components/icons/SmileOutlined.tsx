// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function SmileOutlined(): React.ReactElement {
  const styles =
    '.smile_a{fill:#0032d9;}.smile_b{fill:#fff;stroke:#0032d9;stroke-width:4px;}.smile_c{clip-path:url(#a);}.smile_d{fill:none;}.smile_e{stroke:none;}</style>';
  return (
    <svg aria-hidden="true" width="72" height="72" viewBox="0 0 72 72">
      <defs>
        <style>{styles}</style>
        <clipPath>
          <rect className="smile_a" width="24" height="10.163" />
        </clipPath>
      </defs>
      <g transform="translate(-371 -6483)">
        <g className="smile_b" transform="translate(375 6487)">
          <circle className="smile_e" cx="32" cy="32" r="32" />
          <circle className="smile_d" cx="32" cy="32" r="30" />
        </g>
        <circle className="smile_a" cx="4" cy="4" r="4" transform="translate(394 6510)" />
        <circle className="smile_a" cx="4" cy="4" r="4" transform="translate(412 6510)" />
        <g transform="translate(395 6525.094)">
          <g className="smile_c" transform="translate(0 0)">
            <path
              className="smile_a"
              d="M20.168,1.022a11.24,11.24,0,0,1-1.063,1.67l.325-.418a11.542,11.542,0,0,1-2.046,2.057l.418-.325a11.6,11.6,0,0,1-2.516,1.47c.165-.067.33-.139.495-.206a11.318,11.318,0,0,1-2.814.763c.18-.026.366-.051.547-.072a11.51,11.51,0,0,1-3.036,0c.18.026.366.052.546.072A11.548,11.548,0,0,1,8.209,5.27c.165.067.33.139.495.206a11.386,11.386,0,0,1-2.515-1.47l.417.325A11.522,11.522,0,0,1,4.559,2.275l.325.418a11.681,11.681,0,0,1-1.062-1.67A2.078,2.078,0,0,0,1,.28,2.112,2.112,0,0,0,.26,3.1,13.294,13.294,0,0,0,8.982,9.831,13.383,13.383,0,0,0,19.1,8.115a13.372,13.372,0,0,0,4.629-5.021,2.184,2.184,0,0,0,.206-1.588A2.1,2.1,0,0,0,22.982.275a2.1,2.1,0,0,0-2.814.747"
              transform="translate(0 0)"
            />
          </g>
        </g>
        <rect className="smile_d" width="72" height="72" transform="translate(371 6483)" />
      </g>
    </svg>
  );
}
