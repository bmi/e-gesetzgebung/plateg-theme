// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FunctionComponent } from 'react';
import * as IconComponents from '.';

export type Icons = keyof typeof IconComponents;
export interface IconProps {
  active?: boolean;
  hasHover?: boolean;
  disabled?: boolean;
}
export interface DefaultIconProps extends IconProps {
  defaultIcon: Icons;
  activeIcon?: Icons;
  hoverIcon?: Icons;
  disabledIcon?: Icons;
}

export const Icon: FunctionComponent<DefaultIconProps> = ({
  defaultIcon,
  activeIcon,
  hoverIcon,
  hasHover,
  active,
  disabled,
  disabledIcon,
  ...props
}) => {
  const icon: Icons | undefined = getIcon(active, activeIcon, hasHover, hoverIcon, defaultIcon, disabled, disabledIcon);
  const Component = IconComponents[icon || defaultIcon];
  return <Component {...props} />;
};

const getIcon = (
  active?: boolean,
  activeIcon?: Icons,
  hasHover?: boolean,
  hoverIcon?: Icons,
  defaultIcon?: Icons,
  disabled?: boolean,
  disabledIcon?: Icons,
) => {
  if (disabled) {
    return disabledIcon;
  } else if (active) {
    return activeIcon;
  } else if (hasHover) {
    return hoverIcon;
  } else {
    return defaultIcon;
  }
};

export const ThreeDotsIcon: FunctionComponent<IconProps> = ({ active, hasHover, disabled }) => {
  return (
    <Icon
      defaultIcon="ThreeDots"
      hoverIcon="ThreeDotsHover"
      activeIcon="ThreeDotsActive"
      disabledIcon="ThreeDotsDisabled"
      active={active}
      hasHover={hasHover}
      disabled={disabled}
    />
  );
};
