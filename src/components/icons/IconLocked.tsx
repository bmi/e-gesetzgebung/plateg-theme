// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function IconLocked(props: { label?: string }): React.ReactElement {
  const svgProps = props.label ? { role: 'img', 'aria-label': props.label } : { 'aria-hidden': true };
  return (
    <svg
      {...svgProps}
      data-name="Ebene 1"
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="currentColor"
      className="anticon"
    >
      <rect width="24" height="24" fill="none" />
      <g transform="translate(-1384 -766)">
        <circle
          cx="9"
          cy="9"
          r="9"
          transform="translate(1405 787) rotate(180)"
          stroke="#000"
          style={{ strokeWidth: '1' }}
        />
        <path
          d="M8.091,10.5H.386A.407.407,0,0,1,0,10.075v-5.1a.408.408,0,0,1,.386-.425H1.2V2.7A1.628,1.628,0,0,1,2.745,1H5.731A1.628,1.628,0,0,1,7.272,2.7V4.554h.818a.408.408,0,0,1,.386.425v5.1A.407.407,0,0,1,8.091,10.5ZM4.238,6.356A.972.972,0,0,0,3.7,6.52a.86.86,0,0,0-.334.431.8.8,0,0,0,0,.531.875.875,0,0,0,.337.428v.947A.148.148,0,0,0,3.855,9H4.62a.148.148,0,0,0,.153-.143V7.909a.863.863,0,0,0,.337-.428.792.792,0,0,0,0-.531.852.852,0,0,0-.333-.431A.96.96,0,0,0,4.238,6.356ZM2.745,1.822a.621.621,0,0,0-.673.546V4.554H6.4V2.368a.621.621,0,0,0-.673-.546Z"
          transform="translate(1391.762 771.751)"
          fill="#fff"
        />
      </g>
    </svg>
  );
}
