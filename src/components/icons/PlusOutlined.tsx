// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function PlusOutlined(): React.ReactElement {
  return (
    <svg
      aria-hidden="true"
      data-name="Ebene 1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 13.5 13.5"
      fill="currentColor"
      className="anticon"
      height="1em"
      width="1em"
    >
      <path
        className="cls-1"
        d="M12.75,6H7.5V.75A.75.75,0,0,0,6,.75V6H.75a.75.75,0,0,0,0,1.5H6v5.25a.75.75,0,0,0,1.5,0V7.5h5.25a.75.75,0,0,0,0-1.5Z"
      />
    </svg>
  );
}
