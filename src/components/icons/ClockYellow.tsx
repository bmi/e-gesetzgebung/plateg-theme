// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function ClockYellow(): React.ReactElement {
  return (
    <svg
      aria-hidden="true"
      data-name="Ebene 1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 19 19"
      fill="currentColor"
      height="19"
      width="19"
    >
      <path
        d="M11.36,.18C9.51-.18,7.6,0,5.87,.72c-1.74,.72-3.22,1.94-4.26,3.5C.56,5.78,0,7.62,0,9.5H0c0,2.52,1,4.93,2.78,6.72,1.78,1.78,4.2,2.78,6.71,2.79h0c1.88,0,3.72-.56,5.28-1.6,1.56-1.04,2.78-2.53,3.5-4.26,.72-1.74,.91-3.65,.54-5.49-.37-1.84-1.27-3.54-2.6-4.87C14.89,1.46,13.2,.55,11.36,.18Z"
        fill="#faad14"
        fillRule="evenodd"
      />
      <path d="M9.05,10.63l3.26,1.91h0c.14,.08,.3,.12,.45,.12,.16,0,.31-.04,.45-.12,.14-.08,.25-.19,.33-.32,.06-.1,.1-.21,.12-.33,.02-.12,0-.24-.02-.35s-.08-.22-.15-.31c-.07-.09-.16-.17-.26-.23l-2.81-1.65V4.69c-.01-.23-.11-.45-.28-.6-.17-.16-.39-.25-.62-.25s-.45,.09-.62,.25c-.17,.16-.27,.37-.28,.6v5.16c0,.16,.04,.31,.12,.44,.08,.14,.19,.25,.32,.33Z" />
    </svg>
  );
}
