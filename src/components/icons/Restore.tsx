// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

interface RestoreSVGIcon {
  uniqueId?: string;
}
export function Restore(props: RestoreSVGIcon): React.ReactElement {
  return (
    <svg
      aria-hidden="true"
      data-name="Restore 1"
      className="anticon"
      fill="currentColor"
      xmlns="http://www.w3.org/2000/svg"
      width="1em"
      height="1em"
      viewBox="0 0 15.932 15.789"
    >
      <g id={`Gruppe_4235-${props.uniqueId as string}`} data-name="Gruppe 4235" transform="translate(-4.11 -3.858)">
        <path
          id={`Pfad_5049-${props.uniqueId as string}`}
          data-name="Pfad 5049"
          d="M122.524,75.327h9.061a5.119,5.119,0,1,1,0,10.227h-3.056"
          transform="translate(-117.664 -66.656)"
          fill="none"
          stroke="#0032d9"
          style={{ strokeWidth: '1.5', strokeLinecap: 'round' }}
        />
        <path
          id={`Pfad_5050-${props.uniqueId as string}`}
          data-name="Pfad 5050"
          d="M126.274,81.094l-3.751-3.751,3.751-3.751"
          transform="translate(-117.664 -68.674)"
          fill="none"
          stroke="#0032d9"
          style={{ strokeWidth: '1.5', strokeLinejoin: 'round', strokeLinecap: 'round' }}
        />
      </g>
    </svg>
  );
}
