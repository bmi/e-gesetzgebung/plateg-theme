// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

export function GrayArrowDown(): React.ReactElement {
  return (
    <svg
      aria-hidden="true"
      data-name="Ebene 1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 8 5"
      fill="currentColor"
      className="anticon"
      height="1em"
      width="1em"
    >
      <path data-name="Polygon 2-7" fill="#909090" d="M4,5,0,0H8Z" />
    </svg>
  );
}
