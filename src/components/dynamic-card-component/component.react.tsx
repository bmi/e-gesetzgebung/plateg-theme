// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './dynamic-card.less';

import { Button, Card, Form, FormInstance, Image, Space, Tooltip, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';

import { CheckOutlinedGreen } from '../icons/CheckOutlinedGreen';
import { EditOutlined } from '../icons/EditOutlined';

export interface DynamicCardComponentProps {
  image?: ImageItem;
  headline?: string;
  headlineEdit?: string;
  subHeadline?: string;
  description?: string;
  buttonText?: string;
  buttonRoute?: string;
  form?: FormInstance;
  fieldLabels?: Record<string, string>;
  children?: React.ReactNode;
  onStateChange?: (isFormView: boolean, isFinishView: boolean, values: FormValues) => void;
  onFormSubmit?: (values: FormValues) => void;
  preSelectedValues?: FormValues;
  /**
   * Defines the view at which the component opens
   * */
  startView?: ViewType;
  customRenderMethod?: (value: React.ReactNode) => React.ReactElement;
}

interface ImageItem {
  label: string;
  imgSrc: ImageModule;
  height?: number;
  imgAlt?: string;
}

export interface ImageModule {
  default: string;
}

export interface FormValues {
  [key: string]: string | number | React.ReactNode;
}

export enum ViewType {
  Initial,
  Form,
  Finish,
}

export function DynamicCardComponent(props: DynamicCardComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const defaultImage: ImageItem = {
    label: t('theme.dynamicCardComponent.defaultImageLabel'),
    imgSrc: (require('../../media/empty-state-table.svg') as ImageModule) || '',
    height: 215,
    imgAlt: t('theme.dynamicCardComponent.defaultImageAlt'),
  };
  const { buttonRoute, buttonText, form, fieldLabels, children } = props;
  const history = useHistory();
  const image = props.image || defaultImage;
  const { Title } = Typography;

  const [currentView, setCurrentView] = useState<ViewType>(props.startView || ViewType.Initial);
  const [isHeadlineEdit, setIsHeadlineEdit] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [hasErrors, setHasErrors] = useState(false);
  const [formValues, setFormValues] = useState<FormValues>({});
  const values = Form.useWatch([], form);

  useEffect(() => {
    setCurrentView(props.preSelectedValues ? ViewType.Finish : ViewType.Initial);
    setFormValues(props.preSelectedValues || {});
  }, [props.startView]);

  useEffect(() => {
    if (props.preSelectedValues && props.form) {
      props.form.setFieldsValue(props.preSelectedValues);
    }
  }, [props.preSelectedValues, props.form]);

  useEffect(() => {
    props.onStateChange?.(currentView === ViewType.Form, currentView === ViewType.Finish, formValues);
  }, [currentView, formValues, props.onStateChange]);

  useEffect(() => {
    form?.validateFields({ validateOnly: true }).then(
      () => {
        // No errors
        setHasErrors(false);
      },
      () => {
        // Has errors
        setHasErrors(true);
      },
    );
  }, [form, values]);

  const toggleFormVisibility = () => {
    if (currentView === ViewType.Form) {
      form
        ?.validateFields()
        .then((values) => {
          // Call onFormSubmit and wait for it to complete
          Promise.resolve(props.onFormSubmit?.(values)).then((updatedValues) => {
            // Use updatedValues if available, otherwise fall back to original values
            const finalValues = updatedValues || values;
            setFormValues(finalValues);
            setIsHeadlineEdit(false);
            setCurrentView(ViewType.Finish);
            setHasErrors(false);
            setIsEditing(false);
          });
        })
        .catch(() => {
          setHasErrors(true);
        });
    } else {
      setIsHeadlineEdit(true);
      setCurrentView(ViewType.Form);
      setIsEditing(currentView === ViewType.Finish);
    }
  };

  const handleCancel = () => {
    if (isEditing) {
      setCurrentView(ViewType.Finish);
      form?.setFieldsValue(formValues);
    } else {
      setCurrentView(ViewType.Initial);
      form?.resetFields();
    }
    setIsHeadlineEdit(false);
    setHasErrors(false);
    setIsEditing(false);
  };

  const styles = {
    header: {
      borderBottom: 'none',
    },
  };
  const cardTitle = (
    <Title level={2} className={'card-icon-title'}>
      {currentView === ViewType.Finish && (
        <span className="icon-wrapper">
          <CheckOutlinedGreen />
        </span>
      )}
      {isHeadlineEdit ? props.headlineEdit : props.headline}
    </Title>
  );

  return (
    <span className="dynamic-card-holder">
      <Card
        {...(props.headline ? { title: cardTitle } : {})}
        styles={styles}
        extra={
          currentView === ViewType.Finish && (
            <Tooltip title={t('theme.dynamicCardComponent.editInput')}>
              <Button
                className={'edit-btn'}
                icon={<EditOutlined />}
                onClick={() => {
                  setIsEditing(true);
                  toggleFormVisibility();
                }}
              />
            </Tooltip>
          )
        }
      >
        {currentView === ViewType.Form ? (
          <>
            {children}
            <Space direction="horizontal" size={'middle'} className={`${hasErrors ? 'has-errors' : ''}`}>
              <Button onClick={toggleFormVisibility} type={'primary'} htmlType={'submit'}>
                {t('theme.dynamicCardComponent.checkBtn')}
              </Button>
              <Button onClick={handleCancel}>{t('theme.dynamicCardComponent.cancelBtn')}</Button>
            </Space>
          </>
        ) : currentView === ViewType.Finish ? (
          <div className="finish-view">
            <dl>
              {Object.entries(formValues).map(([key, value]) => (
                <>
                  <dt>{`${!fieldLabels || fieldLabels[key] || key}:`}</dt>
                  <dd>{props.customRenderMethod ? props.customRenderMethod(value) : value}</dd>
                </>
              ))}
            </dl>
          </div>
        ) : (
          <div className="default-container">
            <Image
              preview={false}
              src={image.imgSrc.default}
              height={image.height}
              alt={image.imgAlt}
              loading={'lazy'}
            />
            <div className="subheadline-description">
              <Title level={3}>{props.subHeadline}</Title>
              <p className="ant-typography p-no-style">{props.description}</p>
            </div>
            {(buttonRoute || buttonText) && (
              <Button
                className="ant-btn-secondary dynamic-card-btn"
                size={'large'}
                onClick={buttonRoute ? () => history.push(buttonRoute) : toggleFormVisibility}
                icon={<EditOutlined />}
              >
                {props.buttonText}
              </Button>
            )}
          </div>
        )}
      </Card>
    </span>
  );
}
