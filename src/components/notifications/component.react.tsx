// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './notifications.less';

import { Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import { RightDirectionArrow } from '../icons';
import { ExclamationPointOutlined } from '../icons/ExclamationPointOutlined';
import { IconLockedLarge } from '../icons/IconLockedLarge';
import { IconUnlockedLarge } from '../icons/IconUnlockedLarge';
import { SmileOutlined } from '../icons/SmileOutlined';

interface NotificationPageProps {
  title: string;
  text: string | React.ReactElement;
  link?: string;
  linkText?: string;
  type: string;
  additionalButton?: React.ReactElement;
}

export function NotificationPage(props: Readonly<NotificationPageProps>): React.ReactElement {
  const { Title } = Typography;

  useEffect(() => {
    document.querySelector('.header-component')?.classList.add('hidden');
    return () => {
      document.querySelector('.header-component')?.classList.remove('hidden');
    };
  }, []);

  // FF trick for screenreader
  const [title, setTitle] = useState('');
  useEffect(() => {
    setTimeout(() => {
      setTitle(props.title);
    }, 200);
  }, [props.title]);

  return (
    <div className={`notification ${props.type}`}>
      <div className="holder">
        <div className="icon-holder">
          {props.type === 'success' && <SmileOutlined />}
          {props.type === 'error' && <ExclamationPointOutlined />}
          {props.type === 'locked' && <IconLockedLarge />}
          {props.type === 'unlocked' && <IconUnlockedLarge />}
        </div>
        <Title level={1}>{props.title}</Title>
        <p className="ant-typography p-no-style">{props.text}</p>
        {props.link && (
          <Link
            className="link"
            id={`theme-generic-notificationPage-${props.title.toLowerCase().split(' ').join('-')}-link`}
            to={props.link}
          >
            {props.linkText}
            <RightDirectionArrow />
          </Link>
        )}
        {/* FF trick for screenreader */}
        <p role="alert" className="notification-alert-title">
          {title}
        </p>
        {props.additionalButton}
      </div>
    </div>
  );
}
