// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

import { NotificationPage } from '../component.react';

interface SuccessPageProps {
  title: string;
  text: string | React.ReactElement;
  link: string;
  linkText: string;
}

export function SuccessPage(props: SuccessPageProps): React.ReactElement {
  return (
    <NotificationPage
      title={props.title}
      text={props.text}
      link={props.link}
      linkText={props.linkText}
      type="success"
    />
  );
}
