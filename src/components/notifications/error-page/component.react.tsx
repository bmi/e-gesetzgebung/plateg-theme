// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

import { NotificationPage } from '../component.react';

interface ErrorPageProps {
  title: string;
  text: string | React.ReactElement;
  link: string;
  linkText: string;
  additionalButton?: React.ReactElement;
}

export function ErrorPage(props: ErrorPageProps): React.ReactElement {
  return (
    <NotificationPage
      title={props.title}
      text={props.text}
      link={props.link}
      linkText={props.linkText}
      type="error"
      additionalButton={props.additionalButton}
    />
  );
}
