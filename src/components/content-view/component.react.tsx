// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FilterOptionsRequestDTOSortByEnum } from '@ggp/rest-api';
import { PaginierungDTOSortByEnum } from '@plateg/rest-api';
import type { GetProp, TableProps } from 'antd';
import { ColumnsType, TablePaginationConfig } from 'antd/lib/table';
import { Key, SorterResult } from 'antd/lib/table/interface';
import Text from 'antd/lib/typography/Text';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Observable } from 'rxjs';

import { CardGridComponent } from '../card-grid/component.react';
import { useAppDispatch, useAppSelector } from '../store';
import { setRequestCurrentPage, setRequestSorting } from '../store/slices/tablePaginationSlice';
import { SortedInfo } from '../table-component';
import { ExpandOptionsProps, TableComponent } from '../table-component-new/component.react';
import { ItemsPerPagePickerComponent } from '../table-component/table-sub-components/items-per-page-picker/component.react';
import { FilterBarComponent } from './filter-bar/component.react';
import { DisplayMode, DisplayModeComponent } from './filter-bar/display-mode/component.react';
import { CustomFilterElementsProps, FilterBeOptions } from './filter-bar/filter-be-options-component/component.react';
import { FilterOptions } from './filter-bar/filter-options-component/component.react';
import { SearchComponent } from './filter-bar/search-component/component.react';
import { SorterComponent } from './filter-bar/sorter-component/component.react';

type TableScrollType = GetProp<TableProps<CommonRow>, 'scroll'>;
type TableStickyType = GetProp<TableProps<CommonRow>, 'sticky'>;

export interface PagData {
  totalItems: number;
}

export interface PagDataReq {
  currentPage: number;
  columnKey: PaginierungDTOSortByEnum;
  sortOrder: string | null;
}

export interface CommonRow {
  id: string;
}

export interface ActiveFilter {
  filteredBy: string;
  column: { name: string; columnIndex: number };
}

interface SortOptionsProps {
  config?: { columnKey: React.Key; titleAsc: string; titleDesc: string }[];
  defaultSortIndex?: number;
}

interface FilterOptionsProps<T extends CommonRow> {
  filteredColumns?: { name: string; columnIndex: number }[];
  filterRowsMethod?: (actualContent: T[], filteredBy: string, column: { name: string; columnIndex: number }) => T[];
  prepareFilterButtonMethod?: (
    initialContent: T[],
    column: { name: string; columnIndex: number },
  ) => { displayName: string; labelContent: string; options: Set<string> };
  isFilteringWithFetch?: boolean;
  customFilterButton?: React.ReactElement;
  customFilterContent?: React.ReactElement;
  showCustomFilterDrawer?: boolean;
  setShowCustomFilterDrawer?: (boolean: boolean) => void;
  customFilterElements?: ({ onUpdateFilterValues }: CustomFilterElementsProps) => React.ReactElement;
}

interface SearchOptionsProps {
  isSearchable?: boolean;
  suggestionFn?: (value: string) => Observable<Array<SearchSuggestions>>;
}

interface TableOptionsProps<T extends CommonRow> {
  expandOptions?: ExpandOptionsProps<T>;
  autoHeight?: boolean;
  rowClassName?: (record: CommonRow, index: number) => string;
  className?: string;
  customButtonAriaLables?: { closed: string; opened: string };
  scroll?: TableScrollType;
  sticky?: TableStickyType;
}

export interface SearchSuggestions {
  type: string;
  id: string;
  name: string;
}

export interface ContentViewProps<T extends CommonRow> {
  id: string;
  columns: ColumnsType<T>;
  content: T[];
  tabKey?: string;
  sortOptions?: SortOptionsProps;
  filterOptions?: FilterOptionsProps<T>;
  displayMode?: DisplayMode;
  isDisplayModeChangeable?: boolean;
  onDisplayModeChange?: (displayMode: DisplayMode) => void;
  hiddenRowCount?: boolean;
  additionalRightControl?: React.ReactElement;
  isLight?: boolean;
  scrollElement?: HTMLElement | null;
  itemsPerPage?: number;
  bePagination?: boolean;
  itemsPerPagePicker?: boolean;
  cardComponent: (item: T) => React.ReactElement;
  tableOptions?: TableOptionsProps<T>;
  searchOptions?: SearchOptionsProps;
}

const getColumnKey = (columnKey?: string) => {
  switch (columnKey) {
    case 'bearbeitetam':
      return PaginierungDTOSortByEnum.ZuletztBearbeitet;
    case 'erstellt':
      return PaginierungDTOSortByEnum.ErstelltAm;
    case 'archiviertAm':
      return PaginierungDTOSortByEnum.ArchiviertAm;
    case 'vorlageDauer':
      return PaginierungDTOSortByEnum.ZeitplanungsvorlageDauer;
    case 'fristablauf':
      return PaginierungDTOSortByEnum.Frist;
    case 'fristablaufNeu':
      return PaginierungDTOSortByEnum.NeueFrist;
    case 'initiant':
      return FilterOptionsRequestDTOSortByEnum.Initiant;
    case 'erstelltAm':
      return FilterOptionsRequestDTOSortByEnum.ErstelltAm;
    case 'typ':
      return FilterOptionsRequestDTOSortByEnum.Typ;
    case 'titel':
      return FilterOptionsRequestDTOSortByEnum.Titel;
    default:
      return undefined;
  }
};

export function ContentViewComponent<T extends CommonRow>(props: Readonly<ContentViewProps<T>>): React.ReactElement {
  const { t } = useTranslation();

  const {
    id,
    sortOptions,
    filterOptions,
    isDisplayModeChangeable = false,
    onDisplayModeChange,
    isLight,
    tabKey,
    bePagination,
    cardComponent,
  } = props;

  const {
    filteredColumns = [],
    filterRowsMethod,
    prepareFilterButtonMethod,
    isFilteringWithFetch,
    customFilterButton,
    customFilterContent,
    showCustomFilterDrawer,
    setShowCustomFilterDrawer,
    customFilterElements,
  } = filterOptions || {};

  const columns = props.columns;
  const [content, setContent] = useState(props.content);

  useEffect(() => {
    setContent(props.content);
  }, [props.content]);

  const [rowCount, setRowCount] = useState('');

  const [sortedInfo, setSortedInfo] = useState<SortedInfo>();
  const [currentPage, setCurrentPage] = useState<number | undefined>(1);
  const [activeFiltersNumber, setActiveFiltersNumber] = useState<number | undefined>(0);
  const [displayMode, setDisplayMode] = useState<DisplayMode>(props.displayMode || DisplayMode.Table);

  const [itemsPerPage, setItemsPerPage] = useState<number>(props.itemsPerPage || 20);
  const dispatch = useAppDispatch();
  const tablePaginationTab = useAppSelector((state) => state.tablePagination.tabs[props.tabKey as string]);
  const pagInfoResult = props.tabKey ? tablePaginationTab.result : undefined;

  useEffect(() => {
    if (pagInfoResult?.currentPage !== undefined && props.bePagination) {
      setCurrentPage(pagInfoResult?.currentPage + 1);
    }
  }, [pagInfoResult?.currentPage]);

  useEffect(() => {
    if (!currentPage) return;

    const totalEntries = props.bePagination ? pagInfoResult?.totalItems || 0 : content.length;

    if (totalEntries === 0) {
      setRowCount(t('theme.contentView.filterBar.noEntries'));
      return;
    }

    const firstEntryOnPage = Math.min((currentPage - 1) * itemsPerPage + 1, totalEntries);
    const lastEntryOnPage = Math.min(currentPage * itemsPerPage, totalEntries);

    setRowCount(
      t('theme.contentView.filterBar.entries', {
        first: firstEntryOnPage,
        last: lastEntryOnPage,
        count: totalEntries,
      }),
    );
  }, [currentPage, itemsPerPage, content.length, pagInfoResult?.totalItems]);

  useEffect(() => {
    setCurrentPage(1);
  }, [itemsPerPage]);

  const handleChange = (
    pagination: TablePaginationConfig,
    _filtersRecord: Record<string, (boolean | Key)[] | null>,
    sorter: SorterResult<T> | SorterResult<T>[],
  ) => {
    let singleSorter: SorterResult<T>;
    if (!Array.isArray(sorter)) {
      singleSorter = sorter;
    } else {
      singleSorter = sorter[0];
    }

    if (singleSorter.columnKey && currentPage === pagination.current) {
      const sortedInfo = new SortedInfo(singleSorter.order || null, singleSorter.columnKey);
      setSortedInfo(sortedInfo);
      if (props.bePagination && props.tabKey) {
        setCurrentPage(1);
        dispatch(
          setRequestSorting({
            tabKey: props.tabKey,
            columnKey: getColumnKey(sortedInfo.columnKey as string),
            sortOrder: sortedInfo.order,
          }),
        );
      }
    }

    if (pagination.current) {
      handlePageChange(pagination.current);
    }
  };

  const handlePageChange = (page: number) => {
    if (currentPage !== page) {
      setCurrentPage(page);
      if (props.bePagination && props.tabKey) {
        dispatch(
          setRequestCurrentPage({
            tabKey: props.tabKey,
            currentPage: page,
          }),
        );
      }
      scrollIntoView();
    }
  };

  const getAbsoluteYPosition = (element: HTMLElement) => {
    const rect = element.getBoundingClientRect();
    const scrollTop = window.scrollY || document.documentElement.scrollTop;
    return rect.top + scrollTop;
  };

  const scrollIntoView = () => {
    setTimeout(() => {
      window.scrollTo({
        top: props.scrollElement ? getAbsoluteYPosition(props.scrollElement) : 0,
        behavior: props.scrollElement ? 'auto' : 'smooth',
      });
    }, 15);
  };

  const setSortedInfoHandler = (sortedInfo: SortedInfo) => {
    setSortedInfo(sortedInfo);
    if (props.bePagination && props.tabKey) {
      setCurrentPage(1);
      dispatch(
        setRequestSorting({
          tabKey: props.tabKey,
          columnKey: getColumnKey(sortedInfo.columnKey as string),
          sortOrder: sortedInfo.order,
        }),
      );
    }
  };

  const sorterElements = sortOptions ? (
    <SorterComponent
      setSortedInfo={setSortedInfoHandler}
      sortedInfo={sortedInfo}
      sorterOptions={props.sortOptions?.config}
      customDefaultSortIndex={sortOptions.defaultSortIndex}
      elementId={props.id}
      rowCount={rowCount}
    />
  ) : (
    <></>
  );

  const handleSearchChange = (query: string) => {
    if (!query) {
      setContent(props.content);
    }
  };

  const searchbarElements = props.searchOptions?.isSearchable ? (
    <SearchComponent
      tabKey={props.tabKey || ''}
      onSearchChange={handleSearchChange}
      suggestionFn={props.searchOptions.suggestionFn}
    />
  ) : undefined;

  const handleDisplayModeChange = (displayMode: DisplayMode) => {
    setDisplayMode(displayMode);
    if (onDisplayModeChange) {
      onDisplayModeChange(displayMode);
    }
  };

  const displayModeElements = isDisplayModeChangeable ? (
    <DisplayModeComponent
      elementId={props.id}
      onSelectDisplayMode={handleDisplayModeChange}
      activeDisplayMode={displayMode}
    />
  ) : undefined;

  const filterElements =
    filteredColumns.length > 0 ? (
      <>
        {props.bePagination ? (
          <FilterBeOptions
            tabKey={props.tabKey}
            parentId={props.id}
            filterOnSubmit={true}
            setActiveFiltersNumber={setActiveFiltersNumber}
            customFilterElements={customFilterElements}
          />
        ) : (
          !(props.tabKey === 'meineZeitplanungen' && props.isLight) && ( // no filter for zeit light 1. tab
            <FilterOptions
              content={props.content}
              actualContent={content}
              setContent={setContent}
              filterRowsMethod={filterRowsMethod}
              filteredColumns={filteredColumns}
              prepareFilterButtonMethod={prepareFilterButtonMethod}
              isFilteringWithFetch={isFilteringWithFetch}
              parentId={props.id}
              filterOnSubmit={true}
              setActiveFiltersNumber={setActiveFiltersNumber}
              activeFiltersNumber={activeFiltersNumber}
            />
          )
        )}
      </>
    ) : undefined;

  const itemsPerPagePicker = props.itemsPerPagePicker ? (
    <ItemsPerPagePickerComponent setItemsPerPage={setItemsPerPage} itemsPerPage={itemsPerPage} />
  ) : undefined;

  const rowCountElements = (
    <>
      <div hidden={props.hiddenRowCount} className="row-count-wrapper">
        <Text className={sortOptions ? 'table-row-count' : 'table-row-count table-row-count-with-sorter'}>
          {rowCount}
        </Text>
      </div>
      {props.additionalRightControl}
    </>
  );

  return (
    <div className="content-view">
      <FilterBarComponent
        sorterElements={sorterElements}
        filterElements={filterElements}
        rowCountElements={rowCountElements}
        activeFiltersNumber={activeFiltersNumber}
        customFilterButton={customFilterButton}
        customFilterContent={customFilterContent}
        showCustomFilterDrawer={showCustomFilterDrawer}
        setShowCustomFilterDrawer={setShowCustomFilterDrawer}
        itemsPerPagePicker={itemsPerPagePicker}
        displayModeElements={displayModeElements}
        searchComponent={searchbarElements}
      />
      {displayMode === DisplayMode.Table ? (
        <TableComponent
          {...{ id, columns, content, tabKey, isLight, itemsPerPage, bePagination, sortedInfo }}
          {...props.tableOptions}
          onChange={handleChange}
        />
      ) : (
        <CardGridComponent
          {...{ id, columns, content, tabKey, itemsPerPage, bePagination, sortedInfo, cardComponent }}
          onPageChange={handlePageChange}
        />
      )}
    </div>
  );
}
