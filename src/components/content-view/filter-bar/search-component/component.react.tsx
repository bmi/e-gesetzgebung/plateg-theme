// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './search.less';

import { Button, Input, Row, Space } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { Observable } from 'rxjs';

import { SearchSuggestions } from '../..';
import { MagnifyingGlass } from '../../../icons/MagnifyingGlass';
import { displayMessage } from '../../../messages/message-service';
import { useAppDispatch, useAppSelector } from '../../../store';
import { setRequestSearchquery } from '../../../store/slices/tablePaginationSlice';

interface SearchProps {
  tabKey: string;
  onSearchChange?: (value: string) => void;
  suggestionFn?: (value: string) => Observable<Array<SearchSuggestions>>;
  onSuggestionSelect?: (value: string) => void;
}

export function SearchComponent(props: SearchProps): React.ReactElement {
  const { t } = useTranslation();
  const dispatch = useAppDispatch();

  const [showSearch, setShowSearch] = useState(false);
  const [query, setQuery] = useState('');
  const [showSuggestions, setShowSuggestions] = useState(true);
  const [filteredSuggestions, setFilteredSuggestions] = useState<SearchSuggestions[]>([]);
  const searchRef = useRef<HTMLDivElement>(null);
  const [isSearchSubmitted, setIsSearchSubmitted] = useState(false);
  const [activeSearchNumber, setActiveSearchNumber] = useState(0);

  const pagInfoRequest = useAppSelector((state) => state.tablePagination.tabs[props.tabKey]).request;
  const pagInfoResult = useAppSelector((state) => state.tablePagination.tabs[props.tabKey]).result;

  const openSearchInput = () => {
    setQuery(pagInfoRequest.searchQuery || '');
    setShowSearch(true);
  };

  useEffect(() => {
    if (isSearchSubmitted && pagInfoResult.totalItems !== undefined) {
      showSearchedMsg(query, pagInfoResult.totalItems);
      setActiveSearchNumber(query.length ? 1 : 0);
      setIsSearchSubmitted(false);
    }
  }, [pagInfoResult.filterInfo]);

  const showSearchedMsg = (query: string, newRowsCount: number) => {
    const filterEntriesMsg = `<div style="text-align: left;">${t('theme.contentView.filterBar.filterEntriesMsg', { count: newRowsCount })}</div>`;
    let msg = '';

    if (!query.length) {
      msg = t('theme.contentView.filterBar.searchResetMsg');
    } else {
      msg = t('theme.contentView.filterBar.searchSuccessMsg', { val: query });
    }

    displayMessage(`${msg} ${filterEntriesMsg}`, 'success');
  };

  const onSearchSubmit = () => {
    setShowSuggestions(false);
    dispatch(
      setRequestSearchquery({
        tabKey: props.tabKey,
        searchQuery: query,
      }),
    );
    setIsSearchSubmitted(true);
  };

  const resetSearch = () => {
    setQuery('');
    setShowSearch(false);
    dispatch(
      setRequestSearchquery({
        tabKey: props.tabKey,
        searchQuery: '',
      }),
    );
    setIsSearchSubmitted(true);
  };

  const handleSearchChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value;
    setQuery(value);
    setShowSuggestions(true);

    if (props.onSearchChange) {
      props.onSearchChange(value);
    }
    if (props.suggestionFn && value.length >= 2) {
      props.suggestionFn(value).subscribe((suggestions) => {
        setFilteredSuggestions(suggestions);
      });
    } else {
      setFilteredSuggestions([]);
    }
  };

  const handleSuggestionClick = (suggestion: string) => {
    setQuery(suggestion);
    if (props.onSuggestionSelect) {
      props.onSuggestionSelect(suggestion);
    }
    setShowSuggestions(false);
  };

  const handleClick = (event: MouseEvent) => {
    if (searchRef.current && !searchRef.current.contains(event.target as Node)) {
      setShowSearch(false);
      setQuery('');
      setShowSuggestions(false);
    }
  };

  const handleKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter') {
      onSearchSubmit();
    }
  };

  useEffect(() => {
    document.addEventListener('mousedown', handleClick);
    return () => {
      document.removeEventListener('mousedown', handleClick);
    };
  }, []);

  const highlightQuery = (text: string, query: string) => {
    const lowerCaseText = text.toLowerCase();
    const lowerCaseQuery = query.toLowerCase();
    const startIndex = lowerCaseText.indexOf(lowerCaseQuery);

    if (startIndex !== -1) {
      return (
        <>
          {text.substring(0, startIndex)}
          <span className="highlight-bold">{text.substring(startIndex, startIndex + query.length)}</span>
          {text.substring(startIndex + query.length)}
        </>
      );
    }

    return <>{text}</>;
  };

  return (
    <div className={`search-bar ${showSearch ? 'active' : ''}`} ref={searchRef}>
      <Row>
        <div className="search-bar-container">
          {!showSearch && (
            <Button
              className="search-bar-btn"
              icon={<MagnifyingGlass />}
              iconPosition="start"
              onClick={openSearchInput}
            >
              <Space>
                {t('theme.tableComponent.search.searchBtnLabel')}
                {activeSearchNumber > 0 && <span className="active-filter-number">{activeSearchNumber}</span>}
              </Space>
            </Button>
          )}
          {showSearch && (
            <div className="search-bar-field">
              <Input
                placeholder={t('theme.tableComponent.search.searchInputPlaceholder')}
                className="search-bar-input"
                value={query}
                onChange={handleSearchChange}
                onKeyDown={handleKeyDown}
              />
              {query.length >= 2 && showSuggestions && filteredSuggestions.length > 0 && (
                <div className="search-bar-suggestions">
                  {filteredSuggestions.map((suggestion) => (
                    <li key={suggestion.id} className="search-bar-item">
                      <Link
                        to={`/gesetzgebungsportal/detail/${suggestion.id}`}
                        onClick={() => handleSuggestionClick(suggestion.name)}
                      >
                        {highlightQuery(suggestion.name, query)}
                      </Link>
                    </li>
                  ))}
                </div>
              )}
              <Button
                type="text"
                className="search-bar-back"
                onClick={() => {
                  resetSearch();
                }}
              >
                {t('theme.tableComponent.search.searchResetBtn')}
              </Button>
              <Button className="search-bar-searchBtn" type="primary" onClick={onSearchSubmit}>
                <Space>{t('theme.tableComponent.search.searchBtnLabel')}</Space>
              </Button>
            </div>
          )}
        </div>
      </Row>
    </div>
  );
}
