// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Col, Flex, Row } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

import { displayMessage } from '../../../messages/message-service';
import { ActiveFilter, CommonRow, Filter } from '../../../table-component';

interface ColumnAndDisplayName {
  displayName: string;
  columnName: string;
}

export interface FilterOptionProps<T extends CommonRow> {
  searchQuery?: string;
  searchRowsMethod?: (actualContent: T[], searchWord: string, columns: { name: string; columnIndex: number }[]) => T[];
  onFilterChange?: (filters: ActiveFilter[]) => void;
  activeFilters?: ActiveFilter[];
  setActiveFilters?: React.Dispatch<React.SetStateAction<ActiveFilter[]>>;
  activeFiltersNumber?: number;
  setActiveFiltersNumber?: (number: number) => void;
  filterOnSubmit?: boolean;
  parentId: string;
  isFilteringWithFetch?: boolean;
  prepareFilterButtonMethod?: (
    initialContent: T[],
    column: { name: string; columnIndex: number },
  ) => { displayName: string; labelContent: string; options: Set<string> };
  filterRowsMethod?: (actualContent: T[], filteredBy: string, column: { name: string; columnIndex: number }) => T[];
  filteredColumns?: { name: string; columnIndex: number }[];
  actualContent: T[];
  setContent: React.Dispatch<React.SetStateAction<T[]>>;
  content: T[];
}

export function FilterOptions<T extends CommonRow>(props: FilterOptionProps<T>): React.ReactElement {
  const { t } = useTranslation();
  const [selected, setSelected] = useState<{ value: string; columnName: string } | null>();
  const [filteredBy, setFilteredBy] = useState<ActiveFilter[]>([]);
  const columnNamesAndDisplayNames: ColumnAndDisplayName[] = [];

  const onFilterSubmit = () => {
    let newRows = props.content;

    filteredBy.forEach((filter) => {
      newRows = props.filterRowsMethod ? props.filterRowsMethod(newRows, filter.filteredBy, filter.column) : newRows;
    });

    if (props.searchQuery) {
      newRows = props.searchRowsMethod
        ? props.searchRowsMethod(newRows, props.searchQuery, props.filteredColumns || [])
        : newRows;
    }

    setSelected(undefined);
    props.setContent(newRows);
    showFilteredMsg(filteredBy);
    if (props.setActiveFilters) {
      props.setActiveFilters(filteredBy);
    }
    if (props.onFilterChange) {
      props.onFilterChange(filteredBy);
    }

    props.setActiveFiltersNumber && props.setActiveFiltersNumber(filteredBy.length);
  };

  const onSubmitReset = () => {
    let newRows = props.content;
    if (props.isFilteringWithFetch) {
      newRows = props.filterRowsMethod
        ? props.filterRowsMethod(props.content, '', { name: '', columnIndex: 0 })
        : newRows;
    }
    setFilteredBy([]);
    setSelected(null);
    props.setContent(newRows);
    showFilteredMsg([]);
    if (props.setActiveFilters) {
      props.setActiveFilters([]); // Clear active filters
    }
    if (props.onFilterChange) {
      props.onFilterChange([]); // Notify parent of filter reset
    }
    props.setActiveFiltersNumber && props.setActiveFiltersNumber([].length);
  };

  const showFilteredMsg = (filteredBy: ActiveFilter[]) => {
    let msg = '';
    let list = '<ul style="text-align: left;">'; // Added inline style for left alignment
    if (!filteredBy.length) {
      msg = t('theme.tableComponent.filterBar.resetMessageTitle');
      displayMessage(`${msg}`, 'success');
    } else {
      filteredBy.forEach((filter) => {
        // we want to show the "display name" and not just the column name
        const foundObject = columnNamesAndDisplayNames.find((item) => item.columnName === filter.column.name);
        const displayName = foundObject ? foundObject.displayName : null;
        list += `<li> ${t('theme.tableComponent.filterBar.updateMessagePoint')} ${displayName}</li>`;
      });
      list += '</ul>'; // Corrected closing tag
      msg = t('theme.tableComponent.filterBar.updateMessageTitle');
      displayMessage(`${msg} ${list}`, 'success');
    }
  };

  const filters = props.filteredColumns?.map((element) => {
    if (props.prepareFilterButtonMethod && props.filterRowsMethod) {
      const filterButtonConfig = props.prepareFilterButtonMethod(props.content, element);
      columnNamesAndDisplayNames.push({ columnName: element.name, displayName: filterButtonConfig.displayName });
      return (
        <Filter
          selected={selected}
          filterOnSubmit={props.filterOnSubmit}
          parentId={props.parentId}
          isFilteringWithFetch={props.isFilteringWithFetch}
          filterRowsMethod={props.filterRowsMethod}
          labelContent={filterButtonConfig.labelContent}
          setFilteredBy={setFilteredBy}
          filteredBy={filteredBy}
          actualContent={props.actualContent}
          setActualContent={props.setContent}
          initialContent={props.content}
          options={Array.from(filterButtonConfig.options)}
          displayName={filterButtonConfig.displayName}
          column={element}
          key={`${element.columnIndex}-${element.name}`}
        />
      );
    }
  });
  return (
    <Row gutter={[24, 24]}>
      <Col>
        <Flex gap={24}>{filters}</Flex>
      </Col>
      {props.filterOnSubmit && (
        <Col>
          <Flex gap={24} align="flex-end">
            <Button
              className="ant-btn-primary"
              onClick={onFilterSubmit}
              disabled={!filteredBy.length && !props.activeFiltersNumber}
              htmlType="submit"
              aria-label={t('theme.tableComponent.filterBar.filterSubmit')}
            >
              {t('theme.tableComponent.filterBar.filterSubmit')}
            </Button>
            <Button
              className="ant-btn-secondary"
              onClick={onSubmitReset}
              disabled={!filteredBy.length && !props.activeFiltersNumber}
              htmlType="reset"
              aria-label={t('theme.tableComponent.filterBar.filterReset')}
            >
              {t('theme.tableComponent.filterBar.filterReset')}
            </Button>
          </Flex>
        </Col>
      )}
    </Row>
  );
}
