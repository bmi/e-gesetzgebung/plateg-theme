// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { SelectProps } from 'antd';
import { DefaultOptionType, LabeledValue } from 'antd/lib/select';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { SelectDown } from '../../../icons/SelectDown';
import { MainContentSelectWrapper } from '../../../main-content-select-wrapper/component.react';
import { displayMessage } from '../../../messages/message-service';
import { SortedInfo } from '../../../table-component';

interface Sorter {
  sortedInfo: SortedInfo;
  title: string;
}

interface SorterProps {
  useFilterBar?: boolean;
  rowCount: string;
  elementId: string;
  sortedInfo?: SortedInfo;
  setSortedInfo: (sortedInfo: SortedInfo) => void;
  customDefaultSortIndex?: number;
  sorterOptions?: { columnKey: React.Key; titleAsc: string; titleDesc: string }[];
}

type LabelRender = SelectProps['labelRender'];

export function SorterComponent(props: Readonly<SorterProps>): React.ReactElement {
  const { t } = useTranslation();
  const customDefaultSortIndex = props.customDefaultSortIndex;
  const setSortedInfo = props.setSortedInfo;
  const sortedInfo = props.sortedInfo;
  const [sorters, setSorters] = useState<Sorter[]>([]);
  const [firstRender, setFirstRender] = useState(true);

  useEffect(() => {
    let sortersList = props.sorterOptions?.length
      ? [
          {
            sortedInfo: new SortedInfo(null, props.sorterOptions[0].columnKey),
            title: 'Keine Sortierung',
          },
        ]
      : [];
    sortersList = sortersList.concat(
      props.sorterOptions?.flatMap((sorter) => {
        return [
          {
            sortedInfo: new SortedInfo('ascend', sorter.columnKey),
            title: sorter.titleAsc,
          },
          {
            sortedInfo: new SortedInfo('descend', sorter.columnKey),
            title: sorter.titleDesc,
          },
        ];
      }) ?? [],
    );
    setSorters(sortersList);
    if (firstRender && customDefaultSortIndex && sortersList[customDefaultSortIndex]) {
      if (
        sortedInfo?.columnKey !== sortersList[customDefaultSortIndex].sortedInfo.columnKey ||
        sortedInfo?.order !== sortersList[customDefaultSortIndex].sortedInfo.order
      ) {
        setSortedInfo(sortersList[customDefaultSortIndex].sortedInfo);
      }
    } else if (firstRender && sortersList[0] && firstRender) {
      setSortedInfo(sortersList[0].sortedInfo);
    }
  }, [props.sorterOptions]);

  useEffect(() => {
    if (
      sortedInfo &&
      ((customDefaultSortIndex && sorters[customDefaultSortIndex]?.sortedInfo === sortedInfo) ||
        !customDefaultSortIndex)
    ) {
      setFirstRender(false);
    }
  });

  useEffect(() => {
    if (!firstRender) {
      showSortedMsg();
    }
  }, [sortedInfo]);

  const showSortedMsg = () => {
    let msg = '';
    if (getCurrentSelection() === '0') {
      msg = t('theme.contentView.filterBar.sorterResetMsg');
    } else {
      msg = t('theme.contentView.filterBar.sorterUpdateMsg', {
        val: sorters[getCurrentSelection() as unknown as number].title,
      });
    }
    displayMessage(`${msg}`, 'success');
  };

  const getMenuItems = () => {
    let itemsList: DefaultOptionType[] = [];
    itemsList = [
      ...sorters.map((sorter, index) => {
        const key = sorter.sortedInfo.columnKey.toString() + index.toString();
        const title = sorter.title === '-' ? t('theme.tableComponent.sorterNoOrder') : sorter.title;
        return {
          label: (
            <span
              id={`theme-generic-menuItems-select-${key}`}
              className="filter-option"
              key={key}
              {...{ title: title }}
            >
              {sorter.title}
            </span>
          ),
          value: index.toString(),
          title,
        };
      }),
    ];

    return itemsList;
  };

  const onSelect = (value: string | number | LabeledValue) => {
    setSortedInfo(
      new SortedInfo(sorters[value as number].sortedInfo.order, sorters[value as number].sortedInfo.columnKey),
    );
  };

  const getCurrentSelection = (): string => {
    const selection = sorters.findIndex((sorter) => {
      return sorter.sortedInfo.columnKey === sortedInfo?.columnKey && sorter.sortedInfo.order === sortedInfo?.order;
    });
    if (selection >= 0) {
      return selection.toString();
    } else {
      return '0';
    }
  };

  useEffect(() => {
    getMenuItems();
  }, [sorters]);

  const labelRender: LabelRender = ({ label }) => {
    return (
      <span className="sort">
        <span className="sort-label">{t('theme.tableComponent.filterBar.sortiertNachTitle')}</span> &nbsp;{' '}
        <span className="sort-item">{label}</span>
      </span>
    );
  };

  return (
    <div className="sorter-item">
      <MainContentSelectWrapper
        id={`theme-generic-sorter-select-${props.elementId}`}
        className={'sorter-select filter-bar-select'}
        value={getCurrentSelection()}
        onSelect={(value: string[]) => {
          if (value.length) {
            onSelect(value[0]);
          }
        }}
        placeholder="-"
        suffixIcon={<SelectDown />}
        prefixCls={'filter-bar-sort'}
        labelRender={labelRender}
        options={getMenuItems()}
      />
    </div>
  );
}
