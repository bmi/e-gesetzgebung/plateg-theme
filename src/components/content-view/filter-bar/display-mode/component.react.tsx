// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Flex, SelectProps } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { SelectDown } from '../../../icons/SelectDown';
import { MainContentSelectWrapper } from '../../../main-content-select-wrapper/component.react';
import { displayMessage } from '../../../messages/message-service';

interface DisplayModeProps {
  elementId: string;
  onSelectDisplayMode: (displayMode: DisplayMode) => void;
  activeDisplayMode: DisplayMode;
}

type LabelRender = SelectProps['labelRender'];

export enum DisplayMode {
  Table = 'table',
  Grid = 'grid',
}

export function DisplayModeComponent(props: DisplayModeProps): React.ReactElement {
  const { t } = useTranslation();

  const menuItems = Array.from(Object.values(DisplayMode));

  const getMenuItems = () => {
    return menuItems.map((option, index) => {
      const key = `display-mode-${index}`;
      const title = t(`theme.tableComponent.displayMode.${option}`);
      return {
        label: (
          <span id={`theme-generic-menuItems-select-${key}`} className="filter-option" key={key} title={title}>
            {title}
          </span>
        ),
        value: option,
        title,
      };
    });
  };

  const onSelect = (value: DisplayMode) => {
    props.onSelectDisplayMode(value);
    const title = t(`theme.tableComponent.displayMode.${value}`);
    displayMessage(t('theme.tableComponent.displayMode.changeMsg', { mode: title }), 'success');
  };

  const labelRender: LabelRender = ({ label }) => {
    return (
      <Flex>
        <span className="display-mode-label">{'Darstellung:'}</span> &nbsp;{' '}
        <span className="display-mode-item">{label}</span>
      </Flex>
    );
  };

  return (
    <div className="display-mode-item">
      <MainContentSelectWrapper
        id={`theme-generic-display-mode-select-${props.elementId}`}
        className={'display-mode-select filter-bar-display-mode'}
        value={props.activeDisplayMode}
        onSelect={(value: DisplayMode) => onSelect(value)}
        placeholder="-"
        suffixIcon={<SelectDown />}
        prefixCls={'filter-bar-display-mode'}
        labelRender={labelRender}
        options={getMenuItems()}
      />
    </div>
  );
}
