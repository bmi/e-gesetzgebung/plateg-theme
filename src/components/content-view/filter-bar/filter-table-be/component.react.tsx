// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FilterPropertyType } from '@plateg/rest-api';
import i18n from 'i18next';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { MainContentSelectWrapper } from '../../..';
import { SelectDown } from '../../../icons/SelectDown';
import { useAppDispatch, useAppSelector } from '../../../store';
import { cancelNewFilter, setRequestFilter, setRequestResetFilter } from '../../../store/slices/tablePaginationSlice';
import { CommonRow } from '../../component.react';

export type FilterType = {
  value: string;
  columnName: string;
} | null;

interface FilterObject {
  [k: string]: string;
}
interface FilterProps extends CommonRow {
  column: { name: FilterPropertyType; columnIndex: number };
  options: string[] | FilterObject;
  resetValue?: boolean;
  parentId: string;
  tabKey: string;
  filterOnSubmit?: boolean;
  selectedValues?: FilterObject;
  setSelectedValues?: (selectedValues: FilterObject) => void;
}

export function FilterTableBe(props: FilterProps): React.ReactElement {
  const { t } = useTranslation();
  const dispatch = useAppDispatch();

  const [selectedValue, setSelectedValue] = useState<string | undefined>();
  const isInitialRenderTotalItems = useRef(true);

  const pagInfoRequest = useAppSelector((state) => state.tablePagination.tabs[props.tabKey]).request;
  const pagInfoResult = useAppSelector((state) => state.tablePagination.tabs[props.tabKey]).result;

  //   add translation for other filter
  const displayName = t(`theme.tableComponent.filterNames.${props.column.name}`);
  const column = props.column;

  const optionAll = String(t('theme.multiChoice.all'));

  let options: string[] = [];
  let optionsKeys: string[] = [];

  if (Array.isArray(props.options)) {
    options = (props.options || []).map((item) => item);
  } else {
    // Prepare list of ids according to sorted values
    const optionsObject = props.options;
    options = Object.values(optionsObject).map((value) => value);
    optionsKeys = Object.keys(optionsObject).sort((a, b) =>
      optionsObject[a]?.toLowerCase().localeCompare(optionsObject[b]?.toLowerCase()),
    );
  }

  useEffect(() => {
    if (props.resetValue) {
      setSelectedValue(undefined);
    }
  }, [props.resetValue]);

  useEffect(() => {
    if (pagInfoRequest.newFilter === column.name && selectedValue && pagInfoResult.totalItems !== undefined) {
      props.tabKey &&
        dispatch(
          cancelNewFilter({
            tabKey: props.tabKey,
          }),
        );
    }
  }, [pagInfoResult]);

  useEffect(() => {
    if (props.selectedValues && props.selectedValues[props.column.name] !== selectedValue) {
      setSelectedValue(props.selectedValues[props.column.name]);
    }
  }, [props.selectedValues, props.column.name]);

  useEffect(() => {
    if (!props.filterOnSubmit) {
      if (isInitialRenderTotalItems.current) {
        isInitialRenderTotalItems.current = false;
        return;
      }

      if (!selectedValue || !props.tabKey) {
        return;
      }

      const filterPayload = {
        tabKey: props.tabKey,
        newFilter: column.name,
      };

      if (selectedValue === valueAllReset) {
        dispatch(setRequestResetFilter(filterPayload));
      } else {
        dispatch(
          setRequestFilter({
            ...filterPayload,
            filters: { [column.name]: selectedValue },
          }),
        );
      }
    }
  }, [selectedValue]);

  const getSortedOptions = (): string[] => {
    return options.sort(function (option1, option2) {
      return option1?.toLowerCase().localeCompare(option2?.toLowerCase());
    });
  };

  const getMenuItems = () => {
    let items = [];

    items.push({
      label: (
        <span className="filter-option" key={`reset${props.column.name}`}>
          {optionAll}
        </span>
      ),
      value: valueAllReset,
      title: optionAll,
    });

    items = [
      ...items,
      ...getSortedOptions().map((option, index) => {
        return {
          title: option,
          value: optionsKeys[index] || option,
          label: (
            <span key={`${option}-${index}`} className="filter-option">
              {i18n.exists(`theme.tableComponent.filterValues.${option}`)
                ? t(`theme.tableComponent.filterValues.${option}`)
                : option}
            </span>
          ),
        };
      }),
    ];

    return items;
  };

  const valueAllReset = optionAll;

  return (
    <div className="filter-item">
      <label
        className="filter-label"
        htmlFor={`filter-${displayName.toLowerCase().split(' ').join('-')}-${props.parentId}`}
      >
        {'Filtern nach'} {displayName}
      </label>
      <MainContentSelectWrapper
        options={getMenuItems()}
        className="filter-select"
        onSelect={(value: string) => {
          if (props.filterOnSubmit) {
            props.setSelectedValues && props.setSelectedValues({ ...props.selectedValues, [props.column.name]: value });
            setSelectedValue(value);
          } else {
            setSelectedValue(value);
          }
        }}
        value={selectedValue ? selectedValue : valueAllReset}
        id={`filter-${displayName.toLowerCase().split(' ').join('-')}-${props.parentId}`}
        suffixIcon={<SelectDown />}
      />
    </div>
  );
}
