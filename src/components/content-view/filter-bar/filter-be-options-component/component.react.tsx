// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FilterPropertyType } from '@plateg/rest-api';
import { Button, Col, Flex, Row } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { displayMessage } from '../../../messages/message-service';
import { useAppDispatch, useAppSelector } from '../../../store';
import { PagFilterInfo, setRequestFilter } from '../../../store/slices/tablePaginationSlice';
import { FilterTableBe } from '../filter-table-be/component.react';

interface CustomFilterEventData {
  [key: string]: string | undefined;
}

export interface CustomFilterElementsProps {
  onUpdateFilterValues: (filterValues: CustomFilterEventData) => void;
}

export interface FilterOptionProps {
  setActiveFiltersNumber?: (number: number) => void;
  filterOnSubmit?: boolean;
  tabKey?: string;
  parentId: string;
  customFilterElements?: (props: CustomFilterElementsProps) => React.ReactElement;
}

interface FilterObject {
  [k: string]: string;
}

export function FilterBeOptions(props: FilterOptionProps): React.ReactElement {
  const dispatch = useAppDispatch();
  const { t } = useTranslation();

  const pagInfoRequest = useAppSelector((state) => state.tablePagination.tabs[props.tabKey as string]).request;
  const pagInfoResult = useAppSelector((state) => state.tablePagination.tabs[props.tabKey as string]).result;

  const [resetValue, setResetValue] = useState<boolean>();
  const [selectedValues, setSelectedValues] = useState<FilterObject>({});
  const [isFilterSubmitted, setIsFilterSubmitted] = useState<boolean>(false);

  const onUpdateFilterValues = (filterValues: CustomFilterEventData) => {
    for (const [key, value] of Object.entries(filterValues)) {
      if (value === undefined) {
        const { [key]: _, ...rest } = selectedValues;
        setSelectedValues(rest);
      } else {
        setSelectedValues((prev) => ({ ...prev, [key]: value }));
      }
    }
  };

  const onSubmitReset = () => {
    const updatedObject = Object.fromEntries(Object.entries(pagInfoRequest.filters).map(([key]) => [key, undefined]));

    if (props.tabKey) {
      const newFilter = Object.keys(selectedValues).find((key) => selectedValues[key].length);
      dispatch(
        setRequestFilter({
          tabKey: props.tabKey,
          filters: updatedObject,
          newFilter: newFilter as FilterPropertyType,
        }),
      );
    }

    setSelectedValues({});
    setResetValue(true);
    props.setActiveFiltersNumber && props.setActiveFiltersNumber(0);
    setIsFilterSubmitted(true);
  };

  const onFilterSubmit = () => {
    const optionAll = String(t('theme.multiChoice.all'));

    const filters = Object.fromEntries(
      Object.entries(selectedValues).map(([filter, value]) => {
        return [filter, value === optionAll || !value.length ? undefined : value];
      }),
    );

    if (props.tabKey) {
      const newFilter = Object.keys(selectedValues).find((key) => selectedValues[key].length);
      dispatch(
        setRequestFilter({
          tabKey: props.tabKey,
          filters,
          newFilter: newFilter as FilterPropertyType,
        }),
      );
    }

    const filtersNumber = Object.values(filters).filter((value) => value?.length).length;
    props.setActiveFiltersNumber?.(filtersNumber);

    setResetValue(false);
    setIsFilterSubmitted(true);
  };

  useEffect(() => {
    if (isFilterSubmitted && pagInfoResult.totalItems !== undefined) {
      showFilteredMsg(selectedValues || {}, pagInfoResult.totalItems);
      setIsFilterSubmitted(false);
    }
  }, [pagInfoResult]);

  const showFilteredMsg = (filters: FilterObject, newRowsCount: number) => {
    const keys = Object.keys(filters);

    const createMessage = (msg: string, list: string = '') => {
      const filterEntriesMsg = `<div style="text-align: left;">${t('theme.contentView.filterBar.filterEntriesMsg', { count: newRowsCount })}</div>`;
      displayMessage(`${msg} ${list} ${filterEntriesMsg}`, 'success');
    };

    if (!keys.length) {
      createMessage(t('theme.tableComponent.filterBar.resetMessageTitle'));
    } else {
      const listItems = keys
        .map((filter) => {
          const displayName = t(`theme.tableComponent.filterNames.${filter}`);
          let filterValue = filters[filter];
          if (canBeParsedAsDate(filterValue)) {
            filterValue = formatDateToDDMMYYYY(filterValue);
          }
          return `<li><b>${displayName}:</b> ${filterValue}</li>`;
        })
        .join('');
      const list = `<ul style="text-align: left;">${listItems}</ul>`;
      const msg = t('theme.contentView.filterBar.filterUpdateMsg', { count: keys.length });
      createMessage(msg, list);
    }
  };

  function canBeParsedAsDate(input: any): boolean {
    const date = new Date(input);
    return !isNaN(date.getTime());
  }

  function formatDateToDDMMYYYY(date: Date | string): string {
    const d = new Date(date);

    const day = String(d.getDate()).padStart(2, '0');
    const month = String(d.getMonth() + 1).padStart(2, '0');
    const year = d.getFullYear();

    return `${day}.${month}.${year}`;
  }

  return (
    <Row gutter={[24, 24]}>
      <Col>
        <Flex gap={24} wrap={'wrap'}>
          {pagInfoResult.filterInfo?.filterNames?.map((item: string, columnIndex: number) => {
            // read options from be "...Filter" property
            const options = pagInfoResult.filterInfo?.[
              `${item.toLowerCase()}Filter` as keyof PagFilterInfo
            ] as string[];

            return (
              options &&
              props.tabKey && (
                <FilterTableBe
                  resetValue={resetValue}
                  selectedValues={selectedValues}
                  setSelectedValues={setSelectedValues}
                  filterOnSubmit={props.filterOnSubmit}
                  id={props.parentId}
                  parentId={props.parentId}
                  options={options}
                  column={{
                    name: item as FilterPropertyType,
                    columnIndex,
                  }}
                  key={item}
                  tabKey={props.tabKey}
                />
              )
            );
          })}
          {props.customFilterElements && props.customFilterElements({ onUpdateFilterValues })}
        </Flex>
      </Col>
      {props.filterOnSubmit && (
        <Col>
          <Flex gap={24} align="flex-end" wrap={'wrap'}>
            <Button
              disabled={!Object.keys(selectedValues).length}
              aria-label={t('theme.tableComponent.filterBar.filterSubmit')}
              onClick={onFilterSubmit}
              htmlType="submit"
              className="ant-btn-primary"
            >
              {t('theme.tableComponent.filterBar.filterSubmit')}
            </Button>
            <Button
              disabled={!Object.keys(selectedValues).length}
              aria-label={t('theme.tableComponent.filterBar.filterReset')}
              onClick={onSubmitReset}
              htmlType="reset"
              className="ant-btn-secondary"
            >
              {t('theme.tableComponent.filterBar.filterReset')}
            </Button>
          </Flex>
        </Col>
      )}
    </Row>
  );
}
