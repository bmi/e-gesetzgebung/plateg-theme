// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './filter-bar.less';

import { Button, Col, Row, Space } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { CloseOutlined } from '../../icons/CloseOutlined';
import { FilterIcon } from '../../icons/FilterIcon';

interface FilterBarProps {
  filterElements?: React.ReactElement;
  sorterElements?: React.ReactElement;
  rowCountElements?: React.ReactElement;
  customFilterButton?: React.ReactElement;
  customFilterContent?: React.ReactElement;
  showCustomFilterDrawer?: boolean;
  setShowCustomFilterDrawer?: (boolean: boolean) => void;
  activeFiltersNumber?: number;
  itemsPerPagePicker?: React.ReactElement;
  displayModeElements?: React.ReactElement;
  searchComponent?: React.ReactElement;
}

export function FilterBarComponent(props: FilterBarProps): React.ReactElement {
  const { t } = useTranslation();

  const [showFilterDrawer, setShowFilterDrawer] = useState<boolean>(false);

  useEffect(() => {
    if (showFilterDrawer) {
      props.setShowCustomFilterDrawer && props.setShowCustomFilterDrawer(false);
    }
  }, [showFilterDrawer]);

  useEffect(() => {
    if (props.showCustomFilterDrawer) {
      setShowFilterDrawer(false);
    }
  }, [props.showCustomFilterDrawer]);

  return (
    <div className="filter-bar-new">
      <Row className="filter-row-new filter-top-row">
        {props.filterElements && (
          <Col className="filter-item filter-row-left">
            <Button
              className={showFilterDrawer ? 'filter-bar-button active' : 'filter-bar-button'}
              icon={showFilterDrawer ? <CloseOutlined /> : <FilterIcon />}
              iconPosition="start"
              onClick={() => {
                setShowFilterDrawer(!showFilterDrawer);
              }}
            >
              <Space>
                {t('theme.tableComponent.filterBar.filterTitle')}
                {props.activeFiltersNumber && props.activeFiltersNumber > 0 ? (
                  <span className="active-filter-number">{props.activeFiltersNumber}</span>
                ) : (
                  <></>
                )}
              </Space>
            </Button>
          </Col>
        )}
        {props.customFilterButton && <Col className="filter-item filter-row-left">{props.customFilterButton}</Col>}
        {props.searchComponent && <Col className="filter-item">{props.searchComponent}</Col>}
        <Col className="filter-item filter-row-center">{props.rowCountElements}</Col>
        {props.itemsPerPagePicker && (
          <Col className={`filter-item filter-row-right ${showFilterDrawer ? 'filter-item-active' : ''}`}>
            {props.itemsPerPagePicker}
          </Col>
        )}
        {props.sorterElements && (
          <Col className={`filter-item filter-row-right ${showFilterDrawer ? 'filter-item-active' : ''}`}>
            {props.sorterElements}
          </Col>
        )}
        {props.displayModeElements && (
          <Col className={`filter-item filter-row-right ${showFilterDrawer ? 'filter-item-active' : ''}`}>
            {props.displayModeElements}
          </Col>
        )}
      </Row>
      <Row className={`filter-drawer ${!showFilterDrawer && 'hidden'}`}>
        <Col className="filter-row-new filter-row-left">{props.filterElements}</Col>
      </Row>
      <Row className={`filter-drawer ${!props.showCustomFilterDrawer && 'hidden'}`}>
        <Col className="filter-row-new filter-row-left">{props.customFilterContent}</Col>
      </Row>
    </div>
  );
}
