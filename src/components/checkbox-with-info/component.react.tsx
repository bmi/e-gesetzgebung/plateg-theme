// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Checkbox } from 'antd';
import { CheckboxProps } from 'antd/lib/checkbox';
import React, { useLayoutEffect } from 'react';

import { AriaController } from '../../controllers';

interface CheckboxWithInfoProps extends CheckboxProps {
  title: string;
  wrapperId: string;
}

export function CheckboxWithInfo(props: CheckboxWithInfoProps): React.ReactElement {
  useLayoutEffect(() => {
    AriaController.setAriaLabelsByQuery(`#${props.wrapperId} label`, props.title);
  });
  return <Checkbox {...(({ title, wrapperId, ...o }) => o)(props)} />;
}
