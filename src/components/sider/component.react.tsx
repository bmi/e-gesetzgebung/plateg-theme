// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './sider.less';

import { Layout } from 'antd';
import { SiderProps } from 'antd/lib/layout';
import React from 'react';

import { LeftOutlined } from '../icons/LeftOutlined';
import { RightOutlined } from '../icons/RightOutlined';

export function SiderWrapper(props: SiderProps & React.RefAttributes<HTMLDivElement>): React.ReactElement {
  const { Sider } = Layout;

  return (
    <Sider
      {...props}
      className={`sider-wrapper ${props.className || ''}`}
      collapsedWidth={36}
      trigger={
        <span className="ant-btn" role="button">
          {props.collapsed ? <RightOutlined /> : <LeftOutlined />}
        </span>
      }
    />
  );
}
