// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './nav.less';

import { Menu } from 'antd';
import { FormInstance } from 'antd/lib/form';
import { ItemType } from 'antd/lib/menu/interface';
import React, { useEffect, useState } from 'react';
import { useHistory, useLocation, useRouteMatch } from 'react-router-dom';

import { GrayArrowDown } from '../../components/icons/GrayArrowDown';
import { AriaController } from '../../controllers/AriaController';
import { useAppSelector } from '../store';

interface SubmenuEntry {
  key: string;
  isLink?: boolean;
  isDisabled?: boolean;
}
export interface NavBarItem {
  submenuEntry?: SubmenuEntry;
  menuKeys: (MenuItem | NavBarItem)[];
  menuTitle?: string;
}
export interface MenuItem {
  key: string;
  isDisabled?: boolean;
  dynamicName?: string;
}

interface NavbarComponentProps {
  form?: FormInstance;
  navBarItems: NavBarItem[];
  trans: (name: string, dynamicName?: string) => string;
}
export function NavbarComponent(props: NavbarComponentProps): React.ReactElement {
  const { url } = useRouteMatch();
  const location = useLocation();
  const history = useHistory();
  const [subMenuKey, setSubMenuKey] = useState<React.Key[]>(['']);
  const [auslassungen, setAuslassungen] = useState<boolean>(false);
  const appStore = useAppSelector((state) => state.setting);

  useEffect(() => {
    if (appStore.setting && !appStore.setting.barrierefreiheitMehrzeiligeNavigationspunkte) {
      setAuslassungen(true);
    }
  }, [appStore.setting]);

  useEffect(() => {
    AriaController.setAriaAttrByClassName('ant-menu-submenu-arrow', 'hidden', 'true');
  }, []);

  const filterOnlySubmenu = (list: (MenuItem | NavBarItem)[]): NavBarItem[] => {
    return list.filter((item) => (item as NavBarItem).submenuEntry?.key !== undefined) as NavBarItem[];
  };

  const prepareSubmenuKeys = (navBarItems: NavBarItem[]) => {
    const flatKeys: string[] = [];
    const iterate = (obj: NavBarItem) => {
      flatKeys.push(obj.submenuEntry?.key as string);
      filterOnlySubmenu(obj.menuKeys).forEach((item) => iterate(item));
    };
    filterOnlySubmenu(navBarItems.length === 1 ? navBarItems[0].menuKeys : navBarItems).forEach((item) =>
      iterate(item),
    );

    return flatKeys;
  };

  function routeChange() {
    const keys = prepareSubmenuKeys(props.navBarItems).filter((item: string) => {
      const subKey = item.replace(/\./g, '/');
      const patternSubKey = '\\b' + subKey + '\\b';
      return subKey && location.pathname.match(new RegExp(patternSubKey, 'g'));
    });

    setSubMenuKey(keys);
  }
  useEffect(routeChange, [location]);

  const routeChangeInitiate = (newURL: string) => {
    if (props.form) {
      props.form
        .validateFields()
        .catch(() => {
          // Error will be handelled by the UI
        })
        .finally(() => history.push(newURL));
    } else {
      history.push(newURL);
    }
  };

  const isActive = (key: string) => {
    return key === location.pathname;
  };

  const createNavBarItem = (navBarItem: MenuItem | NavBarItem): ItemType => {
    if ((navBarItem as NavBarItem).submenuEntry) {
      return createSubNavBarItem(navBarItem as NavBarItem) as MenuItem;
    }

    const item = navBarItem as MenuItem;
    const isItemDisabled = item.isDisabled || false;

    let preparedUrl = item.key;
    if (preparedUrl.indexOf('.') !== -1) {
      preparedUrl = preparedUrl.replace(/\./g, '/');
    }
    return {
      label: (
        <span
          dangerouslySetInnerHTML={{
            __html: props.trans(item.key, item.dynamicName),
          }}
        />
      ),
      key: `${url}/${preparedUrl}`,
      disabled: isItemDisabled,
      onKeyPress: (e: { code: string; stopPropagation: () => void }) => {
        if (e.code === 'Enter') {
          history.push(`${url}/${preparedUrl}`);
        }
        e.stopPropagation();
      },
    };
  };

  const createSubNavBarItem = (subNavBarItem: NavBarItem): any => {
    const menuItems = subNavBarItem.menuKeys.map(createNavBarItem);
    const menuItemsObj = { key: `${subNavBarItem.menuTitle}-menu-items`, children: menuItems };

    const submenuEntry = subNavBarItem.submenuEntry;
    if (!submenuEntry?.key) {
      return subNavBarItem.menuTitle ? itemWithTitle([...menuItemsObj.children], subNavBarItem.menuTitle) : menuItems;
    }
    const title = props.trans(submenuEntry.key);
    let preparedUrl = submenuEntry?.key;
    if (submenuEntry.isLink && preparedUrl.indexOf('.') !== -1) {
      preparedUrl = preparedUrl.replace(/\./g, '/');
    }
    const onClick = submenuEntry.isLink ? () => history.push(`${url}/${preparedUrl}`) : undefined;
    const className = isActive(`${url}/${preparedUrl}`) ? 'submenu-selected' : '';
    const subMenu: ItemType & { onKeyPress: (e: { code: string; stopPropagation: () => void }) => void } = {
      key: submenuEntry.key,
      className,
      onTitleClick: onClick,
      label: <span aria-label={title}>{title}</span>,
      disabled: submenuEntry.isDisabled,
      children: menuItems,
      onKeyPress: (e: { code: string; stopPropagation: () => void }) => {
        if (e.code === 'Enter' && submenuEntry.isLink) {
          onClick?.();
        }
        e.stopPropagation();
      },
    };

    return subNavBarItem.menuTitle ? itemWithTitle([subMenu], subNavBarItem.menuTitle) : subMenu;
  };

  const itemWithTitle = (items: ItemType[], titleKey: string): ItemType => {
    return {
      type: 'group',
      label: (
        <span
          data-grouping-label={props.trans(titleKey).replace(/<\/?[^>]+(>|$)/g, '')}
          dangerouslySetInnerHTML={{
            __html: props.trans(titleKey),
          }}
        ></span>
      ),
      key: titleKey,
      children: [...items],
    };
  };

  useEffect(() => {
    document.querySelectorAll('#navbar-component .ant-menu-item').forEach((elem) => {
      const plainAriaLabel = elem.ariaLabel?.replace(' (aktiv)', '');
      if (plainAriaLabel) {
        elem.setAttribute('aria-label', plainAriaLabel);
      }
      elem.removeAttribute('aria-current');
    });
    const activeElem = document.querySelectorAll('#navbar-component .ant-menu-item-selected')?.[0];
    const ariaLabel =
      activeElem?.ariaLabel || activeElem?.getElementsByClassName('ant-menu-title-content')[0]?.innerHTML || '';

    // Remove all html tags from ariaLabel
    const allTagsRegexp = /<[^>]*>/g;
    const sanitizedAriaLabel = ariaLabel.replace(allTagsRegexp, '');
    activeElem?.setAttribute('aria-label', `${sanitizedAriaLabel} (aktiv)`);
    activeElem?.setAttribute('aria-current', 'page');

    // Add aria-label for better navigation by screenreader
    const allGroupsLabels = document.querySelectorAll('[data-grouping-label]');
    allGroupsLabels.forEach((item) => {
      (item.parentNode?.nextSibling as Element)?.setAttribute(
        'aria-label',
        item.getAttribute(`data-grouping-label`) || '',
      );
    });
  }, [location.pathname]);

  return (
    <nav aria-label="Prozessnavigation eVorhaben" id="navbar-component">
      <Menu
        items={props.navBarItems.flatMap(createSubNavBarItem)}
        mode="inline"
        className={auslassungen ? 'with-auslassungen' : ''}
        onClick={(event) => routeChangeInitiate(event?.key)}
        style={{ height: '100%', borderRight: 0 }}
        selectedKeys={[location.pathname]}
        openKeys={subMenuKey.map((key) => String(key))}
        onOpenChange={setSubMenuKey}
        expandIcon={
          <i className="ant-menu-submenu-arrow">
            <span className="svg-holder">
              <GrayArrowDown />
            </span>
          </i>
        }
      />
    </nav>
  );
}
