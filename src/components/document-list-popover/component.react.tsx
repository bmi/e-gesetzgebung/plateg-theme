// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './document-list-popover.less';

import { Button, Popover } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { BASE_PATH, DokumentenmappeDTO, RvFileEntityResponseDTO } from '@plateg/rest-api';

import { CloseOutlined } from '../icons/CloseOutlined';
import { FolderDefault } from '../icons/FolderDefault';
import { Download } from '../table-component';

interface DocumentListPopoverProps {
  dokumentenmappe?: DokumentenmappeDTO;
  documentsList?: RvFileEntityResponseDTO[];
  recordId: string;
  title?: string;
}

export function DocumentListPopover(props: DocumentListPopoverProps): React.ReactElement {
  const { t } = useTranslation();
  const [open, setOpen] = useState<boolean>();
  const btnCloseRef = useRef<HTMLButtonElement>(null);
  const btnOpenRef = useRef<HTMLButtonElement>(null);

  const documentListMappe = (
    <ul className="popover-dokuments-list">
      {props.dokumentenmappe?.dokumente?.map((dokument, ind, arr) => (
        <li key={`${dokument.id}-${dokument.bearbeitetAm}`}>
          <a
            href={`#/editor/document/${dokument.id}`}
            onKeyDown={(e) => {
              handleKeyDown(e, ind + 1 === arr.length);
            }}
          >
            {dokument.titel}
          </a>
        </li>
      ))}
    </ul>
  );

  const enormDocumentList = (
    <ul className="popover-dokuments-list">
      {props.documentsList?.map((dokument, ind, arr) => (
        <li key={`${dokument.base.id}-${dokument.base.bearbeitetAm}`}>
          <Download
            link={BASE_PATH + '/file/download/rv/{fileId}'.replace('{fileId}', encodeURI(dokument?.base.id.toString()))}
            name={dokument.dto.fileName}
            onKeyDown={(e) => {
              handleKeyDown(e, ind + 1 === arr.length);
            }}
            isfile={true}
          />
        </li>
      ))}
    </ul>
  );

  useEffect(() => {
    if (open) {
      setTimeout(() => {
        btnCloseRef.current?.focus();
      }, 100);
    } else if (open === false) {
      btnOpenRef.current?.focus();
    }
  }, [open]);

  const handleKeyDown = (e: React.KeyboardEvent<HTMLAnchorElement>, isLast: boolean) => {
    if (e.key === 'Tab' && isLast) {
      e.preventDefault();
      btnCloseRef.current?.focus();
    }
    if (e.key === 'Escape') {
      setOpen(false);
    }
  };

  const handleOutsideClick = (e: MouseEvent) => {
    const popover = document.querySelector('.ant-popover');
    if (popover && !popover.contains(e.target as Node)) {
      setOpen(false);
    }
  };

  useEffect(() => {
    document.addEventListener('mousedown', handleOutsideClick);
    return () => {
      document.removeEventListener('mousedown', handleOutsideClick);
    };
  }, []);

  return (
    <>
      {props.dokumentenmappe && (
        <>
          <FolderDefault />
          <span>{props.dokumentenmappe?.titel}</span>
          <br />
        </>
      )}

      <Popover
        title={
          <div className="documents-poptitle-holder" role="tooltip">
            <Button
              id={`document-list-close-button-${props.recordId}`}
              className="close-button-dokuments-list"
              onClick={() => setOpen(false)}
              ref={btnCloseRef}
              aria-label={t('theme.enormDocumentListPopover.title')}
              onKeyDown={(e) => {
                if (e.key === 'Escape') {
                  setOpen(false);
                }
              }}
            >
              <CloseOutlined />
            </Button>
            <span>{t(`theme.documentListPopover.title${props.dokumentenmappe ? 'Mappe' : 'List'}`)}</span>
          </div>
        }
        content={props.dokumentenmappe ? documentListMappe : enormDocumentList}
        open={open || false}
        placement="bottomLeft"
      >
        <Button
          id={`document-list-open-button-${props.recordId}`}
          type="text"
          onClick={() => setOpen(!open)}
          size="small"
          ref={btnOpenRef}
          aria-describedby={`document-tooltip-${props.recordId}`}
        >
          <u>{props.title ?? t('theme.documentListPopover.btnOpen')}</u>
        </Button>
      </Popover>
    </>
  );
}
