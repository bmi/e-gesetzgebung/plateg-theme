// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { TFunction } from 'i18next';
import React, { useEffect } from 'react';
import { forkJoin, Subscription } from 'rxjs';

import {
  Configuration,
  UserControllerApi,
  UserEinstellungEntityDTO,
  UserEntityWithStellvertreterResponseDTO,
} from '@plateg/rest-api';

import { configureRestApi } from '../../controllers/RestConfigController';
import { GlobalDI } from '../../shares';
import { useKeycloak } from '../auth/KeycloakAuth';
import { useAppDispatch } from '../store';
import { setSetting } from '../store/slices/settingSlice';
import { clearUser, setUser } from '../store/slices/userSlice';

interface StoreInitProps {
  children: React.ReactElement;
  loadMessages?: (userRessort: string) => Promise<TFunction>;
}

export function StoreInit(props: StoreInitProps): React.ReactElement {
  const dispatch = useAppDispatch();
  const { keycloak } = useKeycloak();
  useEffect(() => {
    let sub: Subscription;
    if (keycloak.authenticated) {
      configureRestApi((configRestCalls: Configuration) => {
        GlobalDI.getOrRegister('userController', () => new UserControllerApi(configRestCalls));
      });
      const controller = GlobalDI.getOrRegister<UserControllerApi>('userController', () => new UserControllerApi());
      sub = forkJoin({
        user: controller.getUser(),
        setting: controller.getUserEinstellung(),
      }).subscribe({
        next: (data: { user: UserEntityWithStellvertreterResponseDTO; setting: UserEinstellungEntityDTO }) => {
          if (data.user.base.id != null) {
            if (data.user.dto.ressort?.kurzbezeichnung === 'BT' || data.user.dto.ressort?.kurzbezeichnung === 'BR') {
              props
                .loadMessages?.(data.user.dto.ressort?.kurzbezeichnung.toLowerCase())
                .then(() => {
                  dispatch(setUser(data.user));
                  dispatch(setSetting(data.setting));
                })
                .catch((error) => {
                  console.log(error);
                });
            }
            dispatch(setUser(data.user));
            dispatch(setSetting(data.setting));
          }
        },
        error: () => {
          dispatch(clearUser());
        },
      });
    }
    return () => {
      sub?.unsubscribe();
    };
  }, [keycloak.authenticated]);

  return keycloak.authenticated ? props.children : <></>;
}
