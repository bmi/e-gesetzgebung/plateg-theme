// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Provider, TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';

import {
  combineReducers,
  configureStore,
  EnhancedStore,
  Reducer,
  ReducersMapObject,
  StateFromReducersMapObject,
} from '@reduxjs/toolkit';

import { AppSettingReducer, AppSettingState } from './slices/appSettingSlice';
import { KcTokenReducer, KcTokenState } from './slices/kcTokenSlice';
import { NeuigkeitenReducer, NeuigkeitenState } from './slices/neuigkeitenSlice';
import { SettingReducer, SettingState } from './slices/settingSlice';
import { TablePaginationReducer, TablePaginationState } from './slices/tablePaginationSlice';
import { UserReducer, UserState } from './slices/userSlice';

export interface EnhancedStoreAsync extends EnhancedStore {
  asyncReducers: ReducersMapObject;
  injectReducer: (key: string, asyncReducer: Reducer) => void;
  removeReducer: (key: string) => void;
}
const createReducer = (asyncReducers?: ReducersMapObject) => {
  return combineReducers({
    ...staticReducers,
    ...asyncReducers,
  });
};
type StaticReducers = {
  user: Reducer<UserState>;
  neuigkeiten: Reducer<NeuigkeitenState>;
  setting: Reducer<SettingState>;
  appSetting: Reducer<AppSettingState>;
  tablePagination: Reducer<TablePaginationState>;
  kcToken: Reducer<KcTokenState>;
  [name: string]: Reducer;
};
const staticReducers: StaticReducers = {
  user: UserReducer,
  neuigkeiten: NeuigkeitenReducer,
  setting: SettingReducer,
  appSetting: AppSettingReducer,
  tablePagination: TablePaginationReducer,
  kcToken: KcTokenReducer,
};
export const initStore = () => {
  const store = configureStore({
    reducer: createReducer(),
  }) as EnhancedStoreAsync;
  store.asyncReducers = {};
  store.injectReducer = (key, asyncReducer) => {
    store.asyncReducers[key] = asyncReducer;
    store.replaceReducer(createReducer(store.asyncReducers));
  };

  store.removeReducer = (key: string) => {
    if (!key || !store.asyncReducers[key]) {
      return;
    }
    delete store.asyncReducers[key];
    store.replaceReducer(createReducer(store.asyncReducers));
  };

  return store;
};

export const store = initStore();
export type RootState = StateFromReducersMapObject<typeof staticReducers>;
export const useAppDispatch: () => typeof store.dispatch = useDispatch;
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
export const StoreProvider = Provider;
