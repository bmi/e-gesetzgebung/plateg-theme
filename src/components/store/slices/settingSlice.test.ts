// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';

import { RootState, store } from '../index';
import { setSetting } from './settingSlice';

describe('user redux store test', () => {
  it('test intial state values', () => {
    expect((store.getState() as RootState).user.user).to.equal(undefined);
  });

  it('test change setting', () => {
    store.dispatch(
      setSetting({
        emailZeitplanungsvorlagen: true,
      }),
    );
    expect((store.getState() as RootState).setting.setting?.emailZeitplanungsvorlagen).to.equal(true);
  });
});
