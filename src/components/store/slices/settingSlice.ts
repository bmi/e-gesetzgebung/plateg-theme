// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { UserEinstellungEntityDTO } from '@plateg/rest-api';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface SettingState {
  setting: UserEinstellungEntityDTO | undefined;
}

const initialState: SettingState = { setting: undefined };

export const settingSlice = createSlice({
  name: 'Setting',
  initialState,
  reducers: {
    setSetting: (state, action: PayloadAction<UserEinstellungEntityDTO>) => {
      state.setting = action.payload;
    },
  },
});

export const SettingReducer = settingSlice.reducer;
export const { setSetting } = settingSlice.actions;
