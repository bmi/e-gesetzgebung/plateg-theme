// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { EinstellungenDTO } from '@plateg/rest-api';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface AppSettingState {
  appSetting: EinstellungenDTO | undefined;
}

const initialState: AppSettingState = { appSetting: undefined };

export const appSettingSlice = createSlice({
  name: 'AppSetting',
  initialState,
  reducers: {
    setAppSetting: (state, action: PayloadAction<EinstellungenDTO>) => {
      state.appSetting = action.payload;
    },
    clearAppSetting: (state) => {
      state.appSetting = undefined;
    },
  },
});

export const AppSettingReducer = appSettingSlice.reducer;
export const { setAppSetting, clearAppSetting } = appSettingSlice.actions;
