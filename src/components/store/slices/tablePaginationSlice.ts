// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import {
  FilterOptionsRequestDTOSortByEnum,
  FilterOptionsRequestDTOSortDirectionEnum,
  FilterOptionsValuesDTO,
} from '@ggp/rest-api';
import {
  BilligungStatusType,
  FilterPropertyType,
  PaginierungDTOSortByEnum,
  PaginierungDTOSortDirectionEnum,
  RegelungsvorhabenTypType,
  UmlaufStatusType,
} from '@plateg/rest-api';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export type SortOrderAntD = 'descend' | 'ascend' | null;

export type PagRequest = {
  currentPage?: number;
  columnKey?: PaginierungDTOSortByEnum | FilterOptionsRequestDTOSortByEnum;
  sortOrder?: PaginierungDTOSortDirectionEnum | FilterOptionsRequestDTOSortDirectionEnum;
  filters: { [key: string]: string | undefined };
  newFilter?: FilterPropertyType | keyof FilterOptionsValuesDTO | null;
  searchQuery?: string;
};

export type PagResult = {
  totalItems?: number;
  currentPage?: number;
  allContentEmpty?: boolean;
  filterInfo?: PagFilterInfo;
};

export type PagFilterInfo = {
  filterNames?: Array<FilterPropertyType> | Array<keyof FilterOptionsValuesDTO>;
  // rv
  vorhabenartFilter?: Array<RegelungsvorhabenTypType>;
  regelungsvorhabenFilter?: Array<string>;
  erstellerFilter?: { [key: string]: string };
  // hra
  abstimmungregelungsvorhabenFilter?: Array<string>;
  abstimmungsartFilter?: Array<string>;
  unterabstimmunganfrageFilter?: Array<string>;
  // zeit
  billigungstatusFilter?: Array<BilligungStatusType>;
  billigungregelungsvorhabenFilter?: Array<string>;
  ursprungFilter?: Array<string>;
  zeitplanungvorhabenartFilter?: Array<RegelungsvorhabenTypType>;
  //print
  drucksachenstatusFilter?: Array<string>;
  initiant_userFilter?: Array<string>;
  initiant_institutionFilter?: Array<string>;
  drucksache_verteilstatusFilter?: Array<string>;
  vorlagenartFilter?: Array<string>;
  // umlauf
  drucksachenummerFilter?: Array<string>;
  umlaufstatusFilter?: Array<UmlaufStatusType>;
  // ggp
  sachgebieteFilter?: Array<string>;
  initiantsFilter?: Array<string>;
  phasesFilter?: Array<string>;
};

export interface PaginationInfo {
  request: PagRequest;
  result: PagResult;
}

export type TablePaginationState = {
  tabs: {
    // rv & ...
    meineRegelungsvorhaben: PaginationInfo;
    meineRegelungsvorhabenBundestag: PaginationInfo;
    entwuerfe: PaginationInfo;
    archiv: PaginationInfo;
    // egfa
    meineGesetzesfolgeabschaetzungen: PaginationInfo;
    // hra
    meineAbstimmung: PaginationInfo;
    bitteUmMitzeichnung: PaginationInfo;
    meineUnterabstimmung: PaginationInfo;
    anfragen: PaginationInfo;
    meineDokumente: PaginationInfo;
    // umlauf
    meineUmlaeufe: PaginationInfo;
    // zeit
    meineZeitplanungen: PaginationInfo;
    zeitplanungsvorlagen: PaginationInfo;
    systemvorlagen: PaginationInfo;
    billigungsanfragen: PaginationInfo;
    neuigkeiten: PaginationInfo;
    // print
    annahmestelle: PaginationInfo;
    tagesausdruckErstellen: PaginationInfo;
    angelegteDrucksachen: PaginationInfo;
    // ggp
    ggpRegelungsvorhaben: PaginationInfo;
    ggpRegelungsvorhabenMerkliste: PaginationInfo;
    // administration
    adminRegelungsvorhaben: PaginationInfo;
    [key: string]: PaginationInfo;
  };
};

export const initialPaginationState: TablePaginationState = {
  tabs: {
    meineRegelungsvorhaben: {
      request: {
        filters: {},
        currentPage: 0,
        columnKey: PaginierungDTOSortByEnum.ZuletztBearbeitet,
        sortOrder: PaginierungDTOSortDirectionEnum.Desc,
      },
      result: {},
    },
    meineRegelungsvorhabenBundestag: {
      request: {
        currentPage: 0,
        filters: {},
        columnKey: PaginierungDTOSortByEnum.Zuleitungsdatum,
        sortOrder: PaginierungDTOSortDirectionEnum.Desc,
      },
      result: {},
    },
    archiv: {
      request: {
        currentPage: 0,
        filters: {},
        columnKey: PaginierungDTOSortByEnum.ArchiviertAm,
        sortOrder: PaginierungDTOSortDirectionEnum.Desc,
      },
      result: {},
    },
    entwuerfe: {
      request: {
        currentPage: 0,
        filters: {},
        columnKey: PaginierungDTOSortByEnum.ZuletztBearbeitet,
        sortOrder: PaginierungDTOSortDirectionEnum.Desc,
      },
      result: {},
    },
    // egfa
    meineGesetzesfolgeabschaetzungen: {
      request: {
        currentPage: 0,
        filters: {},
        columnKey: PaginierungDTOSortByEnum.ErstelltAm,
        sortOrder: PaginierungDTOSortDirectionEnum.Desc,
      },
      result: {},
    },
    // hra
    meineAbstimmung: {
      request: {
        currentPage: 0,
        filters: {},
        columnKey: PaginierungDTOSortByEnum.ErstelltAm,
        sortOrder: PaginierungDTOSortDirectionEnum.Desc,
      },
      result: {},
    },
    meineUnterabstimmung: {
      request: {
        currentPage: 0,
        filters: {},
        columnKey: PaginierungDTOSortByEnum.ErstelltAm,
        sortOrder: PaginierungDTOSortDirectionEnum.Desc,
      },
      result: {},
    },
    bitteUmMitzeichnung: {
      request: {
        currentPage: 0,
        filters: {},
        columnKey: PaginierungDTOSortByEnum.ErstelltAm,
        sortOrder: PaginierungDTOSortDirectionEnum.Desc,
      },
      result: {},
    },
    anfragen: {
      request: {
        currentPage: 0,
        filters: {},
        columnKey: PaginierungDTOSortByEnum.ZuletztBearbeitet,
        sortOrder: PaginierungDTOSortDirectionEnum.Desc,
      },
      result: {},
    },
    meineDokumente: {
      request: {
        currentPage: 0,
        filters: {},
        columnKey: PaginierungDTOSortByEnum.ZuletztBearbeitet,
        sortOrder: PaginierungDTOSortDirectionEnum.Desc,
      },
      result: {},
    },
    versionHistory: {
      request: {
        currentPage: 0,
        filters: {},
        columnKey: 'VERSION' as PaginierungDTOSortByEnum,
        sortOrder: PaginierungDTOSortDirectionEnum.Desc,
      },
      result: {},
    },
    // umlauf
    meineUmlaeufe: {
      request: {
        currentPage: 0,
        filters: {},
        columnKey: PaginierungDTOSortByEnum.ErstelltAm,
        sortOrder: PaginierungDTOSortDirectionEnum.Desc,
      },
      result: {},
    },
    // zeit
    meineZeitplanungen: {
      request: {
        currentPage: 0,
        filters: {},
        columnKey: PaginierungDTOSortByEnum.ZuletztBearbeitet,
        sortOrder: PaginierungDTOSortDirectionEnum.Desc,
      },
      result: {},
    },
    billigungsanfragen: {
      request: {
        currentPage: 0,
        filters: {},
        columnKey: PaginierungDTOSortByEnum.ZuletztBearbeitet,
        sortOrder: PaginierungDTOSortDirectionEnum.Desc,
      },
      result: {},
    },
    zeitplanungsvorlagen: {
      request: {
        currentPage: 0,
        filters: {},
        columnKey: PaginierungDTOSortByEnum.ErstelltAm,
        sortOrder: PaginierungDTOSortDirectionEnum.Desc,
      },
      result: {},
    },
    systemvorlagen: {
      request: {
        currentPage: 0,
        filters: {},
        columnKey: PaginierungDTOSortByEnum.ErstelltAm,
        sortOrder: PaginierungDTOSortDirectionEnum.Desc,
      },
      result: {},
    },
    neuigkeiten: {
      request: {
        currentPage: 0,
        filters: {},
        columnKey: PaginierungDTOSortByEnum.ErstelltAm,
        sortOrder: PaginierungDTOSortDirectionEnum.Desc,
      },
      result: {},
    },
    // print
    tagesausdruckErstellen: {
      request: {
        currentPage: 0,
        filters: {},
        columnKey: PaginierungDTOSortByEnum.Eingangsdatum,
        sortOrder: PaginierungDTOSortDirectionEnum.Desc,
      },
      result: {},
    },
    annahmestelle: {
      request: {
        currentPage: 0,
        filters: {},
        columnKey: PaginierungDTOSortByEnum.Zuleitungsdatum,
        sortOrder: PaginierungDTOSortDirectionEnum.Desc,
      },
      result: {},
    },
    //GGP
    ggpRegelungsvorhaben: {
      request: {
        currentPage: 0,
        filters: {},
        columnKey: FilterOptionsRequestDTOSortByEnum.ErstelltAm,
        sortOrder: FilterOptionsRequestDTOSortDirectionEnum.Desc,
        searchQuery: '',
      },
      result: {},
    },
    ggpRegelungsvorhabenMerkliste: {
      request: {
        currentPage: 0,
        filters: {},
        columnKey: FilterOptionsRequestDTOSortByEnum.ErstelltAm,
        sortOrder: FilterOptionsRequestDTOSortDirectionEnum.Desc,
        searchQuery: '',
      },
      result: {},
    },
    // administration
    adminRegelungsvorhaben: {
      request: {
        currentPage: 0,
        filters: {},
        columnKey: PaginierungDTOSortByEnum.Abkuerzung,
        sortOrder: PaginierungDTOSortDirectionEnum.Asc,
      },
      result: {},
    },
    angelegteDrucksachen: {
      request: {
        currentPage: 0,
        filters: {},
        columnKey: PaginierungDTOSortByEnum.Verteildatum,
        sortOrder: PaginierungDTOSortDirectionEnum.Asc,
      },
      result: {},
    },
  },
};

export const getSortOrder = (sortOrder: SortOrderAntD): PaginierungDTOSortDirectionEnum | undefined => {
  if (sortOrder === 'descend') {
    return PaginierungDTOSortDirectionEnum.Desc;
  } else if (sortOrder === 'ascend') {
    return PaginierungDTOSortDirectionEnum.Asc;
  } else {
    // if sortOrder === null
    return undefined;
  }
};

export const tablePaginationSlice = createSlice({
  name: 'TablePagination',
  initialState: initialPaginationState,
  reducers: {
    setPaginationInitState: (state, action: PayloadAction<{ tabKey: string }>) => {
      const { tabKey } = action.payload;
      if (state.tabs[tabKey]) {
        state.tabs[tabKey] = initialPaginationState.tabs[tabKey];
      }
    },
    setPaginationResult: (state, action: PayloadAction<PagResult & { tabKey: string }>) => {
      const { tabKey, ...result } = action.payload;
      if (state.tabs[tabKey]) {
        state.tabs[tabKey].result = { ...state.tabs[tabKey].result, ...result };
      }
    },
    setPaginationFilterInfo: (state, action: PayloadAction<PagFilterInfo & { tabKey: string }>) => {
      const { tabKey, ...filterInfo } = action.payload;
      if (state.tabs[tabKey]) {
        state.tabs[tabKey].result.filterInfo = filterInfo;
      }
    },
    setRequestSorting: (
      state,
      action: PayloadAction<
        Pick<PagRequest, 'columnKey' | 'currentPage'> & { tabKey: string } & { sortOrder: SortOrderAntD }
      >,
    ) => {
      const { sortOrder, tabKey, columnKey } = action.payload;
      if (state.tabs[tabKey]) {
        state.tabs[tabKey].request.columnKey = sortOrder ? columnKey : undefined;
        state.tabs[tabKey].request.sortOrder = getSortOrder(sortOrder);
        state.tabs[tabKey].request.currentPage = 0;
      }
    },
    setRequestCurrentPage: (state, action: PayloadAction<{ tabKey: string } & Pick<PagRequest, 'currentPage'>>) => {
      const { tabKey, currentPage } = action.payload;
      if (state.tabs[tabKey] && currentPage) {
        state.tabs[tabKey].request.currentPage = currentPage - 1;
      }
    },

    setRequestFilter: (
      state,
      action: PayloadAction<Pick<PagRequest, 'filters' | 'newFilter'> & { tabKey: string }>,
    ) => {
      const { filters, tabKey, newFilter } = action.payload;
      const combinedFilters = { ...state.tabs[tabKey].request.filters, ...filters };
      const cleanedObject = Object.fromEntries(
        Object.entries(combinedFilters).filter(([_, value]) => value !== undefined),
      );

      if (state.tabs[tabKey]) {
        state.tabs[tabKey].request.filters = cleanedObject;
        state.tabs[tabKey].request.currentPage = 0;
        state.tabs[tabKey].request.newFilter = newFilter;
      }
    },
    setRequestResetFilter: (state, action: PayloadAction<Pick<PagRequest, 'newFilter'> & { tabKey: string }>) => {
      const { newFilter, tabKey } = action.payload;

      if (state.tabs[tabKey] && newFilter) {
        const { [newFilter]: _, ...restFilter } = state.tabs[tabKey].request.filters;
        state.tabs[tabKey].request.filters = restFilter;
        state.tabs[tabKey].request.currentPage = 0;
        state.tabs[tabKey].request.newFilter = newFilter;
      }
    },

    setRequestSearchquery: (state, action: PayloadAction<Pick<PagRequest, 'searchQuery'> & { tabKey: string }>) => {
      const { searchQuery, tabKey } = action.payload;

      if (state.tabs[tabKey]) {
        state.tabs[tabKey].request.searchQuery = searchQuery;
      }
    },

    cancelNewFilter: (state, action: PayloadAction<{ tabKey: string }>) => {
      const { tabKey } = action.payload;
      if (state.tabs[tabKey]) {
        state.tabs[tabKey].request.newFilter = null;
      }
    },
  },
});

export const TablePaginationReducer = tablePaginationSlice.reducer;
export const {
  setPaginationResult,
  setRequestSorting,
  setRequestFilter,
  cancelNewFilter,
  setRequestCurrentPage,
  setPaginationFilterInfo,
  setPaginationInitState,
  setRequestResetFilter,
  setRequestSearchquery,
} = tablePaginationSlice.actions;
