// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';

import { FilterOptionsRequestDTOSortByEnum, FilterOptionsRequestDTOSortDirectionEnum } from '@ggp/rest-api';
import {
  FilterPropertyType,
  PaginierungDTOSortByEnum,
  PaginierungDTOSortDirectionEnum,
  RegelungsvorhabenTypType,
} from '@plateg/rest-api';

import { RootState, store } from '../index';
import {
  cancelNewFilter,
  getSortOrder,
  initialPaginationState,
  PagFilterInfo,
  PagRequest,
  setPaginationFilterInfo,
  setPaginationInitState,
  setPaginationResult,
  setRequestCurrentPage,
  setRequestFilter,
  setRequestResetFilter,
  setRequestSearchquery,
  setRequestSorting,
  SortOrderAntD,
} from './tablePaginationSlice';

describe('be pagination redux store test', () => {
  it('test init state values', () => {
    expect((store.getState() as RootState).tablePagination).to.deep.equal({
      tabs: {
        meineRegelungsvorhaben: {
          request: {
            filters: {},
            currentPage: 0,
            columnKey: PaginierungDTOSortByEnum.ZuletztBearbeitet,
            sortOrder: PaginierungDTOSortDirectionEnum.Desc,
          },
          result: {},
        },
        meineRegelungsvorhabenBundestag: {
          request: {
            currentPage: 0,
            filters: {},
            columnKey: PaginierungDTOSortByEnum.Zuleitungsdatum,
            sortOrder: PaginierungDTOSortDirectionEnum.Desc,
          },
          result: {},
        },
        archiv: {
          request: {
            currentPage: 0,
            filters: {},
            columnKey: PaginierungDTOSortByEnum.ArchiviertAm,
            sortOrder: PaginierungDTOSortDirectionEnum.Desc,
          },
          result: {},
        },
        entwuerfe: {
          request: {
            currentPage: 0,
            filters: {},
            columnKey: PaginierungDTOSortByEnum.ZuletztBearbeitet,
            sortOrder: PaginierungDTOSortDirectionEnum.Desc,
          },
          result: {},
        },
        meineGesetzesfolgeabschaetzungen: {
          request: {
            currentPage: 0,
            filters: {},
            columnKey: PaginierungDTOSortByEnum.ErstelltAm,
            sortOrder: PaginierungDTOSortDirectionEnum.Desc,
          },
          result: {},
        },
        meineAbstimmung: {
          request: {
            currentPage: 0,
            filters: {},
            columnKey: PaginierungDTOSortByEnum.ErstelltAm,
            sortOrder: PaginierungDTOSortDirectionEnum.Desc,
          },
          result: {},
        },
        meineUnterabstimmung: {
          request: {
            currentPage: 0,
            filters: {},
            columnKey: PaginierungDTOSortByEnum.ErstelltAm,
            sortOrder: PaginierungDTOSortDirectionEnum.Desc,
          },
          result: {},
        },
        bitteUmMitzeichnung: {
          request: {
            currentPage: 0,
            filters: {},
            columnKey: PaginierungDTOSortByEnum.ErstelltAm,
            sortOrder: PaginierungDTOSortDirectionEnum.Desc,
          },
          result: {},
        },
        anfragen: {
          request: {
            currentPage: 0,
            filters: {},
            columnKey: PaginierungDTOSortByEnum.ZuletztBearbeitet,
            sortOrder: PaginierungDTOSortDirectionEnum.Desc,
          },
          result: {},
        },
        meineDokumente: {
          request: {
            currentPage: 0,
            filters: {},
            columnKey: PaginierungDTOSortByEnum.ZuletztBearbeitet,
            sortOrder: PaginierungDTOSortDirectionEnum.Desc,
          },
          result: {},
        },
        versionHistory: {
          request: {
            currentPage: 0,
            filters: {},
            columnKey: 'VERSION' as PaginierungDTOSortByEnum,
            sortOrder: PaginierungDTOSortDirectionEnum.Desc,
          },
          result: {},
        },
        // umlauf
        meineUmlaeufe: {
          request: {
            currentPage: 0,
            filters: {},
            columnKey: PaginierungDTOSortByEnum.ErstelltAm,
            sortOrder: PaginierungDTOSortDirectionEnum.Desc,
          },
          result: {},
        },
        // zeit
        meineZeitplanungen: {
          request: {
            currentPage: 0,
            filters: {},
            columnKey: PaginierungDTOSortByEnum.ZuletztBearbeitet,
            sortOrder: PaginierungDTOSortDirectionEnum.Desc,
          },
          result: {},
        },
        billigungsanfragen: {
          request: {
            currentPage: 0,
            filters: {},
            columnKey: PaginierungDTOSortByEnum.ZuletztBearbeitet,
            sortOrder: PaginierungDTOSortDirectionEnum.Desc,
          },
          result: {},
        },
        zeitplanungsvorlagen: {
          request: {
            currentPage: 0,
            filters: {},
            columnKey: PaginierungDTOSortByEnum.ErstelltAm,
            sortOrder: PaginierungDTOSortDirectionEnum.Desc,
          },
          result: {},
        },
        systemvorlagen: {
          request: {
            currentPage: 0,
            filters: {},
            columnKey: PaginierungDTOSortByEnum.ErstelltAm,
            sortOrder: PaginierungDTOSortDirectionEnum.Desc,
          },
          result: {},
        },
        neuigkeiten: {
          request: {
            currentPage: 0,
            filters: {},
            columnKey: PaginierungDTOSortByEnum.ErstelltAm,
            sortOrder: PaginierungDTOSortDirectionEnum.Desc,
          },
          result: {},
        },
        // print
        tagesausdruckErstellen: {
          request: {
            currentPage: 0,
            filters: {},
            columnKey: PaginierungDTOSortByEnum.Eingangsdatum,
            sortOrder: PaginierungDTOSortDirectionEnum.Desc,
          },
          result: {},
        },
        annahmestelle: {
          request: {
            columnKey: 'ZULEITUNGSDATUM',
            currentPage: 0,
            filters: {},
            sortOrder: PaginierungDTOSortDirectionEnum.Desc,
          },
          result: {},
        },
        //GGP
        ggpRegelungsvorhaben: {
          request: {
            currentPage: 0,
            filters: {},
            columnKey: FilterOptionsRequestDTOSortByEnum.ErstelltAm,
            sortOrder: FilterOptionsRequestDTOSortDirectionEnum.Desc,
            searchQuery: '',
          },
          result: {},
        },
        ggpRegelungsvorhabenMerkliste: {
          request: {
            currentPage: 0,
            filters: {},
            columnKey: FilterOptionsRequestDTOSortByEnum.ErstelltAm,
            sortOrder: FilterOptionsRequestDTOSortDirectionEnum.Desc,
            searchQuery: '',
          },
          result: {},
        },
        // administration
        adminRegelungsvorhaben: {
          request: {
            currentPage: 0,
            filters: {},
            columnKey: PaginierungDTOSortByEnum.Abkuerzung,
            sortOrder: PaginierungDTOSortDirectionEnum.Asc,
          },
          result: {},
        },
        angelegteDrucksachen: {
          request: {
            columnKey: PaginierungDTOSortByEnum.Verteildatum,
            currentPage: 0,
            filters: {},
            sortOrder: PaginierungDTOSortDirectionEnum.Asc,
          },
          result: {},
        },
      },
    });
  });

  it('test save result', () => {
    const apiResponseData = {
      tabKey: 'entwuerfe',
      allContentEmpty: false,
      currentPage: 0,
      filterNames: [FilterPropertyType.Vorhabenart],
      totalItems: 10,
      vorhabenartFilter: [RegelungsvorhabenTypType.Gesetz, RegelungsvorhabenTypType.Rechtsverordnung],
    };

    store.dispatch(setPaginationResult(apiResponseData));

    const { tabKey, ...result } = apiResponseData;

    expect((store.getState() as RootState).tablePagination.tabs.archiv.result).not.to.deep.equal(result);
    expect((store.getState() as RootState).tablePagination.tabs.entwuerfe.result).to.deep.equal(result);
    expect((store.getState() as RootState).tablePagination.tabs.entwuerfe.result).to.not.have.property(tabKey);
  });

  it('test set filter request', () => {
    store.dispatch(
      setRequestFilter({
        newFilter: FilterPropertyType.Vorhabenart,
        filters: { [FilterPropertyType.Vorhabenart]: RegelungsvorhabenTypType.Gesetz },
        tabKey: 'archiv',
      }),
    );

    expect((store.getState() as RootState).tablePagination.tabs.archiv.request).to.deep.equal({
      filters: { [FilterPropertyType.Vorhabenart]: RegelungsvorhabenTypType.Gesetz },
      currentPage: 0,
      newFilter: FilterPropertyType.Vorhabenart,
      columnKey: PaginierungDTOSortByEnum.ArchiviertAm,
      sortOrder: PaginierungDTOSortDirectionEnum.Desc,
    } as PagRequest);

    expect((store.getState() as RootState).tablePagination.tabs.archiv.request).not.to.have.property('tabKey');
  });
  it('test set filter request with undefined data', () => {
    store.dispatch(
      setRequestFilter({
        newFilter: FilterPropertyType.Vorhabenart,
        filters: { [FilterPropertyType.Vorhabenart]: undefined },
        tabKey: 'archiv',
      }),
    );

    expect((store.getState() as RootState).tablePagination.tabs.archiv.request).to.deep.equal({
      filters: {},
      currentPage: 0,
      newFilter: FilterPropertyType.Vorhabenart,
      columnKey: PaginierungDTOSortByEnum.ArchiviertAm,
      sortOrder: PaginierungDTOSortDirectionEnum.Desc,
    } as PagRequest);

    expect((store.getState() as RootState).tablePagination.tabs.archiv.request).not.to.have.property('tabKey');
  });

  it('test cancel filter', () => {
    store.dispatch(cancelNewFilter({ tabKey: 'archiv' } as { tabKey: string }));

    expect((store.getState() as RootState).tablePagination.tabs.archiv.request.newFilter).to.equal(null);
  });

  it('test set sorting request', () => {
    const sortData: Pick<PagRequest, 'columnKey'> & { tabKey: string } & { sortOrder: SortOrderAntD } = {
      tabKey: 'meineRegelungsvorhaben',
      columnKey: PaginierungDTOSortByEnum.ZuletztBearbeitet,
      sortOrder: 'ascend',
    };
    store.dispatch(setRequestSorting(sortData));

    expect((store.getState() as RootState).tablePagination.tabs.meineRegelungsvorhaben.request).to.deep.equal({
      columnKey: PaginierungDTOSortByEnum.ZuletztBearbeitet,
      sortOrder: PaginierungDTOSortDirectionEnum.Asc,
      filters: {},
      currentPage: 0,
    } as Omit<PagRequest, 'newFilter'>);
  });

  it('test set current page request', () => {
    const pageData: { tabKey: string } & Pick<PagRequest, 'currentPage'> = {
      tabKey: 'meineGesetzesfolgeabschaetzungen',
      currentPage: 2,
    };
    store.dispatch(setRequestCurrentPage(pageData));
    expect(
      (store.getState() as RootState).tablePagination.tabs.meineGesetzesfolgeabschaetzungen.request.currentPage,
    ).to.equal(1);
  });

  it('test getSortorder func', () => {
    expect(getSortOrder('ascend')).to.equal(PaginierungDTOSortDirectionEnum.Asc);
    expect(getSortOrder('descend')).to.equal(PaginierungDTOSortDirectionEnum.Desc);
    expect(getSortOrder(null)).to.equal(undefined);
    expect(getSortOrder(null)).to.equal(undefined);
    expect(getSortOrder(null)).not.to.equal(null);
  });

  const filterInfoData: PagFilterInfo & { tabKey: string } = {
    tabKey: 'meineGesetzesfolgeabschaetzungen',
    regelungsvorhabenFilter: ['rv 1', 'rv 2'],
    filterNames: [FilterPropertyType.Ersteller, FilterPropertyType.Regelungsvorhaben],
  };

  it('test set filter info', () => {
    const { tabKey, ...filterInfo } = filterInfoData;
    store.dispatch(setPaginationFilterInfo(filterInfoData));

    expect((store.getState() as RootState).tablePagination.tabs[tabKey].result.filterInfo).to.deep.equal(filterInfo);
  });

  it('test set init state', () => {
    const tabKey = 'entwuerfe';
    store.dispatch(setPaginationFilterInfo(filterInfoData));
    store.dispatch(setPaginationInitState({ tabKey }));

    expect((store.getState() as RootState).tablePagination.tabs[tabKey]).to.deep.equal(
      initialPaginationState.tabs[tabKey],
    );
  });
  it('test set init state', () => {
    const tabKey = 'annahmestelle';
    store.dispatch(setPaginationFilterInfo(filterInfoData));
    store.dispatch(setPaginationInitState({ tabKey }));

    expect((store.getState() as RootState).tablePagination.tabs[tabKey]).to.deep.equal(
      initialPaginationState.tabs[tabKey],
    );
  });
  it('test set init state angelegteDrucksachen', () => {
    const tabKey = 'angelegteDrucksachen';
    store.dispatch(setPaginationFilterInfo(filterInfoData));
    store.dispatch(setPaginationInitState({ tabKey }));

    expect((store.getState() as RootState).tablePagination.tabs[tabKey]).to.deep.equal(
      initialPaginationState.tabs[tabKey],
    );
  });

  it('test request reset filter ', () => {
    const tabKey = 'archiv';
    store.dispatch(
      setRequestFilter({
        newFilter: FilterPropertyType.Vorhabenart,
        filters: { [FilterPropertyType.Vorhabenart]: RegelungsvorhabenTypType.Gesetz },
        tabKey,
      }),
    );

    store.dispatch(setRequestResetFilter({ tabKey, newFilter: FilterPropertyType.Vorhabenart }));
    expect((store.getState() as RootState).tablePagination.tabs[tabKey].request.newFilter).to.equal(
      FilterPropertyType.Vorhabenart,
    );
    expect((store.getState() as RootState).tablePagination.tabs[tabKey].request.filters).to.deep.equal({});
  });

  it('test request search query', () => {
    const tabKey = 'ggpRegelungsvorhaben';
    const searchQuery = 'search query';
    store.dispatch(
      setRequestSearchquery({
        tabKey,
        searchQuery,
      }),
    );

    expect((store.getState() as RootState).tablePagination.tabs[tabKey].request.searchQuery).to.equal(searchQuery);
  });

  it('test request search query with empty string', () => {
    const tabKey = 'ggpRegelungsvorhaben';
    const searchQuery = '';
    store.dispatch(
      setRequestSearchquery({
        tabKey,
        searchQuery,
      }),
    );

    expect((store.getState() as RootState).tablePagination.tabs[tabKey].request.searchQuery).to.equal('');
  });

  it('test request search query', () => {
    const tabKey = 'ggpRegelungsvorhabenMerkliste';
    const searchQuery = 'search query';
    store.dispatch(
      setRequestSearchquery({
        tabKey,
        searchQuery,
      }),
    );

    expect((store.getState() as RootState).tablePagination.tabs[tabKey].request.searchQuery).to.equal(searchQuery);
  });

  it('test request search query with empty string', () => {
    const tabKey = 'ggpRegelungsvorhabenMerkliste';
    const searchQuery = '';
    store.dispatch(
      setRequestSearchquery({
        tabKey,
        searchQuery,
      }),
    );

    expect((store.getState() as RootState).tablePagination.tabs[tabKey].request.searchQuery).to.equal('');
  });

  it('test request search query with wrong tab key', () => {
    const tabKey = 'wrongTabKey';
    const searchQuery = 'search query';
    store.dispatch(
      setRequestSearchquery({
        tabKey,
        searchQuery,
      }),
    );

    expect((store.getState() as RootState).tablePagination.tabs[tabKey]).to.equal(undefined);
  });
});
