// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';

import { RootState, store } from '../index';
import { clearUser, setUser } from './userSlice';

describe('user redux store test', () => {
  it('test intial state values', () => {
    expect((store.getState() as RootState).user.user).to.equal(undefined);
  });

  it('test save new user', () => {
    store.dispatch(
      setUser({
        base: { bearbeitetAm: '', erstelltAm: '', id: '1' },
        dto: { email: 'ff@gg.vv', name: 'max' },
      }),
    );
    expect((store.getState() as RootState).user.user?.dto.name).to.equal('max');
  });
  it('test clear  user', () => {
    store.dispatch(
      setUser({
        base: { bearbeitetAm: '', erstelltAm: '', id: '1' },
        dto: { email: 'ff@gg.vv', name: 'max' },
      }),
    );
    store.dispatch(clearUser());
    expect((store.getState() as RootState).user.user).to.equal(undefined);
  });
});
