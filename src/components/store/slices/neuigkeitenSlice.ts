// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { NeuigkeitEntityDTO } from '@plateg/rest-api';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface NeuigkeitenState {
  neuigkeitenData: NeuigkeitEntityDTO[];
  neuigkeitenHomeData: NeuigkeitEntityDTO[];
  ungeleseneNeuigkeiten: number;
  neuigkeitenInsgesamt: number;
}

const initialState: NeuigkeitenState = {
  neuigkeitenData: [],
  neuigkeitenHomeData: [],
  ungeleseneNeuigkeiten: 0,
  neuigkeitenInsgesamt: 0,
};

export const NeuigkeitenSlice = createSlice({
  name: 'neuigkeiten',
  initialState,
  reducers: {
    setNeuigkeiten: (state, action: PayloadAction<NeuigkeitenState>) => {
      state.neuigkeitenData = action.payload.neuigkeitenData;
      state.neuigkeitenHomeData = action.payload.neuigkeitenHomeData;
      state.ungeleseneNeuigkeiten = action.payload.ungeleseneNeuigkeiten;
      state.neuigkeitenInsgesamt = action.payload.neuigkeitenInsgesamt;
    },

    setNeuigkeitenGelesen: (state) => {
      state.neuigkeitenData.forEach((neuigkeit) => {
        neuigkeit.gelesen = true;
      });
      state.neuigkeitenHomeData.forEach((neuigkeit) => {
        neuigkeit.gelesen = true;
      });
      state.ungeleseneNeuigkeiten = 0;
    },
    deleteNeuigkeiten: (state) => {
      state.neuigkeitenData = [];
      state.neuigkeitenHomeData = [];
      state.neuigkeitenInsgesamt = 0;
      state.ungeleseneNeuigkeiten = 0;
    },
    deleteNeuigkeit: (state, action: PayloadAction<{ id: string }>) => {
      const deletedNeuigkeit =
        state.neuigkeitenData.find((item) => item.id === action.payload.id) ||
        state.neuigkeitenHomeData.find((item) => item.id === action.payload.id);
      if (deletedNeuigkeit) {
        state.neuigkeitenData = [...state.neuigkeitenData.filter((item) => item.id !== action.payload.id)];
        state.neuigkeitenHomeData = [...state.neuigkeitenHomeData.filter((item) => item.id !== action.payload.id)];
        state.ungeleseneNeuigkeiten = deletedNeuigkeit.gelesen
          ? state.ungeleseneNeuigkeiten
          : state.ungeleseneNeuigkeiten - 1;
        state.neuigkeitenInsgesamt = state.neuigkeitenInsgesamt - 1;
      }
    },
    setNeuigkeitGelesen: (state, action: PayloadAction<{ id: string }>) => {
      const foundNeuigkeitList = [...state.neuigkeitenData, ...state.neuigkeitenHomeData].filter(
        (item) => item.id === action.payload.id,
      );
      if (foundNeuigkeitList.length > 0) {
        const wasRead = foundNeuigkeitList[0].gelesen;
        foundNeuigkeitList.forEach((foundNeuigkeit) => {
          foundNeuigkeit.gelesen = true;
        });
        state.ungeleseneNeuigkeiten = wasRead ? state.ungeleseneNeuigkeiten : state.ungeleseneNeuigkeiten - 1;
      }
    },
    setNeuigkeitLesen: (state, action: PayloadAction<{ id: string; gelesen: boolean }>) => {
      const foundNeuigkeitList = [...state.neuigkeitenData, ...state.neuigkeitenHomeData].filter(
        (item) => item.id === action.payload.id,
      );

      if (foundNeuigkeitList.length > 0) {
        foundNeuigkeitList.forEach((foundNeuigkeit) => {
          foundNeuigkeit.gelesen = action.payload.gelesen;
        });
        state.ungeleseneNeuigkeiten = action.payload.gelesen
          ? state.ungeleseneNeuigkeiten - 1
          : state.ungeleseneNeuigkeiten + 1;
      }
    },
  },
});

export const NeuigkeitenReducer = NeuigkeitenSlice.reducer;
export const {
  deleteNeuigkeit,
  setNeuigkeiten,
  deleteNeuigkeiten,
  setNeuigkeitenGelesen,
  setNeuigkeitGelesen,
  setNeuigkeitLesen,
} = NeuigkeitenSlice.actions;
