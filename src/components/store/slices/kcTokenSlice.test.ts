// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';

import { RootState, store } from '../index';
import { clearKcToken, setKcToken } from './kcTokenSlice';

describe('keycloak token redux store test', () => {
  it('test intial state values', () => {
    expect((store.getState() as RootState).kcToken.kcToken).to.equal(undefined);
  });

  it('test save new KC token', () => {
    store.dispatch(
      setKcToken({
        idToken: 'idtoken',
        refreshToken: 'refreshToken',
        token: 'token',
      }),
    );
    expect((store.getState() as RootState).kcToken.kcToken?.token).to.equal('token');
  });
  it('test clear  App Setting', () => {
    store.dispatch(
      setKcToken({
        idToken: 'idtoken',
        refreshToken: 'refreshToken',
        token: 'token',
      }),
    );
    store.dispatch(clearKcToken());
    expect((store.getState() as RootState).kcToken.kcToken).to.equal(undefined);
  });
});
