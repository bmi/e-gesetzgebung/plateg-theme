// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { UserEntityWithStellvertreterResponseDTO } from '@plateg/rest-api';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface UserState {
  user: UserEntityWithStellvertreterResponseDTO | undefined;
}

const initialState: UserState = { user: undefined };

export const userSlice = createSlice({
  name: 'User',
  initialState,
  reducers: {
    setUser: (state, action: PayloadAction<UserEntityWithStellvertreterResponseDTO>) => {
      state.user = action.payload;
    },
    clearUser: (state) => {
      state.user = undefined;
    },
  },
});

export const UserReducer = userSlice.reducer;
export const { setUser, clearUser } = userSlice.actions;
