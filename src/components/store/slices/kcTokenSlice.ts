// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { AuthClientTokens } from '@react-keycloak/core';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface KcTokenState {
  kcToken: AuthClientTokens | undefined;
}

const initialState: KcTokenState = { kcToken: undefined };

export const kcTokenSlice = createSlice({
  name: 'KcToken',
  initialState,
  reducers: {
    setKcToken: (state, action: PayloadAction<AuthClientTokens>) => {
      state.kcToken = action.payload;
    },
    clearKcToken: (state) => {
      state.kcToken = undefined;
    },
  },
});

export const KcTokenReducer = kcTokenSlice.reducer;
export const { setKcToken, clearKcToken } = kcTokenSlice.actions;
