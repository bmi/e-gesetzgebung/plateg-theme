// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';

import { RootState, store } from '../index';
import { clearAppSetting, setAppSetting } from './appSettingSlice';

describe('App Setting redux store test', () => {
  it('test intial state values', () => {
    expect((store.getState() as RootState).appSetting.appSetting).to.equal(undefined);
  });

  it('test save new App Setting', () => {
    store.dispatch(
      setAppSetting({
        sessionIdle: 30,
        sessionMax: 600,
        iamEinstellungen: {
          realm: 'bund',
          authServerUrl: 'https://iam.example.url.com',
          pkpUrl: 'https://pkp.example.url.com',
          sslRequired: 'external',
          resource: 'egg-public',
          publicClient: true,
          confidentialPort: 0,
          flowType: 'implicit',
        },
      }),
    );
    expect((store.getState() as RootState).appSetting.appSetting?.sessionIdle).to.equal(30);
  });
  it('test clear  App Setting', () => {
    store.dispatch(
      setAppSetting({
        sessionIdle: 30,
        sessionMax: 600,
        iamEinstellungen: {
          realm: 'bund',
          authServerUrl: 'https://iam.example.url.com',
          pkpUrl: 'https://pkp.example.url.com',
          sslRequired: 'external',
          resource: 'egg-public',
          publicClient: true,
          confidentialPort: 0,
          flowType: 'implicit',
        },
      }),
    );
    store.dispatch(clearAppSetting());
    expect((store.getState() as RootState).appSetting.appSetting).to.equal(undefined);
  });
});
