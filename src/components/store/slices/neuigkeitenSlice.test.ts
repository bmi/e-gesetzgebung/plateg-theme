// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';

import { RootState, store } from '../index';
import {
  deleteNeuigkeit,
  deleteNeuigkeiten,
  setNeuigkeiten,
  setNeuigkeitenGelesen,
  setNeuigkeitGelesen,
  setNeuigkeitLesen,
} from './neuigkeitenSlice';

describe('neuigkeiten redux store test', () => {
  const defaultNeigkeiten = [
    {
      id: '11',
      nachricht: 'Neigkeit 1',
      erstelltAm: '01.01.2023',
      gelesen: true,
    },
    {
      id: '12',
      nachricht: 'Neigkeit 2',
      erstelltAm: '02.01.2023',
      gelesen: true,
    },
    {
      id: '13',
      nachricht: 'Neigkeit 3',
      erstelltAm: '03.01.2023',
      gelesen: true,
    },
    {
      id: '14',
      nachricht: 'Neigkeit 4',
      erstelltAm: '04.01.2023',
      gelesen: false,
    },
  ];
  it('test intial state values', () => {
    expect((store.getState() as RootState).neuigkeiten.ungeleseneNeuigkeiten).to.equal(0);
    expect((store.getState() as RootState).neuigkeiten.neuigkeitenInsgesamt).to.equal(0);
    expect((store.getState() as RootState).neuigkeiten.neuigkeitenData.length).to.equal(0);
  });

  it('test save new Neuigkeiten', () => {
    store.dispatch(
      setNeuigkeiten({
        neuigkeitenData: defaultNeigkeiten,
        neuigkeitenInsgesamt: 150,
        ungeleseneNeuigkeiten: 2,
      }),
    );
    expect((store.getState() as RootState).neuigkeiten.ungeleseneNeuigkeiten).to.equal(2);
    expect((store.getState() as RootState).neuigkeiten.neuigkeitenInsgesamt).to.equal(150);
    expect((store.getState() as RootState).neuigkeiten.neuigkeitenData.length).to.equal(4);
  });

  it('test delete all Neuigkeiten', () => {
    store.dispatch(
      setNeuigkeiten({
        neuigkeitenData: defaultNeigkeiten,
        neuigkeitenInsgesamt: 150,
        ungeleseneNeuigkeiten: 1,
      }),
    );
    store.dispatch(deleteNeuigkeiten());

    expect((store.getState() as RootState).neuigkeiten.ungeleseneNeuigkeiten).to.equal(0);
    expect((store.getState() as RootState).neuigkeiten.neuigkeitenInsgesamt).to.equal(0);
    expect((store.getState() as RootState).neuigkeiten.neuigkeitenData.length).to.equal(0);
  });

  it('test delete one Neuigkeiten', () => {
    store.dispatch(
      setNeuigkeiten({
        neuigkeitenData: defaultNeigkeiten,
        neuigkeitenHomeData: defaultNeigkeiten,
        neuigkeitenInsgesamt: 150,
        ungeleseneNeuigkeiten: 1,
      }),
    );
    store.dispatch(deleteNeuigkeit({ id: '14' }));

    expect((store.getState() as RootState).neuigkeiten.ungeleseneNeuigkeiten).to.equal(0);
    expect((store.getState() as RootState).neuigkeiten.neuigkeitenInsgesamt).to.equal(149);
    expect((store.getState() as RootState).neuigkeiten.neuigkeitenData.length).to.equal(3);
  });

  it('test make all Neuigkeiten read ', () => {
    store.dispatch(
      setNeuigkeiten({
        neuigkeitenData: defaultNeigkeiten,
        neuigkeitenHomeData: defaultNeigkeiten,
        neuigkeitenInsgesamt: 150,
        ungeleseneNeuigkeiten: 1,
      }),
    );
    store.dispatch(setNeuigkeitenGelesen());

    expect((store.getState() as RootState).neuigkeiten.ungeleseneNeuigkeiten).to.equal(0);
    expect((store.getState() as RootState).neuigkeiten.neuigkeitenData.filter((item) => !item.gelesen).length).to.equal(
      0,
    );
  });
  it('test make one Neuigkeit read ', () => {
    store.dispatch(
      setNeuigkeiten({
        neuigkeitenData: defaultNeigkeiten,
        neuigkeitenHomeData: defaultNeigkeiten,
        neuigkeitenInsgesamt: 150,
        ungeleseneNeuigkeiten: 1,
      }),
    );
    store.dispatch(setNeuigkeitGelesen({ id: '14' }));

    expect((store.getState() as RootState).neuigkeiten.ungeleseneNeuigkeiten).to.equal(0);
    expect((store.getState() as RootState).neuigkeiten.neuigkeitenData.filter((item) => !item.gelesen).length).to.equal(
      0,
    );
  });
  it('test make one Neuigkeit unread ', () => {
    store.dispatch(
      setNeuigkeiten({
        neuigkeitenData: defaultNeigkeiten,
        neuigkeitenHomeData: defaultNeigkeiten,
        neuigkeitenInsgesamt: 150,
        ungeleseneNeuigkeiten: 1,
      }),
    );
    store.dispatch(setNeuigkeitLesen({ id: '13', gelesen: false }));

    expect((store.getState() as RootState).neuigkeiten.ungeleseneNeuigkeiten).to.equal(2);
    expect((store.getState() as RootState).neuigkeiten.neuigkeitenData.filter((item) => !item.gelesen).length).to.equal(
      2,
    );
  });
});
