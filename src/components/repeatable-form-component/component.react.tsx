// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import React, { useEffect, useState } from 'react';

import { PlusOutlined } from '../icons/PlusOutlined';

interface RepeatableType<T extends { id: string }> {
  emptyItem: () => T;
  textAddItem: string;
  setRepeatableItem: (items: T[]) => void;
  getRepeatableItem: () => T[];
  repeatableComponent: (item: T, index: number, deleteItem: (index: number) => void) => React.ReactElement;
  setDirtyFlag?: Function;
}

export function RepeatableFormComponent<T extends { id: string }>(props: RepeatableType<T>): React.ReactElement {
  const [repeatableElements, setRepeatableElements] = useState<React.ReactElement[]>([]);

  // Method to create all repeatable components
  const createRepeatableElements = (repeatableItem: T[]) => {
    const repeatableElementsTmp = repeatableItem?.map((item, index) => {
      return (
        <section key={item.id} className="repeatable-element">
          {props.repeatableComponent(item, index, deleteItem)}
        </section>
      );
    });
    setRepeatableElements(repeatableElementsTmp);
  };
  // Initiate & update repeatable components
  useEffect(() => {
    createRepeatableElements(props.getRepeatableItem());
  }, [props.repeatableComponent]);

  const changeItem = (changeFunc: (items: T[]) => void) => {
    const items = props.getRepeatableItem();
    changeFunc(items);
    createRepeatableElements(items);
    props.setRepeatableItem(items);
  };

  const addItem = () => {
    if (props.setDirtyFlag) {
      props.setDirtyFlag();
    }
    changeItem((items) => items.push(props.emptyItem()));
  };

  const deleteItem = (index: number) => {
    if (props.setDirtyFlag) {
      props.setDirtyFlag();
    }
    changeItem((items) => items.splice(index, 1));
  };

  return (
    <>
      {repeatableElements}
      <Button
        id={`theme-generic-${props.textAddItem.toLowerCase().split(' ').join('-')}-addItem-btn`}
        icon={<PlusOutlined />}
        type="text"
        onClick={addItem}
        className="blue-text-button"
      >
        {props.textAddItem}
      </Button>
    </>
  );
}
