// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { PickerDateProps } from 'antd/es/date-picker/generatePicker';
import React from 'react';

import Datepicker from '../custom-antd-components/datepicker/component.react';

export function MainContentDatepickerWrapper(
  props: React.PropsWithChildren<Omit<PickerDateProps<Date>, 'picker'>>,
): React.ReactElement {
  const popupContainer = document.getElementsByClassName('main-content-area')[0];
  const getPopupContainerProp = popupContainer ? { getPopupContainer: () => popupContainer as HTMLElement } : undefined;
  return <Datepicker {...props} {...(props.getPopupContainer ?? getPopupContainerProp)} />;
}
