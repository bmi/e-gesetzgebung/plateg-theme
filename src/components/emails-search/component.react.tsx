// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Space } from 'antd';
import { Rule } from 'antd/lib/form';
import { NamePath } from 'antd/lib/form/interface';
import { FormInstance } from 'rc-field-form';
import React, { useEffect, useState } from 'react';
import { Subscription } from 'rxjs';

import {
  FreigabeType,
  RolleGlobalType,
  RolleLokalType,
  UserEntityResponseDTO,
  UserEntityShortDTO,
} from '@plateg/rest-api';

import { FormItemWithInfo, InfoComponent } from '../';
import { DeleteConfirmContent } from '../delete-confirm-popover/component.react';
import { EmailSelectController } from '../emailsSelect/controller';
import { FreigabeWithType } from '../freigabe-modal/component.react';
import { AusschussRoles } from '../freigabe-modal/freigabe-list/component.react';
import { SearchFieldComponent } from './search-field/component.react';
import { SelectedEmailsComponent } from './selected-emails/component.react';

export interface FreigabeList {
  options: FreigabeType[] | RolleGlobalType[] | RolleLokalType[];
  initialVals: {
    freigabenWithType: FreigabeWithType[];
  };
  undeletableEmails: string[];
}
export interface EmailsSearchInterface {
  name: string;
  form: FormInstance;
  currentUserEmail: string;
  currentUserRessortId?: string;
  rules?: Rule[];
  dependencies?: NamePath[];
  extra?: React.ReactNode;
  className?: string;
  checkIntersection?: (value: string[]) => boolean;
  updateFormField: (fieldName: string, value: string[]) => void;
  texts: EmailSearchTexts;
  freigabeListProps?: FreigabeList;
  ersteller?: UserEntityShortDTO;
  readonly?: boolean;
  isRowView?: boolean;
  setSelectionHasChanged?: () => void;
  disabled?: boolean;
  documentEditor?: UserEntityResponseDTO;
  isVorhabenclearing?: boolean;
  isStaticFreigabenText?: boolean;
  setUsersDataOnParent?: (usersList: UserEntityShortDTO[]) => void;
  searchOnlyBRUsers?: boolean;
  searchOnlyBTUsers?: boolean;
  searchOnlyWithRessortId?: string;
  searchOnlyWithReferatId?: string;
  isOnlyOneUser?: boolean;
  onlyOneUserPopoverText?: string;
  allowSelf?: boolean;
  allowUnknownEmail?: boolean;
  ignoreDirectEmail?: boolean;
  deleteUserImmediatelyCall?: (user: UserEntityShortDTO) => void;
  immediatelyDeletedUser?: UserEntityShortDTO;
  isRBACActivated?: boolean;
  deleteConfirmContent?: DeleteConfirmContent;
  isZeitplanungvorlage?: boolean;
  revokeObserverRight?: boolean;
  onRevokeEditingRight?: () => void;
  ccMode?: CCModeInterface;
  onlyEditUsers?: boolean;
  isEGFAModule?: boolean;
  showSelectedBeforeSearchField?: boolean;
  undeletableTagEmails?: string[];
  ausschussRoles?: AusschussRoles[];
}

export interface EmailSearchTexts {
  searchLabel: string;
  searchDrawer:
    | {
        searchDrawerTitle: string;
        searchDrawerText: string;
      }
    | React.ReactElement;
  searchHint: string;
  addressLabel: string;
  addressSubLabel?: string;
  addressDrawer:
    | {
        addressDrawerTitle: string;
        addressDrawerText: string;
      }
    | React.ReactElement;
  noAddresses?: string;
}

interface CCModeInterface {
  name: string;
  texts: EmailSearchTexts;
  rules?: Rule[];
  dependencies?: NamePath[];
  checkIntersection?: (value: string[]) => boolean;
  className?: string;
}

export function EmailsSearchComponent(props: Readonly<EmailsSearchInterface>): React.ReactElement {
  const ctrl = new EmailSelectController();
  const formList = props.form.getFieldValue(props.name) as string[];
  const formListCC = props.form.getFieldValue(props.ccMode?.name) as string[];
  const [usersData, setUsersData] = useState<UserEntityShortDTO[]>([]);
  const [usersDataInCC, setUsersDataInCC] = useState<UserEntityShortDTO[]>([]);

  useEffect(() => {
    if (props.freigabeListProps && usersData.length) {
      setUsersData([]);
    }
  }, [props.freigabeListProps]);

  useEffect(() => {
    let subInitialData: Subscription;
    let subInitialDataCC: Subscription;
    if (formList?.length) {
      subInitialData = ctrl.getUsersByEmailCall(formList).subscribe((data) => {
        setUsersData(data);
      });
    }
    if (formListCC?.length) {
      subInitialDataCC = ctrl.getUsersByEmailCall(formListCC).subscribe((data) => {
        setUsersDataInCC(data);
      });
    }
    return () => {
      subInitialData?.unsubscribe();
      subInitialDataCC?.unsubscribe();
    };
  }, [formList, formListCC]);

  useEffect(() => {
    const localData = usersData.map((user) => user.email).filter((e) => e !== undefined);
    const formValues = props.form.getFieldValue(props.name) as string[];
    //teilnehmer for fix in HRA, breaks permission modal and editor otherwise
    if (
      props.name === 'teilnehmer' &&
      !(localData.length === formValues.length && localData.every((d, i) => d === formValues[i]))
    ) {
      const listOfUsers = formValues.map((email) => {
        return {
          email,
        } as UserEntityShortDTO;
      });
      setUsersData(listOfUsers);
    }
  }, [props.form.getFieldValue(props.name)]);

  const selectUser = (users: UserEntityShortDTO[], isRewrite = false, isCC = false) => {
    let setUserMethod = setUsersData;
    let fieldName = props.name;
    let localUsersData = usersData;
    if (isCC && props.ccMode) {
      setUserMethod = setUsersDataInCC;
      fieldName = props.ccMode?.name;
      localUsersData = usersDataInCC;
    }

    let list: string[] = [];
    if (props.isOnlyOneUser) {
      list = [users[0].email];
      setUserMethod(users);
    } else {
      list = props.form.getFieldValue(fieldName) as string[];
      const preparedNewUsersEmailList = users.map((user) => user.email).filter((item) => !list.includes(item));
      if (isRewrite) {
        list = [...preparedNewUsersEmailList];
        setUserMethod([...users]);
      } else {
        list.push(...preparedNewUsersEmailList);
        // Update users data with full user info
        setUserMethod([...localUsersData, ...users]);
      }
    }
    // Update form field with emails list
    props.updateFormField(fieldName, list);
    props.setSelectionHasChanged?.();
  };

  useEffect(() => {
    if (props.immediatelyDeletedUser) {
      deleteUser(props.immediatelyDeletedUser, false);
    }
  }, [props.immediatelyDeletedUser]);

  const deleteUser = (user: UserEntityShortDTO, freigabe?: boolean, isCC = false) => {
    let setUserMethod = setUsersData;
    let fieldName = props.name;
    let localUsersData = usersData;
    if (isCC && props.ccMode) {
      setUserMethod = setUsersDataInCC;
      fieldName = props.ccMode?.name;
      localUsersData = usersDataInCC;
    }

    let list: string[] = props.form.getFieldValue(fieldName) as string[];
    list = list.filter((email) => email.toLowerCase() !== user.email.toLowerCase());
    // Update form field with emails list
    props.updateFormField(fieldName, list);
    if (!freigabe) {
      const freigabenWithTypeFields = props.form.getFieldValue('freigabenWithType') as FreigabeWithType[];
      if (freigabenWithTypeFields) {
        props.form.setFieldsValue({
          freigabenWithType: freigabenWithTypeFields.map((item) => {
            if (Object.getOwnPropertyNames(item)?.[0].toLowerCase() === user.email.toLowerCase()) {
              item[user.email.toLowerCase()] = props.deleteUserImmediatelyCall
                ? props.freigabeListProps?.options[0]
                : undefined;
            }
            return item;
          }),
        });
      }
    }
    // Update users data with full user info
    setUserMethod([...localUsersData.filter((item) => item.email.toLowerCase() !== user.email.toLowerCase())]);
    props.setSelectionHasChanged?.();
  };

  const searchInfo =
    props.texts.searchDrawer.hasOwnProperty('searchDrawerTitle') &&
    props.texts.searchDrawer.hasOwnProperty('searchDrawerText') ? (
      <InfoComponent title={(props.texts.searchDrawer as { searchDrawerTitle: string }).searchDrawerTitle}>
        <p
          dangerouslySetInnerHTML={{
            __html: (props.texts.searchDrawer as { searchDrawerText: string }).searchDrawerText,
          }}
        />
      </InfoComponent>
    ) : (
      (props.texts.searchDrawer as React.ReactElement)
    );

  const extendMarkedUsers = (usersData: UserEntityShortDTO[]) => {
    const { name, ersteller, documentEditor } = props;
    const localUsersData = [...usersData];

    if (name === 'freigaben') {
      if (ersteller) {
        localUsersData.push(ersteller);
      }
      if (documentEditor?.dto) {
        localUsersData.push(documentEditor.dto as unknown as UserEntityShortDTO);
      }
    }

    return localUsersData;
  };

  const searchField = !(props.readonly || props.onlyEditUsers) ? (
    <FormItemWithInfo
      htmlFor={`email-search-field-${props.name}`}
      style={props.readonly ? { display: 'none' } : {}}
      label={
        <>
          {props.texts.searchLabel}
          {searchInfo}
        </>
      }
    >
      <Space direction="vertical" size={'small'} style={{ width: '100%' }}>
        <span className="hint">{props.texts.searchHint}</span>
        <SearchFieldComponent
          searchOnlyBRUsers={props.searchOnlyBRUsers}
          searchOnlyBTUsers={props.searchOnlyBTUsers}
          searchOnlyWithRessortId={props.searchOnlyWithRessortId}
          searchOnlyWithReferatId={props.searchOnlyWithReferatId}
          disabled={props.disabled}
          onApply={selectUser}
          selectedUsers={[...extendMarkedUsers(usersData)]}
          fieldName={props.name}
          allowUnknownEmail={props.allowUnknownEmail}
          ignoreDirectEmail={props.ignoreDirectEmail}
          isOnlyOneUser={props.isOnlyOneUser}
          onlyOneUserPopoverText={props.onlyOneUserPopoverText}
          isVorhabenclearing={props.isVorhabenclearing}
          ccMode={
            props.ccMode && {
              selectedUsers: usersDataInCC,
              onApply: (users: UserEntityShortDTO[], isRewrite?: boolean | undefined) =>
                selectUser(users, isRewrite, true),
            }
          }
        />
      </Space>
    </FormItemWithInfo>
  ) : null;

  const selectedEmails = (
    <SelectedEmailsComponent
      name={props.name}
      dependencies={props.dependencies}
      texts={props.texts}
      extra={props.extra}
      className={props.className}
      readonly={props.readonly}
      rules={props.rules}
      allowUnknownEmail={props.allowUnknownEmail}
      usersData={usersData}
      emailSearchProps={props}
      deleteUser={deleteUser}
      onlyEditUsers={props.onlyEditUsers}
      isEGFAModule={props.isEGFAModule}
      ausschussRoles={props.ausschussRoles}
    />
  );

  return (
    <div className="white-holder emails-search-holder" role="group" aria-labelledby={props.name}>
      {/* Conditionally render searchField and selectedEmails */}
      {props.showSelectedBeforeSearchField ? (
        <>
          {selectedEmails}
          <br />
          {searchField}
        </>
      ) : (
        <>
          {searchField}
          <br />
          {selectedEmails}
        </>
      )}

      {/*Selected Emails For CC */}
      {props.ccMode && (
        <>
          <br />
          <SelectedEmailsComponent
            name={props.ccMode.name}
            dependencies={props.ccMode.dependencies}
            texts={props.ccMode.texts}
            className={props.ccMode.className}
            rules={props.ccMode.rules}
            readonly={props.readonly}
            allowUnknownEmail={props.allowUnknownEmail}
            checkIntersection={props.ccMode.checkIntersection}
            usersData={usersDataInCC}
            emailSearchProps={props}
            deleteUser={(user: UserEntityShortDTO, freigabe?: boolean | undefined) => deleteUser(user, freigabe, true)}
          />
        </>
      )}
    </div>
  );
}
