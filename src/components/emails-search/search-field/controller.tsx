// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import React from 'react';

import { EmaillisteResponseDTO, UserEntityShortDTO } from '@plateg/rest-api';

import { EmailSelectController } from '../../emailsSelect/controller';
import { CheckOutlined } from '../../icons/CheckOutlined';
import { CheckOutlinedGreen } from '../../icons/CheckOutlinedGreen';
import { ExclamationCircleFilled } from '../../icons/ExclamationCircleFilled';
import { ButtonWithPopover } from './buttonWithPopover/component.react';
import { CcModeInterface } from './component.react';
import { ListButtonsInCCComponent } from './listButtonsInCC/component.react';

interface UserWithCCMode extends CcModeInterface {
  btnApplyCCLabel?: string;
}

export const getPreparedUser = (
  user: UserEntityShortDTO,
  index: number,
  fieldName: string,
  selectedUsers: UserEntityShortDTO[],
  onApply: (user: UserEntityShortDTO[]) => void,
  btnApplyLabel: string,
  userNotActiveLabel: string,
  allowUnknownEmail?: boolean,
  firstNewElementRef: React.RefObject<HTMLButtonElement> | null = null,
  ccMode: UserWithCCMode | null = null,
  isVorhabenclearing = false,
  isOnlyOneUser = false,
  onlyOneUserPopoverText?: string,
): React.ReactElement => {
  const ctrl = new EmailSelectController();
  const ressortString = ctrl.getRessortString(user);
  const name = user.name || user.email;
  const userNotActive = ctrl.isUserNotActive(user);

  const useAddedLabel = isUserAlreadyAdded(user, selectedUsers) ? (
    <>
      <CheckOutlined /> {ccMode && btnApplyLabel}
    </>
  ) : null;
  const useAddedInCCLabel =
    ccMode && isUserAlreadyAdded(user, ccMode.selectedUsers) ? (
      <>
        <CheckOutlined /> {ccMode.btnApplyCCLabel}
      </>
    ) : null;
  return (
    <li key={index} id={`suggestion-${fieldName}-${index}`}>
      <span className="name">
        {userNotActive && <ExclamationCircleFilled />}
        {name}
        {ressortString}
        {userNotActive && <i>{userNotActiveLabel}</i>}
      </span>
      <div className="btns-holder">
        {useAddedLabel || useAddedInCCLabel || (
          <>
            {!isVorhabenclearing &&
              getStatusBtn(
                user,
                name,
                ressortString,
                userNotActive,
                onApply,
                btnApplyLabel,
                allowUnknownEmail,
                firstNewElementRef,
                isOnlyOneUser,
                selectedUsers.length > 0,
                onlyOneUserPopoverText,
              )}
            {ccMode
              ? getStatusBtn(
                  user,
                  name,
                  ressortString,
                  userNotActive,
                  ccMode.onApply,
                  ccMode.btnApplyCCLabel as string,
                  allowUnknownEmail,
                )
              : null}
          </>
        )}
      </div>
    </li>
  );
};

const getStatusBtn = (
  user: UserEntityShortDTO,
  name: string,
  ressortString: string,
  disabled = false,
  onApply: (user: UserEntityShortDTO[]) => void,
  btnApplyLabel: string,
  allowUnknownEmail?: boolean,
  firstNewElementRef: React.RefObject<HTMLButtonElement> | null = null,
  isOnlyOneUser = false,
  hasIntersections = false,
  onlyOneUserPopoverText?: string,
) => {
  if (isOnlyOneUser) {
    return (
      <ButtonWithPopover
        btnApplyLabel={btnApplyLabel}
        user={user}
        hasIntersections={hasIntersections}
        onApplyHandle={() => onApply([user])}
        disabled={disabled}
        popoverContentText={onlyOneUserPopoverText}
      />
    );
  }

  return generateButton(
    btnApplyLabel,
    () => onApply([user]),
    `${name} ${ressortString}`,
    disabled,
    allowUnknownEmail,
    firstNewElementRef,
  );
};

export const getListStatusBtn = (
  list: EmaillisteResponseDTO,
  selectedUsers: UserEntityShortDTO[],
  onApply: (user: UserEntityShortDTO[]) => void,
  btnApplyLabel: string,
  ccMode: UserWithCCMode | null = null,
  isVorhabenclearing = false,
  parentRef: React.RefObject<HTMLLIElement>,
) => {
  const isAllUsersAdded = list.personen.every((user) => isUserAlreadyAdded(user, selectedUsers));

  const useAddedLabel = isAllUsersAdded ? (
    <>
      <CheckOutlined />
    </>
  ) : null;

  if (ccMode) {
    return (
      <ListButtonsInCCComponent
        list={list}
        selectedUsers={selectedUsers}
        onApply={onApply}
        ccMode={ccMode}
        isVorhabenclearing={isVorhabenclearing}
        disabled={isListOutdated(list)}
        parentRef={parentRef}
      />
    );
  }

  return (
    useAddedLabel ||
    generateButton(
      btnApplyLabel,
      (e) => {
        e.stopPropagation();
        onApply(list.personen);
      },
      `${list.titel}`,
      isListOutdated(list),
    )
  );
};

export const isUserAlreadyAdded = (user: UserEntityShortDTO, selectedUsers: UserEntityShortDTO[]) =>
  selectedUsers.some((selectedUser) => selectedUser.email === user.email);

const generateButton = (
  label: string,
  onClick: (e: React.MouseEvent<HTMLElement, MouseEvent>) => void,
  ariaLabel: string,
  disabled = false,
  allowUnknownEmail?: boolean,
  firstNewElementRef: React.RefObject<HTMLButtonElement> | null = null,
) => {
  return (
    <Button
      disabled={allowUnknownEmail ? false : disabled}
      type="text"
      onClick={onClick}
      aria-label={`${label} ${ariaLabel}`}
      ref={firstNewElementRef}
    >
      {label}
    </Button>
  );
};

export const getListName = (
  list: EmaillisteResponseDTO,
  msgLoaded: string,
  msgListUpdated: string,
  updatedListId?: string,
): React.ReactElement => {
  let listName = <>{list.titel}</>;
  if (isListOutdated(list)) {
    listName = (
      <div className="list-title-outdated">
        <ExclamationCircleFilled />
        {list.titel} {msgLoaded}
      </div>
    );
  }
  return (
    <>
      {listName}
      <span> ({list.personen.length})</span>
      {list.id === updatedListId ? (
        <>
          <br />
          <CheckOutlinedGreen /> {msgListUpdated}
        </>
      ) : null}
    </>
  );
};
export const isListOutdated = (list: EmaillisteResponseDTO) => {
  const ctrl = new EmailSelectController();
  return list.personen.some((person) => ctrl.isUserNotActive(person));
};
