// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Collapse, CollapseProps } from 'antd';
import React, { useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { AjaxError } from 'rxjs/ajax';

import { EmaillisteControllerApi, EmaillisteResponseDTO } from '@plateg/rest-api';

import { ErrorController, LoadingStatusController } from '../../../../../controllers';
import { GlobalDI } from '../../../../../shares';
import { EmailSelectController } from '../../../../emailsSelect/controller';
import { SelectDown } from '../../../../icons/SelectDown';
import { displayMessage } from '../../../../messages/message-service';
import { getListName, getListStatusBtn, isListOutdated } from '../../controller';
import { UsersListComponent } from '../../usersList/component.react';
import { MyListsInterface } from '../component.react';

interface MyListInterface extends MyListsInterface {
  list: EmaillisteResponseDTO;
  listIndex: number;
}
export function MyListComponent(props: MyListInterface): React.ReactElement {
  const { t } = useTranslation();
  const ctrl = new EmailSelectController();
  const emailListController = GlobalDI.getOrRegister('emaillisteControllerApi', () => new EmaillisteControllerApi());
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const [updatedListId, setUpdatedListId] = useState<string>();
  const parentRef = useRef<HTMLLIElement>(null);
  const { list } = props;
  const msgLoaded = t('theme.mail.msg.labelOutdated');
  const msgListUpdated = t('theme.mail.msg.listUpdated');
  const btnApplyLabel = t('theme.mail.msg.btnApplyList');

  const actualizeList = (list: EmaillisteResponseDTO) => {
    loadingStatusController.setLoadingStatus(true);
    const updatedPesonen = list.personen.filter((person) => !ctrl.isUserNotActive(person));
    const updatedList = { ...list, personen: updatedPesonen };
    const sub = emailListController
      .modifyEmailliste({
        id: list.id,
        emaillisteSaveDTO: {
          titel: list.titel,
          emails: updatedPesonen.map((person) => person.email),
        },
      })
      .subscribe({
        next: () => {
          setUpdatedListId(list.id);
          if (props.usersList.some((el) => 'personen' in el && el.id === list.id)) {
            props.setUsersList(
              props.usersList.map((item) => ('personen' in item && item.id === list.id ? updatedList : item)),
            );
          }
          loadingStatusController.setLoadingStatus(false);
          displayMessage(
            t(`theme.mail.msg.successMsgUpdate`, {
              listName: list.titel,
            }),
            'success',
          );
          sub.unsubscribe();
        },
        error: (error: AjaxError) => {
          loadingStatusController.setLoadingStatus(false);
          errorCtrl.displayErrorMsg(error, 'egfa.generalErrorMsg');
          sub.unsubscribe();
        },
      });
  };

  const items: CollapseProps['items'] = [
    {
      key: 'index',
      label: getListName(list, msgLoaded, msgListUpdated, updatedListId),
      children: (
        <>
          {isListOutdated(list) && (
            <div className="actualize-hint-holder">
              {t('theme.mail.msg.labelActualize')}
              <Button
                id={`theme-email-list-actualize-btn-${list.id}`}
                type="link"
                size="large"
                onClick={() => {
                  actualizeList(list);
                }}
              >
                {t('theme.mail.msg.linkActualize')}
              </Button>
            </div>
          )}
          <UsersListComponent
            listOfUsers={list.personen}
            listName={'allUsers'}
            selectedUsers={props.selectedUsers}
            onApply={props.onApply}
            allowUnknownEmail={props.allowUnknownEmail}
            fieldName={props.fieldName}
            ccMode={props.ccMode}
            isVorhabenclearing={props.isVorhabenclearing}
            isOnlyOneUser={props.isOnlyOneUser}
            onlyOneUserPopoverText={props.onlyOneUserPopoverText}
          />
          {list.personen.length === 0 && (
            <div className="no-match-holder">
              {t('theme.mail.msg.noMatch')}{' '}
              <Button
                disabled={props.disabled}
                className={`btn-clear`}
                type="text"
                onClick={props.startSearch}
                aria-label={`${t('theme.mail.msg.noMatch')} ${t('theme.mail.emailsSearch.btnClear')}`}
              >
                {t('theme.mail.emailsSearch.btnClear')}
              </Button>
            </div>
          )}
        </>
      ),
      extra:
        !props.isOnlyOneUser &&
        list.personen.length > 0 &&
        getListStatusBtn(
          list,
          props.selectedUsers,
          props.onApply,
          btnApplyLabel,
          props.ccMode,
          props.isVorhabenclearing,
          parentRef,
        ),
    },
  ];
  return (
    <li key={props.listIndex} id={`suggestion-${props.fieldName}-${props.listIndex}`} ref={parentRef} tabIndex={-1}>
      <Collapse items={items} ghost className="users-list-collapse" expandIcon={() => <SelectDown />} />
    </li>
  );
}
