// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EmaillisteResponseDTO, UserEntityShortDTO } from '@plateg/rest-api';

import { CcModeInterface, SuggestionItem } from '../component.react';
import { MyListComponent } from './my-list/component.react';

export interface MyListsInterface {
  myLists: EmaillisteResponseDTO[];
  usersList: SuggestionItem[];
  setUsersList: (users: SuggestionItem[]) => void;
  selectedUsers: UserEntityShortDTO[];
  onApply: (user: UserEntityShortDTO[]) => void;
  allowUnknownEmail?: boolean;
  fieldName: string;
  isOnlyOneUser?: boolean;
  onlyOneUserPopoverText?: string;
  disabled?: boolean;
  startSearch: () => void;
  ccMode?: CcModeInterface;
  isVorhabenclearing?: boolean;
}
export function MyListsComponent(props: MyListsInterface): React.ReactElement {
  const ITEMS_TO_SHOW = 10;
  const { t } = useTranslation();
  const [visibleLists, setVisibleLists] = useState(ITEMS_TO_SHOW);

  const handleShowMore = () => {
    setVisibleLists((prevCount) => {
      const newCount = prevCount + ITEMS_TO_SHOW;
      return newCount > props.myLists.length ? props.myLists.length : newCount;
    });
  };
  return (
    <>
      <ul className="suggestions-list" id={`my-lists`}>
        {props.myLists.slice(0, visibleLists).map((list, index) => {
          return <MyListComponent list={list} listIndex={index} {...props} />;
        })}
      </ul>
      {props.myLists?.length > ITEMS_TO_SHOW && (
        <div className="view-more-holder">
          <span aria-live="polite">
            {t(`theme.mail.msg.listsShown`, {
              lists: visibleLists,
            })}
          </span>

          {visibleLists < props.myLists?.length && (
            <Button id={`theme-email-list-show-more-list`} type="link" size="large" onClick={handleShowMore}>
              {t(`theme.mail.msg.btnShowMoreLists`)}
            </Button>
          )}
        </div>
      )}
    </>
  );
}
