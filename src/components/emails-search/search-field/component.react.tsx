// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './searchField.less';

import { Button, InputRef } from 'antd';
import Search from 'antd/lib/input/Search';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Subject, Subscription } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import { EmaillisteControllerApi, EmaillisteResponseDTO, UserEntityShortDTO } from '@plateg/rest-api';

import { ErrorController } from '../../../controllers';
import { GlobalDI } from '../../../shares';
import { Constants } from '../../../utils';
import { EmailSelectController } from '../../emailsSelect/controller';
import { CategorizedViewComponent } from './categoriezedView/component.react';

interface SearchFieldInterface {
  onApply: (user: UserEntityShortDTO[]) => void;
  selectedUsers: UserEntityShortDTO[];
  fieldName: string;
  disabled?: boolean;
  searchOnlyBRUsers?: boolean;
  searchOnlyBTUsers?: boolean;
  searchOnlyWithRessortId?: string;
  searchOnlyWithReferatId?: string;
  allowUnknownEmail?: boolean;
  isOnlyOneUser?: boolean;
  onlyOneUserPopoverText?: string;
  ccMode?: CcModeInterface;
  isVorhabenclearing?: boolean;
  ignoreDirectEmail?: boolean;
}

export interface CcModeInterface {
  selectedUsers: UserEntityShortDTO[];
  onApply: (user: UserEntityShortDTO[], isRewrite?: boolean) => void;
}

export type SuggestionItem = UserEntityShortDTO | EmaillisteResponseDTO;
export function SearchFieldComponent(props: SearchFieldInterface): React.ReactElement {
  const { t } = useTranslation();
  const ctrl = new EmailSelectController();
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const emailListController = GlobalDI.getOrRegister('emaillisteControllerApi', () => new EmaillisteControllerApi());
  const [usersList, setUsersList] = useState<SuggestionItem[]>([]);
  const [initialUsersLIst, setInitialUsersList] = useState<SuggestionItem[]>([]);
  const [subject] = useState<Subject<string>>(ctrl.searchObserver);
  const [fetchingMsg, setFetchMsg] = useState<string>('');
  const [valueSearch, setValueSearch] = useState<string>('');
  const [typedEmail, setTypedEmail] = useState<string>('');
  const searchRef = useRef<InputRef>(null);
  const getSearchObservable = () => {
    if (props.searchOnlyBTUsers) {
      return ctrl.searchBTObservable();
    } else if (props.searchOnlyBRUsers) {
      return ctrl.searchBRObservable();
    } else {
      return ctrl.searchObservable(props.searchOnlyWithRessortId, props.searchOnlyWithReferatId);
    }
  };

  useEffect(() => {
    const obs = getSearchObservable();
    const sub: Subscription = obs.subscribe({
      next: (data) => {
        setUsersList(data);
        updateFetchingMsg(data);
      },
      error: (error: AjaxError) => {
        setFetchMsg('');
        errorCtrl.displayErrorMsg(error, 'theme.generalErrorMsg');
        console.error(error);
      },
    });
    const initialListsObs = emailListController.getEmaillisten().subscribe({
      next: (data) => {
        setUsersList(data.dtos);
        setInitialUsersList(data.dtos);
      },
      error: (error: AjaxError) => {
        setFetchMsg('');
        errorCtrl.displayErrorMsg(error, 'theme.generalErrorMsg');
        console.error(error);
      },
    });
    return () => {
      sub.unsubscribe();
      initialListsObs.unsubscribe();
    };
  }, []);

  const updateFetchingMsg = (data: SuggestionItem[]) => {
    const dataLength = data.filter((user) => isUserExist(user)).length;
    setFetchMsg('');
    if (!dataLength) {
      setFetchMsg(t(`theme.mail.msg.notExist`));
    } else if (dataLength > 9) {
      setFetchMsg(t(`theme.mail.msg.moreThan10`));
    }
  };

  const getSuggestions = (users: SuggestionItem[], typedUserEmail: string): React.ReactElement | null => {
    const hasUsers = users.some((user) => isUserExist(user));
    if (!hasUsers && (!typedUserEmail || props.ignoreDirectEmail)) {
      return null;
    } else if (!hasUsers && !props.ignoreDirectEmail && typedUserEmail) {
      setFetchMsg('');
      users.push({ email: typedUserEmail } as UserEntityShortDTO);
    }

    return (
      <CategorizedViewComponent
        listOfUsers={users?.filter((user) => !('personen' in user)) as UserEntityShortDTO[]}
        myLists={users?.filter((user) => 'personen' in user) as EmaillisteResponseDTO[]}
        fieldName={props.fieldName}
        selectedUsers={props.selectedUsers}
        onApply={props.onApply}
        allowUnknownEmail={props.allowUnknownEmail}
        usersList={usersList}
        setUsersList={setUsersList}
        isOnlyOneUser={props.isOnlyOneUser}
        onlyOneUserPopoverText={props.onlyOneUserPopoverText}
        disabled={props.disabled}
        startSearch={() => startSearch('', true)}
        ccMode={props.ccMode}
        isVorhabenclearing={props.isVorhabenclearing}
      />
    );
  };

  const isUserExist = (user: SuggestionItem) => {
    if (('personen' in user && user.personen.length) || ('email' in user && user.email)) {
      return true;
    }
    return false;
  };

  const startSearch = (value: string, setFocus?: boolean) => {
    setValueSearch(value);
    setUsersList(initialUsersLIst);
    setFetchMsg('');
    setTypedEmail(Constants.EMAIL_REGEXP_PATTERN.test(value) ? value : '');
    if (value.length && value.length < 3) {
      setFetchMsg(t(`theme.mail.msg.tooShort`));
    } else if (value) {
      setFetchMsg(t(`theme.mail.msg.load`));
      subject.next(value);
    }
    if (setFocus === true) {
      searchRef.current?.focus();
    }
  };

  return (
    <>
      <Search
        disabled={props.disabled}
        className="search-field"
        placeholder={t('theme.mail.emailsSearch.placeholder')}
        size="large"
        enterButton={t('theme.mail.emailsSearch.btnText')}
        onSearch={startSearch}
        value={valueSearch}
        onChange={(e) => setValueSearch(e.target.value)}
        onKeyPress={(e) => {
          if (e.key === 'Enter') {
            e.preventDefault();
            startSearch(valueSearch);
          }
        }}
        suffix={
          <Button
            disabled={props.disabled}
            className={`btn-clear ${!valueSearch ? 'hidden' : ''}`}
            type="text"
            onClick={() => startSearch('')}
          >
            {t('theme.mail.emailsSearch.btnClear')}
          </Button>
        }
        id={`email-search-field-${props.fieldName}`}
        ref={searchRef}
      />
      <div
        className={`search-loader`}
        aria-live="polite"
        id={`email-search-field-${props.fieldName}-error`}
        role="alert"
      >
        {fetchingMsg}
      </div>

      <div className="suggestions-holder">{getSuggestions(usersList, typedEmail)}</div>
    </>
  );
}
