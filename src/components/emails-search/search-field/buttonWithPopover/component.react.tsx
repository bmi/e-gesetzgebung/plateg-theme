// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Popover } from 'antd';
import React, { RefObject, useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EmaillisteResponseDTO, UserEntityShortDTO } from '@plateg/rest-api';

import { ExclamationCircleFilled } from '../../../icons/ExclamationCircleFilled';

interface ButtonWithPopoverInterface {
  btnApplyLabel: string;
  user?: UserEntityShortDTO;
  list?: EmaillisteResponseDTO;
  isCC?: boolean;
  onApplyHandle: () => void;
  hasIntersections?: boolean;
  disabled?: boolean;
  popoverContentText?: string;
}
export function ButtonWithPopover(props: ButtonWithPopoverInterface): React.ReactElement {
  const { t } = useTranslation();
  const popoverTitle = (
    <div className="emails-list-popover-title">
      <ExclamationCircleFilled /> {t(`theme.mail.msg.buttonWithPopover.popoverTitle`)}
    </div>
  );
  const popoverContentText =
    props.popoverContentText ??
    t(`theme.mail.msg.buttonWithPopover.popoverContent${props.list ? 'List' : ''}${props.isCC ? 'CC' : ''}`);
  const cancelBtnRef = useRef<HTMLButtonElement>(null);
  const applyBtnRef = useRef<HTMLButtonElement>(null);
  const mainBtnRef = useRef<HTMLAnchorElement>(null);

  const [opened, setOpened] = useState(false);
  const [inModal, setInModal] = useState(false);

  useEffect(() => {
    if (opened) {
      setTimeout(() => {
        applyBtnRef.current?.focus();
      }, 100);
    }
  }, [opened]);

  useEffect(() => {
    if (mainBtnRef.current) {
      setInModal(hasParentWithClass(mainBtnRef.current, 'ant-modal-body'));
    }
  }, []);

  function hasParentWithClass(element: HTMLElement, className: string): boolean {
    let currentElement: HTMLElement | null = element;
    while (currentElement) {
      if (currentElement.classList.contains(className)) {
        return true;
      }
      currentElement = currentElement.parentElement;
    }
    return false;
  }

  const onKeyDownHandler = (e: React.KeyboardEvent<HTMLElement>, btnRef: RefObject<HTMLButtonElement>) => {
    e.stopPropagation();
    if (e.code === 'Tab' && !e.shiftKey) {
      e.preventDefault();
      btnRef.current?.focus();
    }
  };

  const handleApply = (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
    e.stopPropagation();
    setOpened(false);
    props.onApplyHandle();
  };

  const popoverContent = (
    <div className="emails-list-popover-content" role="alertdialog">
      {popoverTitle}
      <p>{popoverContentText}</p>
      <div className="ant-popover-buttons">
        <Button
          id={`emailSearch-buttonWithPopover-btnApply`}
          ref={applyBtnRef}
          onKeyDown={(e) => onKeyDownHandler(e, cancelBtnRef)}
          onClick={handleApply}
          type="primary"
        >
          {t(`theme.mail.msg.buttonWithPopover.btnApply`)}
        </Button>
        <Button
          id={`emailSearch-buttonWithPopover-btnCancel`}
          ref={cancelBtnRef}
          onKeyDown={(e) => onKeyDownHandler(e, applyBtnRef)}
          onClick={(e) => {
            e.stopPropagation();
            setOpened(false);
            mainBtnRef.current?.focus();
          }}
          type="default"
        >
          {t(`theme.mail.msg.buttonWithPopover.btnCancel`)}
        </Button>
      </div>
    </div>
  );

  const popupContainer = document.getElementsByClassName(inModal ? 'ant-modal-body' : 'main-content-area')[0];
  const getPopupContainerProp = popupContainer ? { getPopupContainer: () => popupContainer as HTMLElement } : undefined;

  const ariaName = (props.user && props.user.name) || (props.list && props.list.titel);
  return (
    <Popover open={opened} placement="top" content={popoverContent} {...getPopupContainerProp}>
      <Button
        type="text"
        onClick={(e) => {
          e.stopPropagation();
          if (!props.hasIntersections) {
            handleApply(e);
          } else {
            setOpened(true);
          }
        }}
        onKeyDown={(e) => {
          e.stopPropagation();
        }}
        aria-label={`${ariaName} ${props.btnApplyLabel}`}
        ref={mainBtnRef}
        disabled={props.disabled}
      >
        {props.btnApplyLabel}
      </Button>
    </Popover>
  );
}
