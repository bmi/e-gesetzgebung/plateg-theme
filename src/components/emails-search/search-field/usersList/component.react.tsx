// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { UserEntityShortDTO } from '@plateg/rest-api';

import { CcModeInterface } from '../component.react';
import { getPreparedUser } from '../controller';

interface UsersListInterface {
  listOfUsers: UserEntityShortDTO[];
  listName: string;
  selectedUsers: UserEntityShortDTO[];
  onApply: (user: UserEntityShortDTO[]) => void;
  allowUnknownEmail?: boolean;
  fieldName: string;
  ccMode?: CcModeInterface;
  isVorhabenclearing?: boolean;
  isOnlyOneUser?: boolean;
  onlyOneUserPopoverText?: string;
}
export function UsersListComponent(props: UsersListInterface): React.ReactElement {
  const ITEMS_TO_SHOW = 10;
  const { t } = useTranslation();
  const [visibleCount, setVisibleCount] = useState(ITEMS_TO_SHOW);
  const firstNewElementRef = useRef<HTMLButtonElement>(null);
  let btnApplyLabel = t('theme.mail.msg.btnApply');
  const btnApplyCCLabel = t('theme.mail.msg.buttonWithPopover.btnApplyListCC');
  const userNotActiveLabel = t('theme.mail.msg.userNotActive');

  if (props.ccMode) {
    btnApplyLabel = t('theme.mail.msg.buttonWithPopover.btnApplyList');
  }

  useEffect(() => {
    if (firstNewElementRef.current && visibleCount > 10) {
      firstNewElementRef.current.focus();
    }
  }, [visibleCount]);

  const handleShowMore = () => {
    setVisibleCount((prevCount) => {
      const newCount = prevCount + ITEMS_TO_SHOW;
      return newCount > props.listOfUsers.length ? props.listOfUsers.length : newCount;
    });
  };

  return (
    <>
      <ul className="suggestions-list" id={`users-list-${props.fieldName}`}>
        {props.listOfUsers.slice(0, visibleCount).map((item, index) => {
          const firstNewIndex =
            visibleCount < props.listOfUsers.length
              ? visibleCount - ITEMS_TO_SHOW
              : visibleCount - (props.listOfUsers.length % ITEMS_TO_SHOW);
          return getPreparedUser(
            item,
            index,
            props.listName,
            props.selectedUsers,
            props.onApply,
            btnApplyLabel,
            userNotActiveLabel,
            props.allowUnknownEmail,
            index === firstNewIndex ? firstNewElementRef : null,
            props.ccMode ? { ...props.ccMode, btnApplyCCLabel } : null,
            props.isVorhabenclearing,
            props.isOnlyOneUser,
            props.onlyOneUserPopoverText,
          );
        })}
      </ul>

      {props.listOfUsers?.length > ITEMS_TO_SHOW && (
        <div className="view-more-holder">
          <span aria-live="polite">
            {t(`theme.mail.msg.itemsShown`, {
              items: visibleCount,
            })}
          </span>

          {visibleCount < props.listOfUsers?.length && (
            <Button id={`theme-email-list-show-more`} type="link" size="large" onClick={handleShowMore}>
              {t(`theme.mail.msg.btnShowMore`)}
            </Button>
          )}
        </div>
      )}
    </>
  );
}
