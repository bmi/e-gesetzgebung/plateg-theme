// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useTranslation } from 'react-i18next';

import { EmaillisteResponseDTO, UserEntityShortDTO } from '@plateg/rest-api';

import { CheckOutlined } from '../../../icons/CheckOutlined';
import { ButtonWithPopover } from '../buttonWithPopover/component.react';
import { CcModeInterface } from '../component.react';
import { isUserAlreadyAdded } from '../controller';

interface ListButtonsInCCInterface {
  list: EmaillisteResponseDTO;
  selectedUsers: UserEntityShortDTO[];
  onApply: (user: UserEntityShortDTO[]) => void;
  ccMode: CcModeInterface;
  isVorhabenclearing?: boolean;
  disabled?: boolean;
  parentRef: React.RefObject<HTMLLIElement>;
}
export function ListButtonsInCCComponent(props: ListButtonsInCCInterface): React.ReactElement {
  const { t } = useTranslation();
  const btnApplyLabel = t('theme.mail.msg.buttonWithPopover.btnApplyList');
  const btnApplyCCLabel = t('theme.mail.msg.buttonWithPopover.btnApplyListCC');

  const isAllUsersAdded = props.list.personen.every((user) => isUserAlreadyAdded(user, props.selectedUsers));
  const isAllUsersAddedInCC = props.list.personen.every((user) =>
    isUserAlreadyAdded(user, props.ccMode?.selectedUsers),
  );

  const useAddedLabel = isAllUsersAdded ? (
    <>
      <CheckOutlined /> {btnApplyLabel}
    </>
  ) : null;
  const useAddedInCCLabel = isAllUsersAddedInCC ? (
    <>
      <CheckOutlined /> {btnApplyCCLabel}
    </>
  ) : null;

  const hasIntersections = (personen: UserEntityShortDTO[], selectedUsers: UserEntityShortDTO[]) => {
    const teilnehmerIntersections = personen.filter((person) =>
      selectedUsers.some((user) => person.email === user.email),
    );
    return teilnehmerIntersections.length > 0;
  };

  const removeSelectedUsers = (personen: UserEntityShortDTO[], selectedUsers: UserEntityShortDTO[]) => {
    return selectedUsers.filter((user) => !personen.some((person) => person.email === user.email));
  };

  const handleApply = (isCC = false) => {
    // e.stopPropagation();
    const users = !isCC ? props.ccMode.selectedUsers : props.selectedUsers;
    const onApplyHandler = !isCC ? props.onApply : props.ccMode.onApply;
    const onApplyHandlerToRemoveUser = !isCC ? props.ccMode.onApply : props.onApply;

    const { personen } = props.list;

    if (!hasIntersections(personen, users)) {
      onApplyHandler(personen);
    } else {
      onApplyHandler(personen);
      onApplyHandlerToRemoveUser(removeSelectedUsers(personen, users), true);
    }

    props.parentRef.current?.focus();
  };

  return (
    useAddedLabel ||
    useAddedInCCLabel || (
      <>
        {!props.isVorhabenclearing && (
          <ButtonWithPopover
            btnApplyLabel={btnApplyLabel}
            list={props.list}
            onApplyHandle={handleApply}
            hasIntersections={hasIntersections(props.list.personen, props.ccMode?.selectedUsers)}
            disabled={props.disabled}
          />
        )}
        <ButtonWithPopover
          btnApplyLabel={btnApplyCCLabel}
          isCC={true}
          list={props.list}
          onApplyHandle={() => handleApply(true)}
          hasIntersections={hasIntersections(props.list.personen, props.selectedUsers)}
          disabled={props.disabled}
        />
      </>
    )
  );
}
