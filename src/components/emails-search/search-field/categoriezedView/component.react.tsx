// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Collapse, CollapseProps } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { EmaillisteResponseDTO, UserEntityShortDTO } from '@plateg/rest-api';

import { SelectDown } from '../../../icons/SelectDown';
import { CcModeInterface, SuggestionItem } from '../component.react';
import { MyListsComponent } from '../myLists/component.react';
import { UsersListComponent } from '../usersList/component.react';

interface CategorizedViewdInterface {
  fieldName: string;
  listOfUsers: UserEntityShortDTO[];
  myLists: EmaillisteResponseDTO[];
  selectedUsers: UserEntityShortDTO[];
  onApply: (user: UserEntityShortDTO[]) => void;
  allowUnknownEmail?: boolean;
  usersList: SuggestionItem[];
  setUsersList: (users: SuggestionItem[]) => void;
  isOnlyOneUser?: boolean;
  onlyOneUserPopoverText?: string;
  disabled?: boolean;
  startSearch: () => void;
  ccMode?: CcModeInterface;
  isVorhabenclearing?: boolean;
}

export function CategorizedViewComponent(props: CategorizedViewdInterface): React.ReactElement {
  const { t } = useTranslation();
  const pluralKey = props.listOfUsers.length > 1 ? 'plural' : 'singular';
  const allUsersCounterMsg = `${t('theme.mail.msg.allLists')} (${props.listOfUsers.length}
        ${props.listOfUsers.length && t(`theme.mail.msg.resultsCategories.${pluralKey}`)})`;

  const allUsers: CollapseProps['items'] = [
    {
      key: 'allUsers',
      label: <span>{allUsersCounterMsg}</span>,
      children: (
        <UsersListComponent
          listOfUsers={props.listOfUsers}
          listName={'allUsers'}
          selectedUsers={props.selectedUsers}
          onApply={props.onApply}
          allowUnknownEmail={props.allowUnknownEmail}
          fieldName={props.fieldName}
          ccMode={props.ccMode}
          isVorhabenclearing={props.isVorhabenclearing}
          isOnlyOneUser={props.isOnlyOneUser}
          onlyOneUserPopoverText={props.onlyOneUserPopoverText}
        />
      ),
    },
  ];

  let numberUsersFromLists = 0;
  props.myLists.forEach((list) => {
    numberUsersFromLists += list.personen.length;
  });

  const getMyEmailsTitle = () => {
    return `(${numberUsersFromLists}${numberUsersFromLists > 0 ? ` ` + t(`theme.mail.msg.resultsCategories.${numberUsersFromLists > 1 ? 'plural' : 'singular'}`) : ''})`;
  };

  const myLists: CollapseProps['items'] = [
    {
      key: 'myLists',
      label: `${t('theme.mail.msg.myLists')} ${props.listOfUsers.length ? getMyEmailsTitle() : ''}`,
      children: (
        <MyListsComponent
          myLists={props.myLists}
          usersList={props.usersList}
          setUsersList={props.setUsersList}
          selectedUsers={props.selectedUsers}
          onApply={props.onApply}
          allowUnknownEmail={props.allowUnknownEmail}
          fieldName={props.fieldName}
          isOnlyOneUser={props.isOnlyOneUser}
          onlyOneUserPopoverText={props.onlyOneUserPopoverText}
          disabled={props.disabled}
          startSearch={props.startSearch}
          ccMode={props.ccMode}
          isVorhabenclearing={props.isVorhabenclearing}
        />
      ),
    },
  ];
  return (
    <>
      <span aria-live="polite" role="alert" className="sr-only">
        {allUsersCounterMsg}
      </span>
      <ul className="suggestions-list" id={`suggestions-list-${props.fieldName}`}>
        {props.listOfUsers.length > 0 && (
          <li>
            <Collapse
              items={allUsers}
              ghost
              className="users-list-collapse"
              expandIcon={() => <SelectDown />}
              defaultActiveKey={'allUsers'}
            />
          </li>
        )}
        <li>
          <Collapse
            items={myLists}
            ghost
            className="users-list-collapse"
            expandIcon={() => <SelectDown />}
            defaultActiveKey={!props.listOfUsers.length ? 'myLists' : undefined}
          />
        </li>
      </ul>
    </>
  );
}
