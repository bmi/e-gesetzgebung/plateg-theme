// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './tagsList.less';

import { Button } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { UserEntityShortDTO } from '@plateg/rest-api';

import { CloseOutlined, DeleteOutlined, ExclamationCircleFilled, ExclamationMarkRed } from '../..';
import { AriaController } from '../../../controllers/AriaController';
import { DeleteConfirmContent } from '../../delete-confirm-popover/component.react';
import { EmailSelectController } from '../../emailsSelect/controller';
import { BtnDeleteRoleComponent } from '../../freigabe-modal/freigabe-list/btn-delete-role/component.react';

interface TagsListInterface {
  selectedUsers: UserEntityShortDTO[];
  deleteUser: (user: UserEntityShortDTO) => void;
  checkIntersection?: (value: string[]) => boolean;
  fieldName: string;
  currentUserEmail: string;
  isRowView?: boolean;
  disabled?: boolean;
  isVorhabenclearing?: boolean;
  noAddressesText?: string;
  allowSelf?: boolean;
  allowUnknownEmail?: boolean;
  deleteConfirmContent?: DeleteConfirmContent;
  undeletableTagEmails?: string[];
}

export function TagsListComponent(props: TagsListInterface): React.ReactElement {
  const { t } = useTranslation();
  const defaultVisibleItems = 8;
  const ctrl = new EmailSelectController();
  const [usersList, setUsersList] = useState<UserEntityShortDTO[]>([]);
  const [hiddenClass, setHiddenClass] = useState('hidden');

  const getDeleteButton = (user: UserEntityShortDTO, index: number) => {
    const deleteMsg = t(`theme.mail.msg.delete`);
    const name = user.name || user.email;
    const ressortString = ctrl.getRessortString(user);
    if (props.deleteConfirmContent) {
      return (
        <BtnDeleteRoleComponent
          deleteConfirmContent={props.deleteConfirmContent}
          user={{ user, deleted: false }}
          deleteUser={props.deleteUser}
          index={index}
          fieldName={props.fieldName}
          disabled={props.disabled}
          undeletableEmails={props.undeletableTagEmails}
          name={name}
          ressortString={ressortString}
          currentUserEmail={props.currentUserEmail}
        />
      );
    } else {
      return (
        <Button
          disabled={props.disabled}
          className="btn-close"
          type="text"
          onClick={() => props.deleteUser(user)}
          aria-label={`${deleteMsg} ${name} ${ressortString}`}
          id={`delete-address-${props.fieldName}-${index}`}
        >
          {props.isRowView ? <DeleteOutlined /> : <CloseOutlined />}
        </Button>
      );
    }
  };

  useEffect(() => {
    setUsersList(props.selectedUsers);
    AriaController.setAttrByClassName('tag-holder', 'role', 'list');
    AriaController.setAttrByClassName('tag-element', 'role', 'listitem');
  }, [props.selectedUsers]);

  return (
    <div className={`tag-holder ${props.isRowView ? 'rows-view' : ''}`}>
      {usersList.map((user, index) => {
        const ressortString = ctrl.getRessortString(user);
        const isIntersection = props.checkIntersection ? props.checkIntersection([user.email]) : false;
        const isCurrentUser = props.currentUserEmail === user.email;
        const isCurrentUserForbidden = isCurrentUser && !props.allowSelf;

        const isUserNotActive = ctrl.isUserNotActive(user);
        const isEmailForbidden = ctrl.isEmailForbidden(user, props.allowUnknownEmail);
        const name = user.name || user.email;
        const inactiveUserErrorLabel = props.allowUnknownEmail ? false : isUserNotActive;
        const conditionShowError =
          isIntersection || isCurrentUserForbidden || inactiveUserErrorLabel || isEmailForbidden;
        return (
          <div
            className={`tag-element ${index + 1 > defaultVisibleItems ? hiddenClass : ''} ${
              conditionShowError ? 'has-error' : ''
            }`}
            key={index}
            id={`selected-address-${props.fieldName}-${index}`}
          >
            {isIntersection || isCurrentUserForbidden ? (
              <ExclamationMarkRed style={{ margin: '4px 4px 2px 0', verticalAlign: 'top' }} />
            ) : null}
            {isUserNotActive ? (
              <span className="attention-icon">
                <ExclamationCircleFilled />
              </span>
            ) : null}
            {name}
            {ressortString}
            {isCurrentUser && !isCurrentUserForbidden ? ' (Sie)' : ''}

            {isUserNotActive ? t(`theme.mail.msg.userNotActive`) : null}
            {!(props.isVorhabenclearing && props.fieldName === 'teilnehmer') &&
              !props.undeletableTagEmails?.includes(user.email) &&
              getDeleteButton(user, index)}
          </div>
        );
      })}
      {usersList.length > defaultVisibleItems && hiddenClass ? (
        <Button
          disabled={props.disabled}
          type="text"
          className="show-more-btn"
          onClick={() => setHiddenClass('')}
          id={`show-more-${props.fieldName}`}
        >
          + {usersList.length - defaultVisibleItems} {t(`theme.mail.msg.moreItems`)}
        </Button>
      ) : null}
      <div className="div-no-addresses">{!usersList.length ? props.noAddressesText : null}</div>
    </div>
  );
}
