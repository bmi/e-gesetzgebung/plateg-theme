// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useTranslation } from 'react-i18next';

import { UserEntityShortDTO } from '@plateg/rest-api';

import { FreigabeListComponent } from '../../../freigabe-modal/freigabe-list/component.react';
import { EmailsSearchInterface } from '../../component.react';
import { TagsListComponent } from '../../tags-list/component.react';

interface SelectedVisualizationInterface extends EmailsSearchInterface {
  usersData: UserEntityShortDTO[];
  deleteUser: (user: UserEntityShortDTO, freigabe?: boolean) => void;
}

export function SelectedVisualizationComponent(props: Readonly<SelectedVisualizationInterface>): React.ReactElement {
  const { t } = useTranslation();
  return props.freigabeListProps ? (
    <FreigabeListComponent
      disabled={props.disabled}
      currentUserRessortId={props.currentUserRessortId}
      selectedUsers={props.usersData}
      fieldName={props.name}
      deleteUser={props.deleteUserImmediatelyCall ? props.deleteUserImmediatelyCall : props.deleteUser}
      options={props.freigabeListProps.options}
      currentUserEmail={props.currentUserEmail}
      form={props.form}
      initialVals={props.freigabeListProps.initialVals}
      ersteller={props.ersteller}
      readonly={props.readonly}
      setSelectionHasChanged={() => props.setSelectionHasChanged?.()}
      undeletableEmails={props.freigabeListProps.undeletableEmails}
      documentEditor={props.documentEditor}
      onRevokeEditingRight={props.onRevokeEditingRight}
      isStaticFreigabenText={props.isStaticFreigabenText}
      allowUnknownEmail={props.allowUnknownEmail}
      isRBACActivated={props.isRBACActivated}
      deleteConfirmContent={props.deleteConfirmContent}
      isZeitplanungvorlage={props.isZeitplanungvorlage}
      revokeObserverRight={props.revokeObserverRight}
      onlyEditUsers={props.onlyEditUsers}
      isEGFAModule={props.isEGFAModule}
      ausschussRoles={props.ausschussRoles}
    />
  ) : (
    <TagsListComponent
      disabled={props.disabled}
      selectedUsers={props.usersData}
      deleteUser={props.deleteUser}
      checkIntersection={props.checkIntersection}
      fieldName={props.name}
      currentUserEmail={props.currentUserEmail}
      isRowView={props.isRowView}
      isVorhabenclearing={props.isVorhabenclearing}
      noAddressesText={props.texts.noAddresses || t(`theme.mail.msg.noAddresses`)}
      allowSelf={props.allowSelf}
      allowUnknownEmail={props.allowUnknownEmail}
      deleteConfirmContent={props.deleteConfirmContent}
      undeletableTagEmails={props.undeletableTagEmails}
    />
  );
}
