// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Space } from 'antd';
import Title from 'antd/lib/typography';
import { NamePath, Rule } from 'rc-field-form/lib/interface';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { UserEntityShortDTO } from '@plateg/rest-api';

import { FormItemWithInfo } from '../../form-item-with-info/component.react';
import { AusschussRoles } from '../../freigabe-modal/freigabe-list/component.react';
import { InfoComponent } from '../../info-component/component.react';
import { EmailSearchTexts, EmailsSearchInterface } from '../component.react';
import { SelectedVisualizationComponent } from './selected-visualization/component.react';

interface SelectedEmailsInterface {
  name: string;
  dependencies?: NamePath[];
  extra?: React.ReactNode;
  className?: string;
  readonly?: boolean;
  texts: EmailSearchTexts;
  rules?: Rule[];
  allowUnknownEmail?: boolean;
  checkIntersection?: (value: string[]) => boolean;
  usersData: UserEntityShortDTO[];
  emailSearchProps: EmailsSearchInterface;
  deleteUser: (user: UserEntityShortDTO, freigabe?: boolean) => void;
  onlyEditUsers?: boolean;
  isEGFAModule?: boolean;
  ausschussRoles?: AusschussRoles[];
}

export function SelectedEmailsComponent(props: Readonly<SelectedEmailsInterface>): React.ReactElement {
  const { t } = useTranslation();

  useEffect(() => {
    if (
      props.usersData &&
      (props.emailSearchProps.form.isFieldsTouched([props.name]) || props.emailSearchProps.freigabeListProps)
    ) {
      props.emailSearchProps.setUsersDataOnParent?.(props.usersData);
      void props.emailSearchProps.form.validateFields([props.name]);
    }
  }, [props.usersData]);

  const localRules = [...(props.rules || [])];
  if (!props.allowUnknownEmail) {
    localRules.push({
      validator: () => {
        return props.usersData.some((item) => !item.gid)
          ? Promise.reject(t('theme.mail.msg.notIAMUser'))
          : Promise.resolve();
      },
    });
  }

  const addressInfo =
    props.texts.addressDrawer.hasOwnProperty('addressDrawerTitle') &&
    props.texts.addressDrawer.hasOwnProperty('addressDrawerText') ? (
      <InfoComponent title={(props.texts.addressDrawer as { addressDrawerTitle: string }).addressDrawerTitle}>
        <p
          dangerouslySetInnerHTML={{
            __html: (props.texts.addressDrawer as { addressDrawerText: string }).addressDrawerText,
          }}
        />
      </InfoComponent>
    ) : (
      (props.texts.addressDrawer as React.ReactElement)
    );

  const visualizationTitle = (
    <span>
      <span className="selected-addresses-label">{props.texts.addressLabel}</span>
      {addressInfo}
    </span>
  );

  return (
    <FormItemWithInfo
      name={props.name}
      rules={localRules}
      dependencies={props.dependencies}
      extra={props.extra}
      className={props.className}
    >
      {/* no label for non-input form elements*/}
      <Space direction="vertical" style={{ width: '100%' }}>
        {!props.onlyEditUsers &&
          (props.readonly ? (
            <Title level={2}>{visualizationTitle}</Title>
          ) : (
            <>
              <span>{visualizationTitle}</span>
              {props.texts.addressSubLabel ? <span>{props.texts.addressSubLabel}</span> : <></>}
            </>
          ))}
        <SelectedVisualizationComponent
          {...props.emailSearchProps}
          name={props.name}
          checkIntersection={props.checkIntersection}
          usersData={props.usersData}
          deleteUser={props.deleteUser}
          ausschussRoles={props.ausschussRoles}
        />
      </Space>
    </FormItemWithInfo>
  );
}
