// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { useEffect } from 'react';
import { useLocation } from 'react-router-dom';
export function ScrollToTop(): null {
  const { pathname } = useLocation();

  useEffect(() => {
    window.setTimeout(function () {
      window.scrollTo(0, 0);
    }, 0);
  }, [pathname]);
  return null;
}
