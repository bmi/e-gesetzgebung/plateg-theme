// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './open-entries-indicator.less';

import React from 'react';

export interface OpenEntriesIndicatorProps {
  numberOfOpenEntries: number;
}

export function OpenEntriesIndicator(props: OpenEntriesIndicatorProps): React.ReactElement {
  if (props.numberOfOpenEntries <= 0) {
    return <></>;
  }
  return <div className="open-entries-indicator">{props.numberOfOpenEntries}</div>;
}
