// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Tabs, TabsProps } from 'antd';
import React, { useEffect, useLayoutEffect, useState } from 'react';

import { AriaController } from '../../controllers';

export interface TabsWrapperProps extends TabsProps {
  moduleName?: string;
  items: Required<TabsProps>['items'];
  activeKey: string;
}

export function TabsWrapper(props: TabsWrapperProps): React.ReactElement {
  const { moduleName, ...restProps } = props;
  const [currentArrowedTab, setCurrentArrowedTab] = useState(props.activeKey);

  useLayoutEffect(() => {
    AriaController.setAriaLabelsByClassName('anticon-ellipsis', 'Weitere Funktionen');
  }, []);

  useEffect(() => {
    const timeout = setTimeout(() => {
      if (props.activeKey !== undefined && props.items) {
        const currentIndexActiveKey = props.items.findIndex((tab) => tab.key === props.activeKey);
        handleTabIndexChange(currentIndexActiveKey);
      }
    }, 100);

    return () => {
      clearTimeout(timeout);
    };
  }, [props.activeKey]);

  const tabsId = `${moduleName ? moduleName.split(' ').join('-') + '-' : ''}` + 'tab-nav';

  const handleTabIndexChange = (indexSkip: number | string) => {
    // skips empty tab step when enter tab content
    const tabPanel = document.getElementById(`${tabsId}-panel-${props.activeKey}`);
    tabPanel?.setAttribute('tabindex', '-1');

    // remove tabindex from all tabs but not the selected
    const parentEl = document.querySelector('.ant-tabs-nav-list');
    const tabELs = parentEl?.querySelectorAll('.ant-tabs-tab-btn');
    tabELs?.forEach((el, ind) => {
      if (ind !== indexSkip) {
        el.setAttribute('tabindex', '-1');
      } else {
        el.setAttribute('tabindex', '0');
      }
    });
  };

  const handleKeyDown = (e: React.KeyboardEvent<HTMLDivElement>) => {
    const { activeKey, items } = props;
    const currentIndex = items.findIndex((tab) => tab.key === currentArrowedTab);
    const totalTabs = items.length;

    const blurActiveElement = () => {
      (document.activeElement as HTMLDivElement).blur();
    };

    const focusTabByIndex = (index: number) => {
      const nextKey = items[index].key;
      const activeTabEl = document.querySelector(`[aria-controls="${tabsId}-panel-${nextKey}"]`) as HTMLDivElement;
      if (activeTabEl) {
        activeTabEl.focus();
        setCurrentArrowedTab(nextKey);
      }
    };

    // Remove focus when tab to content from not selected tab
    if (e.key === 'Tab' && activeKey !== currentArrowedTab) {
      blurActiveElement();
      setCurrentArrowedTab(activeKey);
    }

    // Arrow keys navigation
    const arrowLeftOrRight = e.key === 'ArrowLeft' || e.key === 'ArrowRight';
    if (arrowLeftOrRight && (e.target as Element).className.includes('ant-tabs-tab-btn')) {
      e.preventDefault();

      let nextIndex = currentIndex;

      if (e.key === 'ArrowLeft') {
        nextIndex = (currentIndex - 1 + totalTabs) % totalTabs;
      } else if (e.key === 'ArrowRight') {
        nextIndex = (currentIndex + 1) % totalTabs;
      }

      focusTabByIndex(nextIndex);
    }
  };

  return <Tabs onKeyDown={handleKeyDown} id={tabsId} {...restProps} />;
}
