// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './table-scroll.less';

import React, { useLayoutEffect, useRef, useState } from 'react';

interface TableScrollProps {
  children: React.ReactNode;
}

export function TableScroll(props: TableScrollProps): React.ReactElement {
  const [size, setSize] = useState(0);
  const tableScroll = useRef<HTMLDivElement>(null);
  useLayoutEffect(() => {
    function updateSize() {
      setTimeout(() => {
        if (tableScroll && tableScroll.current) {
          const rect = tableScroll.current.getBoundingClientRect();
          setSize(window.innerWidth - rect.left);
        }
      }, 250);
    }
    window.addEventListener('resize', updateSize);
    updateSize();
    return () => {
      window.removeEventListener('resize', updateSize);
    };
  }, []);
  return (
    <div className="table-scroll" ref={tableScroll} style={{ width: size }}>
      {props.children}
    </div>
  );
}
