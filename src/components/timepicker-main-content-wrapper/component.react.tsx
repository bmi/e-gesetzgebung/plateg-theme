// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { PickerTimeProps } from 'antd/es/date-picker/generatePicker';
import React from 'react';

import TimePicker from '../custom-antd-components/timepicker/component.react';

export function MainContentTimepickerWrapper(
  props: React.PropsWithChildren<Omit<PickerTimeProps<Date>, 'picker'>>,
): React.ReactElement {
  const popupContainer = document.getElementsByClassName('main-content-area')[0];
  const getPopupContainerProp = popupContainer ? { getPopupContainer: () => popupContainer as HTMLElement } : undefined;
  return <TimePicker {...props} {...(props.getPopupContainer ?? getPopupContainerProp)} />;
}
