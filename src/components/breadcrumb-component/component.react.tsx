// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './breadcrumb.less';

import { Breadcrumb } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import type { ItemType } from 'antd/es/breadcrumb/Breadcrumb';

interface BreadcrumbComponentProps {
  items?: React.ReactElement[];
}
function HomeBreadcrumb(): React.ReactElement {
  const { t } = useTranslation();
  return (
    <Link id="home-link" key="home" to={`/cockpit`}>
      {t('theme.startLink')}
    </Link>
  );
}
export function BreadcrumbComponent(props: BreadcrumbComponentProps): React.ReactElement {
  const [items, setItems] = useState<ItemType[]>();

  useEffect(() => {
    let newItems =
      props?.items?.map((item, index) => {
        return { title: item, key: `item${index}` };
      }) || [];
    newItems = [
      {
        title: <HomeBreadcrumb />,
        key: 'home-link',
      },
      ...newItems,
    ];
    setItems(newItems);
  }, [props.items]);
  return (
    <nav aria-label="Pfadnavigation">
      <Breadcrumb items={items} className="breadcrumb" />
    </nav>
  );
}
