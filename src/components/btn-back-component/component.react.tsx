// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './btn-back-component.less';

import { Button } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';

import { LeftOutlined } from '../icons/LeftOutlined';

interface BtnBackComponentProps {
  id: string;
  url: string;
  styleBtn?: React.CSSProperties;
}
export function BtnBackComponent(props: BtnBackComponentProps): React.ReactElement {
  const history = useHistory();
  const { t } = useTranslation();
  return (
    <Button
      style={props.styleBtn ? props.styleBtn : {}}
      id={props.id}
      type="text"
      size={'large'}
      className="btn-back-component"
      onClick={() => history.push(props.url)}
    >
      <LeftOutlined />
      {t(`theme.btnBack`)}
    </Button>
  );
}
