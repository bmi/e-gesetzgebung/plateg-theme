// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import React from 'react';

import { getAllRows } from '../';
import { getDateTimeString } from './';
import { CommonRow } from './component.react';
import { compareDates, getExpandableConfig, SortedInfo } from './controller';
const buttonOpened = (onClick: React.MouseEventHandler<HTMLElement>) =>
  React.createElement('button', { onClick: onClick, id: 'open' });

const buttonClosed = (onClick: React.MouseEventHandler<HTMLElement>) =>
  React.createElement('button', { onClick: onClick, id: 'closed' });
const expandedRowRender = <T extends CommonRow>(record: T) => React.createElement('span', 'test');
const expandableConditionTrue = <T extends CommonRow>(record: T) => true;
const expandableConditionFalse = <T extends CommonRow>(record: T) => false;

describe('Test getExpandableConfig', () => {
  it('not expandable', () => {
    expect(getExpandableConfig(false, buttonOpened, buttonClosed, expandedRowRender, expandableConditionFalse)).to.eql(
      undefined,
    );
  });
  it('is expandable, no renderer, no condition, open button ', () => {
    const result = getExpandableConfig(true, buttonOpened, buttonClosed, undefined, undefined);
    expect(result?.rowExpandable).to.not.be.undefined;
    if (result?.rowExpandable) {
      expect(result?.rowExpandable({ id: 'abc' })).to.eql(true);
    }
    expect(result?.expandedRowRender).to.not.be.undefined;
    if (result?.expandedRowRender) {
      expect(result?.expandedRowRender({ id: 'abc' }, 1, 0, false)).to.eql(undefined);
    }
    expect(result?.expandIcon).to.not.be.undefined;
    if (result?.expandIcon) {
      const iconResult = result?.expandIcon({
        expanded: true,
        expandable: true,
        onExpand: () => {},
        record: { id: 'abcd' },
        prefixCls: 'abcd',
      });
      expect(iconResult)
        .property('props')
        .property('id')
        .to.eql(buttonOpened(() => {}).props.id);
    }
  });

  it('is expandable, with renderer, with condition (true), close button ', () => {
    const result = getExpandableConfig(true, buttonOpened, buttonClosed, expandedRowRender, expandableConditionTrue);
    expect(result?.rowExpandable).to.not.be.undefined;
    if (result?.rowExpandable) {
      expect(result?.rowExpandable({ id: 'abc' })).to.eql(true);
    }
    expect(result?.expandedRowRender).to.not.be.undefined;
    if (result?.expandedRowRender) {
      expect(result?.expandedRowRender({ id: 'abc' }, 1, 0, false)).to.eql(expandedRowRender({ id: 'abc' }));
    }
    expect(result?.expandIcon).to.not.be.undefined;
    if (result?.expandIcon) {
      const iconResult = result?.expandIcon({
        expanded: false,
        expandable: true,
        onExpand: () => {},
        record: { id: 'abcd' },
        prefixCls: 'abcd',
      });
      expect(iconResult)
        .property('props')
        .property('id')
        .to.eql(buttonClosed(() => {}).props.id);
    }
  });

  it('is expandable (-> theoretically, but not because condition), with renderer, with condition (-> always false), no button ', () => {
    const result = getExpandableConfig(true, buttonOpened, buttonClosed, expandedRowRender, expandableConditionFalse);
    expect(result?.rowExpandable).to.not.be.undefined;
    if (result?.rowExpandable) {
      expect(result?.rowExpandable({ id: 'abc' })).to.eql(false);
    }
    expect(result?.expandedRowRender).to.not.be.undefined;
    if (result?.expandedRowRender) {
      expect(result?.expandedRowRender({ id: 'abc' }, 1, 0, false)).to.eql(expandedRowRender({ id: 'abc' }));
    }
    expect(result?.expandIcon).to.not.be.undefined;
    if (result?.expandIcon) {
      const iconResult = result?.expandIcon({
        expanded: false,
        expandable: true,
        onExpand: () => {},
        record: { id: 'abcd' },
        prefixCls: 'abcd',
      });
      expect(iconResult).to.eql(undefined);
    }
  });
});

describe('Test SortedInfo -> getSortOrderOf', () => {
  const sortedInfoAscend = new SortedInfo('ascend', 'c1');
  const sortedInfoDescend = new SortedInfo('descend', 'c1');
  const sortedInfoNull = new SortedInfo(null, 'c1');

  it('sortOrder is ascending', () => {
    expect(sortedInfoAscend.getSortOrderOf('c1')).to.eql('ascend');
  });
  it('sortOrder is descend', () => {
    expect(sortedInfoDescend.getSortOrderOf('c1')).to.eql('descend');
  });
  it('sortOrder is null', () => {
    expect(sortedInfoNull.getSortOrderOf('c1')).to.eql(null);
  });
  it('columnKey doesnt exist', () => {
    expect(sortedInfoAscend.getSortOrderOf('c2')).to.eql(null);
  });
});

describe('Test compareDates', () => {
  const date1 = '2021-05-28 00:23:47';
  const date2 = '2021-05-29 00:23:47';
  const date3 = '2021-05-29 00:23:47';

  it('date1 < date2', () => {
    expect(compareDates(date1, date2)).to.eql(-1);
  });

  it('date2 > date1', () => {
    expect(compareDates(date2, date1)).to.eql(1);
  });

  it('date2 = date3', () => {
    expect(compareDates(date2, date3)).to.eql(0);
  });

  it('input a is not a valid date, expect equality', () => {
    expect(compareDates('abc', date3)).to.eql(0);
  });

  it('input a is undefined, expect input a < date1 ', () => {
    expect(compareDates(undefined, date1)).to.eql(-1);
  });

  it('input b is undefined, expect date1 > b ', () => {
    expect(compareDates(date1, undefined)).to.eql(1);
  });
});

describe('Test getDateTimeString', () => {
  const date1 = '2021-05-28 00:23:47';
  const date2 = '2029-09-29 12:23:47';
  const date3 = '2021-05-19 01:23:47';

  it('correct format: date1', () => {
    expect(getDateTimeString(date1)).to.eql('28.05.2021  ·  00:23');
  });
  it('correct format: date2', () => {
    expect(getDateTimeString(date2)).to.eql('29.09.2029  ·  12:23');
  });
  it('correct format: date3', () => {
    expect(getDateTimeString(date3)).to.eql('19.05.2021  ·  01:23');
  });
});

describe('Test getAllRows', () => {
  const nestedStructure = [{ id: '1', children: [{ id: '1.1', children: [{ id: '1.1.1' }] }] }];
  const flatStructure = [{ id: '1' }, { id: '2' }, { id: '3' }];
  it('should extract rows of all three levels', () => {
    const result = getAllRows(nestedStructure as { children?: []; id: string }[]);
    expect(result.length).to.eql(3);
    expect(result[0].id).to.eql('1');
    expect(result[1].id).to.eql('1.1');
    expect(result[2].id).to.eql('1.1.1');
  });
  it('should extract rows of same level', () => {
    const result = getAllRows(flatStructure as { children?: []; id: string }[]);
    expect(result.length).to.eql(3);
    expect(result[0].id).to.eql('1');
    expect(result[1].id).to.eql('2');
    expect(result[2].id).to.eql('3');
  });
});
