// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './table-component.less';

import { Button, Col, Row, Table } from 'antd';
import { ColumnsType, TablePaginationConfig } from 'antd/lib/table';
import { Key, SorterResult } from 'antd/lib/table/interface';
import Text from 'antd/lib/typography/Text';
import Title from 'antd/lib/typography/Title';
import { PanelRender } from 'rc-table/lib/interface';
import React, { ReactElement, useEffect, useLayoutEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Subscription } from 'rxjs';

import { PaginierungDTOSortByEnum } from '@plateg/rest-api';

import { AriaController } from '../../controllers';
import { DirectionDownOutlined } from '../icons/DirectionDownOutlined';
import { DirectionRightOutlined } from '../icons/DirectionRightOutlined';
import { useAppDispatch, useAppSelector } from '../store';
import { PagFilterInfo, setRequestCurrentPage, setRequestSorting } from '../store/slices/tablePaginationSlice';
import { getAllRows, getExpandableConfig, SortedInfo } from './controller';
import { ExpandRowsButton } from './table-sub-components/expand-rows-button/component.react';
import { FilterBarComponent } from './table-sub-components/filter-bar/component.react';
import { FilterBeOptions } from './table-sub-components/filter-be-options-component/component.react';
import { FilterOptions } from './table-sub-components/filter-options-component/component.react';
import { FilterTableBe } from './table-sub-components/filter-table-be/component.react';
import { ItemsPerPagePickerComponent } from './table-sub-components/items-per-page-picker/component.react';
import {
  ButtonType,
  ShowAllOrLessDocumentsButton,
} from './table-sub-components/show-all-or-less-documents-button/component.react';
import { SorterComponent } from './table-sub-components/sorter-component/component.react';

export interface PagData {
  totalItems: number;
}

export interface PagDataReq {
  currentPage: number;
  columnKey: PaginierungDTOSortByEnum;
  sortOrder: string | null;
}

export interface CommonRow {
  id: string;
}

export interface TableColumn {
  content: string;
  dataIndex?: string;
  key: string;
  sorter?: {};
}

export interface ActiveFilter {
  filteredBy: string;
  column: { name: string; columnIndex: number };
}

export interface TableComponentProps<T extends CommonRow> {
  id: string;
  columns: ColumnsType<T>;
  content: T[];
  filteredColumns?: { name: string; columnIndex: number }[];
  filterRowsMethod?: (actualContent: T[], filteredBy: string, column: { name: string; columnIndex: number }) => T[];
  prepareFilterButtonMethod?: (
    initialContent: T[],
    column: { name: string; columnIndex: number },
  ) => { displayName: string; labelContent: string; options: Set<string> };
  expandable: boolean;
  expandableCondition?: (record: T) => boolean;
  expandedRowRender?: (record: T) => ReactElement;
  expandAllRowsButtonIsVisible?: boolean;
  expandAllRowsButtonCustomTexts?: { open: string; close: string };
  sorterOptions?: { columnKey: React.Key; titleAsc: string; titleDesc: string }[];
  customDefaultSortIndex?: number;
  rowClassName?: (record: T, index: number) => string;
  className?: string;
  hiddenRowCount?: boolean;
  customButtonAriaLables?: { closed: string; opened: string };
  isFilteringWithFetch?: boolean;
  additionalRightControl?: React.ReactElement;
  isLight?: boolean;
  autoHeight?: boolean;
  userIsStellvertreter?: boolean;
  scrollElement?: HTMLElement | null;
  itemsPerPage?: number;
  expandRows?: boolean;
  bePagination?: boolean;
  tabKey?: string;
  showAllorLessDocuments?: {
    method: (action: ButtonType) => void;
    buttonType: ButtonType;
    setButtonType: (expand: ButtonType) => void;
    icon: React.ReactElement;
  };
  useFilterBar?: boolean;
  customFilterButton?: React.ReactElement;
  customFilterContent?: React.ReactElement;
  showCustomFilterDrawer?: boolean;
  setShowCustomFilterDrawer?: (boolean: boolean) => void;
  itemsPerPagePicker?: boolean;
  rowSelection?: {};
  footer?: PanelRender<T>;
}

export interface Sorter {
  sortedInfo: SortedInfo;
  title: string;
}

export const paginationButtonsRender = (type: string, originalElement: React.ReactNode) => {
  if (type === 'prev' && React.isValidElement(originalElement)) {
    return React.cloneElement(originalElement as ReactElement, {
      'aria-label': 'vorherige Seite',
    });
  }
  if (type === 'next' && React.isValidElement(originalElement)) {
    return React.cloneElement(originalElement as ReactElement, {
      'aria-label': 'nächste Seite',
    });
  }
  return originalElement;
};

const getColumnKey = (columnKey?: string) => {
  switch (columnKey) {
    case 'bearbeitetam':
      return PaginierungDTOSortByEnum.ZuletztBearbeitet;
    case 'erstellt':
      return PaginierungDTOSortByEnum.ErstelltAm;
    case 'archiviertAm':
      return PaginierungDTOSortByEnum.ArchiviertAm;
    case 'vorlageDauer':
      return PaginierungDTOSortByEnum.ZeitplanungsvorlageDauer;
    case 'fristablauf':
      return PaginierungDTOSortByEnum.Frist;
    case 'fristablaufNeu':
      return PaginierungDTOSortByEnum.NeueFrist;
    case 'regelungsvorhaben':
      return PaginierungDTOSortByEnum.Abkuerzung;
    case 'vorhabentyp':
      return PaginierungDTOSortByEnum.Vorhabentyp;
    case 'status':
      return PaginierungDTOSortByEnum.Status;
    case 'zuleitungsDatum':
    case 'zuleitungAm':
      return PaginierungDTOSortByEnum.Zuleitungsdatum;
    case 'eingangsdatum':
      return PaginierungDTOSortByEnum.Eingangsdatum;
    case 'umlaufbrdrucksachennummer':
      return PaginierungDTOSortByEnum.UmlaufBrDrucksachenNummer;
    case 'verteildatum':
      return PaginierungDTOSortByEnum.Verteildatum;
    default:
      return undefined;
  }
};

export function TableComponent<T extends CommonRow>(props: TableComponentProps<T>): React.ReactElement {
  const { t } = useTranslation();

  const sorterDefined = (props.sorterOptions?.length ?? 0) > 0;
  const expandedRowRender = props.expandedRowRender;
  const expandable = props.expandable;
  const expandableCondition = props.expandableCondition;
  const passedContent = props.content;
  const [columns, setColumns] = useState<ColumnsType<T>>();
  const [content, setContent] = useState(props.content);

  const [rowCount, setRowCount] = useState('');
  const rowClassName = props.rowClassName;
  const appStoreSettings = useAppSelector((state) => state.setting);

  const [sortedInfo, setSortedInfo] = useState<SortedInfo>();
  const [customExpandedRows, setCustomExpandedRows] = useState<string[] | undefined>();
  const [currentExpandedRowsIds, setCurrentExpandedRowsIds] = useState<string[]>([]);
  const [currentPage, setCurrentPage] = useState<number | undefined>(1);
  const [activeFiltersNumber, setActiveFiltersNumber] = useState<number | undefined>(0);

  const [itemsPerPage, setItemsPerPage] = useState<number>(props.itemsPerPage || 20);

  const showAllorLessDocuments = props.showAllorLessDocuments;
  const updateHeight = () => {
    const table = document.querySelector(`#${props.id} .ant-table-content`) as HTMLElement;
    if (!table) {
      return;
    }
    const height =
      window.innerHeight - (table.getBoundingClientRect().top + document.documentElement.scrollTop) + window.scrollY;
    table.style.maxHeight = `${height}px`;
  };
  const [isBarrierefreiheitGeoeffneteAnmerkungenInTabellen, setIsBarrierefreiheitGeoeffneteAnmerkungenInTabellen] =
    useState<boolean>(false);

  const dispatch = useAppDispatch();
  const tablePaginationTab = useAppSelector((state) => state.tablePagination.tabs[props.tabKey as string]);
  const pagInfoResult = props.tabKey ? tablePaginationTab.result : undefined;
  const [isInitialRender, setIsInitialRender] = useState(true);

  useEffect(() => {
    setIsInitialRender(false);
  }, []);

  useEffect(() => {
    if (props.bePagination && pagInfoResult?.totalItems !== undefined) {
      const noOfRows = pagInfoResult.totalItems;
      setRowCount(`${noOfRows} ${noOfRows === 1 ? 'Eintrag' : 'Einträge'}`);
    }
  }, [pagInfoResult?.totalItems]);

  useEffect(() => {
    if (!props.autoHeight) {
      updateHeight();
      window.addEventListener('scroll', updateHeight, true);
    }
    setExpandTableHeadLabel();
    let subscribe: Subscription;

    if (!props.isLight && appStoreSettings.setting?.barrierefreiheitGeoeffneteAnmerkungenInTabellen === true) {
      setIsBarrierefreiheitGeoeffneteAnmerkungenInTabellen(true);
      setCustomExpandedRows(
        getAllRows(content).map((val) => {
          return val.id;
        }),
      );
    }

    return () => {
      subscribe?.unsubscribe();
      if (!props.autoHeight) {
        window.removeEventListener('scroll', updateHeight, true);
      }
    };
  }, [appStoreSettings]);

  useEffect(() => {
    const newColumns = props.columns.slice();
    newColumns.forEach((column) => {
      column.sortOrder = sortedInfo?.getSortOrderOf(column.key?.toString() || '');
      if (column.key === sortedInfo?.columnKey && props.bePagination) {
        column.sorter = true;
      }
    });
    AriaController.removeAriaAttrByClassName('ant-table-column-sorter-up', 'aria-label');
    AriaController.removeAriaAttrByClassName('ant-table-column-sorter-down', 'aria-label');
    setColumns(newColumns);
  }, [props.columns, sortedInfo]);

  useLayoutEffect(() => {
    AriaController.setAriaLabelsByQuery('.ant-table-column-sorter-up.active', t('theme.tableComponent.sorterActiveUp'));
    AriaController.setAriaLabelsByQuery(
      '.ant-table-column-sorter-down.active',
      t('theme.tableComponent.sorterActiveDown'),
    );
    AriaController.setAriaAttrByClassName('ant-table-pagination', 'role', 'list');
    AriaController.setAriaLabelsByClassName('ant-table-pagination', 'Paginierung');
    AriaController.setAriaAttrByClassName('ant-pagination-item', 'role', 'listitem');
  });

  const setExpandTableHeadLabel = () => {
    const invisibleLabel = document.createElement('span');
    invisibleLabel.textContent = 'erweiterbare Einträge';
    invisibleLabel.className = 'sr-only';
    document.querySelectorAll('th.ant-table-row-expand-icon-cell')?.forEach((item) => {
      item.appendChild(invisibleLabel);
    });
  };

  const buttonOpened = (onClick: React.MouseEventHandler<HTMLElement>, elementId: string) => (
    <Button
      id={`theme-generic-styleGuide-openDetailView-btn-${elementId}`}
      aria-label={
        props.customButtonAriaLables?.opened ??
        'Detailansicht (zuklappbar) wird in der nächsten Tabellenzeile angezeigt'
      }
      onClick={onClick}
      type="text"
    >
      <DirectionDownOutlined />
    </Button>
  );
  const buttonClosed = (onClick: React.MouseEventHandler<HTMLElement>, elementId: string) => (
    <Button
      id={`theme-generic-styleGuide-expnadDetailView-btn-${elementId}`}
      aria-label={props.customButtonAriaLables?.closed ?? 'Detailsicht aufklappen'}
      onClick={onClick}
      type="text"
    >
      <DirectionRightOutlined />
    </Button>
  );
  const expandedConfig = getExpandableConfig(
    expandable,
    buttonOpened,
    buttonClosed,
    expandedRowRender,
    expandableCondition,
  );

  useEffect(() => {
    if (!props.bePagination) {
      const noOfRows = content.length;
      setRowCount(`${noOfRows} ${noOfRows === 1 ? 'Eintrag' : 'Einträge'}`);
    }
    if (content.length > 0 && (isBarrierefreiheitGeoeffneteAnmerkungenInTabellen || props.expandRows)) {
      setCustomExpandedRows(
        getAllRows(content).map((val) => {
          return val.id;
        }),
      );
      setCurrentExpandedRowsIds(
        getAllRows(content)
          .filter((item) => item.children && item.children?.length > 0)
          .map((val) => {
            return val.id;
          }),
      );
    }
  }, [content]);

  useEffect(() => {
    setContent(passedContent);
  }, [passedContent]);

  useEffect(() => {
    if (pagInfoResult?.currentPage !== undefined && props.bePagination) {
      setCurrentPage(pagInfoResult?.currentPage + 1);
    }
  }, [pagInfoResult?.currentPage]);

  useEffect(() => {
    if (!isInitialRender) {
      setCurrentPage(1);
    }
  }, [itemsPerPage]);

  const handleChange = (
    pagination: TablePaginationConfig,
    _filtersRecord: Record<string, (boolean | Key)[] | null>,
    sorter: SorterResult<T> | SorterResult<T>[],
  ) => {
    let singleSorter: SorterResult<T>;
    if (!Array.isArray(sorter)) {
      singleSorter = sorter;
    } else {
      singleSorter = sorter[0];
    }

    if (singleSorter.columnKey && currentPage === pagination.current) {
      const sortedInfo = new SortedInfo(singleSorter.order || null, singleSorter.columnKey);
      setSortedInfo(sortedInfo);
      if (props.bePagination && props.tabKey) {
        setCurrentPage(1);
        dispatch(
          setRequestSorting({
            tabKey: props.tabKey,
            columnKey: getColumnKey(sortedInfo.columnKey as string),
            sortOrder: sortedInfo.order,
          }),
        );
      }
    }

    if (currentPage !== pagination.current) {
      setCurrentPage(pagination.current);
      if (props.bePagination && props.tabKey) {
        dispatch(
          setRequestCurrentPage({
            tabKey: props.tabKey,
            currentPage: pagination.current,
          }),
        );
      }
      // Fix for scrolling in Firefox on last page
      setTimeout(() => {
        const scrollElement = props.scrollElement || window;
        scrollElement.scrollTo({
          top: 0,
          behavior: 'smooth',
        });
      }, 15);
    }
  };

  const filterBePeg = pagInfoResult?.filterInfo?.filterNames?.map((item, columnIndex) => {
    // read options from be "...Filter" property
    const options = pagInfoResult?.filterInfo?.[`${item.toLowerCase()}Filter` as keyof PagFilterInfo] as string[];
    return (
      options &&
      props.tabKey && (
        <FilterTableBe
          tabKey={props.tabKey}
          key={item}
          column={{
            name: item,
            columnIndex,
          }}
          id={props.id}
          options={options}
          parentId={props.id}
        />
      )
    );
  });

  const setSortedInfoHandler = (sortedInfo: SortedInfo) => {
    setSortedInfo(sortedInfo);
    if (props.bePagination && props.tabKey) {
      setCurrentPage(1);
      dispatch(
        setRequestSorting({
          tabKey: props.tabKey,
          columnKey: getColumnKey(sortedInfo.columnKey as string),
          sortOrder: sortedInfo.order,
        }),
      );
    }
  };

  const paginationBeProps: TablePaginationConfig = {
    ...(props.bePagination && pagInfoResult?.currentPage !== undefined && { current: pagInfoResult.currentPage + 1 }),
    ...(props.bePagination && pagInfoResult?.totalItems !== undefined && { total: pagInfoResult.totalItems }),
  };

  const sorterElements =
    props.sorterOptions && props.sorterOptions.length ? (
      <SorterComponent
        useFilterBar={props.useFilterBar}
        setSortedInfo={setSortedInfoHandler}
        sortedInfo={sortedInfo}
        sorterOptions={props.sorterOptions}
        customDefaultSortIndex={props.customDefaultSortIndex}
        elementId={props.id}
        rowCount={rowCount}
      />
    ) : undefined;

  const filterElements =
    props.filteredColumns && props.filteredColumns?.length > 0 ? (
      <>
        {props.bePagination ? (
          <FilterBeOptions
            tabKey={props.tabKey}
            parentId={props.id}
            filterOnSubmit={true}
            setActiveFiltersNumber={setActiveFiltersNumber}
          />
        ) : (
          !(props.tabKey === 'meineZeitplanungen' && props.isLight) && ( // no filter for zeit light 1. tab
            <FilterOptions
              content={props.content}
              actualContent={content}
              setContent={setContent}
              filterRowsMethod={props.filterRowsMethod}
              filteredColumns={props.filteredColumns}
              prepareFilterButtonMethod={props.prepareFilterButtonMethod}
              isFilteringWithFetch={props.isFilteringWithFetch}
              parentId={props.id}
              filterOnSubmit={true}
              setActiveFiltersNumber={setActiveFiltersNumber}
              activeFiltersNumber={activeFiltersNumber}
            />
          )
        )}
      </>
    ) : undefined;

  const itemsPerPagePicker = props.itemsPerPagePicker ? (
    <ItemsPerPagePickerComponent setItemsPerPage={setItemsPerPage} itemsPerPage={itemsPerPage} />
  ) : undefined;

  const rowCountElements = (
    <>
      <div hidden={props.hiddenRowCount} className="row-count-wrapper">
        <Text className={sorterDefined ? 'table-row-count' : 'table-row-count table-row-count-with-sorter'}>
          {rowCount}
        </Text>
      </div>
      {props.additionalRightControl}
    </>
  );

  return (
    <div className={'table-component'}>
      {props.useFilterBar ? (
        <FilterBarComponent
          sorterElements={sorterElements}
          filterElements={filterElements}
          rowCountElements={rowCountElements}
          activeFiltersNumber={activeFiltersNumber}
          customFilterButton={props.customFilterButton}
          customFilterContent={props.customFilterContent}
          showCustomFilterDrawer={props.showCustomFilterDrawer}
          setShowCustomFilterDrawer={props.setShowCustomFilterDrawer}
          itemsPerPagePicker={itemsPerPagePicker}
        />
      ) : (
        <>
          <Title level={2} className="sr-only">
            {t('theme.tableComponent.titleFilterSorter')} {rowCount}
          </Title>
          <Row className="filter-row">
            <Col className="filter-row-left">
              <ExpandRowsButton
                tableId={props.id}
                content={content}
                setCustomExpandedRows={(keys) => {
                  setCustomExpandedRows(keys);
                  setCurrentExpandedRowsIds(keys || []);
                }}
                expandAllRowsButtonCustomTexts={props.expandAllRowsButtonCustomTexts}
                expandAllRowsButtonIsVisible={props.expandAllRowsButtonIsVisible}
                isBarrierefreiheitGeoeffneteAnmerkungenInTabellen={isBarrierefreiheitGeoeffneteAnmerkungenInTabellen}
              />
              {props.bePagination
                ? filterBePeg
                : !(props.tabKey === 'meineZeitplanungen' && props.isLight) && ( // no filter for zeit light 1. tab
                    <FilterOptions
                      content={props.content}
                      actualContent={content}
                      setContent={setContent}
                      filterRowsMethod={props.filterRowsMethod}
                      filteredColumns={props.filteredColumns}
                      prepareFilterButtonMethod={props.prepareFilterButtonMethod}
                      isFilteringWithFetch={props.isFilteringWithFetch}
                      parentId={props.id}
                    />
                  )}
            </Col>
            <Col className="filter-row-spacer" />
            <Col className="filter-row-center">
              {sorterDefined && (
                <SorterComponent
                  setSortedInfo={setSortedInfoHandler}
                  sortedInfo={sortedInfo}
                  sorterOptions={props.sorterOptions}
                  customDefaultSortIndex={props.customDefaultSortIndex}
                  elementId={props.id}
                  rowCount={rowCount}
                />
              )}
            </Col>
            <Col className="filter-row-right">
              <div hidden={props.hiddenRowCount} className="row-count-wrapper">
                <Text className={sorterDefined ? 'table-row-count' : 'table-row-count table-row-count-with-sorter'}>
                  {rowCount}
                </Text>
              </div>
              {props.additionalRightControl}
            </Col>
          </Row>
          {showAllorLessDocuments && (
            <Row>
              <ShowAllOrLessDocumentsButton
                showAllOrLessHandler={showAllorLessDocuments.method}
                setButtonType={showAllorLessDocuments.setButtonType}
                buttonType={showAllorLessDocuments.buttonType}
                icon={showAllorLessDocuments.icon}
              />
            </Row>
          )}
        </>
      )}

      <Table
        id={props.id}
        className={props.className ? props.className : ''}
        rowSelection={props.rowSelection}
        expandable={{
          ...expandedConfig,
          expandedRowKeys: customExpandedRows,
          onExpand: (expanded, record) => {
            setCustomExpandedRows(undefined);
            if (expanded) {
              setCurrentExpandedRowsIds([...currentExpandedRowsIds, record.id]);
            } else {
              setCurrentExpandedRowsIds([...currentExpandedRowsIds.filter((id) => id !== record.id)]);
            }
          },
          expandedRowClassName: () => {
            return `expanded-row`;
          },
        }}
        columns={columns}
        dataSource={content}
        pagination={{
          defaultPageSize: itemsPerPage,
          pageSize: itemsPerPage,
          position: ['bottomCenter'],
          hideOnSinglePage: true,
          showSizeChanger: false,
          showTitle: true,
          itemRender: (_, type, originalElement) => {
            return paginationButtonsRender(type, originalElement);
          },
          ...paginationBeProps,
        }}
        rowKey={(record) => record.id}
        onChange={handleChange}
        rowClassName={rowClassName}
        showSorterTooltip={false}
        onRow={(record) => {
          if (currentExpandedRowsIds.includes(record.id)) {
            return { className: 'expanded-parent' };
          }
          return {};
        }}
        footer={props.footer}
      />
    </div>
  );
}
