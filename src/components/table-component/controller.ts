// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { ExpandableConfig, SortOrder } from 'antd/lib/table/interface';
import { ReactElement } from 'react';

import { Filters } from '../../shares/filters';
import { CommonRow } from './';

export function getExpandableConfig<T extends CommonRow>(
  expandable: boolean,
  buttonOpened: (onClick: React.MouseEventHandler<HTMLElement>, elementId: string) => React.ReactElement,
  buttonClosed: (onClick: React.MouseEventHandler<HTMLElement>, elementId: string) => React.ReactElement,
  expandedRowRender?: (record: T) => ReactElement,
  expandableCondition?: (record: T) => boolean,
): ExpandableConfig<T> | undefined {
  if (!expandable) {
    return undefined;
  }
  return {
    expandedRowRender: expandedRowRender
      ? (record: T) => expandedRowRender(record)
      : () => {
          return undefined;
        },
    rowExpandable: (record) => {
      {
        return expandableCondition === undefined || expandableCondition(record);
      }
    },
    expandIcon: ({ expanded, onExpand, record }) => {
      if ((expandableCondition !== undefined && expandableCondition(record)) || expandableCondition === undefined) {
        if (expanded) {
          return buttonOpened((e) => onExpand(record, e), record.id);
        } else {
          return buttonClosed((e) => onExpand(record, e), record.id);
        }
      } else {
        return undefined;
      }
    },
  };
}
export class SortedInfo {
  public columnKey: React.Key;
  public order: SortOrder;

  public constructor(order: SortOrder, key: React.Key) {
    this.columnKey = key;
    this.order = order;
  }

  public getSortOrderOf(key: string): SortOrder {
    return this.columnKey === key ? this.order : null;
  }
}

export function compareDates(a?: string, b?: string): number {
  if (a && b) {
    const dateA = new Date(a);
    const dateB = new Date(b);
    if (dateA > dateB) {
      return 1;
    }
    if (dateA < dateB) {
      return -1;
    }
  } else if (a) {
    return 1;
  } else if (b) {
    return -1;
  }
  return 0;
}

export function getDateTimeString(dateString: string): string {
  return `${Filters.date(new Date(dateString))}  ·  ${Filters.time(new Date(dateString))}`;
}

export function getAllRows(content: { children?: []; id: string }[]): { children?: []; id: string }[] {
  let keys: [] = [];

  return content
    .map((c) => {
      if (c.children && c.children?.length) {
        keys = [...keys, ...c.children];
      }
      return c;
    })
    .concat(keys.length ? getAllRows(keys) : keys)
    .filter((val) => {
      return val !== undefined;
    });
}
