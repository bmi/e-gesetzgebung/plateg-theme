// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './archive-confirm.less';

import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { ModalWrapper } from '../../../modal-wrapper/component.react';

interface ArchivConfirmComponentProps {
  okText: string;
  cancelText: string;
  modalTitle?: string;
  actionTitle: string;
  visible: boolean;
  setVisible: (visible: boolean) => void;
  setArchived: () => void;
}

export function ArchivConfirmComponent(props: ArchivConfirmComponentProps): React.ReactElement {
  const { t } = useTranslation();
  return (
    <ModalWrapper
      className="archiv-confirm-modal"
      open={props.visible}
      onOk={props.setArchived}
      onCancel={() => props.setVisible(!props.visible)}
      okText={props.okText}
      cancelText={props.cancelText}
      title={<h3>{props.modalTitle}</h3>}
    >
      <Title level={3} className="archiv-confirm-title">
        {t('theme.archivConfirm.hinweis')}
      </Title>
      <p
        className="archiv-confirm-content"
        dangerouslySetInnerHTML={{
          __html: props.actionTitle,
        }}
      />
    </ModalWrapper>
  );
}
