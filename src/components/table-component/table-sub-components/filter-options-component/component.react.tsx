// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Flex, Space } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

import { ActiveFilter, CommonRow, Filter } from '../..';
import { displayMessage } from '../../../messages/message-service';

export interface FilterOptionProps<T extends CommonRow> {
  content: T[];
  setContent: React.Dispatch<React.SetStateAction<T[]>>;
  actualContent: T[];
  filteredColumns?: { name: string; columnIndex: number }[];
  filterRowsMethod?: (actualContent: T[], filteredBy: string, column: { name: string; columnIndex: number }) => T[];
  prepareFilterButtonMethod?: (
    initialContent: T[],
    column: { name: string; columnIndex: number },
  ) => { displayName: string; labelContent: string; options: Set<string> };
  isFilteringWithFetch?: boolean;
  parentId: string;
  filterOnSubmit?: boolean;
  setActiveFiltersNumber?: (number: number) => void;
  activeFiltersNumber?: number;
}
interface ColumnAndDisplayName {
  displayName: string;
  columnName: string;
}

export function FilterOptions<T extends CommonRow>(props: FilterOptionProps<T>): React.ReactElement {
  const [filteredBy, setFilteredBy] = useState<ActiveFilter[]>([]);
  const [selected, setSelected] = useState<{ value: string; columnName: string } | null>();
  const columnNamesAndDisplayNames: ColumnAndDisplayName[] = [];
  const { t } = useTranslation();

  const onFilterSubmit = () => {
    let newRows = props.content;
    filteredBy.forEach((filter) => {
      newRows = props.filterRowsMethod ? props.filterRowsMethod(newRows, filter.filteredBy, filter.column) : newRows;
    });
    setSelected(undefined);
    props.setContent(newRows);
    showFilteredMsg(filteredBy);
    props.setActiveFiltersNumber && props.setActiveFiltersNumber(filteredBy.length);
  };

  const onSubmitReset = () => {
    let newRows = props.content;
    if (props.isFilteringWithFetch) {
      newRows = props.filterRowsMethod
        ? props.filterRowsMethod(props.content, '', { name: '', columnIndex: 0 })
        : newRows;
    }
    setFilteredBy([]);
    setSelected(null);
    props.setContent(newRows);
    showFilteredMsg([]);
    props.setActiveFiltersNumber && props.setActiveFiltersNumber([].length);
  };

  const showFilteredMsg = (filteredBy: ActiveFilter[]) => {
    let msg = '';
    let list = '<ul style="text-align: left;">'; // Added inline style for left alignment
    if (!filteredBy.length) {
      msg = t('theme.tableComponent.filterBar.resetMessageTitle');
      displayMessage(`${msg}`, 'success');
    } else {
      filteredBy.forEach((filter) => {
        // we want to show the "display name" and not just the column name
        const foundObject = columnNamesAndDisplayNames.find((item) => item.columnName === filter.column.name);
        const displayName = foundObject ? foundObject.displayName : null;
        const filterValue = filter.filteredBy; //added filter value to the display message
        list += `<li> ${t('theme.tableComponent.filterBar.updateMessagePoint')} ${displayName}: ${filterValue}</li>`;
      });
      list += '</ul>'; // Corrected closing tag
      msg = t('theme.tableComponent.filterBar.updateMessageTitle');
      displayMessage(`${msg} ${list}`, 'success');
    }
  };

  const filters = props.filteredColumns?.map((element) => {
    if (props.prepareFilterButtonMethod && props.filterRowsMethod) {
      const filterButtonConfig = props.prepareFilterButtonMethod(props.content, element);
      columnNamesAndDisplayNames.push({ columnName: element.name, displayName: filterButtonConfig.displayName });
      return (
        <Filter
          key={`${element.columnIndex}-${element.name}`}
          column={element}
          displayName={filterButtonConfig.displayName}
          options={Array.from(filterButtonConfig.options)}
          initialContent={props.content}
          setActualContent={props.setContent}
          actualContent={props.actualContent}
          filteredBy={filteredBy}
          setFilteredBy={setFilteredBy}
          labelContent={filterButtonConfig.labelContent}
          filterRowsMethod={props.filterRowsMethod}
          isFilteringWithFetch={props.isFilteringWithFetch}
          parentId={props.parentId}
          filterOnSubmit={props.filterOnSubmit}
          selected={selected}
        />
      );
    } else {
      return undefined;
    }
  });
  return (
    <>
      {filters}
      {props.filterOnSubmit && (
        <Flex align="flex-end" justify="space-between">
          <Space>
            <Button
              className="ant-btn-primary"
              htmlType="submit"
              onClick={onFilterSubmit}
              disabled={!filteredBy.length && !props.activeFiltersNumber}
              aria-label={t('theme.tableComponent.filterBar.filterSubmit')}
            >
              {t('theme.tableComponent.filterBar.filterSubmit')}
            </Button>
            <Button
              className="ant-btn-secondary"
              htmlType="reset"
              onClick={onSubmitReset}
              disabled={!filteredBy.length && !props.activeFiltersNumber}
              aria-label={t('theme.tableComponent.filterBar.filterReset')}
            >
              {t('theme.tableComponent.filterBar.filterReset')}
            </Button>
          </Space>
        </Flex>
      )}
    </>
  );
}
