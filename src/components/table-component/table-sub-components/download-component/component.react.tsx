// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

import { openFile } from '../../../../controllers/RestConfigController';
import { DocumentOutlined } from '../../../icons/DocumentOutlined';
import { DownloadOutlined } from '../../../icons/DownloadOutlined';

export interface DownloadProps
  extends React.DetailedHTMLProps<React.AnchorHTMLAttributes<HTMLAnchorElement>, HTMLAnchorElement> {
  link?: string;
  name: string;
  isfile?: boolean;
}

export function Download(props: DownloadProps): React.ReactElement {
  const { link, name, isfile, ...rest } = props;

  const handleFileDownload = (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
    if (isfile) {
      e.preventDefault();
      const pdfUrl = link as string;
      openFile(pdfUrl, {
        target: '_blank',
        fileName: name,
        onError: (error) => {
          console.log(error);
        },
      });
    }
  };

  let onClickHandler = undefined;

  if (props.onClick) {
    onClickHandler = props.onClick;
  } else if (isfile) {
    onClickHandler = handleFileDownload;
  }

  return (
    <a
      id="theme-generic-tableComponentDownload-link"
      className="download-link"
      type="text"
      href={link}
      onClick={onClickHandler}
      {...rest}
    >
      <DocumentOutlined />
      {name}
      <DownloadOutlined />
    </a>
  );
}
