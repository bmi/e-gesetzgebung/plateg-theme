// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './sorter.less';

import { SelectProps } from 'antd';
import { DefaultOptionType, LabeledValue } from 'antd/lib/select';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { SelectDown } from '../../../icons/SelectDown';
import { MainContentSelectWrapper } from '../../../main-content-select-wrapper/component.react';
import { displayMessage } from '../../../messages/message-service';
import { Sorter } from '../../component.react';
import { SortedInfo } from '../../controller';

interface SorterProps {
  sorterOptions?: { columnKey: React.Key; titleAsc: string; titleDesc: string }[];
  customDefaultSortIndex?: number;
  setSortedInfo: (sortedInfo: SortedInfo) => void;
  sortedInfo?: SortedInfo;
  elementId: string;
  rowCount: string;
  useFilterBar?: boolean;
}

type LabelRender = SelectProps['labelRender'];

export function SorterComponent(props: SorterProps): React.ReactElement {
  const { t } = useTranslation();
  const customDefaultSortIndex = props.customDefaultSortIndex;
  const setSortedInfo = props.setSortedInfo;
  const sortedInfo = props.sortedInfo;
  const [sorters, setSorters] = useState<Sorter[]>([]);
  const [firstRender, setFirstRender] = useState(true);

  useEffect(() => {
    getMenuItems();
  }, [sorters]);

  useEffect(() => {
    if (!firstRender) {
      showSortedMsg();
    }
  }, [sortedInfo]);

  useEffect(() => {
    if (
      sortedInfo &&
      ((customDefaultSortIndex && sorters[customDefaultSortIndex]?.sortedInfo === sortedInfo) ||
        !customDefaultSortIndex)
    ) {
      setFirstRender(false);
    }
  });

  useEffect(() => {
    let sortersList = props.sorterOptions?.length
      ? [
          {
            sortedInfo: new SortedInfo(null, props.sorterOptions[0].columnKey),
            title: 'Keine Sortierung',
          },
        ]
      : [];
    sortersList = sortersList.concat(
      props.sorterOptions?.flatMap((sorter) => {
        return [
          {
            sortedInfo: new SortedInfo('ascend', sorter.columnKey),
            title: sorter.titleAsc,
          },
          {
            sortedInfo: new SortedInfo('descend', sorter.columnKey),
            title: sorter.titleDesc,
          },
        ];
      }) ?? [],
    );
    setSorters(sortersList);
    if (firstRender && customDefaultSortIndex && sortersList[customDefaultSortIndex]) {
      if (
        sortedInfo?.columnKey !== sortersList[customDefaultSortIndex].sortedInfo.columnKey ||
        sortedInfo?.order !== sortersList[customDefaultSortIndex].sortedInfo.order
      ) {
        setSortedInfo(sortersList[customDefaultSortIndex].sortedInfo);
      }
    } else if (firstRender && sortersList[0] && firstRender) {
      setSortedInfo(sortersList[0].sortedInfo);
    }
  }, [props.sorterOptions]);

  const labelRender: LabelRender = ({ label }) => {
    if (label && props.useFilterBar) {
      return (
        <span className="sort">
          <span className="sort-label">{t('theme.tableComponent.filterBar.sortiertNachTitle')}</span> &nbsp;{' '}
          <span className="sort-item">{label}</span>
        </span>
      );
    }
    return <span>{label}</span>;
  };

  const getCurrentSelection = (): string => {
    const selection = sorters.findIndex((sorter) => {
      return sorter.sortedInfo.columnKey === sortedInfo?.columnKey && sorter.sortedInfo.order === sortedInfo?.order;
    });
    if (selection >= 0) {
      return selection.toString();
    } else {
      return '0';
    }
  };

  const showSortedMsg = () => {
    let msg = '';
    if (getCurrentSelection() === '0') {
      msg = t('theme.tableComponent.sorterResetMsg');
    } else {
      msg = t('theme.tableComponent.sorterUpdateMsg', {
        val: sorters[getCurrentSelection() as unknown as number].title,
      });
    }
    displayMessage(`${msg}`, 'success');
  };

  const onSelect = (value: string | number | LabeledValue) => {
    setSortedInfo(
      new SortedInfo(sorters[value as number].sortedInfo.order, sorters[value as number].sortedInfo.columnKey),
    );
  };

  const getMenuItems = () => {
    let itemsList: DefaultOptionType[] = [];
    itemsList = [
      ...sorters.map((sorter, index) => {
        const key = sorter.sortedInfo.columnKey.toString() + index.toString();
        const title = sorter.title === '-' ? t('theme.tableComponent.sorterNoOrder') : sorter.title;
        return {
          label: (
            <span
              id={`theme-generic-menuItems-select-${key}`}
              className="filter-option"
              key={key}
              {...{ title: title }}
            >
              {sorter.title}
            </span>
          ),
          value: index.toString(),
          title,
        };
      }),
    ];

    return itemsList;
  };

  return (
    <div className="sorter-item">
      {!props.useFilterBar && (
        <label htmlFor={`theme-generic-sorter-select-${props.elementId}`} className="sorter-label">
          Sortieren nach
        </label>
      )}
      <MainContentSelectWrapper
        id={`theme-generic-sorter-select-${props.elementId}`}
        className={props.useFilterBar ? 'sorter-select filter-bar-select' : 'sorter-select'}
        value={getCurrentSelection()}
        onSelect={(value: string) => {
          if (value) {
            onSelect(value);
          }
        }}
        placeholder="-"
        suffixIcon={<SelectDown />}
        prefixCls={props.useFilterBar ? 'filter-bar-sort' : ''}
        labelRender={labelRender}
        options={getMenuItems()}
      />
    </div>
  );
}
