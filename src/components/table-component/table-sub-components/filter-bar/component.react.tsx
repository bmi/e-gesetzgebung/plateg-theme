// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './filter-bar.less';

import { Button, Col, Row, Space, Tooltip } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { CloseOutlined } from '../../../icons/CloseOutlined';
import { FilterIcon } from '../../../icons/FilterIcon';

interface FilterBarProps {
  filterElements?: React.ReactElement;
  sorterElements?: React.ReactElement;
  rowCountElements?: React.ReactElement;
  customFilterButton?: React.ReactElement;
  customFilterContent?: React.ReactElement;
  showCustomFilterDrawer?: boolean;
  setShowCustomFilterDrawer?: (boolean: boolean) => void;
  activeFiltersNumber?: number;
  itemsPerPagePicker?: React.ReactElement;
}

export function FilterBarComponent(props: Readonly<FilterBarProps>): React.ReactElement {
  const { t } = useTranslation();
  const filterHoverText = t('theme.tableComponent.filterBar.filterActive');

  const [showFilterDrawer, setShowFilterDrawer] = useState<boolean>(false);
  const filterBtnRef = useRef<HTMLButtonElement | null>(null);

  useEffect(() => {
    if (showFilterDrawer) {
      props.setShowCustomFilterDrawer && props.setShowCustomFilterDrawer(false);
    }
  }, [showFilterDrawer]);

  useEffect(() => {
    if (props.showCustomFilterDrawer) {
      setShowFilterDrawer(false);
    }
  }, [props.showCustomFilterDrawer]);
  useEffect(() => {
    const firstFilterSelector = document.querySelector('.filter-drawer input');
    const pressTabEvent = (event: KeyboardEvent) => {
      if (event.key === 'Tab') {
        event.preventDefault();
        (firstFilterSelector as HTMLInputElement | null)?.focus();
      }
    };
    const pressBackTabEvent = (event: KeyboardEvent) => {
      if (event.key === 'Tab' && event.shiftKey) {
        event.preventDefault();
        filterBtnRef.current?.focus();
      }
    };
    if (showFilterDrawer) {
      filterBtnRef.current?.addEventListener('keydown', pressTabEvent);
      (firstFilterSelector as HTMLInputElement | null)?.addEventListener('keydown', pressBackTabEvent);
    }
    return () => {
      filterBtnRef.current?.removeEventListener('keydown', pressTabEvent);
      (firstFilterSelector as HTMLInputElement | null)?.removeEventListener('keydown', pressBackTabEvent);
    };
  }, [showFilterDrawer]);
  return (
    <div className="filter-bar">
      <Row className="filter-row filter-top-row">
        {props.filterElements && (
          <Col className="filter-item filter-row-left">
            <Tooltip
              title={
                !showFilterDrawer && props.activeFiltersNumber && props.activeFiltersNumber > 0
                  ? `${props.activeFiltersNumber} ${filterHoverText}`
                  : ''
              }
            >
              <Button
                ref={filterBtnRef}
                className={showFilterDrawer ? 'filter-bar-button active' : 'filter-bar-button'}
                icon={showFilterDrawer ? <CloseOutlined /> : <FilterIcon />}
                iconPosition="start"
                onClick={() => {
                  setShowFilterDrawer(!showFilterDrawer);
                }}
              >
                <Space>
                  {t('theme.tableComponent.filterBar.filterTitle')}
                  {props.activeFiltersNumber && props.activeFiltersNumber > 0 ? (
                    <output
                      aria-label={
                        props.activeFiltersNumber > 1
                          ? `${props.activeFiltersNumber} ${t('theme.tableComponent.filterBar.filterPlural')}`
                          : `${props.activeFiltersNumber} ${t('theme.tableComponent.filterBar.filterSingular')}`
                      }
                      className="active-filter-number"
                    >
                      {props.activeFiltersNumber}
                    </output>
                  ) : (
                    <></>
                  )}
                </Space>
              </Button>
            </Tooltip>
          </Col>
        )}
        {props.customFilterButton && <Col className="filter-item filter-row-left">{props.customFilterButton}</Col>}
        <Col className="filter-item filter-row-center">{props.rowCountElements}</Col>
        {props.itemsPerPagePicker && <Col className="filter-item filter-row-right"> {props.itemsPerPagePicker} </Col>}
        {props.sorterElements && <Col className="filter-item filter-row-right">{props.sorterElements}</Col>}
      </Row>
      <Row className={`filter-drawer ${!showFilterDrawer && 'hidden'}`}>
        <Col className="filter-row filter-row-left">{props.filterElements}</Col>
      </Row>
      <Row className={`filter-drawer ${!props.showCustomFilterDrawer && 'hidden'}`}>
        <Col className="filter-row filter-row-left">{props.customFilterContent}</Col>
      </Row>
    </div>
  );
}
