// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './text-and-contact.less';

import Text from 'antd/lib/typography/Text';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { UserEntityDTO } from '@plateg/rest-api';

import { ContactPerson } from '../../../contact-person/component.react';
import { InfoComponent } from '../../../info-component/component.react';

interface TextAndContactProps {
  firstRow: string | React.ReactElement;
  user: UserEntityDTO;
  drawerTitle: string;
  drawerId: string;
  reverseLineOrder?: boolean;
  isStellvertretung?: boolean;
}

export function TextAndContact(props: TextAndContactProps): React.ReactElement {
  const { t } = useTranslation();
  const contactPerson = (
    <InfoComponent isContactPerson={true} title={props.drawerTitle} buttonText={props.user.name} id={props.drawerId}>
      <ContactPerson user={props.user} />
    </InfoComponent>
  );
  const stellvertretungLabel = props.isStellvertretung && (
    <span className="stellvertretung-label">{t('theme.stellvertretungLabel')}</span>
  );
  return (
    <div className="thin-text">
      {!props.reverseLineOrder && (
        <div className="small-text">
          <span>
            <Text>{props.firstRow}</Text>
            <br />
          </span>
          <span className="flexbox-span">
            {stellvertretungLabel}
            {contactPerson}
          </span>
        </div>
      )}
      {props.reverseLineOrder && (
        <div>
          <span className="flexbox-span">
            {stellvertretungLabel}
            {contactPerson}
          </span>
          <span>
            <br />
            <Text style={{ fontSize: '14px' }}>{props.firstRow}</Text>
          </span>
        </div>
      )}
    </div>
  );
}
