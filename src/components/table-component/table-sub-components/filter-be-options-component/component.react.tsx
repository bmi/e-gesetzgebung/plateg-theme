// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Flex, Space } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { FilterPropertyType } from '@plateg/rest-api';

import { useAppDispatch, useAppSelector } from '../../../store';
import { PagFilterInfo, setRequestFilter } from '../../../store/slices/tablePaginationSlice';
import { FilterTableBe } from '../filter-table-be/component.react';

interface FilterObject {
  [k: string]: string;
}

export interface FilterOptionProps {
  parentId: string;
  tabKey?: string;
  filterOnSubmit?: boolean;
  setActiveFiltersNumber?: (number: number) => void;
}
export function FilterBeOptions(props: FilterOptionProps): React.ReactElement {
  const { t } = useTranslation();
  const dispatch = useAppDispatch();

  const pagInfoResult = useAppSelector((state) => state.tablePagination.tabs[props.tabKey as string]);

  const [selectedValues, setSelectedValues] = useState<FilterObject>({});
  const [resetValue, setResetValue] = useState<boolean>();

  const isInitialRenderTotalItems = useRef(true);

  useEffect(() => {
    if (isInitialRenderTotalItems.current) {
      isInitialRenderTotalItems.current = false;
    }
  }, []);

  const onFilterSubmit = () => {
    const optionAll = String(t('theme.multiChoice.all'));
    const filters = Object.fromEntries(
      Object.entries(selectedValues).map(([filter, value]) => {
        const displayName = t(`theme.tableComponent.filterNames.${filter}`);
        const valueAllReset = `${optionAll} ${displayName}`;

        return [filter, value === valueAllReset ? undefined : value];
      }),
    );
    const filtersNumber = Object.values(filters).filter((value) => value !== undefined).length;

    if (props.tabKey) {
      dispatch(
        setRequestFilter({
          tabKey: props.tabKey,
          filters,
          newFilter: Object.keys(selectedValues)[0] as FilterPropertyType,
        }),
      );
    }
    props.setActiveFiltersNumber?.(filtersNumber);
    setResetValue(false);
  };

  const onSubmitReset = () => {
    const updatedObject = Object.fromEntries(
      Object.entries(pagInfoResult.request.filters).map(([key]) => [key, undefined]),
    );
    props.tabKey &&
      dispatch(
        setRequestFilter({
          tabKey: props.tabKey,
          filters: updatedObject,
          newFilter: Object.keys(selectedValues)[0] as FilterPropertyType,
        }),
      );
    setSelectedValues({});
    setResetValue(true);
    props.setActiveFiltersNumber && props.setActiveFiltersNumber(0);
  };

  return (
    <>
      {pagInfoResult?.result?.filterInfo?.filterNames?.map((item: string, columnIndex: number) => {
        // read options from be "...Filter" property
        const options = pagInfoResult?.result?.filterInfo?.[
          `${item.toLowerCase()}Filter` as keyof PagFilterInfo
        ] as string[];

        return (
          options &&
          props.tabKey && (
            <FilterTableBe
              tabKey={props.tabKey}
              key={item}
              column={{
                name: item as FilterPropertyType,
                columnIndex,
              }}
              options={options}
              parentId={props.parentId}
              id={props.parentId}
              filterOnSubmit={props.filterOnSubmit}
              setSelectedValues={setSelectedValues}
              selectedValues={selectedValues}
              resetValue={resetValue}
            />
          )
        );
      })}
      {props.filterOnSubmit && (
        <Flex align="flex-end" justify="space-between">
          <Space>
            <Button
              className="ant-btn-primary"
              htmlType="submit"
              onClick={onFilterSubmit}
              aria-label={t('theme.tableComponent.filterBar.filterSubmit')}
              disabled={!Object.keys(selectedValues).length}
            >
              {t('theme.tableComponent.filterBar.filterSubmit')}
            </Button>
            <Button
              className="ant-btn-secondary"
              htmlType="reset"
              onClick={onSubmitReset}
              aria-label={t('theme.tableComponent.filterBar.filterReset')}
              disabled={!Object.keys(selectedValues).length}
            >
              {t('theme.tableComponent.filterBar.filterReset')}
            </Button>
          </Space>
        </Flex>
      )}
    </>
  );
}
