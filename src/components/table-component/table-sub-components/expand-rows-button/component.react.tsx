// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './expand-rows-button.less';

import { Button } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { CommonRow, getAllRows } from '../..';
import { ViewOutlined } from '../../..';

export interface ExpandRowsButtonProps {
  expandAllRowsButtonIsVisible?: boolean;
  expandAllRowsButtonCustomTexts?: { open: string; close: string };
  setCustomExpandedRows: (customExpandedRows: string[] | undefined) => void;
  content: CommonRow[];
  tableId?: string;
  isBarrierefreiheitGeoeffneteAnmerkungenInTabellen?: boolean;
}

export function ExpandRowsButton(props: ExpandRowsButtonProps): React.ReactElement {
  const { t } = useTranslation();

  const [rowsAreExpanded, setRowsAreExpanded] = useState(props.isBarrierefreiheitGeoeffneteAnmerkungenInTabellen);
  const [keys, setKeys] = useState<string[]>();

  useEffect(() => {
    if (props.isBarrierefreiheitGeoeffneteAnmerkungenInTabellen) {
      props.setCustomExpandedRows(keys);
      setRowsAreExpanded(true);
    }
  }, [props.isBarrierefreiheitGeoeffneteAnmerkungenInTabellen]);

  useEffect(() => {
    setKeys(
      getAllRows(props.content)
        .filter((item) => item.children && item.children?.length > 0)
        .map((val) => {
          return val.id;
        }),
    );
  }, [props.content]);

  const hideBtn = !props.expandAllRowsButtonIsVisible || props.content.length === keys?.length;
  if (!hideBtn) {
    return (
      <Button
        className="expand-all-rows-btn"
        id={`${props.tableId ? props.tableId : ''}_theme-generic-table-expandAll-btn`}
        type="text"
        onClick={() => {
          if (rowsAreExpanded) {
            props.setCustomExpandedRows([]);
            setRowsAreExpanded(false);
          } else {
            props.setCustomExpandedRows(keys);
            setRowsAreExpanded(true);
          }
        }}
      >
        <ViewOutlined />
        {!rowsAreExpanded && (props.expandAllRowsButtonCustomTexts?.open ?? t('theme.tableComponent.expandAllRows'))}
        {rowsAreExpanded && (props.expandAllRowsButtonCustomTexts?.close ?? t('theme.tableComponent.closeAllRows'))}
      </Button>
    );
  } else {
    return <></>;
  }
}
