// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import Text from 'antd/lib/typography/Text';
import React from 'react';

interface TwoLineTextProps {
  firstRowLink?: string;
  firstRow: string;
  firstRowBold: boolean;
  secondRow: string | React.ReactElement;
  secondRowBold: boolean;
  secondRowLight: boolean;
  elementId: string;
}

export function TwoLineText(props: TwoLineTextProps): React.ReactElement {
  const firstRowLink = props.firstRowLink;
  const firstRow = props.firstRow;
  const firstRowBold = props.firstRowBold;
  const secondRow = props.secondRow;
  const secondRowBold = props.secondRowBold;
  const secondRowLight = props.secondRowLight;

  return (
    <>
      {firstRowLink === undefined && <Text strong={firstRowBold}>{firstRow}</Text>}
      {firstRowLink && (
        <a
          id={`theme-generic-tableComponentFirtsRow-link-${props.elementId}`}
          type="link"
          style={firstRowBold ? { fontWeight: 'bold' } : { fontWeight: 'normal' }}
          href={firstRowLink}
        >
          {firstRow}
        </a>
      )}
      <br />
      <Text className={secondRowLight ? 'text-light' : ''} strong={secondRowBold}>
        {secondRow}
      </Text>
    </>
  );
}
