// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './dropdown-button-component.less';

import { Button, Dropdown } from 'antd';
import { ItemType, MenuItemType } from 'antd/lib/menu/hooks/useItems';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';

import { FocusController } from '../../../../controllers/FocusController';
import { ThreeDotsIcon } from '../../../icons/ThreeDotsIcon';

import type { DropdownProps, MenuProps } from 'antd';

interface DropdownMenuProps extends DropdownProps {
  items: DropdownMenuItem[];
  buttonText?: React.ReactElement | string;
  elementId: string;
  openLink?: string;
  overlayClass?: string;
  isDisabled?: boolean;
}

export interface DropdownMenuItem {
  element?: React.ReactElement | string;
  type?: 'divider';
  disabled?: () => boolean;
  onClick?: () => void;
  children?: DropdownMenuItem[];
}

export function DropdownMenu(props: DropdownMenuProps): React.ReactElement {
  const { t } = useTranslation();
  const history = useHistory();
  const [isMenuVisible, setIsMenuVisible] = useState<boolean>(false);
  const [hasHover, setHasHover] = useState(false);
  const rootRef = useRef<HTMLAnchorElement>(null);
  const menuRefs: React.RefObject<HTMLAnchorElement>[] = [];
  const standardActions: DropdownMenuItem[] = props.openLink
    ? [{ element: t(`theme.tableComponent.openBtn`), onClick: () => history.push(props.openLink as string) }]
    : [];
  function createMenuList(menuItems: DropdownMenuItem[], level = 0): ItemType[] {
    return menuItems.map((element, _ind) => {
      if (element.type === 'divider') {
        return { type: 'divider' };
      }
      const isElementDisabled = element.disabled ? element.disabled() : false;
      const index = menuRefs.push(React.createRef());
      const onClick = () => {
        element.onClick?.();
        if (level === 0) {
          setIsMenuVisible(false);
        }
      };
      return {
        label: (
          <Button
            // Used tabIndex={undefined} to prevent skipping element in screen reader
            tabIndex={undefined}
            aria-disabled={isElementDisabled}
            type="text"
            ref={menuRefs[index - 1]}
            role="button"
            className={isElementDisabled ? 'btn-disabled' : ''}
            aria-label={`${element.element as string}${isElementDisabled ? ' deaktiviert' : ''}`}
            id={`theme-generic-styleGuide-dropDownMenu-btn-${props.elementId}`}
          >
            {element.element}
          </Button>
        ),
        children: element.children ? createMenuList(element.children, level + 1) : undefined,
        key: `theme-generic-styleGuide-dropDownMenu-btn-${props.elementId}-${index}-${level}`,
        onClick: isElementDisabled ? undefined : onClick,
      };
    });
  }

  const items: MenuProps['items'] = createMenuList([...standardActions, ...props.items]);

  const focusCtrl = new FocusController(setIsMenuVisible, menuRefs, rootRef);

  useEffect(() => {
    if (isMenuVisible) {
      focusFirstItem(items, props.elementId);
    }
  }, [isMenuVisible, items]);

  return (
    <span
      onKeyDown={(event) => {
        if (isMenuVisible && (event.key === 'Tab' || event.key === 'Escape')) {
          focusCtrl.setFocus(event);
        }
      }}
    >
      <Dropdown
        getPopupContainer={props.getPopupContainer}
        menu={{
          id: props.elementId,
          items,
          selectable: true,
          selectedKeys: [`theme-generic-styleGuide-dropDownMenu-btn-${props.elementId}-1-0`],
        }}
        trigger={['click']}
        open={isMenuVisible}
        onOpenChange={(open, info) => {
          if (info.source === 'trigger' || open) {
            setIsMenuVisible(open);
          }
        }}
        aria-label="Zusatzoptionen"
        aria-expanded={isMenuVisible}
        overlayClassName={props.overlayClass}
        disabled={props.isDisabled}
        placement={props.placement || 'bottomLeft'}
      >
        <Button
          id={`theme-generic-styleGuide-dropDownMenuZusatzOptionen-btn-${props.elementId}`}
          className="table-menu-button"
          aria-label="Weitere Aktionen"
          role="menu"
          aria-activedescendant={`theme-generic-styleGuide-dropDownMenu-btn-${props.elementId}`}
          type="text"
          onMouseEnter={() => !props.isDisabled && setHasHover && setHasHover(true)}
          onMouseLeave={() => !props.isDisabled && setHasHover && setHasHover(false)}
          onFocus={() => !props.isDisabled && setHasHover && setHasHover(true)}
          onBlur={() => !props.isDisabled && setHasHover && setHasHover(false)}
          icon={<ThreeDotsIcon active={isMenuVisible} hasHover={hasHover} disabled={props.isDisabled} />}
          ref={rootRef}
          title={'Klicken Sie hier, um weitere Aktionen anzuzeigen'}
        >
          {props.buttonText}
        </Button>
      </Dropdown>
    </span>
  );
}

const focusFirstItem = (items: ItemType[], id: string) => {
  if (items.length === 0) return;
  const item = items.find((item) => {
    return item && !(item as MenuItemType).disabled;
  });
  if (item) {
    const element = document.querySelector(`[data-menu-id="${id}-${item?.key || ''}"]`) as HTMLElement;
    if (element) {
      setTimeout(() => element.focus());
    }
  }
};
