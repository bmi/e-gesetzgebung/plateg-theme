// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './show-all-or-less.less';

import { Button } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

export enum ButtonType {
  SHOW_ALL = 'SHOW_ALL',
  SHOW_LESS = 'SHOW_LESS',
}

type ShowAllorLessProps = {
  showAllOrLessHandler: (expand: ButtonType) => void;
  buttonType: ButtonType;
  setButtonType: (expand: ButtonType) => void;
  icon: React.ReactElement;
};

export function ShowAllOrLessDocumentsButton(props: ShowAllorLessProps): React.ReactElement {
  const { t } = useTranslation();
  const clickHandler = () => {
    props.showAllOrLessHandler(props.buttonType);
    props.buttonType === ButtonType.SHOW_ALL
      ? props.setButtonType(ButtonType.SHOW_LESS)
      : props.setButtonType(ButtonType.SHOW_ALL);
  };

  return (
    <div className="show-all-or-less-button-area">
      <Button type="link" onClick={clickHandler} icon={props.icon} className="show-all-or-less-button">
        {t(`theme.tableComponent.showAllOrLessDocumentsButton.${props.buttonType}`)}
      </Button>
    </div>
  );
}
