// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { LabelInValueType, RawValueType } from 'antd/node_modules/rc-select/lib/Select';
import { DefaultOptionType } from 'rc-cascader';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { SelectDown } from '../../../icons/SelectDown';
import { MainContentSelectWrapper } from '../../../main-content-select-wrapper/component.react';
import { displayMessage } from '../../../messages/message-service';
import { ActiveFilter, CommonRow } from '../../component.react';

interface FilterProps<T extends CommonRow> {
  column: { name: string; columnIndex: number };
  displayName: string;
  options: string[];
  initialContent: T[];
  setActualContent: Function;
  actualContent: T[];
  filteredBy: ActiveFilter[];
  setFilteredBy: Function;
  labelContent?: string;
  filterRowsMethod: (actualContent: T[], filteredBy: string, column: { name: string; columnIndex: number }) => T[];
  isFilteringWithFetch?: boolean;
  parentId: string;
  filterOnSubmit?: boolean;
  selected?: { value: string; columnName: string } | null;
}

export function Filter<T extends CommonRow>(props: FilterProps<T>): React.ReactElement {
  const { t } = useTranslation();
  const options = props.options;
  const displayName = props.displayName;
  const initialContent = props.initialContent;
  const setActualContent = props.setActualContent;
  const [selected, setSelected] = useState<{ value: string; columnName: string } | null>();
  const filteredBy = props.filteredBy;
  const setFilteredBy = props.setFilteredBy;
  const column = props.column;
  const labelContent = props.labelContent;

  useEffect(() => {
    if (!props.isFilteringWithFetch) {
      setSelected(null);
      setFilteredBy([]);
    }
  }, [props.initialContent]);

  useEffect(() => {
    props.selected === null && setSelected(props.selected);
  }, [props.selected]);

  const getSortedOptions = (): string[] => {
    return options.sort(function (option1, option2) {
      return option1?.toLowerCase().localeCompare(option2?.toLowerCase());
    });
  };

  const optionAll = String(t('theme.multiChoice.all'));

  const showFilteredMsg = (value: string, newRowsCount: number) => {
    let msg = '';
    if (value === `${optionAll} ${displayName}`) {
      msg = t('theme.tableComponent.filterResetMsg', { type: displayName });
    } else {
      msg = t('theme.tableComponent.filterUpdateMsg', { type: displayName, val: value });
    }
    const numberOfRows = ` - ${newRowsCount} ${newRowsCount === 1 ? 'Eintrag' : 'Einträge'}`;
    displayMessage(`${msg} ${numberOfRows}`, 'success');
  };

  const getMenuItems = (): DefaultOptionType[] => {
    let items: DefaultOptionType[] = [];
    items.push({
      label: (
        <span className="filter-option" key={`reset${props.column.name}`}>
          {optionAll}
        </span>
      ),
      value: `${optionAll} ${displayName}`,
      title: optionAll,
    });

    items = [
      ...items,
      ...getSortedOptions().map((option, index) => {
        return {
          label: (
            <span key={`${option}-${index}`} className="filter-option">
              {option}
            </span>
          ),
          value: option,
          title: option,
        };
      }),
    ];

    return items;
  };

  const onSelect = (value: string) => {
    let newRows = initialContent;
    const otherFilters = filteredBy?.filter((filter) => {
      return filter.column !== column;
    });
    if (props.filterOnSubmit) {
      value !== `${optionAll} ${displayName}` && otherFilters.push({ filteredBy: value, column: props.column });
    } else {
      otherFilters.push({ filteredBy: value, column: props.column });
    }
    otherFilters.forEach((filter) => {
      newRows = props.filterRowsMethod(newRows, filter.filteredBy, filter.column);
    });
    if (props.filterOnSubmit) {
      setFilteredBy(otherFilters);
      setSelected({ value, columnName: displayName });
    } else {
      if (value === `${optionAll} ${displayName}`) {
        onReset(value);
      } else {
        setFilteredBy(otherFilters);
        setSelected({ value, columnName: displayName });
        newRows = props.filterRowsMethod(newRows, value, props.column);
        setActualContent(newRows);
        showFilteredMsg(value, newRows.length);
      }
    }
  };

  const onReset = (value: string) => {
    if (filteredBy) {
      let newRows = initialContent;
      // to trigger BE call if reseting all filters
      if (props.isFilteringWithFetch) {
        newRows = props.filterRowsMethod(initialContent, '', { name: '', columnIndex: 0 });
      }
      const otherFilters = filteredBy.filter((filter) => {
        return filter.column !== column;
      });
      otherFilters.forEach((filter) => {
        newRows = props.filterRowsMethod(newRows, filter.filteredBy, filter.column);
      });
      setFilteredBy(otherFilters);
      setActualContent(newRows);
      setSelected(null);
      showFilteredMsg(value, newRows.length);
    }
  };

  return (
    <div className="filter-item">
      <label
        className="filter-label"
        htmlFor={`filter-${displayName.toLowerCase().split(' ').join('-')}-${props.parentId}`}
      >
        {labelContent} {displayName}
      </label>
      <MainContentSelectWrapper
        options={getMenuItems()}
        className="filter-select"
        onSelect={(value: RawValueType | LabelInValueType) => {
          if (typeof value === 'string' || typeof value === 'number') {
            onSelect(value.toString());
          } else {
            onSelect(value.value.toString());
          }
        }}
        value={selected ? selected.value : 'Alle ' + displayName}
        id={`filter-${displayName.toLowerCase().split(' ').join('-')}-${props.parentId}`}
        suffixIcon={<SelectDown />}
      />
    </div>
  );
}
