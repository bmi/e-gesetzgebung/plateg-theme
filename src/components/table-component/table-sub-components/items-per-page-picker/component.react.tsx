// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../sorter-component/sorter.less';

import { SelectProps } from 'antd';
import React from 'react';

import { SelectDown } from '../../../icons/SelectDown';
import { MainContentSelectWrapper } from '../../../main-content-select-wrapper/component.react';

interface ItemsPerPagePickerProps {
  itemsPerPage: number;
  setItemsPerPage: (itemsPerPage: number) => void;
}

type LabelRender = SelectProps['labelRender'];

export function ItemsPerPagePickerComponent(props: ItemsPerPagePickerProps): React.ReactElement {
  const labelRender: LabelRender = ({ label }) => {
    if (label) {
      return (
        <span className="sort">
          <span className="sort-label">{'Einträge pro Seite:'}</span> &nbsp; <span className="sort-item">{label}</span>
        </span>
      );
    }
    return <span>{label}</span>;
  };

  return (
    <div className="theme-generic-items-per-page-picker-select-">
      <MainContentSelectWrapper
        value={props.itemsPerPage}
        onSelect={(value: number) => {
          if (value) {
            props.setItemsPerPage(value);
          }
        }}
        placeholder={props.itemsPerPage}
        id={`theme-generic-items-per-page-select-items-per-page-picker`}
        className={'items-per-page-picker-select filter-bar-select'}
        suffixIcon={<SelectDown />}
        prefixCls={'filter-bar-sort'}
        labelRender={labelRender}
        options={[
          {
            title: '20',
            value: 20,
            label: (
              <span id={'theme-generic-menuItems-select-20'} className="filter-option" aria-label={'20'}>
                {20}
              </span>
            ),
          },
          {
            title: '50',
            value: 50,
            label: (
              <span id={'theme-generic-menuItems-select-50'} className="filter-option" aria-label={'50'}>
                {50}
              </span>
            ),
          },
          {
            title: '100',
            value: 100,
            label: (
              <span id={'theme-generic-menuItems-select-100'} className="filter-option" aria-label={'100'}>
                {100}
              </span>
            ),
          },
        ]}
      />
    </div>
  );
}
