// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FilterPropertyType } from '@plateg/rest-api';
import i18n from 'i18next';
import { DefaultOptionType } from 'rc-cascader';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { displayMessage, MainContentSelectWrapper } from '../../..';
import { SelectDown } from '../../../icons/SelectDown';
import { useAppDispatch, useAppSelector } from '../../../store';
import { cancelNewFilter, setRequestFilter, setRequestResetFilter } from '../../../store/slices/tablePaginationSlice';
import { CommonRow } from '../../component.react';

export type FilterType = {
  value: string;
  columnName: string;
} | null;

interface FilterObject {
  [k: string]: string;
}
interface FilterProps extends CommonRow {
  column: { name: FilterPropertyType; columnIndex: number };
  options: string[] | FilterObject;
  parentId: string;
  tabKey: string;
  setSelectedValues?: (selectedValues: FilterObject) => void;
  selectedValues?: FilterObject;
  filterOnSubmit?: boolean;
  resetValue?: boolean;
}

export function FilterTableBe(props: FilterProps): React.ReactElement {
  const { t } = useTranslation();
  const dispatch = useAppDispatch();

  const pagInfoRequest = useAppSelector((state) => state.tablePagination.tabs[props.tabKey]).request;
  const pagInfoResult = useAppSelector((state) => state.tablePagination.tabs[props.tabKey]).result;

  const [selectedValue, setSelectedValue] = useState<string | undefined>();

  const isInitialRenderTotalItems = useRef(true);

  let optionsKeys: string[] = [];
  let options: string[] = [];
  if (Array.isArray(props.options)) {
    options = (props.options || []).map((item) => item);
  } else {
    // Prepare list of ids according to sorted values
    const optionsObject = props.options;
    optionsKeys = Object.keys(optionsObject).sort((a, b) =>
      optionsObject[a]?.toLowerCase().localeCompare(optionsObject[b]?.toLowerCase()),
    );
    options = Object.values(optionsObject).map((value) => value);
  }

  //   add translation for other filter
  const displayName = t(`theme.tableComponent.filterNames.${props.column.name}`);
  const column = props.column;

  const getSortedOptions = (): string[] => {
    return options.sort(function (option1, option2) {
      return option1?.toLowerCase().localeCompare(option2?.toLowerCase());
    });
  };

  const optionAll = String(t('theme.multiChoice.all'));

  useEffect(() => {
    if (!props.filterOnSubmit) {
      if (isInitialRenderTotalItems.current) {
        isInitialRenderTotalItems.current = false;
        return;
      }

      if (selectedValue) {
        if (selectedValue === valueAllReset) {
          props.tabKey &&
            dispatch(
              setRequestResetFilter({
                tabKey: props.tabKey,
                newFilter: column.name,
              }),
            );
        } else {
          props.tabKey &&
            dispatch(
              setRequestFilter({
                tabKey: props.tabKey,
                filters: { [column.name]: selectedValue },
                newFilter: column.name,
              }),
            );
        }
      }
    }
  }, [selectedValue]);
  useEffect(() => {
    if (props.selectedValues && props.selectedValues[props.column.name] !== selectedValue) {
      setSelectedValue(props.selectedValues[props.column.name]);
    }
  }, [props.selectedValues, props.column.name]);
  useEffect(() => {
    if (pagInfoRequest.newFilter === column.name && selectedValue && pagInfoResult.totalItems !== undefined) {
      showFilteredMsg(getOptionNameById(selectedValue), pagInfoResult.totalItems);

      props.tabKey &&
        dispatch(
          cancelNewFilter({
            tabKey: props.tabKey,
          }),
        );
    }
  }, [pagInfoResult]);

  useEffect(() => {
    if (props.resetValue) {
      setSelectedValue(undefined);
    }
  }, [props.resetValue]);

  const getOptionNameById = (selectedId: string) => {
    const index = optionsKeys.indexOf(selectedId);

    if (index !== -1) {
      return options[index];
    } else {
      return i18n.exists(`theme.tableComponent.filterValues.${selectedId}`)
        ? t(`theme.tableComponent.filterValues.${selectedId}`)
        : selectedId;
    }
  };

  const showFilteredMsg = (value: string, newRowsCount: number) => {
    let msg = '';
    if (value === `${optionAll} ${displayName}`) {
      msg = t('theme.tableComponent.filterResetMsg', { type: displayName });
    } else {
      msg = t('theme.tableComponent.filterUpdateMsg', { type: displayName, val: value });
    }
    const numberOfRows = ` - ${newRowsCount} ${newRowsCount === 1 ? 'Eintrag' : 'Einträge'}`;
    displayMessage(`${msg} ${numberOfRows}`, 'success');
  };

  const valueAllReset = `${optionAll} ${displayName}`;
  const getMenuItems = (): DefaultOptionType[] => {
    let items = [];
    items.push({
      label: (
        <span className="filter-option" key={`reset${props.column.name}`}>
          {optionAll}
        </span>
      ),
      value: valueAllReset,
      title: optionAll,
    });

    items = [
      ...items,
      ...getSortedOptions().map((option, index) => {
        return {
          label: (
            <span key={`${option}-${index}`} className="filter-option">
              {i18n.exists(`theme.tableComponent.filterValues.${option}`)
                ? t(`theme.tableComponent.filterValues.${option}`)
                : option}
            </span>
          ),
          value: optionsKeys[index] || option,
          title: option,
        };
      }),
    ];

    return items;
  };

  return (
    <div className="filter-item">
      <label
        className="filter-label"
        htmlFor={`filter-${displayName.toLowerCase().split(' ').join('-')}-${props.parentId}`}
      >
        {'Filtern nach'} {displayName}
      </label>
      <MainContentSelectWrapper
        options={getMenuItems()}
        className="filter-select"
        onSelect={(value: string) => {
          // till now all be filter options are strings
          if (typeof value === 'string') {
            if (props.filterOnSubmit) {
              props.setSelectedValues &&
                props.setSelectedValues({ ...props.selectedValues, [props.column.name]: value });
              setSelectedValue(value);
            } else {
              setSelectedValue(value);
            }
          }
        }}
        value={selectedValue ? selectedValue : valueAllReset}
        id={`filter-${displayName.toLowerCase().split(' ').join('-')}-${props.parentId}`}
        suffixIcon={<SelectDown />}
      />
    </div>
  );
}
