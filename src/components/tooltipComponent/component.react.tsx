// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './tooltip.less';

import { Tooltip } from 'antd';
import { TooltipPropsWithTitle } from 'antd/es/tooltip';
import React, { isValidElement, useState } from 'react';

interface Props extends TooltipPropsWithTitle {
  children?: React.ReactElement;
  onClickTooltip?: () => void;
}

type TooltipEventProps = Pick<
  React.HTMLAttributes<HTMLDivElement>,
  'onBlur' | 'onFocus' | 'onKeyDown' | 'onMouseOut' | 'onMouseOver' | 'onClick'
>;

export function TooltipComponent(props: Props): React.ReactElement {
  const [isOpened, setIsOpened] = useState(false);
  const { children, ...tooltipProps } = props;

  const handleKeyDownEvent = (event: React.KeyboardEvent<HTMLElement>) => {
    if (event.key === 'Escape') {
      setIsOpened(false);
    }
  };

  // events to better controll bitv navigation (possible esc from tooltip)
  const tooltipEventProps: TooltipEventProps = {
    onBlur: () => setIsOpened(false),
    onFocus: () => setIsOpened(true),
    onKeyDown: handleKeyDownEvent,
    onMouseOver: () => setIsOpened(true),
    onMouseOut: () => setIsOpened(false),
    onClick: props.onClickTooltip,
  };

  return (
    <div className="tooltip-comp-div" {...tooltipEventProps}>
      <Tooltip open={isOpened} autoAdjustOverflow={true} placement="right" {...tooltipProps}>
        <>{isValidElement(children) && children}</>
      </Tooltip>
    </div>
  );
}
