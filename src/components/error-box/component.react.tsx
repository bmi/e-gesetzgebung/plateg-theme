// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './errorBox.less';

import { Typography } from 'antd';
import { FieldError } from 'rc-field-form/lib/interface.d';
import React, { useEffect } from 'react';

import { AriaController } from '../../controllers';
import { ExclamationMarkRed } from '../icons/ExclamationMarkRed';

interface ErrorBoxProps {
  fieldsErrors: FieldError[];
}
export function ErrorBox(props: ErrorBoxProps): React.ReactElement {
  const { Title } = Typography;
  const errorList = props.fieldsErrors
    .filter((item) => item.errors.length > 0)
    .map((item, index) => (
      <li key={index}>
        <ExclamationMarkRed style={{ marginBottom: '-3px', marginRight: '10px' }} />
        {item.errors[0]}
      </li>
    ));

  useEffect(() => {
    let timer: NodeJS.Timeout;
    if (props.fieldsErrors.length) {
      timer = setTimeout(() => {
        AriaController.setAriaAttrByClassName('ant-form-item-explain-error', 'invalid', 'true');
      }, 100);
    }

    return () => {
      if (timer) {
        clearTimeout(timer);
      }
    };
  }, [props.fieldsErrors]);

  return (
    <>
      {errorList.length > 0 && (
        <div id="errorBox" className="errorBox">
          <Title level={2} id="errorBox-title" role={'alert'} tabIndex={-1}>
            <span>Es sind folgende Fehler aufgetreten:</span>
          </Title>
          <ul>{errorList}</ul>
        </div>
      )}
    </>
  );
}
