// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Radio } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { DocumentType } from '@plateg/rest-api';

import { FormItemWithInfo } from '../../form-item-with-info/component.react';

interface DocumentTypeProps {
  setDocumentType?: (type: DocumentType) => void;
  disabled: boolean;
  disabledItem?: DocumentType;
}
export function DocumentTypeComponent(props: DocumentTypeProps): React.ReactElement {
  const { t } = useTranslation();

  return (
    <fieldset className="fieldset-form-items">
      <legend className="seo">{t('theme.fileUpload.typeOfDocument.label')}</legend>
      <FormItemWithInfo
        name="documentTyp"
        label={<span>{t('theme.fileUpload.typeOfDocument.label')}</span>}
        initialValue={DocumentType.Editor}
        rules={[{ required: true, message: t('theme.fileUpload.typeOfDocument.error') }]}
      >
        <Radio.Group
          disabled={props.disabled}
          onChange={(event) => {
            if (props.setDocumentType) {
              props.setDocumentType(event?.target.value as DocumentType);
            }
          }}
          className="horizontal-radios"
          name="documentTyp"
        >
          <Radio
            id="fileupload-editorDocument-radio"
            value={DocumentType.Editor}
            disabled={props.disabledItem == DocumentType.Editor}
          >
            {t('theme.fileUpload.typeOfDocument.typeEditorDocument')}
          </Radio>
          <Radio
            id="fileupload-enormDocument-radio"
            value={DocumentType.Enorm}
            disabled={props.disabledItem == DocumentType.Enorm}
          >
            {t('theme.fileUpload.typeOfDocument.typeEnormDocument')}
          </Radio>
        </Radio.Group>
      </FormItemWithInfo>
    </fieldset>
  );
}
