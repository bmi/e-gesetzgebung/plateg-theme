// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, ButtonProps, Popover } from 'antd';
import Text from 'antd/lib/typography/Text';
import React, { RefObject, useEffect, useRef, useState } from 'react';

import { ExclamationCircleFilled } from '../../../icons/ExclamationCircleFilled';

interface DeletePopupComponentProps {
  handleDelete: () => void;
  id: string;
  title: string;
  content: string;
  deleteBtnText: string;
  cancelBtnText: string;
  triggerBtnProps: ButtonProps;
}

export function DeletePopupComponent(props: DeletePopupComponentProps): React.ReactElement {
  const deleteMainBtnRef = useRef<HTMLAnchorElement>(null);
  const cancelBtnRef = useRef<HTMLButtonElement>(null);
  const deleteBtnRef = useRef<HTMLButtonElement>(null);
  const [opened, setOpened] = useState(false);

  const popoverTitle = (
    <>
      <span role="img" aria-label="Achtung-Icon" className="anticon anticon-exclamation-circle">
        <ExclamationCircleFilled />
      </span>
      <Text className="ant-popover-message-title">{props.title}</Text>
    </>
  );
  useEffect(() => {
    if (opened) {
      setTimeout(() => {
        deleteBtnRef.current?.focus();
      }, 100);
    }
  }, [opened]);
  const popoverContent = (
    <div style={{ maxWidth: '300px' }}>
      <p style={{ lineHeight: '20px', fontSize: '16px' }}>{props.content}</p>
      <div className="ant-popover-buttons">
        <Button
          id={`fileupload-handleFileDelete-btn-${props.id}`}
          onClick={() => {
            setOpened(false);
            props.handleDelete();
          }}
          ref={deleteBtnRef}
          onKeyDown={(e) => onKeyDownHandler(e, cancelBtnRef)}
          type="primary"
        >
          {props.deleteBtnText}
        </Button>
        <Button
          id={`fileupload-abortFileDelete-btn-${props.id}`}
          ref={cancelBtnRef}
          onKeyDown={(e) => onKeyDownHandler(e, deleteBtnRef)}
          onClick={() => {
            setOpened(false);
            deleteMainBtnRef.current?.focus();
          }}
          type="default"
        >
          {props.cancelBtnText}
        </Button>
      </div>
    </div>
  );

  const onKeyDownHandler = (e: React.KeyboardEvent<HTMLElement>, btnRef: RefObject<HTMLButtonElement>) => {
    if (e.code === 'Tab' && !e.shiftKey) {
      e.preventDefault();
      btnRef.current?.focus();
    }
  };

  return (
    <>
      <Popover title={popoverTitle} open={opened} placement="top" content={popoverContent}>
        <Button
          {...props.triggerBtnProps}
          onClick={() => {
            setOpened(true);
          }}
          ref={deleteMainBtnRef}
        ></Button>
      </Popover>
    </>
  );
}
