// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { UploadFile } from 'antd/lib/upload/interface';
import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';

import { DeleteOutlined } from '../../icons/DeleteOutlined';
import { UploadComponent } from '../uploadComponent/component.react';
import { DeletePopupComponent } from '../uploadComponent/delete-popup/component.react';

interface AdditionalUploadProps {
  fileList?: UploadFile[];
  setFileList: React.Dispatch<React.SetStateAction<UploadFile[]>>;
  fileFormat: string;
  fileSize: number;
  singleFile: boolean;
  messagesConfig: MessageItem;
  onFormChange?: () => void;
}

interface MessageItem {
  [name: string]: string | React.ReactElement;
}

export function AdditionalUploadComponent(props: AdditionalUploadProps): React.ReactElement {
  const { t } = useTranslation();
  const uploadButtonRef = useRef<HTMLButtonElement>(null);

  const deleteAll = () => {
    props.setFileList([]);
    props.onFormChange?.();
  };

  return (
    <>
      <div>
        <UploadComponent
          onFormChange={props.onFormChange}
          messagesConfig={props.messagesConfig}
          fileList={props.fileList}
          setFileList={props.setFileList}
          fileFormat={props.fileFormat}
          fileSize={props.fileSize}
          onlySingleFileAllowed={props.singleFile}
          restrictRenaming={false}
          uploadButtonRef={uploadButtonRef}
          errorProps={{
            errorTitle: t('theme.fileUpload.uploadAttachmentsErrorTitle'),
            formatString: t('theme.fileUpload.uploadAttachmentsFormatShort'),
          }}
          isBearbeiten={false}
          uniqueKey={'AdditionalUpload'}
        />
      </div>
      <div style={{ textAlign: 'right', marginTop: '8px' }}>
        <DeletePopupComponent
          title={t('theme.fileUpload.uploadAttachmentsDeleteAllPopover.title')}
          content={t('theme.fileUpload.uploadAttachmentsDeleteAllPopover.text')}
          deleteBtnText={t('theme.fileUpload.uploadAttachmentsDeleteAllPopover.delete')}
          cancelBtnText={t('theme.fileUpload.uploadAttachmentsDeleteAllPopover.cancel')}
          handleDelete={deleteAll}
          id={'fileupload-AdditionalUploadComponent-delete-popup'}
          triggerBtnProps={{
            id: `fileupload-anlagenEntfernen-btn`,
            size: 'small',
            type: 'text',
            icon: <DeleteOutlined />,
            children: t('theme.fileUpload.uploadAttachmentsDeleteAll'),
            className: 'blue-text-button',
          }}
        />
      </div>
    </>
  );
}
