// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { DokumentenmappeDTO, EditorRemoteControllerApi } from '@plateg/rest-api';

import { LoadingStatusController } from '../../../controllers';
import { GlobalDI } from '../../../shares';
import { HinweisComponent } from '../../hinweis-component/component.react';
import { SelectDown } from '../../icons/SelectDown';
import { SelectWrapper } from '../../select-wrapper/component.react';

export interface RVCompProps {
  isBearbeiten: boolean;
  selectedRvId: string | undefined;
}

export function RegelungsentwurfComponent(props: RVCompProps): React.ReactElement {
  const { t } = useTranslation();
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const [regelungsentwuerfe, setRegelungsentwuerfe] = useState<DokumentenmappeDTO[]>([]);
  const editorCtrl = GlobalDI.get<EditorRemoteControllerApi>('editorRemoteControllerApi');

  useEffect(() => {
    if (props.selectedRvId) {
      loadingStatusController.setLoadingStatus(false);
      const obs = editorCtrl.getDokumentenmappen({ rvId: props.selectedRvId });
      obs.subscribe({
        next: (data) => {
          setRegelungsentwuerfe(data);
          loadingStatusController.setLoadingStatus(false);
        },
        error: (error) => {
          console.error('Fetching Regelungsentwürfe was not succesful', error);
          loadingStatusController.setLoadingStatus(false);
        },
      });
    }
  }, [props.selectedRvId]);

  return (
    <div role="note" aria-describedby={'regelungsentwurf-hinweis'}>
      <Form.Item
        name="dokumentenmappeId"
        label={<span>{t('theme.fileUpload.regelungsentwurf.label')}</span>}
        rules={[{ required: true, message: t('theme.fileUpload.regelungsentwurf.error') }]}
      >
        {props.selectedRvId === undefined || regelungsentwuerfe?.length > 0 ? (
          <SelectWrapper
            disabled={props.selectedRvId === undefined || props.isBearbeiten}
            placeholder={t('theme.fileUpload.regelungsentwurf.placeholder')}
            style={{ width: '100%' }}
            suffixIcon={<SelectDown />}
            options={regelungsentwuerfe.map((item) => ({
              label: (
                <span key={item.id} aria-label={item.titel}>
                  {item.titel}
                </span>
              ),
              value: item.id,
              title: item.titel,
            }))}
          />
        ) : (
          <HinweisComponent
            title={t('theme.fileUpload.regelungsentwurf.hinweisTitle')}
            content={<p>{t('theme.fileUpload.regelungsentwurf.hinweisContent')}</p>}
          />
        )}
      </Form.Item>
      {props.selectedRvId === undefined || regelungsentwuerfe?.length > 0 ? (
        <HinweisComponent
          mode="warning"
          title={t('theme.fileUpload.regelungsentwurf.editorDoc.hinweisTitle')}
          content={<p>{t('theme.fileUpload.regelungsentwurf.editorDoc.hinweisContent')}</p>}
          hinweisId="regelungsentwurf-hinweis"
        />
      ) : null}
    </div>
  );
}
