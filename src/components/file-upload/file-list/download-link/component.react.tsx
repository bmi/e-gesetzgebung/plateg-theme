// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './download-link.less';

import React from 'react';

import {
  Download,
  DownloadProps,
} from '../../../table-component/table-sub-components/download-component/component.react';

export function DownloadLink(props: DownloadProps): React.ReactElement {
  return (
    <div className="download-link-container">
      <Download {...props} />
    </div>
  );
}
