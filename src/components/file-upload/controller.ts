// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';

export function incFileNameVersionNumber(
  fileName: string,
  increaseNumber: boolean,
  fileNameSuggestion?: string,
  version?: number,
): string {
  if (fileNameSuggestion === undefined) {
    return fileName;
  }
  const fileFormat = fileName.slice(fileName.lastIndexOf('.'));
  const currentDate = new Date().toISOString().split('T')[0].replace(/-/g, '');
  let newFileName = '';
  if (increaseNumber && version) {
    newFileName = version.toString();
  }
  newFileName = `${currentDate}_${fileNameSuggestion}_${newFileName}${fileFormat}`;
  return newFileName;
}

export function getFileExtension(fileName: string | undefined | null): string {
  const empty = '';
  return fileName ? /\.([^.]*?)(?=\?|#|$)/.exec(fileName.trim())?.[0] || empty : empty;
}

export function getUploadMessagesConfig(): { [name: string]: string } {
  return {
    uploadErrorFileType: i18n.t('theme.fileUpload.uploadErrorFileType'),
    uploadErrorFileSize: i18n.t('theme.fileUpload.uploadErrorFileSize'),
    uploadErrorFileNameLength: i18n.t('theme.fileUpload.uploadErrorFileNameLength'),
    uploadError: i18n.t('theme.fileUpload.regelungsentwurfUploadError'),
    uploadBtnRegelungsentwurf: i18n.t('theme.fileUpload.uploadButtonRegelungsentwurf'),
    uploadBtnAttachments: i18n.t('theme.fileUpload.uploadButtonAttachments'),
    uploadHintRegelungsentwurf: i18n.t('theme.fileUpload.uploadHintRegelungsentwurf'),
    uploadHintAttachments: i18n.t('theme.fileUpload.uploadHintAttachments'),
    changeFileNameTitle: i18n.t('theme.fileUpload.uploadChangeFileNameModalTitel'),
    changeFileNameCancelText: i18n.t('theme.fileUpload.uploadChangeFileNameCancelTextButton'),
    changeFileNameItemLabel: i18n.t('theme.fileUpload.uploadChangeFileNameLabel'),
    changeFileNameErrorMsg: i18n.t('theme.fileUpload.uploadChangeFileNameError'),
    uploadedDraftTitle: i18n.t('theme.fileUpload.uploadedDraftTitle'),
  };
}
