// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';

import { getFileExtension, incFileNameVersionNumber } from './controller';

describe('Increment Version Number in File Name', () => {
  const currentDate = new Date().toISOString().split('T')[0].replace(/-/g, '');
  const regelungsvorhabenTitle = 'Regelentwurf';
  it('Can Modify Version Number', () => {
    expect(incFileNameVersionNumber('20201108_Regelentwurf_2.doc', true, regelungsvorhabenTitle, 3)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_3.doc`,
    );
    expect(incFileNameVersionNumber('20201108_Regelentwurf_02.doc', true, regelungsvorhabenTitle, 3)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_3.doc`,
    );
    expect(incFileNameVersionNumber('20201108_Regelentwurf_99.pdf', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.pdf`,
    );
    expect(incFileNameVersionNumber('20201108_Regelentwurf_099.pdf', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.pdf`,
    );
    expect(incFileNameVersionNumber('20201108_Regelentwurf_ 000.docx', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.docx`,
    );
    expect(incFileNameVersionNumber('20201108_Regelentwurf_00099.pdf', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.pdf`,
    );
    expect(incFileNameVersionNumber('20201108_Regelentwurf_000200.doc', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.doc`,
    );
    expect(incFileNameVersionNumber('20201108_Regelentwurf_2.DOCX', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.DOCX`,
    );
    expect(incFileNameVersionNumber('JJJJMMTT_ RVAbkürzung_123.xyz', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.xyz`,
    );
    expect(incFileNameVersionNumber('Filename_0.docx', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.docx`,
    );
    expect(incFileNameVersionNumber('Filename_3   .docx', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.docx`,
    );
    expect(incFileNameVersionNumber('Filename_   4.docx', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.docx`,
    );
    expect(incFileNameVersionNumber('Filename_   5    .docx', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.docx`,
    );
    expect(incFileNameVersionNumber('File_344_name_1.docx', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.docx`,
    );
    expect(incFileNameVersionNumber('344_10.pdf', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.pdf`,
    );
    expect(incFileNameVersionNumber('some_filename _ 4.DOC', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.DOC`,
    );
    expect(incFileNameVersionNumber('Regelentwurf_02.a', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.a`,
    );
    expect(incFileNameVersionNumber('Regelentwurf_03.ab', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.ab`,
    );
    expect(incFileNameVersionNumber('Regelentwurf_04.abc', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.abc`,
    );
    expect(incFileNameVersionNumber('Regelentwurf_05.abcd', true, regelungsvorhabenTitle, 100)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_100.abcd`,
    );
  });

  it('Cannot Modify Version Number', () => {
    expect(incFileNameVersionNumber('4.docx', true, regelungsvorhabenTitle)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_.docx`,
    );
    expect(incFileNameVersionNumber('Filename_5db.docx', true, regelungsvorhabenTitle)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_.docx`,
    );
    expect(incFileNameVersionNumber('Filename_db5.docx', true, regelungsvorhabenTitle)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_.docx`,
    );
    expect(incFileNameVersionNumber('Filename_5d6.PDF', true, regelungsvorhabenTitle)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_.PDF`,
    );
    expect(incFileNameVersionNumber('Filename_5 6.PDF', true, regelungsvorhabenTitle)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_.PDF`,
    );
    expect(incFileNameVersionNumber('Filename_5 06.PDF', true, regelungsvorhabenTitle)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_.PDF`,
    );
    expect(incFileNameVersionNumber('some_filename.DOC', true, regelungsvorhabenTitle)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_.DOC`,
    );
    expect(incFileNameVersionNumber('Regelentwurf_09.abcde', true, regelungsvorhabenTitle)).is.eq(
      `${currentDate}_${regelungsvorhabenTitle}_.abcde`,
    );
  });
});

describe('Get File Extension', () => {
  it('Positives', () => {
    expect(getFileExtension('test.PDF')).is.eq('.PDF');
    expect(getFileExtension('test.pdf')).is.eq('.pdf');
    expect(getFileExtension('.pdf')).is.eq('.pdf');
    expect(getFileExtension('test1.test2.pdf')).is.eq('.pdf');
    expect(getFileExtension('test.1')).is.eq('.1');
    expect(getFileExtension('test.12')).is.eq('.12');
    expect(getFileExtension('test.123')).is.eq('.123');
    expect(getFileExtension('test.1234')).is.eq('.1234');
    expect(getFileExtension('test.test.1234567890')).is.eq('.1234567890');
    expect(getFileExtension('test..pdf')).is.eq('.pdf');
    expect(getFileExtension('1_. docx')).is.eq('. docx');
    expect(getFileExtension('1_. Doc ')).is.eq('. Doc');
    expect(getFileExtension('test.')).is.eq('.');
    expect(getFileExtension(' .')).is.eq('.');
    expect(getFileExtension('.')).is.eq('.');
    expect(getFileExtension('..')).is.eq('.');
    expect(getFileExtension('...')).is.eq('.');
  });
  it('Negatives', () => {
    expect(getFileExtension(null)).is.eq('');
    expect(getFileExtension(undefined)).is.eq('');
    expect(getFileExtension('')).is.eq('');
    expect(getFileExtension(' ')).is.eq('');
    expect(getFileExtension('test')).is.eq('');
  });
});
