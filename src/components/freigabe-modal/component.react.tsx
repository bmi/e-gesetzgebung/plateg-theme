// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './freigabe-modal.less';

import { Button, Form } from 'antd';
import Title from 'antd/lib/typography/Title';
import { ValidateErrorEntity } from 'rc-field-form/lib/interface';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
  FreigabeAnBenutzerDTO,
  FreigabeEntityResponseDTO,
  FreigabeType,
  RolleLokalType,
  UserEntityDTO,
  UserEntityResponseDTO,
  UserEntityWithStellvertreterDTO,
} from '@plateg/rest-api';

import { Constants } from '../../utils';
import { DeleteConfirmContent } from '../delete-confirm-popover/component.react';
import { EmailsSearchComponent, FreigabeList } from '../emails-search/component.react';
import { GeneralFormWrapper } from '../general-form-wrapper/component.react';
import { HiddenInfoComponent } from '../hidden-info/component.react';
import { BerechtigungenOutlined } from '../icons/BerechtigungenOutlined';
import { MultiPageModalComponent } from '../multi-page-modal-component/component.react';
import { useAppSelector } from '../store';

export interface ExtendedFregabe extends FreigabeEntityResponseDTO {
  rolleType?: RolleLokalType;
}
export interface FreigabeModalProps {
  freigaben: ExtendedFregabe[];
  freigeben: (vals: FreigabeAnBenutzerDTO[], resetModal: (isVisible: boolean) => void) => void;
  disabled?: boolean;
  texts: {
    title: string;
    subTitle?: string;
  };
  isZeitplanung?: boolean;
  isVisible?: boolean;
  setIsVisible?: (isVisible: boolean) => void;
  leserechtOnly?: boolean;
  ersteller?: UserEntityDTO;
  className?: string;
  ressortCompare?: boolean;
  checkOwner?: boolean;
  documentEditor?: UserEntityResponseDTO;
  setDocumentEditor?: (user: UserEntityResponseDTO | undefined) => void;
  selectedEgfa?: unknown; // @plateg/theme doesn't know the type of this prop but it is passed by @plateg/egfa into this component
  isRBACActivated?: boolean;
  isChangeRightsOnlyOnRV?: boolean;
  isZeitplanungvorlage?: boolean;
  isEGFAModule?: boolean;
  allowChanges?: boolean;
  revokeObserverRight?: boolean;
  customModalText?: {
    title: string;
    bodyText: React.ReactElement;
  };
  deleteConfirmContent?: DeleteConfirmContent;
}

export interface FreigabeWithType {
  [key: string]: string | undefined;
}

const prepareRoleGast = (isZeitplanungvorlage = false, isRBACActivated = false) => {
  const RBACGast = isZeitplanungvorlage ? RolleLokalType.ZpvBeobachter : RolleLokalType.Gast;
  const roleGast = isRBACActivated ? RBACGast : FreigabeType.Leserechte;
  return roleGast;
};
const prepareRoleMitarbeiter = (isRBACActivated = false, isEGFAModule = false, rolleType?: RolleLokalType) => {
  let roleMitarbeiter = isRBACActivated ? RolleLokalType.Mitarbeiter : FreigabeType.Schreibrechte;
  roleMitarbeiter = isEGFAModule ? RolleLokalType.EgfaModulMitarbeit : roleMitarbeiter;
  roleMitarbeiter = isEGFAModule && rolleType ? rolleType : roleMitarbeiter;
  return roleMitarbeiter;
};

export function FreigabeModal(props: FreigabeModalProps): React.ReactElement {
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [isLocalVisible, setIsLocalVisible] = useState<boolean>(false);
  const isVisible = props.isVisible !== undefined ? props.isVisible : isLocalVisible;
  const setIsVisible = props.setIsVisible ? props.setIsVisible : setIsLocalVisible;

  const [currentUser, setCurrentUser] = useState<UserEntityWithStellvertreterDTO>();
  const [initialVals, setInitialVals] = useState<{ freigaben: string[]; freigabenWithType: FreigabeWithType[] }>();
  const [freigabeListProps, setFreigabeListProps] = useState<FreigabeList>();
  const [submitIsDisabled, setSubmitIsDisabled] = useState(true);
  const [undeletableEmails, setUndeletableEmails] = useState<string[]>([]);
  const appStore = useAppSelector((state) => state.user);

  const roleGast = prepareRoleGast(props.isZeitplanungvorlage, props.isRBACActivated);
  const roleMitarbeiter = prepareRoleMitarbeiter(props.isRBACActivated, props.isEGFAModule);

  useEffect(() => {
    if (appStore.user) {
      setCurrentUser(appStore.user.dto);
    }
  }, [appStore.user]);

  useEffect(() => {
    if (props.freigaben && isVisible) {
      setInitialVals({
        freigaben: props.freigaben?.map((freigabe) => {
          return freigabe.dto.user.dto.email;
        }),
        freigabenWithType: props.freigaben?.map((freigabe) => {
          return {
            [freigabe.dto.user.dto.email]: freigabe.dto.readOnlyAccess
              ? roleGast
              : prepareRoleMitarbeiter(props.isRBACActivated, props.isEGFAModule, freigabe.rolleType),
          };
        }),
      });
      setUndeletableEmails(
        props.freigaben
          ?.filter((item) => {
            return item.dto.disallowedToRemove;
          })
          .map((item) => {
            return item.dto.user.dto.email;
          }),
      );
    }
  }, [props.freigaben, isVisible]);

  useEffect(() => {
    if (!isVisible && initialVals) {
      setInitialVals(undefined);
      setSubmitIsDisabled(true);
      form.setFieldsValue(undefined);
    }
  }, [isVisible, initialVals]);

  useEffect(() => {
    if (initialVals && isVisible) {
      const options = props.leserechtOnly ? [roleGast] : [roleGast, roleMitarbeiter];
      setFreigabeListProps({
        options: options,
        initialVals: {
          freigabenWithType: initialVals.freigabenWithType,
        },
        undeletableEmails,
      });
      form.setFieldsValue(initialVals);
    }
  }, [initialVals, isVisible, undeletableEmails]);

  const freigabeModalFormName = 'freigabe-modal_main_page';

  const handleError = (errorInfo: ValidateErrorEntity) => {
    document.getElementById(`${freigabeModalFormName}_${errorInfo.errorFields[0].name.join('_')}`)?.focus();
  };

  const onFinish = () => {
    const values = form.getFieldsValue(true) as { freigabenWithType: FreigabeWithType[]; freigaben: string[] };
    const freigaben: FreigabeAnBenutzerDTO[] = values.freigaben.map((freigabe) => {
      const matchingFreigabeWithType = values.freigabenWithType.find((freigabeWithType) => {
        return freigabe === Object.getOwnPropertyNames(freigabeWithType)?.[0];
      });
      const freigabeType = matchingFreigabeWithType?.[Object.getOwnPropertyNames(matchingFreigabeWithType)?.[0]];

      return {
        email: freigabe,
        [props.isRBACActivated ? 'rolleType' : 'freigabeType']: (freigabeType || roleGast) as FreigabeType,
      };
    });

    props.freigeben(freigaben, setIsVisible);
  };

  const onchange = () => {
    const vals = form.getFieldsValue(true) as { freigabenWithType: FreigabeWithType[]; freigaben: string[] };
    const inialParticipantsArePresent =
      initialVals?.freigabenWithType
        .map((freigabeWithType) => {
          return Object.getOwnPropertyNames(freigabeWithType)?.[0];
        })
        .find((mail) => {
          return !vals.freigaben.includes(mail);
        }) === undefined;
    const sameNoOfParticipants = initialVals?.freigabenWithType.length === vals.freigaben.length;
    const initalParticipantsHaveSameRights =
      initialVals?.freigabenWithType.filter((initialFreigabenWithType) => {
        const initalMail = Object.getOwnPropertyNames(initialFreigabenWithType)?.[0];
        const initialRight = initialFreigabenWithType?.[initalMail];
        return (
          vals.freigabenWithType.find((freigabeWithType) => {
            const mail = Object.getOwnPropertyNames(freigabeWithType)?.[0];
            const right = freigabeWithType?.[mail];
            return initalMail === mail && initialRight === right;
          }) !== undefined
        );
      }).length === initialVals?.freigabenWithType.length;
    const isDisabled = inialParticipantsArePresent && sameNoOfParticipants && initalParticipantsHaveSameRights;
    setSubmitIsDisabled(isDisabled);
  };
  let modalTitle: string | React.ReactNode = t('theme.freigabe.title', { title: props.texts.title });
  if (props.texts.subTitle) {
    modalTitle = (
      <span className="modal-title-holder">
        <span>{props.texts.title}</span>
        <span>{t('theme.freigabe.title', { title: props.texts.subTitle })}</span>
      </span>
    );
  }
  return (
    <>
      {props.isVisible === undefined && (
        <Button
          id="freigabe-btn"
          className="no-wrap blue-text-button"
          type="text"
          onClick={() => setIsVisible(!isVisible)}
        >
          <BerechtigungenOutlined /> {t('theme.freigabe.triggerBtn')}
        </Button>
      )}
      {currentUser && initialVals && freigabeListProps && (
        <MultiPageModalComponent
          className={props.className}
          key={'freigabe-dialog' + isVisible.toString()}
          componentReferenceContext={'freigabe-dialog' + isVisible.toString()}
          isVisible={isVisible}
          setIsVisible={setIsVisible}
          title={modalTitle}
          cancelBtnText={t('theme.freigabe.cancelBtnText')}
          nextBtnText={t('theme.freigabenextBtnText')}
          prevBtnText=""
          activePageIndex={0}
          pages={[
            {
              content: (
                <>
                  <GeneralFormWrapper
                    disabled={props.checkOwner ? currentUser.email !== props.ersteller?.email : false}
                    onFinishFailed={handleError}
                    form={form}
                    id={`freigabe-modal`}
                    name={freigabeModalFormName}
                    className="freigabe-modal"
                    layout="vertical"
                    onFinish={onFinish}
                    initialValues={{
                      freigaben: initialVals.freigaben,
                      freigabenWithType: initialVals.freigabenWithType,
                    }}
                  >
                    {props.customModalText ? (
                      <>
                        <Title level={2}>{props.customModalText.title}</Title>
                        {props.customModalText.bodyText}
                      </>
                    ) : (
                      <Title style={props.disabled ? { display: 'none' } : {}} level={2}>
                        {t('theme.freigabe.einladenTitle')}&nbsp;
                        <HiddenInfoComponent
                          title={t('theme.freigabe.einladenDrawerLabel')}
                          text={t('theme.freigabe.einladenDrawer')}
                          customSelector="h2"
                        />
                      </Title>
                    )}

                    <EmailsSearchComponent
                      deleteConfirmContent={props.deleteConfirmContent}
                      documentEditor={props.documentEditor}
                      onRevokeEditingRight={() => {
                        props.setDocumentEditor?.(undefined);
                        setSubmitIsDisabled(false);
                      }}
                      disabled={
                        (props.checkOwner && props.ersteller?.email
                          ? currentUser.email !== props.ersteller.email
                          : false) || (!props.documentEditor ? currentUser.email !== props.ersteller?.email : false)
                      }
                      readonly={props.disabled}
                      name="freigaben"
                      form={form}
                      freigabeListProps={freigabeListProps}
                      ersteller={props.ersteller}
                      texts={{
                        searchLabel: t('theme.freigabe.searchLabel'),
                        searchDrawer: (
                          <HiddenInfoComponent
                            title={t('theme.freigabe.searchDrawerLabel')}
                            text={
                              <p
                                dangerouslySetInnerHTML={{
                                  __html: t('theme.freigabe.searchDrawer', {
                                    interpolation: { escapeValue: false },
                                  }),
                                }}
                              />
                            }
                          />
                        ),
                        searchHint: t('theme.freigabe.searchHint'),
                        addressLabel: t('theme.freigabe.addressLabel'),
                        addressDrawer: props.isChangeRightsOnlyOnRV ? (
                          <p
                            dangerouslySetInnerHTML={{
                              __html: t('theme.freigabe.changeRightsOnlyOnRV', {
                                interpolation: { escapeValue: false },
                              }),
                            }}
                          />
                        ) : (
                          <HiddenInfoComponent
                            title={t('theme.freigabe.addressDrawerLabel')}
                            text={
                              <>
                                <p>
                                  {props.disabled
                                    ? t('theme.freigabe.addressDrawerTextReadonly')
                                    : t('theme.freigabe.addressDrawerText')}
                                </p>
                              </>
                            }
                          />
                        ),
                      }}
                      rules={[
                        {
                          pattern: Constants.EMAIL_REGEXP_PATTERN,
                          message: t('theme.freigabe.errorShouldBeEmail'),
                        },
                      ]}
                      updateFormField={(fieldName: string, value: string[]) => {
                        form.setFieldsValue({
                          [fieldName]: value,
                        });
                      }}
                      currentUserEmail={currentUser?.email}
                      currentUserRessortId={props.ressortCompare ? currentUser?.ressort?.id : ''}
                      setSelectionHasChanged={() => {
                        onchange();
                      }}
                      isRBACActivated={props.isRBACActivated}
                      isZeitplanungvorlage={props.isZeitplanungvorlage}
                      revokeObserverRight={props.revokeObserverRight}
                      onlyEditUsers={props.isEGFAModule}
                      isEGFAModule={props.isEGFAModule}
                    />
                  </GeneralFormWrapper>
                </>
              ),
              primaryInsteadNextBtn: {
                buttonText: t('theme.freigabe.nextBtnText'),
                shouldNavToNextPage: false,
                hidden: !props.allowChanges,
                disabled: submitIsDisabled,
              },
              nextOnClick: () => {
                form.submit();
              },
            },
          ]}
        />
      )}
    </>
  );
}
