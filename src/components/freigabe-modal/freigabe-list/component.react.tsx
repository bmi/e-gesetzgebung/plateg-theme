// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './freigabe-list.less';

import { Button, Form, List, Select } from 'antd';
import { FormInstance } from 'rc-field-form';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
  FreigabeType,
  RolleGlobalType,
  RolleLokalType,
  UserEntityDTOAusschussEnum,
  UserEntityResponseDTO,
  UserEntityShortDTO,
} from '@plateg/rest-api';

import { resolveFreigabeRole, resolveFreigabeRoleString } from '../../../shares/translatationHelpers';
import { DeleteConfirmContent } from '../../delete-confirm-popover/component.react';
import { EmailSelectController } from '../../emailsSelect/controller';
import { DeleteOutlined } from '../../icons/DeleteOutlined';
import { SelectWrapper } from '../../select-wrapper/component.react';
import { FreigabeWithType } from '../component.react';
import { BtnDeleteRoleComponent } from './btn-delete-role/component.react';

export interface UserWithRolle extends UserEntityResponseDTO {
  rolleType?: RolleLokalType;
}

interface FreigabeListComponentProps {
  selectedUsers: UserEntityShortDTO[];
  deleteUser: (user: UserEntityShortDTO, freigabe?: boolean) => void;
  fieldName: string;
  options: FreigabeType[] | RolleGlobalType[] | RolleLokalType[];
  currentUserEmail: string;
  currentUserRessortId?: string;
  form: FormInstance;
  initialVals: {
    freigabenWithType: FreigabeWithType[];
  };
  ersteller?: UserEntityShortDTO;
  readonly?: boolean;
  setSelectionHasChanged?: () => void;
  disabled?: boolean;
  undeletableEmails?: string[];
  documentEditor?: UserWithRolle;
  isStaticFreigabenText?: boolean;
  allowUnknownEmail?: boolean;
  isRBACActivated?: boolean;
  deleteConfirmContent?: DeleteConfirmContent;
  isZeitplanungvorlage?: boolean;
  revokeObserverRight?: boolean;
  onRevokeEditingRight?: () => void;
  onlyEditUsers?: boolean;
  isEGFAModule?: boolean;
  ausschussRoles?: AusschussRoles[];
}
export interface AusschussRoles {
  [val: string]: UserEntityDTOAusschussEnum | undefined;
}
export interface FreigabeUserEntry {
  user: UserEntityShortDTO;
  deleted: boolean;
}

export function FreigabeListComponent(props: Readonly<FreigabeListComponentProps>): React.ReactElement {
  const { t } = useTranslation();
  const ctrl = new EmailSelectController();

  const [usersList, setUsersList] = useState<FreigabeUserEntry[]>([]);
  const [ausschussRoles, setAusschussRoles] = useState<AusschussRoles[]>([]);
  useEffect(() => {
    if (props.ausschussRoles) {
      setAusschussRoles(props.ausschussRoles);
    }
  }, [props.ausschussRoles]);
  useEffect(() => {
    const users = [...usersList];

    if (!users.length) {
      setUsersList(
        props.selectedUsers.map((selectedUsers) => {
          return {
            user: selectedUsers,
            deleted: false,
          };
        }),
      );
    } else {
      const deletedUsers = users.filter((user) => {
        return !props.selectedUsers
          .map((selectedUser) => {
            return selectedUser.email;
          })
          .includes(user.user.email);
      });
      const newUsers: FreigabeUserEntry[] = props.selectedUsers
        .filter((user) => {
          return (
            !users
              .flatMap((userEntry) => {
                return userEntry.user.email;
              })
              .includes(user.email) &&
            deletedUsers.find((deletedUser) => {
              return user.email === deletedUser.user.email;
            }) === undefined
          );
        })
        .map((user) => {
          return { user: user, deleted: false };
        });

      users.forEach((user) => {
        user.deleted = deletedUsers
          .flatMap((deletedUser) => {
            return deletedUser.user.email;
          })
          .includes(user.user.email);
      });
      setUsersList(users.concat(newUsers));
    }
  }, [props.selectedUsers]);

  const freigabenWithTypeFields = props.form.getFieldValue('freigabenWithType') as FreigabeWithType[];
  const deleteMsg = t(`theme.mail.msg.delete`);
  const freigabeOnchange = (val: string, email: string) => {
    props.setSelectionHasChanged?.();
    if (props.ausschussRoles) {
      if (val === RolleGlobalType.Ausschusssekretariat || val === RolleGlobalType.Ausschussmitglied) {
        if (!ausschussRoles.find((item) => item.hasOwnProperty(email))) {
          setAusschussRoles([
            ...ausschussRoles,
            { [email]: props.ausschussRoles.find((item) => item[email])?.[email] ?? UserEntityDTOAusschussEnum._3 },
          ]);
        }
      } else {
        setAusschussRoles([...ausschussRoles.filter((item) => !item.hasOwnProperty(email))]);
      }
    }
  };
  const ausschussRolesTranslation: { [key: string]: string } = t('theme.freigabe.ausschussRoles', {
    returnObjects: true,
  });
  let ausschussindex = 0;
  return (
    <List className="freigabe-list">
      <List.Item className="freigabe-list-entry first-list-item title-row" key="freigabe-list-title-entry">
        <div className="span-user-name">{t('theme.freigabe.nameCol')}</div>
        <span className="span-create-doc">{t('theme.freigabe.berechtigungCol')}</span>
      </List.Item>
      {props.ersteller && (
        <List.Item
          className="freigabe-list-entry"
          key="freigabe-list-ersteller-entry"
          id={`selected-freigabe-${props.fieldName}-ersteller`}
        >
          <div className="span-user-name">
            {props.ersteller.name}
            {props.ersteller.email === props.currentUserEmail ? ' (Sie)' : ''}
          </div>
          <span className="span-create-doc">
            <span className="user-role">
              {props.isRBACActivated
                ? resolveFreigabeRole(
                    t,
                    props.isZeitplanungvorlage ? RolleLokalType.ZpvBesitzer : RolleLokalType.Federfuehrer,
                  )
                : 'Dokument erstellt'}
            </span>
          </span>
        </List.Item>
      )}
      {props.documentEditor && (
        <List.Item
          className="freigabe-list-entry"
          key="freigabe-list-editor-entry"
          id={`selected-freigabe-${props.fieldName}-editor`}
        >
          <div className="span-user-name">
            {props.documentEditor.dto.name}
            {props.documentEditor.dto.email === props.currentUserEmail ? ' (Sie)' : ''}
          </div>
          <span className="span-create-doc">
            <span className="user-role">
              {props.isRBACActivated
                ? resolveFreigabeRole(
                    t,
                    props.isEGFAModule && props.documentEditor.rolleType
                      ? props.documentEditor.rolleType
                      : RolleLokalType.Mitarbeiter,
                  )
                : 'Dokument Schreibrechte'}
            </span>
            {!(props.isEGFAModule && props.documentEditor.rolleType === RolleLokalType.Mitarbeiter) && (
              <div className="btn-delete-role">
                <Button
                  type="text"
                  className="blue-text-button"
                  aria-label={`${deleteMsg} ${props.documentEditor.dto.name}`}
                  id={`delete-freigabe-revokeEditingRight`}
                  onClick={props.onRevokeEditingRight}
                >
                  <DeleteOutlined />
                </Button>
              </div>
            )}
          </span>
        </List.Item>
      )}

      {usersList.map((user, index) => {
        const ressortString = ctrl.getRessortString(user.user as unknown as UserEntityShortDTO);
        const name = user.user.name || user.user.email;
        const foundFreigabeWithType = props.initialVals.freigabenWithType.find((freigabeWithType) => {
          return freigabeWithType.hasOwnProperty(user.user.email);
        });
        const foundFreigabe = freigabenWithTypeFields.find((item) => {
          if (item) {
            return Object.getOwnPropertyNames(item)?.[0].toLowerCase() === user.user.email.toLowerCase();
          }
          return false;
        });
        const inputIndex = foundFreigabeWithType
          ? props.initialVals.freigabenWithType.indexOf(foundFreigabeWithType)
          : index;
        const isUserNotActive = ctrl.isUserNotActive(user.user);
        const isEmailForbidden = ctrl.isEmailForbidden(user.user, props.allowUnknownEmail);
        const conditionShowError = isUserNotActive || isEmailForbidden;
        let preparedOptions = props.options;
        if (user.user.email !== props.currentUserEmail) {
          preparedOptions = [
            ...new Set(
              [...preparedOptions, foundFreigabeWithType?.[user.user.email]].filter(
                (option): option is FreigabeType => option !== undefined,
              ),
            ),
          ];
        }
        const roleDisabledForBR = (option: FreigabeType | RolleGlobalType | RolleLokalType) => {
          if (props.currentUserRessortId !== 'BR') {
            return false;
          }
          return !props.options.some((elem) => elem === option);
        };

        const userDisabledForGFAModal =
          (props.isEGFAModule && foundFreigabeWithType?.[user.user.email] === RolleLokalType.Mitarbeiter) || false;
        const isSelectDisabled =
          props.readonly ||
          props.disabled ||
          props.isStaticFreigabenText ||
          user.user.email === props.currentUserEmail ||
          userDisabledForGFAModal;

        return (
          <List.Item
            className={`freigabe-list-entry ${conditionShowError ? 'has-error' : ''}`}
            key={`${index}`}
            id={`selected-freigabe-${props.fieldName}-${index}`}
            hidden={user.deleted}
          >
            <div className="span-user-name">
              {name}
              {user.user.email === props.currentUserEmail ? ' (Sie)' : ressortString}
            </div>
            <div className="span-role-select">
              {isSelectDisabled ? (
                <span className="value-disabled-mode">
                  {resolveFreigabeRoleString(
                    t,
                    props.form.getFieldValue(['freigabenWithType', inputIndex, user.user.email]),
                  )}
                </span>
              ) : (
                <>
                  <Form.Item
                    id={`${user.user.email}-user`}
                    key={`${user.user.email}-user`}
                    initialValue={foundFreigabeWithType?.[user.user.email] || props.options[0]}
                    name={['freigabenWithType', inputIndex, user.user.email]}
                    rules={[
                      {
                        validator: (_rule, value) => {
                          const roleChanged = foundFreigabe?.[user.user.email] !== value;
                          if (
                            props.currentUserEmail === user.user.email &&
                            foundFreigabe?.[user.user.email] &&
                            !user.deleted &&
                            roleChanged
                          ) {
                            return Promise.reject(t('theme.freigabe.errorParticipant'));
                          }
                          return Promise.resolve();
                        },
                      },
                    ]}
                  >
                    <SelectWrapper
                      size="middle"
                      onChange={(val: string) => freigabeOnchange(val, user.user.email)}
                      disabled={isSelectDisabled}
                    >
                      {preparedOptions.map((option, innerIndex) => {
                        return (
                          <Select.Option
                            disabled={roleDisabledForBR(option)}
                            id={`freigabe-list-entry-${user.user.email}-${option}`}
                            value={option}
                            key={`titel-select-${innerIndex}`}
                          >
                            {resolveFreigabeRoleString(t, option)}
                          </Select.Option>
                        );
                      })}
                    </SelectWrapper>
                  </Form.Item>
                  {props.ausschussRoles && ausschussRoles.find((item) => item.hasOwnProperty(user.user.email)) && (
                    <Form.Item
                      className="ausschuss-select"
                      id={`${user.user.email}-ausschuss-user`}
                      key={`${user.user.email}-ausschuss-user`}
                      initialValue={
                        props.ausschussRoles.find((item) => item[user.user.email])?.[user.user.email] ||
                        UserEntityDTOAusschussEnum._3
                      }
                      name={['freigabenWithAusschuss', ausschussindex++, user.user.email]}
                      required
                    >
                      <SelectWrapper
                        size="middle"
                        disabled={isSelectDisabled}
                        onChange={() => props.setSelectionHasChanged?.()}
                      >
                        {Object.keys(ausschussRolesTranslation).map((key, innerIndex) => {
                          return (
                            <Select.Option
                              id={`ausschuss-list-entry-${user.user.email}-${key}`}
                              value={key}
                              key={`titel-select-${innerIndex}`}
                            >
                              {ausschussRolesTranslation[key]}
                            </Select.Option>
                          );
                        })}
                      </SelectWrapper>
                    </Form.Item>
                  )}
                </>
              )}

              <BtnDeleteRoleComponent
                deleteConfirmContent={props.deleteConfirmContent}
                user={user}
                deleteUser={props.deleteUser}
                index={index}
                fieldName={props.fieldName}
                disabled={props.disabled}
                undeletableEmails={props.undeletableEmails}
                readonly={props.readonly || props.onlyEditUsers}
                name={name}
                ressortString={ressortString}
                revokeObserverRight={props.revokeObserverRight}
                currentUserEmail={props.currentUserEmail}
              />
            </div>
          </List.Item>
        );
      })}
      <div className="div-no-addresses">{!usersList.length ? t(`theme.mail.msg.noAddresses`) : null}</div>
    </List>
  );
}
