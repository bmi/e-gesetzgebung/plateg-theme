// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

import { UserEntityShortDTO } from '@plateg/rest-api';

import { DeleteConfirmContent, DeleteConfirmPopover } from '../../../delete-confirm-popover/component.react';
import { DeleteOutlined } from '../../../icons/DeleteOutlined';
import { FreigabeUserEntry } from '../component.react';

interface BtnDeleteRoleProps {
  deleteConfirmContent?: DeleteConfirmContent;
  user: FreigabeUserEntry;
  deleteUser: (user: UserEntityShortDTO, freigabe?: boolean) => void;
  index: number;
  fieldName: string;
  disabled?: boolean;
  undeletableEmails?: string[];
  readonly?: boolean;
  name: string;
  ressortString: string;
  revokeObserverRight?: boolean;
  currentUserEmail: string;
}

export function BtnDeleteRoleComponent(props: Readonly<BtnDeleteRoleProps>): React.ReactElement {
  const { t } = useTranslation();
  const [deleteConfirmationIdOpened, setDeleteConfirmationIdOpened] = useState<string>('');
  const deleteMsg = t(`theme.mail.msg.delete`);
  const isRevokeBeobachtungZPV = props.revokeObserverRight && props.user.user.email === props.currentUserEmail;
  return (
    <div className="btn-delete-role">
      {props.deleteConfirmContent ? (
        <DeleteConfirmPopover
          opened={deleteConfirmationIdOpened === props.user.user.email}
          close={() => {
            setDeleteConfirmationIdOpened('');
          }}
          onDelete={() => {
            props.deleteUser(props.user.user, true);
          }}
          content={props.deleteConfirmContent}
          mainDeleteBtnId={`show-delete-freigabe-${props.fieldName}-${props.index}`}
        >
          <Button
            disabled={
              isRevokeBeobachtungZPV
                ? false
                : props.disabled || props.undeletableEmails?.includes(props.user.user.email)
            }
            hidden={isRevokeBeobachtungZPV ? false : props.readonly}
            type="text"
            className="blue-text-button"
            aria-label={`${deleteMsg} ${props.name} ${props.ressortString}`}
            id={`show-delete-freigabe-${props.fieldName}-${props.index}`}
            onClick={() => setDeleteConfirmationIdOpened(props.user.user.email)}
          >
            <DeleteOutlined />
          </Button>
        </DeleteConfirmPopover>
      ) : (
        <Button
          disabled={
            isRevokeBeobachtungZPV ? false : props.disabled || props.undeletableEmails?.includes(props.user.user.email)
          }
          hidden={isRevokeBeobachtungZPV ? false : props.readonly}
          type="text"
          className="blue-text-button"
          aria-label={`${deleteMsg} ${props.name} ${props.ressortString}`}
          id={`delete-freigabe-${props.fieldName}-${props.index}`}
          onClick={() => props.deleteUser(props.user.user, true)}
        >
          <DeleteOutlined />
        </Button>
      )}
    </div>
  );
}
