// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export function formatTimeToISOString(date: Date): string {
  // The time zone offset is the difference, in minutes, between UTC and local time. By subtracting it before formatting we prevent the date from being converted to UTC.
  // Without this, the date would be converted to UTC and the time would be off by the time zone offset. e.g. 2024-06-25T00:00:00+00:00 would be converted to 2024-06-24T23:00:00+00:00.
  const dateWithoutTimeZone = new Date(date.getTime() - date.getTimezoneOffset() * 60000);
  return dateWithoutTimeZone.toISOString();
}
