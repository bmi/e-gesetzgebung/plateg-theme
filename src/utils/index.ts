// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export { Constants } from './Constants';

export const fetchUrl = async (url: string) => {
  try {
    const response = await fetch(url);

    const data: unknown = await response.json();
    return data;
  } catch (error) {
    console.error('Fehler beim Laden der Url: ', error);
    throw error;
  }
};
