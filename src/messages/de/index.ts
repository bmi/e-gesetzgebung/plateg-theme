// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export const de = {
  theme: {
    startLink: 'Startseite',
    generalErrorMsg:
      'Es ist ein unerwarteter Fehler aufgetreten. Bitte wenden Sie sich an den Support. Wir entschuldigen uns für entstandene Umstände. {{fehlercode}}',
    emptyContent: {
      title: 'Bearbeitungshinweise',
    },
    contentView: {
      filterBar: {
        filterUpdateMsg_one: 'Folgender Filter wurde erfolgreich angewendet:',
        filterUpdateMsg_other: 'Folgende {{count}} Filter wurden erfolgreich angewendet:',
        filterEntriesMsg_one: 'Es wurde {{count}} Eintrag gefunden.',
        filterEntriesMsg_other: 'Es wurden {{count}} Einträge gefunden.',
        sorterUpdateMsg: 'Es wurde erfolgreich sortiert nach: {{val}}',
        sorterResetMsg: 'Die Sortierung wurde zurückgesetzt',
        searchSuccessMsg: 'Es wurde erfolgreich nach <b>{{val}}</b> gesucht.',
        searchResetMsg: 'Die Suche wurde zurückgesetzt.',
        entries_one: '{{first}}-{{last}} von {{count}} Eintrag',
        entries_other: '{{first}}-{{last}} von {{count}} Einträgen',
        noEntries: 'Keine Einträge',
      },
    },
    tableComponent: {
      filterBar: {
        filterActive: 'Filter aktiv',
        filterPlural: 'angewendete Filter',
        filterSingular: 'angewendeter Filter',
        filterTitle: 'Filter',
        filterSubmit: 'Ausgewählte Filter anwenden',
        filterReset: 'Alle Filter zurücksetzen',
        sortiertNachTitle: 'Sortiert nach:',
        updateMessageTitle: 'Folgende Filter wurden erfolgreich angewendet:',
        updateMessagePoint: 'Filtern nach',
        resetMessageTitle: 'Alle Filter wurden erfolgreich zurückgesetzt.',
      },
      displayMode: {
        table: 'Tabelle',
        grid: 'Kacheln',
        changeMsg: 'Die Darstellung wurde geändert: {{mode}}',
      },
      search: {
        searchBtnLabel: 'Suchen',
        searchInputPlaceholder: 'Suchbegriff eingeben',
        searchResetBtn: 'Suche zurücksetzen',
      },
      titleFilterSorter: 'Ergebnisbereich: ',
      filterUpdateMsg: 'Die Tabelle wurde aktualisiert: Filtern nach {{type}} {{val}}',
      filterResetMsg: 'Die Tabelle wurde aktualisiert: Filtern nach {{type}} zurückgesetzt',
      sorterUpdateMsg: 'Die Tabelle wurde aktualisiert: Sortierung nach {{val}}',
      sorterResetMsg: 'Die Tabelle wurde aktualisiert: Sortierung zurückgesetzt',
      sorterActiveUp: 'aufsteigend sortiert',
      sorterActiveDown: 'absteigend sortiert',
      sorterNoOrder: 'Keine Sortierung',
      expandAllRows: 'Alle Einträge öffnen',
      closeAllRows: 'Alle Einträge schließen',
      showAllOrLessDocumentsButton: {
        SHOW_ALL: 'Alle Einzeldokumente anzeigen',
        SHOW_LESS: 'Weniger Einzeldokumente anzeigen',
      },
      openBtn: 'Öffnen',
      filterNames: {
        // rv
        VORHABENART: 'Vorhabentyp',
        ERSTELLER: 'Erstellerin oder Ersteller',
        REGELUNGSVORHABEN: 'Regelungsvorhaben',
        // hra
        ABSTIMMUNGSART: 'Abstimmungsarten',
        ABSTIMMUNGREGELUNGSVORHABEN: 'Regelungsvorhaben',
        MITZEICHNUNGABSTIMMUNGSART: 'Abstimmungsarten',
        MITZEICHNUNGERSTELLER: 'Erstellerin oder Ersteller',
        MITZEICHNUNGSTATUS: 'Status',
        MITZEICHNUNGREGELUNGSVORHABEN: 'Regelungsvorhaben',
        UNTERABSTIMMUNGANFRAGE: 'Abstimmungen',
        ANFRAGEABSTIMMUNGSART: 'Abstimmungsarten',
        ANFRAGENTYP: 'Anfragentypen',
        ANFRAGENSTATUS: 'Status',
        // zeit
        BILLIGUNGSTATUS: 'Rückmeldung',
        BILLIGUNGREGELUNGSVORHABEN: 'Regelungsvorhaben',
        URSPRUNG: 'Ursprung',
        ZEITPLANUNGVORHABENART: 'Vorhabentyp',
        // print
        DRUCKSACHENSTATUS: 'Status',
        VORLAGENART: 'Typ',
        INITIANT_USER: 'Initiantinnen und Initianten',
        INITIANT_INSTITUTION: 'Initiant',
        DRUCKSACHE_VERTEILSTATUS: 'Status',
        BT_TAGESORDNUNGSRELEVANT: 'Tagesordnungsrelevanz',
        // umlauf
        DRUCKSACHENUMMER: 'BR-Drucksachennummer',
        UMLAUFSTATUS: 'Status',
        // ggp
        sachgebiete: 'Themengebiet',
        initiants: 'Initiant',
        phases: 'Phase',
        eingangVom: 'Eingang vom',
        eingangBis: 'Eingang bis',
      },
      filterValues: {
        // rv
        GESETZ: 'Gesetz',
        RECHTSVERORDNUNG: 'Rechtsverordnung',
        VERWALTUNGSVORSCHRIFT: 'Verwaltungsvorschrift',
        // hra
        HAUSABSTIMMUNG: 'Hausabstimmung',
        VORHABENCLEARING: 'Vorhabenclearing',
        RESSORTABSTIMMUNG: 'Ressortabstimmung',
        UNTERABSTIMMUNG: 'Unterabstimmung',
        SONSTIGE_ABSTIMMUNG: 'Sonstige Abstimmung',
        SCHLUSSABSTIMMUNG: 'Schlussabstimmung',
        ABSTIMMUNG_DER_VORLAGE_FUER_DEN_REGIERUNGSENTWURF:
          'Abstimmung der Vorlage für den Regierungsentwurf mit der Hausleitung',
        MITZEICHNUNG: 'Mitgezeichnet',
        MITZEICHNUNG_UNTER_VORBEHALT: 'Mitgezeichnet u. V.',
        KEINE_MITZEICHNUNG: 'Abgelehnt',
        NICHT_BEARBEITET: 'Offen',
        CC: 'CC: Kenntnisnahme',
        KEINE_MITZEICHNUNG_STELLUNGNAHME_DES_NATIONALEN_NORMENKONTROLLRATS: 'NKR-Stellungnahme abgegeben',
        TEILNAHME_ANGEFRAGT: 'Teilnahme angefragt',
        TEILNAHME_ZURUECKGEZOGEN: 'Teilnahmeanfrage zurückgezogen',
        TEILNAHME_ABGELEHNT: 'Teilnahme abgelehnt',
        FRISTVERLAENGERUNG: 'Fristverlängerung',
        ABSTIMMUNGSTEILNAHME: 'Abstimmungsteilnahme',
        OFFEN: 'Offen',
        ZUGESTIMMT: 'Zugestimmt',
        ABGELEHNT: 'Abgelehnt',
        ABGELAUFEN: 'Abgelaufen',
        ZUERUCKGEZOGEN: 'Zurückgezogen',
        // zeit
        BILLIGUNG: 'Gebilligt',
        BILLIGUNG_UNTER_VORBEHALT: 'Gebilligt u.V.',
        KEINE_BILLIGUNG: 'Abgelehnt',
      },
    },
    documentListPopover: {
      titleMappe: 'Dokumente der Dokumentenmappe',
      titleList: 'Dokumente',
      btnOpen: 'Dokumente anzeigen',
      btnClose: 'Schließen tooltip',
    },
    mail: {
      msg: {
        noAddresses: 'Es wurde noch keine Adressatin bzw. kein Adressat übernommen.',
        notExist: 'Es wurde kein Ergebnis gefunden oder eine ungültige E-Mail-Adresse eingegeben.',
        moreThan10:
          'Es gibt mehr als 10 Ergebnisse. Bitte nutzen Sie die "Mehr anzeigen" Schaltfläche oder verfeinern Sie Ihre Suche.',
        tooShort: 'Der Suchstring ist zu kurz. Für die Suche muss mindestens ein Wort 3 Zeichen oder länger sein',
        btnApply: 'Übernehmen',
        btnApplyList: 'Liste übernehmen',
        load: 'Laden...',
        labelOutdated: '(Liste veraltet)',
        labelActualize: 'Liste aktualisieren:',
        linkActualize: 'Nicht mehr registrierte Einträge entfernen',
        listUpdated: 'Liste aktualisiert.',
        successMsgUpdate: 'Die E-Mail-Liste <b>{{listName}}</b> wurde erfolgreich aktualisiert.',
        results: {
          singular: 'Eintrag',
          plural: 'Einträge',
        },
        resultsCategories: {
          singular: 'Ergebnis',
          plural: 'Ergebnisse',
        },
        delete: 'Löschen',
        moreItems: 'weitere einblenden',
        userNotActive: ' (nicht mehr registriert)',
        notIAMUser:
          'Es können keine Benutzerinnen oder Benutzer, die nicht für E-Gesetzgebung registriert sind, ausgewählt werden.',
        noMatch: 'Keine Übereinstimmung mit Ihrem Suchbegriff.',
        allLists: 'Alle',
        myLists: 'Meine E-Mail-Listen',
        itemsShown: '{{items}} Einträge',
        btnShowMore: 'Mehr anzeigen',
        listsShown: '{{lists}} Listen',
        btnShowMoreLists: 'Mehr Listen anzeigen',
        buttonWithPopover: {
          popoverTitle: 'Möchten Sie die Auswahl überschreiben?',
          popoverContent:
            'Sie haben bereits eine Adressatin oder einen Adressaten ausgewählt. Wollen Sie stattdessen eine andere Auswahl tätigen?',
          popoverContentList:
            'In der Liste sind Adressatinnen oder Adressaten vorhanden, die Sie bereits zur Kenntnisnahme vorgemerkt haben. Wollen Sie diese stattdessen für die Teilnahme vormerken?',
          popoverContentListCC:
            'In der Liste sind Adressatinnen oder Adressaten vorhanden, die Sie bereits für die Teilnahme vorgemerkt haben. Wollen Sie diese stattdessen zur Kenntnisnahme vormerken?',
          btnApply: 'Überschreiben',
          btnCancel: 'Abbrechen',
          btnApplyList: 'Teilnahme',
          btnApplyListCC: 'Kenntnisnahme',
        },
      },
      emailsSearch: {
        placeholder: 'Suchbegriff eingeben',
        btnText: 'Suchen',
        btnClear: 'Suche zurücksetzen',
      },
    },
    freigabe: {
      title: '{{title}}: Berechtigungen',
      triggerBtn: 'Berechtigungen',
      cancelBtnText: 'Abbrechen',
      nextBtnText: 'Berechtigungen jetzt speichern',
      einladenTitle: 'Zum Dokument einladen',
      einladenDrawerLabel: 'Erklärung zu: Zum Dokument einladen',
      einladenDrawer:
        'Hier können Sie Ihr Dokument anderen Personen freigeben. Benutzen Sie dazu die untenstehende Suche und erteilen Sie dann die gewünschten Berechtigungen. Die Personen werden über das Zuteilen von Rechten automatisch per E-Mail informiert.',
      searchLabel: 'Suche nach Adressatinnen und Adressaten',
      searchDrawerLabel: 'Erklärung zu: Suche nach Adressatinnen und Adressaten',
      searchDrawer: `<p>Hier können Sie die Adressdatenbank durchsuchen. Tippen Sie dazu einfach den Namen, das Ressort oder eine E-Mail-Adresse ein. Sie können hier auch E-Mail-Listen verwenden, die Sie über die Einstellungen verwalten können.</p>
        <p>Sie können mehrere Suchbegriffe eintragen. Trennen Sie diese dazu einfach durch Leerzeichen. Die Begriffe werden dann für die Suche kombiniert. Die Treffer enthalten alle angegebenen Suchbegriffe. Es wird eine Und-Verknüpfung der Begriffe verwendet.</p>
        <p>Sie können nur Personen auswählen, die für die E-Gesetzgebung registriert sind.</p>`,
      searchHint: '(z. B. Name, Ressort, E-Mail-Adresse, E-Mail-Liste)',
      addressLabel: 'Berechtigungen',
      addressDrawerLabel: 'Erklärung zu: Berechtigungen',
      addressDrawerText:
        'Hier sehen Sie alle Personen, die Berechtigungen für das Dokument haben. Ändern Sie die Berechtigung pro Person mithilfe des Umschalters zu „Mitarbeit“ oder „Beobachtung“. Falls Sie die notwendigen Berechtigungen haben, können Sie Berechtigungen pro Person mit dem Papierkorb entziehen. Sämtliche Änderungen werden angewendet, sobald Sie unten auf „Berechtigungen jetzt speichern“ klicken.',
      addressDrawerTextReadonly: 'Hier sehen Sie alle Personen, die Berechtigungen für das Dokument haben.',
      changeRightsOnlyOnRV: `<p>Hier sehen Sie alle Personen, die eine Berechtigung für die Zeitplanung haben.</p>
          <p>Die Berechtigungen für eine Zeitplanung richten sich nach den Berechtigungen des zugehörigen Regelungsvorhabens. Falls Sie Änderungen an den Berechtigungen vornehmen möchten, müssen Sie das beim zugehörigen Regelungsvorhaben tun.</p>`,
      errorShouldBeEmail: 'Bitte geben Sie eine gültige E-Mail Adresse ein.',
      errorParticipant: 'Sie können keine Berechtigungen an sich selbst vergeben.',
      ausschussRoles: {
        PA_3: 'PA 3: Auswärtiger Ausschuss',
        PA_4: 'PA 4: Ausschuss für Inneres und Heimat',
        PA_5: 'PA 5: Sportausschuss',
        PA_6: 'PA 6: Rechtsausschuss',
        PA_7: 'PA 7: Finanzausschuss',
        PA_8: 'PA 8: Haushaltsausschuss',
        PA_9: 'PA 9: Wirtschaftsausschuss',
        PA_10: 'PA 10: Ausschuss für Ernährung und Landwirtschaft',
        PA_11: 'PA 11: Ausschuss für Arbeit und Soziales',
        PA_12: 'PA 12: Verteidigungsausschuss',
        PA_13: 'PA 13: Ausschuss für Familie, Senioren, Frauen und Jugend',
        PA_14: 'PA 14: Ausschuss für Gesundheit',
        PA_15: 'PA 15: Verkehrsausschuss',
        PA_16: 'PA 16: Ausschuss für Umwelt, Naturschutz, nukleare Sicherheit und Verbraucherschutz',
        PA_17: 'PA 17: Ausschuss für Menschenrechte und humanitäre Hilfe',
        PA_18: 'PA 18: Ausschuss für Bildung, Forschung und Technologiefolgenabschätzung',
        PA_19: 'PA 19: Ausschuss für wirtschaftliche Zusammenarbeit und Entwicklung',
        PA_20: 'PA 20: Ausschuss für Tourismus',
        PA_22: 'PA 22: Ausschuss für Kultur und Medien',
        PA_23: 'PA 23: Ausschuss für Digitales',
        PA_24: 'PA 24: Ausschuss für Wohnen, Stadtentwicklung, Bauwesen und Kommunen',
        PA_25: 'PA 25: Ausschuss für Klimaschutz und Energie',
      },
      type: {
        /**
         * Type keys must correspond to database table rbac.rollen.bezeichnung.
         * Use method {@link ../../shares/translationHelpers#resolveFreigabeRole} to lookup translations and default to uppercase key, if not found here.
         * Keys are referenced in {@link @plateg/rest-api/models/FreigabeType}, {@link @plateg/rest-api/models/FreigabeRolleType},
         * {@link @plateg/rest-api/models/RolleGlobalType}, and {@link @plateg/rest-api/models/RolleLokalType}.
         */
        ADMINISTRATION: 'Administration', //                                             // from enum RolleGlobalType.
        ADMINISTRATION_UND_SACHBEARBEITUNG: 'Administration und Sachbearbeitung (PD 1)', // from enum RolleGlobalType.
        AUSSCHUSSMITGLIED: 'Ausschussmitglied', //                                       // from enum RolleGlobalType.
        AUSSCHUSSSEKRETARIAT: 'Ausschusssekretariat', //                                 // from enum RolleGlobalType.
        BR_ADMINISTRATION: 'Administration BR', //                                       // from enum RolleGlobalType.
        BR_AUSSCHUSSBUERO: 'Ausschussbüro BR', //                                        // from enum RolleGlobalType.
        /** @deprecated instead use BR_AUSSCHUSSSEKRETARIAT */
        BR_AUSSCHUSSMITGLIED: 'Ausschusssmitglied (jetzt: Ausschussekretariat BR)', //   // from enum RolleGlobalType.
        BR_AUSSCHUSSSEKRETARIAT: 'Ausschusssekretariat BR', //                           // from enum RolleGlobalType.
        BR_BUNDESRAT: 'Bundesratsnutzer oder Bundesratsnutzerin', //                     // from enum RolleGlobalType.
        BR_FACHADMINISTRATION_AUSSCHUESSE: 'Fachadministration Ausschüsse', //           // from enum RolleGlobalType.
        BR_FACHADMINISTRATION_P1: 'Fachadministration P1', //                            // from enum RolleGlobalType.
        BR_P1: 'P1', //                                                                  // from enum RolleGlobalType.
        BT_FACHADMINISTRATION_AUSSCHUESSE: 'Fachadministration Ausschüsse', // from enum RolleGlobalType.
        BT_FACHADMINISTRATION_PD1: 'Fachadministration PD1', // from enum RolleGlobalType.
        BUNDESTAGSFRAKTION: 'Bundestagsfraktion', // from enum RolleGlobalType.
        EGFA_MODUL_BEOBACHTUNG: 'eGFA Modul Beobachtung', // present in table rbac.rollen, but not in FreigabeType, FreigabeRolleType, RolleGlobalType, RolleLokalType.
        EGFA_MODUL_MITARBEIT: 'GFA Modul Mitarbeit', //  // from enum RolleLokalType.
        ERSTELLER: 'Ersteller', //                        // from enum FreigabeRolleType AND RolleLokalType. Present in table rbac.rollen.
        FACHADMINISTRATOR_BUNDESREGIERUNG: 'Fachadministration', // from enum RolleGlobalType.
        FEDERFUEHRER: 'Federführung', //                  // from enum FreigabeType AND RolleLokalType.
        GAST: 'Beobachtung', //                           // from enum FreigabeType AND RolleLokalType.
        /** @deprecated: LESERECHTE not in table rbac.rollen. **/
        LESERECHTE: 'Leserechte', //                      // from enum FreigabeType.
        /** @deprecated: SCHREIBRECHTE not in table rbac.rollen. **/
        SCHREIBRECHTE: 'Schreibrechte', //                // from enum FreigabeType.
        MITARBEITER: 'Mitarbeit', //                      // from enum FreigabeType AND RolleLokalType.
        PD1_ANNAHMESTELLE: 'PD 1 - Annahmestelle', //     // from enum RolleGlobalType.
        PD1_LEKTORAT: 'PD 1 - Lektorat', //               // from enum RolleGlobalType.
        SACHBEARBEITUNG: 'Sachbearbeitung (PD 1)', //     // from enum RolleGlobalType.
        // SCHREIBER: undefined, //                       // present in table rbac.rollen, but not in FreigabeType, RolleGlobalType, RolleLokalType.
        ZPV_BEOBACHTER: 'Beobachtung', //                 // from enum RolleLokalType.
        ZPV_BESITZER: 'Besitzerin/Besitzer', //           // from enum RolleLokalType.
      },
      nameCol: 'Name',
      berechtigungCol: 'Berechtigung',
    },
    btnBack: 'Zurück zur vorherigen Seite',
    multiChoice: {
      btnShowChoices: 'Auswahlmöglichkeiten einblenden',
      btnHideChoices: 'Auswahlmöglichkeiten ausblenden',
      selectedItemsLabel: '{{selectedItems}} von {{allItems}} Einträgen ausgewählt',
      btnApply: 'Auswahl übernehmen',
      btnCancel: 'Abbrechen',
      noSelectionText: 'Es wurde kein{{e}} {{itemType}} ausgewählt.',
      removeItemText: 'Löschen',
      checkAll: 'Alle auswählen',
      all: 'Alle',
    },
    infoComponent: {
      title: `Erklärung zu: „{{title}}“`,
      contact: 'Kontakt',
    },
    datepicker: {
      close: 'Schließen',
      nextYear: 'Nächstes Jahr',
      nextMonth: 'Nächsten Monat',
      prevMonth: 'Vorheriger Monat',
      prevYear: 'Vorheriges Jahr',
      navHint: 'Mit den Alt+Cursor-Tasten können Sie durch Datumsangaben navigieren',
      placeholder: 'Datum eingeben',
    },
    icons: {
      checkOutlinedTitle: 'Element ausgewählt',
    },
    stellvertretungLabel: 'i.V. ',
    rvInfoComponent: {
      regelungsvorhaben: 'Regelungsvorhaben',
      kurzbezeichnung: 'Kurzbezeichnung',
      abkuerzung: 'Abkürzung',
      kurzbeschreibung: 'Kurzbeschreibung',
      vorhabentyp: 'Vorhabentyp',
      ffRessort: 'Federführendes Ressort',
      ffAbteilung: 'Federführende Abteilung',
      ffReferat: 'Federführendes Referat',
      einleiter: 'Einleitung',
      link: 'Zum aktuellen Datenblatt',
      vorhabenart: {
        GESETZ: 'Gesetz',
        RECHTSVERORDNUNG: 'Verordnung',
        VERWALTUNGSVORSCHRIFT: 'Vorschrift',
      },
    },
    fileUpload: {
      uploadErrorFileNameLength: 'Maximale Länge der Dateiname {{maxChars}} wurde überschritten',
      uploadChangeFileNameLabel: 'Neuer Dateiname',
      uploadChangeFileNameError: 'Bitte geben Sie einen Dateinamen ein.',
      regelungsentwurfUploadError: 'Bitte laden Sie einen Regelungsentwurf hoch.',
      uploadButtonRegelungsentwurf: 'Regelungsentwurf hochladen',
      uploadButtonAttachments: 'Anlagen hochladen',
      uploadHintRegelungsentwurf:
        'Ziehen Sie den Regelungsentwurf einfach in dieses Feld, um ihn hochzuladen, oder klicken Sie hinein, um den Auswahldialog zu öffnen. Eine Mehrfachauswahl ist nicht möglich.',
      uploadHintAttachments:
        'Ziehen Sie Ihre Anlagen einfach in dieses Feld, um sie hochzuladen, oder klicken Sie hinein, um den Auswahldialog zu öffnen. Eine Mehrfachauswahl ist möglich.',
      uploadChangeFileNameCancelTextButton: 'Abbrechen',
      uploadChangeFileNameModalTitel: 'Dateinamen ändern',
      uploadDraftTitle: 'Regelungsentwurf hochladen (PDF/Word-Dokument, max. 20 MB)',
      uploadDraftTitleDrawer: 'Regelungsentwurf hochladen',
      uploadDraftDrawerTooltip:
        'Hier kann nur ein Regelungsentwurf hochgeladen werden. Bitte wählen Sie eine Datei aus. Bei Bedarf können Sie die Bezeichnung der Datei über das Stift-Icon anpassen.',
      uploadedDraftTitle: 'Hochgeladener Regelungsentwurf',
      uploadDraftErrorTitle: 'Abschnitt "Regelungsentwurf hochladen"',
      uploadDraftFormatShort: 'DOCX (eNorm oder Word)',
      uploadAttachmentsTitle: 'Hochladen von Anlagen (PDF/Word-Dokumente, max. 20 MB)',
      uploadedAttachmentsTitle: 'Hochgeladene Anlagen',
      uploadAttachmentsErrorTitle: 'Abschnitt "Hochladen von Anlagen"',
      uploadAttachmentsFormatShort: 'DOCX (Word) oder PDF',
      uploadAttachmentsDeleteAllPopover: {
        title: 'Möchten Sie alle Anlagen löschen?',
        text: 'Wenn Sie die Anlagen löschen, werden sie hier entfernt.',
        delete: 'Löschen',
        cancel: 'Abbrechen',
      },
      uploadAttachmentsDeleteAll: 'Alle Anlagen entfernen',
      typeOfDocument: {
        label: 'Format des Dokuments',
        typeEnormDocument: 'eNorm-Dokument',
        typeEditorDocument: 'Editor-Dokument',
        error: 'Bitte geben Sie die Art des Dokuments an.',
        UnterabstimmungInfo:
          'Hinweis: Unterabstimmungen zu laufenden Abstimmungen können aktuell nur mit e-Norm Dokumenten durchgeführt werden. Ab dem nächsten Release im Oktober 2023 sind Unterabstimmungen zu laufenden Abstimmungen auch mit Editor-Dokumenten möglich.',
      },
      regelungsentwurf: {
        label: 'Regelungsentwurf (Editor-Dokumentenmappe)',
        placeholder: 'Bitte auswählen',
        hinweisTitle: 'Auswahl eines Regelungsentwurfs',
        hinweisContent:
          'Sie haben keinen Regelungsentwurf für dieses Regelungsvorhaben erstellt. Bitte wählen Sie ein anderes Regelungsvorhaben aus.',
        error: 'Bitte wählen Sie einen Regelungsentwurf aus.',
        errorNoRE:
          'Sie haben keinen Regelungsentwurf dür dieses Regelungsvorhaben erstellt. Bitte wählen Sie ein anderes Regelungsvorhaben aus.',
        editorDoc: {
          hinweisTitle: 'Ihre Zugriffsrechte',
          hinweisContent:
            'Mit Einleitung einer Abstimmung werden die Zugriffsrechte für das abzustimmende Dokument geändert. Die an der Abstimmung Teilnehmenden können das Dokument lesen. Der das Dokument Erstellende verliert zugleich die Möglichkeit das Dokument zu bearbeiten. Dadurch ist sichergestellt, dass nachvollziehbar bleibt, welcher Stand des Dokuments zu welchem Zeitpunkt abgestimmt wurde.',
        },
      },
      uploadErrorConfirm: 'Okay, gelesen',
      uploadError:
        'Dieses Dateiformat kann nicht verarbeitet werden. Bitte laden Sie die Datei im Format {{format}} erneut hoch. Achten Sie dabei auf die maximale Dateigröße von {{fileSize}} MB.',
      upload: {
        mainTitle: 'Dateien hochladen',
        delete: 'Datei löschen',
        rename: 'Datei umbenennen',
        uploadDrawerText:
          '<p>Mit Upload der Dokumente wird bestätigt, dass diese keine personenbezogenen Daten nach <a href="https://dejure.org/gesetze/DSGVO/4.html" target="_blank">Art. 4 Nr. 1 DSGVO</a> beinhalten, die personenbezogenen Daten anonymisiert wurden oder eine Einwilligung der betroffenen Person nach <a href="https://dejure.org/gesetze/DSGVO/6.html" target="_blank">Art. 6 Nr. 1 lit. a DSGVO</a> vorliegt.</p>',
        popover: {
          title: 'Möchten Sie das Dokument löschen?',
          text: 'Wenn Sie das hochgeladene Dokument „{{title}}“ löschen, wird es hier entfernt.',
          delete: 'Löschen',
          cancel: 'Abbrechen',
        },
      },
    },
    archivConfirm: {
      hinweis: 'Hinweis',
    },
    contactPerson: {
      keineAngabe: 'Keine Angabe',
      name: 'Name',
      anrede: 'Anrede',
      titel: 'Titel',
      funktion: 'Funktion',
      eMail: 'E-Mail',
      telefon: 'Telefon',
      mobil: 'Mobil',
      ressort: 'Ressort',
      abteilung: 'Abteilung',
      referat: 'Referat',
      adresse: 'Adresse',
    },
    dynamicCardComponent: {
      checkBtn: 'Prüfen und speichern',
      cancelBtn: 'Abbrechen',
      defaultImageLabel: 'Platzhalterbild',
      defaultImageAlt: 'Platzhalterbild: Grafik eines Laptops',
      editInput: 'Eingaben bearbeiten',
    },
  },
};
