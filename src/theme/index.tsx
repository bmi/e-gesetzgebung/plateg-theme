// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { ThemeConfig } from 'antd';

const platformBlue1 = '#0032d9';
const platformBlue2 = '#4065e2';
const platformBlue4 = '#e5eafb';
const platformRed1 = '#aa0000';
const textColor = '#000';
const linksGreyColor = '#909090';
const platformBtnPrimaryColor = '#fff';
const selectDisabledColor = '#f5f5f5';

export const plategThemeConfig: ThemeConfig = {
  token: {
    fontSize: 18,
    fontSizeSM: 18,
    fontFamily: 'IBM Plex Sans',
    fontSizeHeading1: 24,
    fontSizeHeading2: 20,
    colorPrimary: platformBlue1,
    colorBgLayout: platformBlue4,
    colorError: platformRed1,
    borderRadiusSM: 3,
    borderRadius: 3,
    colorText: textColor,
  },
  components: {
    Input: {
      colorBorder: platformBlue2,
      colorTextPlaceholder: textColor,
    },
    Select: {
      controlHeightLG: 48,
      colorBorder: platformBlue2,
      colorText: textColor,
      colorTextPlaceholder: textColor,
      fontSizeLG: 18,
      colorBgContainerDisabled: selectDisabledColor,
    },
    Typography: {
      fontSizeHeading1: 24,
      fontSizeHeading2: 20,
      fontWeightStrong: 500,
    },
    Button: {
      fontSize: 14,
      fontSizeLG: 16,
      colorPrimaryHover: textColor,
      colorBgContainer: 'transparent',
      colorBorder: platformBlue2,
      colorText: platformBlue1,
      colorLink: textColor,
    },
    Radio: {
      colorBorder: platformBlue1,
      colorPrimary: platformBtnPrimaryColor,
      colorWhite: textColor,
      colorTextDisabled: linksGreyColor,
    },
    Checkbox: {
      controlInteractiveSize: 20,
      colorBorder: platformBlue1,
      colorPrimary: textColor,
      colorPrimaryHover: textColor,
    },
    Tabs: {
      titleFontSize: 16,
    },
    TreeSelect: {
      colorText: textColor,
    },
    DatePicker: {
      colorTextPlaceholder: textColor,
    },
  },
};
