// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export class MenuCollapseController {
  private isCollapsed: boolean;
  private readonly setIsCollapsed: (fn: (newValue: boolean) => boolean) => void;
  private readonly eventHandler: () => void;
  private readonly nameClassSideTrigger = 'ant-layout-sider-trigger';
  private readonly navbarAffix = 'navbar-component';

  public constructor(isCollapsed: boolean, setIsCollapsed: (fn: (newValue: boolean) => boolean) => void) {
    this.isCollapsed = isCollapsed;
    this.setIsCollapsed = setIsCollapsed;
    this.eventHandler = this.updateButtonPosition.bind(this);
  }

  public onKeyEvent = (event: KeyboardEvent): void => {
    if (event.key === 'Enter') {
      this.isCollapsed = !this.isCollapsed;
      this.setIsCollapsed((prevState: boolean) => !prevState);
      setTimeout(() => {
        document.getElementById('collapseButton')?.focus();
      }, 100);
    }
  };

  public registerListener(): void {
    const triggerDiv = document.getElementsByClassName(this.nameClassSideTrigger)[0];
    if (triggerDiv) {
      triggerDiv.addEventListener('keydown', (event: Event) => this.onKeyEvent(event as KeyboardEvent));
    }
    window.addEventListener('resize', this.eventHandler);
    document.addEventListener('scroll', this.eventHandler);
    document.addEventListener('click', this.eventHandler);
    setTimeout(this.updateButtonPosition.bind(this), 400);
  }

  public removeListener(): void {
    window.removeEventListener('resize', this.eventHandler);
    document.removeEventListener('scroll', this.eventHandler);
    document.removeEventListener('click', this.eventHandler);
  }

  // Important attribute must be added each time the nav collapses, because the underlying span is newly rendered
  public configureCollapseButton(): void {
    setTimeout(() => {
      const triggerDiv = document.getElementsByClassName(this.nameClassSideTrigger)[0];
      const triggerSpan = triggerDiv?.firstElementChild as HTMLSpanElement | null;
      if (triggerSpan) {
        triggerSpan.tabIndex = 0;
        !this.isCollapsed
          ? triggerSpan.setAttribute('aria-label', 'Navigationsmenü schließen')
          : triggerSpan.setAttribute('aria-label', 'Navigationsmenü öffnen');
        triggerSpan.setAttribute('aria-expanded', (!this.isCollapsed).toString());
        triggerSpan.setAttribute('aria-haspopup', 'true');
        triggerSpan.id = 'collapseButton';
      }
    }, 50);
    setTimeout(this.updateButtonPosition.bind(this), 200);
  }

  // Adjusts the position of the collapse button in the nav, so that it is not covered by the footer in the cockpit
  public updateButtonPosition(): void {
    const footerPosition = document.getElementById('cockpitFooter')?.getBoundingClientRect() as DOMRect;
    const triggerDiv = document.getElementsByClassName(this.nameClassSideTrigger)[0] as HTMLDivElement | null;
    if (triggerDiv && footerPosition) {
      const bottomOffset = window.innerHeight - footerPosition.top;
      triggerDiv.style.marginBottom = `${Math.max(bottomOffset, 0)}px`;
      this.recalculateMenuHeight(footerPosition.top, triggerDiv.getBoundingClientRect().height);
    }
  }

  public recalculateMenuHeight(footerTop: number, buttonCloseHeight: number): void {
    const affixHolder = document.getElementById(this.navbarAffix) as HTMLDivElement;
    const screenWithoutFooter = Math.max(window.innerHeight - footerTop, 0);
    if (affixHolder) {
      const affixOffsetTop = affixHolder.getBoundingClientRect().top;
      const visibleArea = window.innerHeight - affixOffsetTop - buttonCloseHeight - screenWithoutFooter;
      affixHolder.style.height = `${visibleArea}px`;
    }
  }
}
