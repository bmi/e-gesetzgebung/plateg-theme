// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Observable, of } from 'rxjs';

import { UserControllerApi, UserEntityWithStellvertreterResponseDTO } from '@plateg/rest-api';

import { GlobalDI } from '../shares/injector';

export class UserController {
  private readonly userController = GlobalDI.getOrRegister<UserControllerApi>(
    'userController',
    () => new UserControllerApi(),
  );
  private route: string | undefined;
  private user: UserEntityWithStellvertreterResponseDTO | undefined;

  public getUser(stellvertretungActiveId?: string): Observable<UserEntityWithStellvertreterResponseDTO> {
    let obs: Observable<UserEntityWithStellvertreterResponseDTO>;
    const currentRoute = document.URL;
    if (!this.user || this.route !== currentRoute) {
      obs = stellvertretungActiveId
        ? this.userController.getUserWithStellvertreter({ userId: stellvertretungActiveId })
        : this.userController.getUser();
      obs.subscribe((data) => {
        this.user = data;
        this.route = currentRoute;
      });
    } else {
      obs = of(this.user);
    }
    return obs;
  }
}
