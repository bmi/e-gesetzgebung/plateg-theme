// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export class DrawerHeightController {
  private readonly eventHandler: () => void;

  public constructor() {
    this.eventHandler = this.updateDrawerHeight.bind(this);
  }

  public registerListener(): void {
    window.addEventListener('resize', this.eventHandler);
    document.addEventListener('scroll', this.eventHandler);
    setTimeout(this.updateDrawerHeight.bind(this), 200);
  }

  public removeListener(): void {
    window.addEventListener('resize', this.eventHandler);
    document.removeEventListener('scroll', this.eventHandler);
  }

  public calculateDrawerHeightAndOffset(
    footerPositionTop: number,
    headerPositionBottom: number,
    headerPositionTop: number,
    windowHeight: number,
  ): { topOffset: number; drawerHeight: number } {
    const bottomOffset = Math.max(windowHeight - footerPositionTop, 0);
    const drawerHeight = windowHeight - bottomOffset - headerPositionBottom;
    const topOffset = headerPositionBottom - headerPositionTop;
    return { drawerHeight, topOffset };
  }

  // Adjusts the height of the drawer, so that it is not overlapped by the footer
  public updateDrawerHeight(): void {
    const footerPosition = document.getElementById('cockpitFooter')?.getBoundingClientRect() as DOMRect;
    const header = document.getElementsByClassName('header-wrapper')[0] as HTMLDivElement | null;
    const headerPosition = header?.getBoundingClientRect() as DOMRect;
    if (footerPosition && headerPosition) {
      const calculatedDrawerHeightAndOffset = this.calculateDrawerHeightAndOffset(
        footerPosition.top,
        headerPosition.bottom,
        headerPosition.top,
        window.innerHeight,
      );
      if (calculatedDrawerHeightAndOffset.drawerHeight && calculatedDrawerHeightAndOffset.topOffset) {
        const drawerDivs = document.getElementsByClassName('ant-drawer');
        // there may be more than one drawer. Since they are indistingushable, we have to set the correct height for all
        for (const drawerDiv of drawerDivs as HTMLCollectionOf<HTMLDivElement>) {
          drawerDiv.style.marginTop = `${calculatedDrawerHeightAndOffset.topOffset}px`;
          drawerDiv.style.height = `${calculatedDrawerHeightAndOffset.drawerHeight}px`;
        }
      }
    }
  }
}
