// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';

import { LoadingStatusController } from './loadingStatusController';

const loadingStatusController = new LoadingStatusController();

describe('Test for LoadingStatusController', () => {
  it('can initialize loading status', () => {
    expect(() => loadingStatusController.initLoadingStatus()).to.not.throw();
  });

  it('can subscribe to loading status, status should be false', () => {
    let status = true;
    expect(() =>
      loadingStatusController.subscribeLoadingStatus().subscribe({
        next: (data) => {
          status = data;
        },
        complete: () => {
          expect(status).to.eql(false);
        },
      }),
    ).to.not.throw();
  });

  it('can set loading status, status should be false on direct check', () => {
    let status = false;
    loadingStatusController.setLoadingStatus(true);
    loadingStatusController.subscribeLoadingStatus().subscribe({
      next: (data) => {
        status = data;
      },
      complete: () => {
        expect(status).to.eql(false);
      },
    });
  });
  it('can set loading status, status should be true after 300ms', () => {
    let status = false;
    loadingStatusController.setLoadingStatus(true);
    setTimeout(() => {
      loadingStatusController.subscribeLoadingStatus().subscribe({
        next: (data) => {
          status = data;
        },
        complete: () => {
          expect(status).to.eql(true);
        },
      });
    }, 300);
  });
});
