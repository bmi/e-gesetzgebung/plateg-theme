// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Observable, Subject, of } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

import { AbteilungDTO, CodelistenControllerApi, CodelistenResponseDTO, ReferatDTO, RessortDTO } from '@plateg/rest-api';

import { GlobalDI } from '../shares/injector';

export class CodeListenController {
  private codeListen: CodelistenResponseDTO | undefined;
  private readonly codelistenControllerApi = GlobalDI.get<CodelistenControllerApi>('codelistenControllerApi');
  private subject = new Subject();

  public getCodeListen(): Observable<CodelistenResponseDTO> {
    let obs: Observable<CodelistenResponseDTO>;
    if (!this.codeListen) {
      obs = this.codelistenControllerApi.getCodelisten();
      obs.pipe(shareReplay(1));
      obs.subscribe((data) => {
        this.codeListen = data;
        this.subject.next(data);
      });
      obs = this.subject.asObservable() as Observable<CodelistenResponseDTO>;
    } else {
      obs = of(this.codeListen);
    }
    return obs;
  }

  public getRessortName(codeList?: CodelistenResponseDTO, id?: string | RessortDTO): string | undefined {
    if (!codeList || !id) {
      return undefined;
    }
    let names = '';
    codeList?.ressorts.forEach((ressort) => {
      if (id === ressort.id) {
        names += ressort.bezeichnung;
      }
    });
    return names.length > 0 ? names : undefined;
  }

  public getAbteilungName(codeList?: CodelistenResponseDTO, id?: string | AbteilungDTO): string | undefined {
    if (!codeList || !id) {
      return undefined;
    }
    let names = '';
    codeList?.ressorts?.forEach((ressort) => {
      ressort.abteilungen.forEach((abteilung) => {
        if (id === abteilung.id) {
          names += abteilung.bezeichnung;
        }
      });
    });
    return names.length > 0 ? names : undefined;
  }

  public getReferatName(codeList?: CodelistenResponseDTO, id?: string | ReferatDTO): string | undefined {
    if (!codeList || !id) {
      return undefined;
    }
    let names = '';
    codeList?.ressorts?.forEach((ressort) => {
      ressort.abteilungen.forEach((abteilung) => {
        const abteilungsReferate = abteilung.referate;
        const unterabteilungsReferate = abteilung.unterabteilungen?.flatMap(
          (unterabteilung) => unterabteilung.referate,
        );
        abteilungsReferate.concat(unterabteilungsReferate).forEach((referat) => {
          if (id === referat.id) {
            names += referat.bezeichnung;
          }
        });
      });
    });
    return names.length > 0 ? names : undefined;
  }
}
