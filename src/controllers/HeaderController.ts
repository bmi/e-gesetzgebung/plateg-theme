// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { HeaderContent } from '../components/header/component.react';
type HeaderContentSetterFunctionType = (headerContent: HeaderContent) => void;

export class HeaderController {
  private headerContent: HeaderContent = {};
  private headerContentStateSetter?: HeaderContentSetterFunctionType;
  public setHeaderContentState(setter: HeaderContentSetterFunctionType): void {
    this.headerContentStateSetter = setter;
  }
  public setHeaderProps(props: HeaderContent): void {
    const newProps = { ...this.headerContent, ...props };
    this.headerContent = newProps;
    this.headerContentStateSetter?.(newProps);
  }
  public getHeaderContent = (): HeaderContent => {
    return this.headerContent;
  };
}
