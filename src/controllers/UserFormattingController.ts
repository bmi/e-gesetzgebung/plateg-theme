// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Configuration, UserControllerApi, UserEntityShortDTO } from '@plateg/rest-api';

import { GlobalDI } from '../shares';

export class UserFormattingController {
  private readonly userController = GlobalDI.getOrRegister(
    'userController',
    () => new UserControllerApi(new Configuration()),
  );

  public getUsersByEmailCall(emails: string[]): Observable<string> {
    const tmp = this.userController.getUserShortListByEmails({ emails });
    return tmp.pipe(
      map((data) => {
        return this.formatUsers(data.dtos);
      }),
    );
  }

  public formatUsers(users: UserEntityShortDTO[]): string {
    return users
      .map((item) => {
        const name = item.name || item.email;
        const ressort = item.ressortKurzbezeichnung ?? null;
        const abteilungString = item.abteilung ? `, ${item.abteilung}` : '';
        const fachreferatString = item.abteilung && item.fachreferat ? `, ${item.fachreferat}` : '';
        const ressortString = ressort ? ` (${ressort}${abteilungString}${fachreferatString})` : '';
        return `${name}${ressortString}`;
      })
      .join('; ');
  }
}
