// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { AjaxConfig, AjaxResponse } from 'rxjs/ajax';

import { Configuration, Middleware } from '@plateg/rest-api';

import { RootState, store } from '../components/store';
import { LastRestApiCallController } from './lastRestApiCallController';

const lastRestApiCallController = new LastRestApiCallController();

export function configureRestApi(loadApp: (configRestCalls: Configuration) => void): void {
  const configRestCalls = new Configuration({ middleware: getMiddleware() });
  loadApp(configRestCalls);
}

export function getMiddleware(): Middleware[] {
  return [
    {
      pre: (request: AjaxConfig) => {
        const user = (store.getState() as RootState).user.user;
        const crsfToken = getCrsfToken();
        request.headers = {
          ...request.headers,
          'X-XSRF-TOKEN-PLATEG': crsfToken,
          Authorization: 'Bearer ' + getJWToken(),
          'Stellvertretung-Is-Active': user?.dto.stellvertreter || false,
          'Stellvertretung-Active-Id': user?.dto.stellvertreter ? user?.base.id : null,
          'Stellvertretung-Logged-In-User-Id': user?.dto.stellvertreter ? user?.dto.stelleninhaber?.base.id : null,
        };
        request.withCredentials = true;
        return request;
      },
      post: (response: AjaxResponse<any>): AjaxResponse<any> => {
        lastRestApiCallController.setLastRestApiCall(new Date().toISOString());
        return response;
      },
    },
  ];
}

function getCrsfToken(): string {
  return ('; ' + document.cookie).split('; XSRF-TOKEN-PLATEG=').pop()?.split(';').shift() ?? '';
}
function getJWToken(): string {
  if (process.env.NODE_ENV === 'test') return '';
  return (store.getState() as RootState).kcToken.kcToken?.token || '';
}
/**
 * Opens a file from the provided URL, allowing for customization of behavior.
 *
 * @param fileUrl - The URL of the file to open.
 * @param options - Additional options to customize the opening of the file.
 *   @property {string} target - The target window or tab in which to open the file. Default is '_self'.
 *   @property {string} fileName - The desired filename for the downloaded file. If provided, triggers a download with the specified filename.
 *   @property {() => void} onStart - A callback function called when the file retrieval process starts.
 *   @property {() => void} onSuccess - A callback function called when the file is successfully retrieved and ready for opening.
 *   @property {(error?: Error) => void} onError - A callback function called if there is an error during the file retrieval process.
 */
export function openFile(
  fileUrl: string,
  options?: {
    target?: '_self' | '_blank';
    fileName?: string;
    onStart?: () => void;
    onSuccess?: () => void;
    onError?: (error?: Error) => void;
  },
): void {
  const user = (store.getState() as RootState).user.user;
  const headers = new Headers();
  headers.append('Authorization', `Bearer ${getJWToken()}`);
  headers.append('Stellvertretung-Is-Active', user?.dto.stellvertreter ? 'true' : 'false');
  headers.append('Stellvertretung-Active-Id', user?.dto.stellvertreter ? user?.base.id : 'null');
  headers.append(
    'Stellvertretung-Logged-In-User-Id',
    user?.dto.stellvertreter ? (user?.dto.stelleninhaber?.base.id as string) : 'null',
  );

  options?.onStart?.();
  fetch(fileUrl, {
    method: 'GET',
    headers: headers,
  })
    .then((response) => response.blob())
    .then((fileBlob) => {
      options?.onSuccess?.();
      const blobUrl = URL.createObjectURL(fileBlob);
      if (options?.fileName) {
        const a = document.createElement('a');
        a.target = options?.target ?? '_self';
        a.href = blobUrl;
        a.download = options?.fileName;
        a.click();
      } else {
        window.open(blobUrl + '', options?.target ?? '_self');
      }
    })
    .catch((error) => {
      options?.onError?.(error as Error);
    });
}
