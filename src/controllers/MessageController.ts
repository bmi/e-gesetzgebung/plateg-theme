// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { displayMessage } from '../components/messages/message-service';

export class MessageController {
  public displayMessage = (msg: string, type: string): void => {
    displayMessage(msg, type);
  };
}
