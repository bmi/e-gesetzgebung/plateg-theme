// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect, use } from 'chai';
import * as chaiArrays from 'chai-arrays';
import { of } from 'rxjs';
import sinon from 'sinon';

import { ArbeitshilfenControllerApi, Configuration } from '@plateg/rest-api';
import { WorkHelpEntry, WorkHelpEntryFormatEnum } from '@plateg/rest-api/models';

import { GlobalDI } from '../shares/injector';
import { WorkingAidsController } from './WorkingAidsController';

use(chaiArrays.default);

describe('Test getArbeitshilfenListCall', () => {
  const ctrl = new WorkingAidsController();
  const arbeitshilfenController = GlobalDI.getOrRegister(
    'arbeitshilfenController',
    () => new ArbeitshilfenControllerApi(new Configuration()),
  );
  it('getWorkHelpEntries gets called', () => {
    const returnedObs = of([] as Array<WorkHelpEntry>);
    const getWorkHelpEntriesStub = sinon.stub(arbeitshilfenController, 'getWorkHelpEntries').returns(returnedObs);
    const resp1 = ctrl.getArbeitshilfenListCall();
    const resp2 = ctrl.getArbeitshilfenListCall();
    resp1.subscribe((data: Array<WorkHelpEntry>) => {
      sinon.assert.match(data, []);
    });
    resp2.subscribe((data: Array<WorkHelpEntry>) => {
      sinon.assert.match(data, []);
    });
    sinon.assert.calledOnce(getWorkHelpEntriesStub);
    getWorkHelpEntriesStub.restore();
  });
});

describe('Test for searchEntry which should return boolean depending on if the search term is present in an entry', () => {
  const ctrl = new WorkingAidsController();
  const entry: WorkHelpEntry = {
    title: 'Arbeitshilfe1',
    description: 'Dies ist die Arbeitshilfe1',
    format: WorkHelpEntryFormatEnum.Pdf,
    url: 'http://www.exampleWorkEntry.de/Arbeitshilfe1.pdf',
    category: { name: 'info' },
    subjects: [{ name: 'subject1' }],
    publishers: [{ name: 'Arbeitshilfen Tester', shortName: 'ATester' }],
  };
  it('search term is present - full match title', () => {
    expect(ctrl.searchEntry('Arbeitshilfe1', entry)).to.eql(true);
  });
  it('search term is present - full match title case insensitive', () => {
    expect(ctrl.searchEntry('arbeitshilfe1', entry)).to.eql(true);
  });
  it('search term is present - partial match title', () => {
    expect(ctrl.searchEntry('Arbeitshi', entry)).to.eql(true);
  });
  it('search term is present - partial match title case insensitive', () => {
    expect(ctrl.searchEntry('arbeitshi', entry)).to.eql(true);
  });
  it('search term is present - full match description', () => {
    expect(ctrl.searchEntry('Dies ist die Arbeitshilfe1', entry)).to.eql(true);
  });
  it('search term is present - full match description case insensitive', () => {
    expect(ctrl.searchEntry('dies ist die arbeitshilfe1', entry)).to.eql(true);
  });
  it('search term is present - partial match description', () => {
    expect(ctrl.searchEntry('Dies ist die Arb', entry)).to.eql(true);
  });
  it('search term is present - partial match description case insensitive', () => {
    expect(ctrl.searchEntry('dies ist die arb', entry)).to.eql(true);
  });
  it('search term not present', () => {
    expect(ctrl.searchEntry('Xhdwejnkd', entry)).to.eql(false);
  });
});
