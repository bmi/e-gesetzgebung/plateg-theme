// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Observable, Subject } from 'rxjs';

const subject = new Subject();

export class LastRestApiCallController {
  private currentDate = new Date().toISOString();
  public setLastRestApiCall(date: string): void {
    this.currentDate = date;
    subject.next(this.currentDate);
  }
  public subscribeLastRestApiCall(): Observable<string> {
    return subject.asObservable() as Observable<string>;
  }
}
