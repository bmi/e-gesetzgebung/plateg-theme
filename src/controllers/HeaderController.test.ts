// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import React from 'react';
import sinon from 'sinon';

import { HeaderController } from './HeaderController';
const headerCtrl = new HeaderController();

describe(`Test: HeaderController -> setHeaderProps`, () => {
  it('can set headerLeft - span', () => {
    headerCtrl.setHeaderProps({ headerLeft: [React.createElement('span', 'TEST')] });
    const content = headerCtrl.getHeaderContent().headerLeft ?? [];
    expect(content[0].type).to.eql('span');
  });
  it('can set headerCenter - button', () => {
    headerCtrl.setHeaderProps({ headerCenter: [React.createElement('button', 'TEST')] });
    const content = headerCtrl.getHeaderContent().headerCenter ?? [];
    expect(content[0].type).to.eql('button');
  });
  it('can set headerRight - a', () => {
    headerCtrl.setHeaderProps({ headerRight: [React.createElement('a', 'TEST')] });
    const content = headerCtrl.getHeaderContent().headerRight ?? [];
    expect(content[0].type).to.eql('a');
  });
  it('can set headerLast - title', () => {
    headerCtrl.setHeaderProps({ headerLast: [React.createElement('title', 'TEST')] });
    const content = headerCtrl.getHeaderContent().headerLast ?? [];
    expect(content[0].type).to.eql('title');
  });

  it('header props are persistent', () => {
    const content = headerCtrl.getHeaderContent();
    expect(content.headerLeft ? content.headerLeft[0].type : undefined).to.eql('span');
    expect(content.headerCenter ? content.headerCenter[0].type : undefined).to.eql('button');
    expect(content.headerRight ? content.headerRight[0].type : undefined).to.eql('a');
    expect(content.headerLast ? content.headerLast[0].type : undefined).to.eql('title');
  });

  it('can reset header props', () => {
    headerCtrl.setHeaderProps({
      headerLeft: undefined,
      headerCenter: undefined,
      headerRight: undefined,
      headerLast: undefined,
    });
    const content = headerCtrl.getHeaderContent();
    expect(content.headerLeft).undefined;
    expect(content.headerCenter).undefined;
    expect(content.headerRight).undefined;
    expect(content.headerLast).undefined;
  });
});
