// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { should } from 'chai';
import { of } from 'rxjs';
import sinon from 'sinon';

import { CodelistenControllerApi, CodelistenResponseDTO, Configuration } from '@plateg/rest-api';

import { GlobalDI } from '../shares';
import { CodeListenController } from './CodeListenController';

describe(`Test: GetCodeListen`, () => {
  const codelistenControllerApi = GlobalDI.getOrRegister(
    'codelistenControllerApi',
    () => new CodelistenControllerApi(new Configuration()),
  );
  const codelisteCtrl = new CodeListenController();
  it(`Load Modules testen`, () => {
    const codelistenResponse: CodelistenResponseDTO = { ressorts: [], codelisten: [] };
    const returnedObs = of(codelistenResponse);
    const rvControllerStub = sinon.stub(codelistenControllerApi, 'getCodelisten').returns(returnedObs);
    const resp = codelisteCtrl.getCodeListen();
    const resp2 = codelisteCtrl.getCodeListen();

    resp.subscribe((data: CodelistenResponseDTO) => {
      sinon.assert.match(data.ressorts, []);
      sinon.assert.match(data.codelisten, []);
    });

    resp2.subscribe((data: CodelistenResponseDTO) => {
      sinon.assert.match(data.ressorts, []);
      sinon.assert.match(data.codelisten, []);
    });
    sinon.assert.calledOnce(rvControllerStub);
    rvControllerStub.restore();
  });

  const codeliste: CodelistenResponseDTO = {
    codelisten: [],
    ressorts: [
      {
        id: '0',
        kurzbezeichnung: 'BMVg',
        bezeichnung: 'Bundesministerium der Verteidigung',
        aktiv: true,
        abteilungen: [],
      },
      {
        id: '1',
        kurzbezeichnung: 'BMI',
        bezeichnung: 'Bundesministerium des Innern und für Heimat',
        aktiv: true,
        abteilungen: [
          {
            id: '7ceba0c6-d693-4d70-8862-7c0cc29032e8',
            bezeichnung: 'D',
            unterabteilungen: [],
            referate: [],
          },
          {
            id: '161af421-516c-4d12-bc6f-2ea14ebf5b35',
            bezeichnung: 'G',
            unterabteilungen: [],
            referate: [],
          },
          {
            id: '14a67ec6-7077-4366-8e40-a58db2907274',
            bezeichnung: 'Z',
            unterabteilungen: [
              {
                id: '8d3dccdb-4003-4880-bdf5-0a80b108135d',
                bezeichnung: 'Z II',
                unterabteilungen: [],
                referate: [
                  {
                    id: '0d488328-8009-4ae1-8a89-aaca9505e08d',
                    bezeichnung: 'Z II 1',
                  },
                  {
                    id: '3d6ce95a-0af4-414c-aef4-69b9d36378fd',
                    bezeichnung: 'Z II 2',
                  },
                ],
              },
              {
                id: 'be04e225-bd50-44d1-b107-7a8b92598ecc',
                bezeichnung: 'Z I',
                unterabteilungen: [],
                referate: [],
              },
              {
                id: 'e0a8e60d-4405-4064-a80c-e284df3793b2',
                bezeichnung: 'Z III',
                unterabteilungen: [],
                referate: [],
              },
            ],
            referate: [],
          },
        ],
      },
      {
        id: '2',
        kurzbezeichnung: 'BKAmt',
        bezeichnung: 'Bundeskanzleramt',
        aktiv: true,
        abteilungen: [
          {
            id: 'cf94e3c3-e526-4eed-8108-31a0d7414a21',
            bezeichnung: '3',
            unterabteilungen: [
              {
                id: '04191bfd-ac49-4d91-adc7-4deeb62d3fc6',
                bezeichnung: '32',
                unterabteilungen: [],
                referate: [],
              },
              {
                id: '9fcbd2aa-ad66-44e7-8f2f-c69ec5dac815',
                bezeichnung: '31',
                unterabteilungen: [],
                referate: [],
              },
            ],
            referate: [
              {
                id: '06768090-99ab-47e0-8343-2f4027516b3a',
                bezeichnung: '33',
              },
            ],
          },
          {
            id: '00ac7d67-8d87-4cc0-a0d3-714d1f05a891',
            bezeichnung: '1',
            unterabteilungen: [
              {
                id: '666c0dd3-d1c7-4c41-ae74-6814174082c4',
                bezeichnung: '11',
                unterabteilungen: [],
                referate: [],
              },
            ],
            referate: [],
          },
          {
            id: 'db108d4c-1ef0-49a7-af63-e51fa694c163',
            bezeichnung: '2',
            unterabteilungen: [
              {
                id: '552c719b-44a4-4775-8f8f-c6a17268c0b0',
                bezeichnung: '21',
                unterabteilungen: [],
                referate: [
                  {
                    id: '7328f67d-7717-4eaf-b786-9de19ec4aebc',
                    bezeichnung: '211',
                  },
                  {
                    id: 'bdbe4c4b-dcca-49af-a04b-f7ed835d1df5',
                    bezeichnung: '212',
                  },
                ],
              },
              {
                id: 'd8bbf4d3-3b5e-4d75-8d63-48d973e65e20',
                bezeichnung: '22',
                unterabteilungen: [],
                referate: [
                  {
                    id: '8a937d24-a8c9-46b6-a5c2-7089957c7594',
                    bezeichnung: '221',
                  },
                  {
                    id: '3e74c80b-2859-487d-9cbf-20cb45d1dd73',
                    bezeichnung: '222',
                  },
                ],
              },
            ],
            referate: [],
          },
        ],
      },
    ],
  };

  describe('TEST getRessortNames', () => {
    it('getRessortNames returns correct name, ids exist', () => {
      const result = codelisteCtrl.getRessortName(codeliste, '0');
      should().equal(result, 'Bundesministerium der Verteidigung');
    });
    it('getRessortNames returns correct name, one id exists', () => {
      const result = codelisteCtrl.getRessortName(codeliste, '0');
      should().equal(result, 'Bundesministerium der Verteidigung');
    });
    it('getRessortNames returns undefined, no id exists', () => {
      const result = codelisteCtrl.getRessortName(codeliste, '9');
      should().equal(result, undefined);
    });
    it('getRessortNames returns undefined, no codeList passed in', () => {
      const result = codelisteCtrl.getRessortName(undefined, '0');
      should().equal(result, undefined);
    });
    it('getRessortNames returns undefined, no ids passed in', () => {
      const result = codelisteCtrl.getRessortName(codeliste);
      should().equal(result, undefined);
    });
    it('getRessortNames returns undefined, nothing passed in', () => {
      const result = codelisteCtrl.getRessortName();
      should().equal(result, undefined);
    });
  });

  describe('TEST getAbteilungenNames', () => {
    it('getAbteilungenNames returns correct names, id exists', () => {
      const result = codelisteCtrl.getAbteilungName(codeliste, '7ceba0c6-d693-4d70-8862-7c0cc29032e8');
      should().equal(result, 'D');
    });
    it('getAbteilungenNames returns correct names, no id exists', () => {
      const result = codelisteCtrl.getAbteilungName(codeliste, '4');
      should().equal(result, undefined);
    });

    it('getAbteilungenNames returns undefined, no codeList passed in', () => {
      const result = codelisteCtrl.getAbteilungName(undefined, '0');
      should().equal(result, undefined);
    });
    it('getAbteilungenNames returns undefined, no id passed in', () => {
      const result = codelisteCtrl.getAbteilungName(codeliste);
      should().equal(result, undefined);
    });
    it('getAbteilungenNames returns undefined, nothing passed in', () => {
      const result = codelisteCtrl.getAbteilungName();
      should().equal(result, undefined);
    });
  });

  describe('TEST getReferatNames', () => {
    it('getReferatNames returns correct referat names, id exist, referate [] is empty', () => {
      const result = codelisteCtrl.getReferatName(codeliste, '0');
      should().equal(result, undefined);
    });
    it('getReferatNames returns correct names, one id exists', () => {
      const result = codelisteCtrl.getReferatName(codeliste, '06768090-99ab-47e0-8343-2f4027516b3a');
      should().equal(result, '33');
    });
    it('getReferatNames returns correct names, one id exists in unterabteilung', () => {
      const result = codelisteCtrl.getReferatName(codeliste, '7328f67d-7717-4eaf-b786-9de19ec4aebc');
      should().equal(result, '211');
    });
    it('getReferatNames returns undefined, no id exists', () => {
      const result = codelisteCtrl.getReferatName(codeliste, '9');
      should().equal(result, undefined);
    });
    it('getReferatNames returns undefined, no codeList passed in', () => {
      const result = codelisteCtrl.getReferatName(undefined, '3');
      should().equal(result, undefined);
    });
    it('getReferatNames returns undefined, no id passed in', () => {
      const result = codelisteCtrl.getReferatName(codeliste);
      should().equal(result, undefined);
    });
    it('getReferatNames returns undefined, nothing passed in', () => {
      const result = codelisteCtrl.getReferatName();
      should().equal(result, undefined);
    });
  });
});
