// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import React, { SetStateAction } from 'react';
import ReactTestUtils from 'react-dom/test-utils';
import sinon from 'sinon';

import { FocusController } from './FocusController';
const ref1 = React.createRef<HTMLAnchorElement>();
const btn1 = React.createElement('button', { ref: ref1, id: 'ref1' });
const ref2 = React.createRef<HTMLAnchorElement>();
const btn2 = React.createElement('button', { ref: ref2, id: 'ref2' });
const ref3 = React.createRef<HTMLAnchorElement>();
const btn3 = React.createElement('button', { ref: ref3, id: 'ref3' });
const refs = [ref1, ref2, ref3];

let isVisible = true;
const setIsVisible = (value: SetStateAction<boolean>) => {
  isVisible = !isVisible;
};

const focusController = new FocusController(setIsVisible, refs, refs[0]);

ReactTestUtils.renderIntoDocument(btn1);
ReactTestUtils.renderIntoDocument(btn2);
ReactTestUtils.renderIntoDocument(btn3);

const ref1FocusStub = sinon.stub(ref1.current, 'focus' as never);
const ref2FocusStub = sinon.stub(ref2.current, 'focus' as never);
const ref3FocusStub = sinon.stub(ref3.current, 'focus' as never);

const resetStubs = () => {
  ref1FocusStub.reset();
  ref2FocusStub.reset();
  ref3FocusStub.reset();
};

describe('Test FocusController > setFocus', () => {
  afterEach(() => resetStubs());
  it('focussing on arrowDown works', () => {
    const event = new KeyboardEvent('keydown', { key: 'ArrowDown' });
    focusController.setFocus(event as unknown as React.KeyboardEvent);
    focusController.setFocus(event as unknown as React.KeyboardEvent);
    focusController.setFocus(event as unknown as React.KeyboardEvent);
    focusController.setFocus(event as unknown as React.KeyboardEvent);
    sinon.assert.calledTwice(ref1FocusStub);
    sinon.assert.calledOnce(ref2FocusStub);
    sinon.assert.calledOnce(ref3FocusStub);
  });

  it('focussing on arrowUp works', () => {
    const event = new KeyboardEvent('keydown', { key: 'ArrowUp' });
    focusController.setFocus(event as unknown as React.KeyboardEvent);
    focusController.setFocus(event as unknown as React.KeyboardEvent);
    focusController.setFocus(event as unknown as React.KeyboardEvent);
    focusController.setFocus(event as unknown as React.KeyboardEvent);
    sinon.assert.calledOnce(ref1FocusStub);
    sinon.assert.calledOnce(ref2FocusStub);
    sinon.assert.calledTwice(ref3FocusStub);
  });

  it('close on escape works', () => {
    const event = new KeyboardEvent('keydown', { key: 'Escape' });
    focusController.setFocus(event as unknown as React.KeyboardEvent);
    expect(isVisible).false;
    setIsVisible(true);
  });
});
