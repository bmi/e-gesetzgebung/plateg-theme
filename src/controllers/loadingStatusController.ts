// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Observable, Subject } from 'rxjs';

const subject = new Subject<boolean>();

export class LoadingStatusController {
  private currentStatus = false;
  private delayTimeout: NodeJS.Timeout | null = null;

  public initLoadingStatus(): void {
    return subject.next(this.currentStatus);
  }

  public setLoadingStatus(status: boolean): void {
    if (this.delayTimeout) {
      clearTimeout(this.delayTimeout);
    }

    if (status) {
      // If loading, delay the status emission by 300ms
      this.delayTimeout = setTimeout(() => {
        this.currentStatus = status;
        subject.next(this.currentStatus);
        this.delayTimeout = null;
      }, 300);
    } else {
      // If not loading, update immediately
      this.currentStatus = status;
      subject.next(this.currentStatus);
    }
  }

  public subscribeLoadingStatus(): Observable<boolean> {
    return subject.asObservable();
  }
}
