// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { assert, expect } from 'chai';
import { of } from 'rxjs';
import sinon from 'sinon';

import { Configuration, UserControllerApi, UserEntityShortDTO } from '@plateg/rest-api';

import { GlobalDI } from '../shares';
import { UserFormattingController } from './UserFormattingController';

describe(`Test: UserFormattingController`, () => {
  const userList = {
    dtos: [
      { name: 'Gerd Müller', ressortKurzbezeichnung: 'M', abteilung: 'O', fachreferat: 'P' },
      { email: 'abc@xyz.de' },
    ] as UserEntityShortDTO[],
  };
  const userListResponse = of(userList);
  const userFormattingCtrl = new UserFormattingController();
  const userController = GlobalDI.getOrRegister<UserControllerApi>(
    'userController',
    () => new UserControllerApi(new Configuration()),
  );
  describe(`Test: getUsersByEmailCall`, () => {
    let userControllerStub: sinon.SinonStub;
    let formatterSpy: sinon.SinonSpy;
    before(() => {
      userControllerStub = sinon.stub(userController, 'getUserShortListByEmails').returns(userListResponse);
      formatterSpy = sinon.spy(userFormattingCtrl, 'formatUsers');
    });
    after(() => {
      userControllerStub.restore();
      formatterSpy.restore();
    });
    it(`Testing correct call of formatter`, (done) => {
      userFormattingCtrl.getUsersByEmailCall(['a', 'b']).subscribe(() => {
        assert(formatterSpy.calledWith(userList.dtos));
        done();
      });
    });
  });

  describe(`Test: formatUsers`, () => {
    it(`Testing correct formatting`, () => {
      expect(userFormattingCtrl.formatUsers(userList?.dtos)).equals('Gerd Müller (M, O, P); abc@xyz.de');
    });
  });
});
