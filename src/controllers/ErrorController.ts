// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';
import { AjaxError } from 'rxjs/ajax';

import { displayMessage } from '../components/messages/message-service';
export class ErrorController {
  public displayErrorMsg = (error: AjaxError, msg: string): void => {
    const status = (error.response as { status: number })?.status;
    displayMessage(
      i18n.t(msg, {
        fehlercode: status && status >= 1000 ? `(${status})` : '',
      }),
      'error',
    );
  };
}
