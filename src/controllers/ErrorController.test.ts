// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';
import { AjaxError } from 'rxjs/ajax';
import sinon from 'sinon';

import { ErrorController } from './';
describe(`Test: ErrorController`, () => {
  describe(`Test: displayErrorMsg`, () => {
    let i18nStub: sinon.SinonStub;
    const ctrl = new ErrorController();

    before(() => {
      i18nStub = sinon.stub(i18n, 't').callsFake(() => {
        return 'Test';
      });
    });
    after(() => {
      i18nStub.restore();
    });
    afterEach(() => {
      i18nStub.resetHistory();
    });
    it(`status === 2000, i18n.t is called with correct fehlercode`, () => {
      const error = { response: { status: 2000 } };
      ctrl.displayErrorMsg(error as AjaxError, 'test1');
      sinon.assert.calledOnce(i18nStub);
      sinon.assert.calledWith(i18nStub, 'test1', { fehlercode: '(2000)' });
    });
    it(`status === 100, i18n.t is called with empty fehlercode`, () => {
      const error = { response: { status: 100 } };
      ctrl.displayErrorMsg(error as AjaxError, 'test2');
      sinon.assert.calledOnce(i18nStub);
      sinon.assert.calledWith(i18nStub, 'test2', { fehlercode: '' });
    });
  });
});
