// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';

import { LastRestApiCallController } from './lastRestApiCallController';

const lastRestApiCallController = new LastRestApiCallController();

describe('Test for LastRestApiCallController', () => {
  it('can set last user activity time', () => {
    let status = '';
    const testData = new Date('01-02-2023').toISOString();
    lastRestApiCallController.setLastRestApiCall(testData);
    expect(() =>
      lastRestApiCallController.subscribeLastRestApiCall().subscribe({
        next: (data) => {
          status = data;
        },
        complete: () => {
          expect(status).to.eql(testData);
        },
      }),
    ).to.not.throw();
  });
});
