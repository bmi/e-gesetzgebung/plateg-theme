// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export class AriaController {
  public static setAriaLabelsByClassName = (className: string, ariaLabelContent: string): void => {
    this.setAriaAttrByClassName(className, 'label', ariaLabelContent);
  };

  public static setAriaLabelsByQuery = (query: string, ariaLabelContent: string): void => {
    Array.from(document.querySelectorAll(query)).forEach((element) => {
      element.setAttribute(`aria-label`, ariaLabelContent);
    });
  };

  public static setAriaAttrByClassName = (className: string, ariaAttrName: string, ariaContent: string): void => {
    Array.from(document.getElementsByClassName(className)).forEach((element) => {
      element.setAttribute(`aria-${ariaAttrName}`, ariaContent);
    });
  };

  public static setAttrByClassName = (className: string, attrType: string, attrValue: string): void => {
    Array.from(document.getElementsByClassName(className)).forEach((element) => {
      element.setAttribute(attrType, attrValue);
    });
  };

  public static removeAriaAttrByClassName = (className: string, ariaAttrName: string): void => {
    Array.from(document.getElementsByClassName(className)).forEach((element) => {
      element.removeAttribute(ariaAttrName);
    });
  };

  public static setElementIdByClassName = (className: string, idValue: string): void => {
    Array.from(document.getElementsByClassName(className)).forEach((element, index) => {
      element.setAttribute('Id', `theme-generic-${idValue}-${index}`);
    });
  };
}
