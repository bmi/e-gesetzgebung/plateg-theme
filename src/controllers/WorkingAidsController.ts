// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { UploadFile } from 'antd/lib/upload/interface';
import { Observable, of } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

import { ArbeitshilfenControllerApi, Configuration } from '@plateg/rest-api';
import { WorkHelpEntry } from '@plateg/rest-api/models';

import { GlobalDI } from '../shares/injector';
export interface Files {
  files: UploadFile[];
  additionalFiles: UploadFile[];
}

export class WorkingAidsController {
  private readonly arbeitshilfenController = GlobalDI.getOrRegister<ArbeitshilfenControllerApi>(
    'arbeitshilfenController',
    () => new ArbeitshilfenControllerApi(new Configuration()),
  );
  private arbeitshilfenList: WorkHelpEntry[] | undefined;

  public getArbeitshilfenListCall(): Observable<WorkHelpEntry[]> {
    let obs: Observable<Array<WorkHelpEntry>>;
    if (!this.arbeitshilfenList) {
      obs = this.arbeitshilfenController.getWorkHelpEntries().pipe(shareReplay(1));
      obs.subscribe((data) => {
        this.arbeitshilfenList = data;
      });
    } else {
      obs = of(this.arbeitshilfenList);
    }
    return obs;
  }

  public searchEntry = (searchString: string, entry: WorkHelpEntry): boolean => {
    const searchValues = searchString.split(' ');
    const lowerTitle = entry.title.toLowerCase();
    const lowerDescription = entry.description.toLowerCase();
    const searchResults = searchValues.map(
      (value) => lowerTitle.includes(value.toLowerCase()) || lowerDescription.includes(value.toLowerCase()),
    );
    return searchResults.every((value) => value);
  };
}
