// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export class FocusController {
  private focusIndex: number | undefined;
  private readonly setIsVisible: React.Dispatch<React.SetStateAction<boolean>>;
  private readonly refs: React.MutableRefObject<HTMLAnchorElement | null>[];
  private readonly nextElementRef: React.MutableRefObject<HTMLAnchorElement | null>;

  public constructor(
    setIsVisible: React.Dispatch<React.SetStateAction<boolean>>,
    refs: React.MutableRefObject<HTMLAnchorElement | null>[],
    nextElementRef: React.MutableRefObject<HTMLAnchorElement | null>,
  ) {
    this.setIsVisible = setIsVisible;
    this.refs = refs;
    this.nextElementRef = nextElementRef;
  }

  public setFocus(event: React.KeyboardEvent): void {
    const n = this.refs.length;
    switch (event.key) {
      case 'Tab':
      case 'Escape':
        this.setIsVisible(false);
        this.nextElementRef?.current?.focus();
        break;
      case 'ArrowUp':
        if (this.focusIndex !== undefined) {
          // More complex formula necessary, because to make mod behave like real mod for negative numbers
          //and not just the simple remainder so that -1 gets translated to 4
          this.focusIndex = (((this.focusIndex - 1) % n) + n) % n;
        } else {
          this.focusIndex = n - 1;
        }
        this.refs[this.focusIndex]?.current?.focus();
        event.preventDefault();
        break;
      case 'ArrowDown':
        if (this.focusIndex !== undefined) {
          this.focusIndex = (this.focusIndex + 1) % n;
        } else {
          this.focusIndex = 0;
        }
        this.refs[this.focusIndex]?.current?.focus();
        event.preventDefault();
        break;
    }
  }
}
