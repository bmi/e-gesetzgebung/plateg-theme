// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import { FreigabeType, RolleGlobalType, RolleLokalType } from '@plateg/rest-api';
import { resolveFreigabeRole, resolveFreigabeRoleString } from './translatationHelpers';

describe('TEST resolveFreigabeRole', () => {
  // Simulates function i18n.t.
  function lookup_f(k: string) {
    switch (k) {
      case 'theme.freigabe.type.ADMINISTRATION':
        return 'EXISTING VALUE 1: Administration';
      case 'theme.freigabe.type.FEDERFUEHRER':
        return 'EXISTING VALUE 1: Federfuehrer';
      case 'theme.freigabe.type.LESERECHTE':
        return 'EXISTING VALUE 1: Leserechte';
      default:
        return k;
    }
  }

  it('Existing FreigabeType must be found.', () => {
    //
    const value = resolveFreigabeRole(lookup_f, FreigabeType.Leserechte);
    expect(value).to.eql('EXISTING VALUE 1: Leserechte');
  });

  it('Existing RolleGlobalType must be found.', () => {
    //
    const value = resolveFreigabeRole(lookup_f, RolleGlobalType.Administration);
    expect(value).to.eql('EXISTING VALUE 1: Administration');
  });

  it('Existing RolleLokalType must be found.', () => {
    //
    const value = resolveFreigabeRole(lookup_f, RolleLokalType.Federfuehrer);
    expect(value).to.eql('EXISTING VALUE 1: Federfuehrer');
  });

  it('Non-existing key must default to input, and not to e.g. theme.freigabe.type.NON_EXISTING_KEY.', () => {
    //
    const value = resolveFreigabeRole(lookup_f, 'NON_EXISTING_KEY' as RolleLokalType);
    expect(value).to.eql('NON_EXISTING_KEY');
  });
});

describe('TEST resolveFreigabeRoleString', () => {
  // Simulates function i18n.t.
  function lookup_f(k: string) {
    return k === 'theme.freigabe.type.EXISTING_KEY' ? 'EXISTING VALUE' : k;
  }

  it('Existing key must be found.', () => {
    //
    const value = resolveFreigabeRoleString(lookup_f, 'EXISTING_KEY');
    expect(value).to.eql('EXISTING VALUE');
  });

  it('Non-existing key must default to input, and not to e.g. theme.freigabe.type.NON_EXISTING_KEY.', () => {
    //
    const value = resolveFreigabeRoleString(lookup_f, 'NON_EXISTING_KEY');
    expect(value).to.eql('NON_EXISTING_KEY');
  });
});
