// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';

import { BerechtigungAnBenutzerDTO, RolleLokalType } from '@plateg/rest-api';

import { Filters } from './filters';

describe('Test for Filters.numberFormat', function () {
  it('Keine Änderung', () => {
    expect(Filters.numberFormat('11')).equal('11');
  });

  it('Ein 1000er Punkt', () => {
    expect(Filters.numberFormat('1111')).equal('1,111');
  });

  it('Zwei 1000er Punkt', () => {
    expect(Filters.numberFormat('1111111')).equal('1,111,111');
  });

  it('Drei 1000er Punkt', () => {
    expect(Filters.numberFormat('1111111111')).equal('1,111,111,111');
  });

  it('Zwei 1000er Punkt mit Komma Zahl / abrunden', () => {
    expect(Filters.numberFormat('1111111.11')).equal('1,111,111.1');
  });

  it('Zwei 1000er Punkt mit Komma Zahl / aufrunden', () => {
    expect(Filters.numberFormat('1111111.15')).equal('1,111,111.2');
  });

  it('date', () => {
    expect(Filters.date(new Date('2021-04-17T13:53:24.831493'))).equal('17.04.2021');
    expect(Filters.date(new Date('2021-04-17T11:49:17.830Z'))).equal('17.04.2021');
    expect(Filters.date(undefined)).equal('');
    expect(Filters.date(null)).equal('');
  });

  it('dateTimeFromString', () => {
    expect(Filters.dateTimeFromString('2021-04-27T17:00:00Z')).equal('27.04.2021, 17:00');
    expect(Filters.dateTimeFromString('2021-04-17T13:53:24.831493')).equal('17.04.2021, 13:53');
    expect(Filters.dateTimeFromString('2021-04-17T11:49:17.830Z')).equal('17.04.2021, 11:49');
    expect(Filters.dateTimeFromString(undefined)).equal('');
    expect(Filters.dateTimeFromString(null)).equal('');
  });

  it('time', () => {
    expect(Filters.time(new Date('2021-04-17T13:53:24.831493'))).equal('13:53');
    expect(Filters.time(new Date('2021-04-17T11:49:17.830Z'))).equal('11:49');
    expect(Filters.time(undefined)).equal('');
    expect(Filters.time(null)).equal('');
  });

  it('dateFromString', () => {
    expect(Filters.dateFromString('2021-04-27T17:00:00Z')).equal('27.04.2021');
    expect(Filters.dateFromString('2021-04-17T13:53:24.831493')).equal('17.04.2021');
    expect(Filters.dateFromString('2021-04-17T11:49:17.830Z')).equal('17.04.2021');
    expect(Filters.dateFromString(undefined)).equal('');
    expect(Filters.dateFromString(null)).equal('');
  });

  it('timeFromString', () => {
    expect(Filters.timeFromString('2021-04-27T17:00:00Z')).equal('17:00');
    expect(Filters.timeFromString('2021-04-17T13:53:24.831493')).equal('13:53');
    expect(Filters.timeFromString('2021-04-17T11:49:17.830Z')).equal('11:49');
    expect(Filters.timeFromString(undefined)).equal('');
    expect(Filters.timeFromString(null)).equal('');
  });

  it('shortIsoDateFromString', () => {
    expect(Filters.shortIsoDateFromString('2021-04-27T17:00:00Z')).equal('2021-04-27');
    expect(Filters.shortIsoDateFromString('2021-04-16T13:53:24.831493')).equal('2021-04-16');
    expect(Filters.shortIsoDateFromString('2029-05-13T11:49:17.830Z')).equal('2029-05-13');
    expect(Filters.shortIsoDateFromString(undefined)).equal('');
    expect(Filters.shortIsoDateFromString(null)).equal('');
  });
});

describe('Test for Filters.currency', () => {
  it('simple round value', () => {
    expect(Filters.currency(11)).equal('11,00');
  });

  it('simple fractional', () => {
    expect(Filters.currency(0.11)).equal('0,11');
  });

  it('value + simple fractional', () => {
    expect(Filters.currency(11.11)).equal('11,11');
  });

  it('fractional, three digits after comma -> round up', () => {
    expect(Filters.currency(0.119)).equal('0,12');
  });

  it('fractional, four digits after comma -> round up', () => {
    expect(Filters.currency(0.1158)).equal('0,12');
  });

  it('fractional, three digits after comma -> round off', () => {
    expect(Filters.currency(0.112)).equal('0,11');
  });

  it('fractional, four digits after comma -> round off', () => {
    expect(Filters.currency(0.1143)).equal('0,11');
  });
});

describe('Test for Filters.date', () => {
  it('date only', () => {
    expect(Filters.date(new Date('2021-05-31'))).equal('31.05.2021');
  });
  it('date + time', () => {
    expect(Filters.date(new Date('2021-05-31 07:17:02'))).equal('31.05.2021');
  });
});

describe('Test for Filters.time', () => {
  it('date + time (hours + minutes)', () => {
    expect(Filters.time(new Date('2021-06-30 07:17'))).equal('07:17');
  });

  it('date + time (hours + minutes + seconds)', () => {
    expect(Filters.time(new Date('2021-06-30 07:17:17'))).equal('07:17');
  });

  it('date + time (hours + minutes + seconds + miliseconds)', () => {
    expect(Filters.time(new Date('2021-06-30 07:17:17:17'))).equal('07:17');
  });
});

describe('Test for Filters.dateFromString', () => {
  it('date only', () => {
    expect(Filters.dateFromString('2021-05-31')).equal('31.05.2021');
  });
  it('date + time', () => {
    expect(Filters.dateFromString('2021-05-31 07:17:02')).equal('31.05.2021');
  });

  it('no input', () => {
    expect(Filters.dateFromString()).equal('');
  });
});

describe('Test for Filters.dateTimeFromString', () => {
  it('date + time (hours + minutes)', () => {
    expect(Filters.dateTimeFromString('2021-06-30 07:17')).equal('30.06.2021, 07:17');
  });

  it('date + time (hours + minutes + seconds)', () => {
    expect(Filters.dateTimeFromString('2021-04-30 07:17:17')).equal('30.04.2021, 07:17');
  });

  it('date + time (hours + minutes + seconds + miliseconds)', () => {
    expect(Filters.dateTimeFromString('2021-03-30 07:17:17:17')).equal('30.03.2021, 07:17');
  });

  it('no input', () => {
    expect(Filters.dateTimeFromString()).equal('');
  });
});

describe('Test for Filters.numberParse', function () {
  it('no change', () => {
    expect(Filters.numberParse('11')).equal('11');
  });

  it('one delimeter', () => {
    expect(Filters.numberParse('1,111')).equal('1111');
  });

  it('two delimeters', () => {
    expect(Filters.numberParse('1,111,111')).equal('1111111');
  });

  it('three delimeters', () => {
    expect(Filters.numberParse('1,111,111,111')).equal('1111111111');
  });

  it('two delimeters + franction', () => {
    expect(Filters.numberParse('1,111,111.1')).equal('1111111.1');
  });
});

describe('Test for Filters.percentage', function () {
  it('no franction, 100%', () => {
    expect(Filters.percentage(1)).equal('1,0');
  });

  it('no franction, 300%', () => {
    expect(Filters.percentage(3)).equal('3,0');
  });

  it('one value after delimeter', () => {
    expect(Filters.percentage(0.1)).equal('0,1');
  });

  it('two values after delimeter', () => {
    expect(Filters.percentage(0.1234)).equal('0,123');
  });

  it('three values after delimeter, round off', () => {
    expect(Filters.percentage(0.1234)).equal('0,123');
  });

  it('three values after delimeter, round up', () => {
    expect(Filters.percentage(0.1236)).equal('0,124');
  });
});

describe('Test for Filters.numberToDecimalString', function () {
  it('no decimal separator', () => {
    expect(Filters.numberToDecimalString(1)).equal('1');
    expect(Filters.numberToDecimalString(10)).equal('10');
    expect(Filters.numberToDecimalString(100)).equal('100');
  });
  it('one decimal separator', () => {
    expect(Filters.numberToDecimalString(1000)).equal('1.000');
    expect(Filters.numberToDecimalString(10000)).equal('10.000');
    expect(Filters.numberToDecimalString(100000)).equal('100.000');
  });
});

describe('Test for Filters.sanitizeStringForCSS', () => {
  it('Several words and spaces', () => {
    expect(Filters.sanitizeStringForCSS('Some very long title')).equal('Some-very-long-title');
    expect(Filters.sanitizeStringForCSS('Another, very, long, title')).equal('Another-very-long-title');
    expect(Filters.sanitizeStringForCSS('A  little   shorter    title')).equal('A-little-shorter-title');
  });
  it('German Umlauts and unwanted characters', () => {
    expect(Filters.sanitizeStringForCSS('Über Östereich läßt sich nicht streiten')).equal(
      'Ueber-Oestereich-laesst-sich-nicht-streiten',
    );
    expect(Filters.sanitizeStringForCSS('Viele # böse ? Sonderzeichen & anderes : Gemüse')).equal(
      'Viele-boese-Sonderzeichen-und-anderes-Gemuese',
    );
    expect(Filters.sanitizeStringForCSS('Oder.77.88.Pünktchen')).equal('Oder-77-88-Puenktchen');
  });
});

describe('Test for Filters.prepareBerechtigungenRequestBody', () => {
  it('should identify users to add, update, and remove correctly', () => {
    const usersInState: BerechtigungAnBenutzerDTO[] = [
      { email: 'user1@gmail.com', rolleType: RolleLokalType.Gast },
      { email: 'user2@gmail.com', rolleType: RolleLokalType.Mitarbeiter },
      { email: 'userNotUpdated@gmail.com', rolleType: RolleLokalType.Mitarbeiter },
    ];

    const updatedUsers: BerechtigungAnBenutzerDTO[] = [
      { email: 'user1@gmail.com', rolleType: RolleLokalType.Mitarbeiter },
      { email: 'user3@gmail.com', rolleType: RolleLokalType.Mitarbeiter },
      { email: 'userNotUpdated@gmail.com', rolleType: RolleLokalType.Mitarbeiter },
    ];

    const expectedResult = {
      add: [{ email: 'user3@gmail.com', rolleType: RolleLokalType.Mitarbeiter }],
      modify: [{ email: 'user1@gmail.com', rolleType: RolleLokalType.Mitarbeiter }],
      remove: [{ email: 'user2@gmail.com', rolleType: RolleLokalType.Mitarbeiter }],
    };

    expect(Filters.prepareBerechtigungenRequestBody(updatedUsers, usersInState)).to.eql(expectedResult);
  });

  it('should handle empty initial and updated users', () => {
    const usersInState: BerechtigungAnBenutzerDTO[] = [];
    const updatedUsers: BerechtigungAnBenutzerDTO[] = [];

    const expectedResult = {
      add: [],
      modify: [],
      remove: [],
    };

    expect(Filters.prepareBerechtigungenRequestBody(updatedUsers, usersInState)).to.eql(expectedResult);
  });

  it('should handle only new users', () => {
    const usersInState: BerechtigungAnBenutzerDTO[] = [];
    const updatedUsers: BerechtigungAnBenutzerDTO[] = [
      { email: 'user3@gmail.com', rolleType: RolleLokalType.Mitarbeiter },
    ];

    const expectedResult = {
      add: [{ email: 'user3@gmail.com', rolleType: RolleLokalType.Mitarbeiter }],
      modify: [],
      remove: [],
    };

    expect(Filters.prepareBerechtigungenRequestBody(updatedUsers, usersInState)).to.eql(expectedResult);
  });

  it('should handle only removed users', () => {
    const usersInState: BerechtigungAnBenutzerDTO[] = [
      { email: 'user2@gmail.com', rolleType: RolleLokalType.Mitarbeiter },
    ];
    const updatedUsers: BerechtigungAnBenutzerDTO[] = [];

    const expectedResult = {
      add: [],
      modify: [],
      remove: [{ email: 'user2@gmail.com', rolleType: RolleLokalType.Mitarbeiter }],
    };

    expect(Filters.prepareBerechtigungenRequestBody(updatedUsers, usersInState)).to.eql(expectedResult);
  });

  it('should handle no changes', () => {
    const usersInState: BerechtigungAnBenutzerDTO[] = [
      { email: 'user2@gmail.com', rolleType: RolleLokalType.Mitarbeiter },
    ];
    const updatedUsers: BerechtigungAnBenutzerDTO[] = [
      { email: 'user2@gmail.com', rolleType: RolleLokalType.Mitarbeiter },
    ];

    const expectedResult = {
      add: [],
      modify: [],
      remove: [],
    };

    expect(Filters.prepareBerechtigungenRequestBody(updatedUsers, usersInState)).to.eql(expectedResult);
  });
});
