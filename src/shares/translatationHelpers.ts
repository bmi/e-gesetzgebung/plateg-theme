// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FreigabeType, RolleGlobalType, RolleLokalType } from '@plateg/rest-api';

/**
 * Resolves a role name from database rbac.rollen.bezeichnung
 * into a German label.
 * @param role uppcase underscore `bezeichnung´, e.g. `BR_BUNDESRAT´ to use as default if lookup fails
 * @return resolved label or input 'role', if no translation is found.
 */
export function resolveFreigabeRole(
  t: (key: string) => string,
  role: FreigabeType | RolleGlobalType | RolleLokalType,
): string {
  const lookup_key = `theme.freigabe.type.${role.toString()}`;
  const translation = t(lookup_key);
  // Defaults to role instead of the whole lookup key, when lookup fails.
  return translation === lookup_key ? role.toString() : translation;
}

/**
 * Resolves a role name into a German label.
 * Provides backwards compatibility for untyped calls.
 */
export function resolveFreigabeRoleString(t: (key: string) => string, roleStr: string): string {
  const lookup_key = `theme.freigabe.type.${roleStr}`;
  const translation = t(lookup_key);
  // Defaults to role instead of the whole lookup key, when lookup fails.
  return translation === lookup_key ? roleStr : translation;
}
