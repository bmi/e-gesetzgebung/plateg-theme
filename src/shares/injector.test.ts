// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

// @flow

import { expect } from 'chai';

import { GlobalDI } from './injector';

describe(`Test: Injector-Service (DI)`, () => {
  const containsErrorMsg = 'not registered!';

  describe(`Service registrieren`, () => {
    function test(type: string, service: any) {
      it(`Test-Case register (${type})`, () => {
        expect(() => {
          GlobalDI.register(type, service);
        }).not.throw();
      });

      it(`Test-Case get (${type})`, () => {
        expect(() => {
          GlobalDI.get(type);
        }).not.throw();
      });
    }
    test(`Array`, []);
    test(`Object`, {});
    test(`number`, 0);
    test(`string`, '');
    test(`boolean`, true);
    test(`null`, null);
  });
  describe(`Service fehlerhaft registrieren`, () => {
    function test(type: string, service: any) {
      it(`Test-Case register (${type})`, () => {
        expect(() => {
          GlobalDI.register(type, service);
        }).throw(Error, `The service '${type}' could not be registered!`);
      });

      it(`Test-Case get (${type})`, () => {
        expect(() => {
          GlobalDI.get(type);
        }).throw(containsErrorMsg);
      });
    }
    test(`undefined`, undefined);
  });
});
