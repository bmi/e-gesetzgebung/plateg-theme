// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

class Injector {
  private readonly services: Map<string, Object> = new Map<string, Object>();

  public register<T>(identifier: string, service: T): T {
    if (typeof identifier === 'string' && this.services.has(identifier) === false && service !== undefined) {
      this.services.set(identifier, service);
      return service;
    } else {
      throw new Error(`The service '${identifier}' could not be registered!`);
    }
  }

  public get<T>(identifier: string): T {
    if (typeof identifier === 'string' && this.services.has(identifier) === true) {
      return this.services.get(identifier) as T;
    } else {
      throw new Error(`The service '${identifier}' is not registered!`);
    }
  }

  public getOrRegister<T>(identifier: string, serviceCreator: () => T): T {
    try {
      return this.get(identifier);
    } catch (e) {
      return this.register(identifier, serviceCreator());
    }
  }
}

export const GlobalDI: Injector = new Injector();
