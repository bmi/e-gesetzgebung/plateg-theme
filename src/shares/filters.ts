// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { BerechtigungAnBenutzerDTO, UpdatePermissionsDTO } from '@plateg/rest-api';

// Probably should be linked to the ConfigProvider / Context from react -> so have the localization in one place
const CURRENCY_FORMATTER: Intl.NumberFormat = new Intl.NumberFormat('de-DE', {
  currency: 'EUR',
  maximumFractionDigits: 2,
  minimumFractionDigits: 2,
});

const PERCENTAGE_FORMATTER: Intl.NumberFormat = new Intl.NumberFormat('de-DE', {
  minimumFractionDigits: 1,
});

const DATE_FORMATTER: Intl.DateTimeFormat = new Intl.DateTimeFormat('de-DE', {
  day: '2-digit',
  month: '2-digit',
  year: 'numeric',
});

const TIME_FORMATTER: Intl.DateTimeFormat = new Intl.DateTimeFormat('de-DE', {
  hour: '2-digit',
  minute: '2-digit',
});

const DATE_TIME_FORMATTER: Intl.DateTimeFormat = new Intl.DateTimeFormat('de-DE', {
  day: '2-digit',
  month: '2-digit',
  year: 'numeric',
  hour: '2-digit',
  minute: '2-digit',
});

function createCurrencyFormatter(digits: number): Intl.NumberFormat {
  // en-US da wir aktuell bei Numbern durch einen Bug in der rc-input-number Komponente bei 1,000,000.0 bleiben
  return new Intl.NumberFormat('en-US', {
    maximumFractionDigits: digits,
    minimumFractionDigits: 0,
    useGrouping: true,
  });
}
export class Filters {
  public static currency(value: number): string {
    if (isNaN(value) === false && typeof value === 'number') {
      return CURRENCY_FORMATTER.format(value);
    } else {
      return '';
    }
  }

  public static date(value?: Date | null): string {
    if (value instanceof Date) {
      return DATE_FORMATTER.format(value);
    } else {
      return '';
    }
  }

  public static time(value?: Date | null): string {
    if (value instanceof Date) {
      return TIME_FORMATTER.format(value);
    } else {
      return '';
    }
  }

  public static dateFromString(value?: string | null): string {
    if (value === undefined || value === null) {
      return '';
    }
    return Filters.date(new Date(value));
  }

  public static timeFromString(value?: string | null): string {
    if (value === undefined || value === null) {
      return '';
    }
    return Filters.time(new Date(value));
  }

  public static dateTimeFromString(value?: string | null): string {
    if (value === undefined || value === null) {
      return '';
    }
    return DATE_TIME_FORMATTER.format(new Date(value));
  }

  public static shortIsoDateFromString(value?: string | null): string {
    if (value === undefined || value === null) {
      return '';
    }
    return new Date(value).toISOString().split('T')[0];
  }

  public static numberFormat(value: string, digits = 1): string {
    const numberValue = Number(value);
    if (!isNaN(numberValue) && !value.endsWith('.')) {
      return createCurrencyFormatter(digits).format(numberValue);
    }
    return value;
  }
  public static numberParse(value: string): string {
    return value.replace(/,/g, '');
  }

  public static percentage(value: number): string {
    if (value === undefined || value === null) {
      return '';
    }
    return PERCENTAGE_FORMATTER.format(value);
  }

  public static numberToDecimalString(value: number): string {
    return value.toLocaleString('de-DE');
  }

  public static sanitizeStringForCSS(value: string): string {
    // replacement record for german umlauts and special chars
    const replacementMap: Record<string, string> = {
      '\u00dc': 'Ue', // Ü
      '\u00c4': 'Ae', // Ä
      '\u00d6': 'Oe', // Ö
      '\u00fc': 'ue', // ü
      '\u00e4': 'ae', // ä
      '\u00f6': 'oe', // ö
      '\u00df': 'ss', // ß
      '\u0026': 'und', // &
    };
    // trim input value
    let result = value.trim();
    // replace german umlauts and special chars
    result = result.replace(
      new RegExp('[' + Object.keys(replacementMap).join('|') + ']', 'g'),
      (char) => replacementMap[char],
    );
    // replace all unwanted chars with '-'
    result = result.replace(/[^a-zA-Z\d]+/g, '-');
    // return sanitized string
    return result;
  }

  public static prepareBerechtigungenRequestBody(
    newUsers: BerechtigungAnBenutzerDTO[],
    oldUsers: BerechtigungAnBenutzerDTO[],
  ): UpdatePermissionsDTO {
    const result = {
      add: [] as BerechtigungAnBenutzerDTO[],
      modify: [] as BerechtigungAnBenutzerDTO[],
      remove: [] as BerechtigungAnBenutzerDTO[],
    };

    // Create a map for quick lookup
    const oldUserMap = new Map(oldUsers.map((user) => [user.email, user]));
    const newUserMap = new Map(newUsers.map((user) => [user.email, user]));

    // Find users to add or update
    for (const newUser of newUsers) {
      if (!oldUserMap.has(newUser.email)) {
        result.add.push(newUser);
      } else if (oldUserMap.get(newUser.email)!.rolleType !== newUser.rolleType) {
        result.modify.push(newUser);
      }
    }

    // Find users to remove
    for (const oldUser of oldUsers) {
      if (!newUserMap.has(oldUser.email)) {
        result.remove.push(oldUser);
      }
    }

    return result;
  }
}
